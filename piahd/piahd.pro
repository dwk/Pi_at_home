#-------------------------------------------------
#
# Project created by QtCreator 2016-10-09T14:04:36
#
#-------------------------------------------------

QT       += core gui xml network serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

versionTarget.target = versionTarget
#../piah_common/buildtst.cpp
versionTarget.depends = FORCE
versionTarget.commands = ../piah_common/createtst $$OUT_PWD
versionTarget.commands += touch ../piah_common/buildtst.cpp
QMAKE_EXTRA_TARGETS += versionTarget

PRE_TARGETDEPS += versionTarget
#../piah_common/buildtst.cpp

QMAKE_POST_LINK = ../piah_common/tagbinary $$OUT_PWD $$TARGET

#INCLUDEPATH += /opt/qtrpi/raspbian/sysroot/usr/include
#LIBPATH     += /opt/qtrpi/raspbian/sysroot/usr/lib
INCLUDEPATH += /opt/qtrpi515/sysroot/usr/include
LIBPATH     += /opt/qtrpi515/sysroot/usr/lib
LIBS        += -L/opt/qtrpi515/sysroot/usr/lib/ -lwiringPi
LIBS        += -L/opt/qtrpi515/sysroot/lib/arm-linux-gnueabihf/ -lsystemd

# turn off note: messages of the GCC8.3 (quite annoying, from std::vector implementation mostly)
# see https://unix.stackexchange.com/questions/547567/want-to-turn-off-note-level-messages-in-gcc
QMAKE_CXXFLAGS += -fcompare-debug-second

TARGET = piahd
TEMPLATE = app

target.path = /home/pi/deploy
INSTALLS += target

# MAKE SURE that devices and alg are compiled AFTER device.cpp, otherwise you'll get
# weired SegFault during startup (sequence of static initialization!)
SOURCES += main.cpp dwkapp.cpp \
	datafile.cpp \
	socketserver.cpp \
	../piah_common/nodeaddress.cpp \
	../piah_common/command.cpp \
	../piah_common/netcon.cpp \
	../piah_common/node.cpp \
    ../piah_common/device.cpp \
	../piah_common/algorithm.cpp \
	indicator.cpp \
	datacond.cpp \
	../piah_common/nodebitport.cpp \
    ../piah_common/nodeswitchport.cpp \
    ../piah_common/nodetadlbus.cpp \
    ../piah_common/nodetlv1548.cpp \
    ../piah_common/nodeds1820.cpp \
    ../piah_common/nodealg.cpp \
    ../piah_common/gpiowrapper.cpp \
	../piah_common/algspecialaction.cpp \
    ../piah_common/algsolarprotector.cpp \
    ../piah_common/algbitlogic.cpp \
    ../piah_common/algshutter.cpp \
    ../piah_common/algtimer.cpp \
    ../piah_common/algdummy.cpp \
    ../piah_common/conpool.cpp \
    ../piah_common/nodelcd84x48.cpp \
    ../piah_common/alginfopanel.cpp \
    ../piah_common/nodeardubridge.cpp \
	../piah_common/hostdict.cpp \
    ../piah_common/buildtst.cpp \
	../piah_common/algcompare.cpp \
	../piah_common/algpulse.cpp \
	../piah_common/algbinary.cpp \
	../piah_common/algdespike.cpp \
	../piah_common/algcount.cpp
# \
#	../piah_common/algarithmetic.cpp

HEADERS  += piadh.h piadi.h \
	dwkapp.h \
	datafile.h \
	socketserver.h \
	../piah_common/ex.h \
	../piah_common/nodeaddress.h \
	../piah_common/singleton.h \
	../piah_common/command.h \
    ../piah_common/commandtarget.h \
    ../piah_common/node.h \
    ../piah_common/device.h \
    ../piah_common/nodebitport.h \
    ../piah_common/nodeswitchport.h \
    ../piah_common/nodebitport_fac.h \
    ../piah_common/nodeswitchport_fac.h \
    ../piah_common/netcon.h \
    ../piah_common/nodetadlbus.h \
    ../piah_common/nodetadlbus_fac.h \
    ../piah_common/nodetlv1548_fac.h \
    ../piah_common/nodetlv1548.h \
    ../piah_common/nodeds1820_fac.h \
    ../piah_common/nodeds1820.h \
    indicator.h \
    ../piah_common/algorithm.h \
    ../piah_common/enum_result.h \
    ../piah_common/nodealg.h \
    ../piah_common/gpiowrapper.h \
	../piah_common/algspecialaction.h \
	../piah_common/algspecialaction_fac.h \
    ../piah_common/algsolarprotector.h \
    ../piah_common/algsolarprotector_fac.h \
    ../piah_common/algbitlogic_fac.h \
    ../piah_common/algbitlogic.h \
    ../piah_common/algshutter_fac.h \
    ../piah_common/algshutter.h \
    ../piah_common/algtimer_fac.h \
    ../piah_common/algtimer.h \
    ../piah_common/algdummy.h \
    ../piah_common/algdummy_fac.h \
    ../piah_common/conpool.h \
    datacond.h \
    ../piah_common/nodebitport_nm.h \
    ../piah_common/nodeds1820_nm.h \
    ../piah_common/nodetadlbus_nm.h \
    ../piah_common/nodetlv1548_nm.h \
    ../piah_common/nodeswitchport_nm.h \
    ../piah_common/nodealg_nm.h \
    ../piah_common/nodelcd84x48_fac.h \
    ../piah_common/nodelcd84x48_nm.h \
    ../piah_common/nodelcd84x48.h \
    ../piah_common/alginfopanel_fac.h \
    ../piah_common/alginfopanel.h \
    ../piah_common/nodeardubridge_fac.h \
    ../piah_common/nodeardubridge_nm.h \
    ../piah_common/nodeardubridge.h \
	../piah_common/hostdict.h \
    ../piah_common/buildtst.h \
    ../piah_common/algcompare.h \
	../piah_common/algcompare_fac.h \
	../piah_common/algpulse.h \
	../piah_common/algpulse_fac.h \
	../piah_common/algbinary.h \
	../piah_common/algbinary_fac.h \
	../piah_common/algdespike.h \
	../piah_common/algdespike_fac.h \
	../piah_common/algcount.h \
	../piah_common/algcount_fac.h
# \
#	../piah_common/algarithmetic.h \
#	../piah_common/algarithmetic_fac.h
