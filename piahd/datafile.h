#ifndef DATAFILE_H
#define DATAFILE_H

#include "piadh.h"
#include <QObject>
#include <QFile>
#include <QMutex>
#include <QDateTime>
#include <time.h>

namespace pi_at_home
{
/*
 * version piahd1.1
 * start="2019-04-20T00:00:04" is the base for tst offsets
 * tst="3" offset in seconds, always integer
 *
 * version piahd1.2
 * start="2019-04-20"
 * Reference for tst offset is always 0:00 of the day given as start in yyyy-mm-dd format.
 * The file is usually created a few seconds after 0:00, so tst mey get slightly over 86400.
 * t="3000"
 * milliseconds after 0:00 of start date, always integer
 *
 * example
<sessions>
<session start="2019-04-20" version="piahd1.2">
<nodes>
  <node name="LEDg" log="-1" unit="BIT" adr=":Pi3SolarFull:10:0"/>
  <node name="T_freshw_cold" log="1000" unit="DEGREE_CELSIUS" adr=":Pi3SolarFull:50:0"/>
  <node name="T_earthc_cold" log="60000" unit="DEGREE_CELSIUS" adr=":Pi3SolarFull:50:1"/>
</nodes>
<d t="3000" l="T_freshw_cold" v="22.65"/>
<d t="3000" l="T_earthc_cold" v="17.8"/>
<d t="86398000" l="T_earthc_cold" v="16.67"/>
</session>
</sessions>
*/
class DataFile : public QObject
{
	Q_OBJECT
public:
	explicit DataFile(QObject *parent = nullptr);
	~DataFile();
	bool setup(const QString dataPath);
	//bool write(const QString label, const QString data);
	bool write(const QString label, const QVariant data, qint64 timestamp_us);
protected:
	void timerEvent(QTimerEvent *event);
	bool createFile();
private:
	QFile f;
	QMutex mtx;
	QString dp; //, nowTst;
	bool doLog=false, canLog=false;
	QDate today;
	qint64 baseMs=0;
	//time_t timeOffset=0, sessionStart=0;
};
}
#endif // DATAFILE_H
