/********************************************************************************
** This is a dummy file so that piahd can be built without access to the
** build directory of the GUI. The asumption is that the GUI classes will never
** be used on the server and the actual content doesn't matter - as long as it
** compiles. If necessary replace this file with an up-to-date copy from the
** GUI build dir.
********************************************************************************/

#ifndef UI_WIDNODE_H
#define UI_WIDNODE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WidNode
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *toolOnline;
    QToolButton *toolUpdate;
    QToolButton *toolSim;
    QLabel *labelAlias;
    QLabel *labelAddress;
    QLabel *valueDummy;
    QComboBox *comboState;
    QLabel *labelType;

    void setupUi(QWidget *WidNode)
    {
        if (WidNode->objectName().isEmpty())
            WidNode->setObjectName(QStringLiteral("WidNode"));
        WidNode->resize(249, 63);
        WidNode->setAutoFillBackground(true);
        gridLayout = new QGridLayout(WidNode);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        toolOnline = new QToolButton(WidNode);
        toolOnline->setObjectName(QStringLiteral("toolOnline"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/res/heartsleep.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon.addFile(QStringLiteral(":/res/heart.png"), QSize(), QIcon::Normal, QIcon::On);
        toolOnline->setIcon(icon);
        toolOnline->setCheckable(true);

        horizontalLayout->addWidget(toolOnline);

        toolUpdate = new QToolButton(WidNode);
        toolUpdate->setObjectName(QStringLiteral("toolUpdate"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/res/arrow_rotate_clockwise.png"), QSize(), QIcon::Normal, QIcon::On);
        toolUpdate->setIcon(icon1);

        horizontalLayout->addWidget(toolUpdate);

        toolSim = new QToolButton(WidNode);
        toolSim->setObjectName(QStringLiteral("toolSim"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/res/sim_off.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon2.addFile(QStringLiteral(":/res/sim.png"), QSize(), QIcon::Normal, QIcon::On);
        toolSim->setIcon(icon2);
        toolSim->setCheckable(true);

        horizontalLayout->addWidget(toolSim);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        labelAlias = new QLabel(WidNode);
        labelAlias->setObjectName(QStringLiteral("labelAlias"));

        gridLayout->addWidget(labelAlias, 1, 0, 1, 1);

        labelAddress = new QLabel(WidNode);
        labelAddress->setObjectName(QStringLiteral("labelAddress"));
        QFont font;
        font.setPointSize(7);
        labelAddress->setFont(font);

        gridLayout->addWidget(labelAddress, 1, 1, 1, 1);

        valueDummy = new QLabel(WidNode);
        valueDummy->setObjectName(QStringLiteral("valueDummy"));

        gridLayout->addWidget(valueDummy, 0, 1, 1, 1);

        comboState = new QComboBox(WidNode);
        comboState->setObjectName(QStringLiteral("comboState"));
        comboState->setEnabled(false);

        gridLayout->addWidget(comboState, 0, 2, 1, 1);

        labelType = new QLabel(WidNode);
        labelType->setObjectName(QStringLiteral("labelType"));
        labelType->setFont(font);
        labelType->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelType, 1, 2, 1, 1);


        retranslateUi(WidNode);

        QMetaObject::connectSlotsByName(WidNode);
    } // setupUi

    void retranslateUi(QWidget *WidNode)
    {
        WidNode->setWindowTitle(QApplication::translate("WidNode", "Node", 0));
        labelAlias->setText(QApplication::translate("WidNode", "Alias", 0));
        labelAddress->setText(QApplication::translate("WidNode", "Address", 0));
        valueDummy->setText(QApplication::translate("WidNode", "value", 0));
        comboState->clear();
        comboState->insertItems(0, QStringList()
         << QApplication::translate("WidNode", "state", 0)
        );
        labelType->setText(QApplication::translate("WidNode", "type", 0));
    } // retranslateUi

};

namespace Ui {
    class WidNode: public Ui_WidNode {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDNODE_H
