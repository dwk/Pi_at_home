#ifndef DATACOND_H
#define DATACOND_H

#include "piadh.h"
#include <QObject>
#include <QMutex>
#include "../piah_common/command.h"
#include "../piah_common/conpool.h"
#include "../piah_common/device.h"
#include "../piah_common/algorithm.h"
#include "../piah_common/nodeaddress.h"
#include "../piah_common/netcon.h"
#include "../piah_common/node.h"
#include "datafile.h"
#include "indicator.h"

// DOC MOVED TO ARCHITECTURE.ODT
namespace pi_at_home
{
struct NodeCon
{
	NodeCon(const QString & src, const QString & dest, bool ignoreFail) :
		src_(src), dest_(dest), ignoreFail_(ignoreFail) {}
	QString src_, dest_;
	bool ignoreFail_;
};
typedef std::vector< NodeCon > NodeConVV;

class DataConD : public QObject
{
	Q_OBJECT
public:
	enum class Platform {UNDEF=0, PI2B, PI3B, PI3BPLUS};
	enum class ConfigFlavor {UNDEF=0, RAW, NORMAL};
	explicit DataConD(QObject *parent = nullptr);
	~DataConD();
	DataConD *self() {return this;}
	void setup(const QString configFile, const QString dataPath);
	void shutdown();
	DataConD::ConfigFlavor getConfigFlavor() const {return configFlavor;}
	QString getConfigFlavorStr() const {switch(configFlavor) {case ConfigFlavor::RAW: return QStringLiteral("raw"); case ConfigFlavor::NORMAL: return QStringLiteral("normal");default:return QStringLiteral("undefined");}}
	const QDateTime &getConfigTst() const {return configTst;}
	DataConD::Platform getPlatform() const {return platform;}
	QString buildTimestamp(){return buildTimestamp_;}
	int getBroadcastDelayMs(){return delayBroadcast_;}
	void triggerWatchdog();
	void triggerAlgExec(int heartbeat);
	void triggerAlgShutdown();
	bool algSafePending();
	int delayHeartbeat() {return delayHeartbeat_;}
	const QString & uid() const {return uid_;}
	unsigned long long int nextId() {return ++luid_;}
	void setUid(const QString uid) {uid_=uid; NodeAddress::setDefaultUid(uid);}
	qint64 getUsecTst() const;
	//qint64 getMsecTst() const;
	void registerCmdTarget(const QString &cmd, CommandTarget * target);
	void registerCmdTarget(const CommandFilter & filter, CommandTarget * target);
	void registerNode(Node * node);
	Node * findNode(const QString alias, bool throwIfNotFound=true);
	Node * findNode(const NodeAddress & adr, bool throwIfNotFound=true);
	const NAdrNodesMP & allNodes() const {return nodeAdrMP;}
	void addNodeFragments(Command & cmd) const;
	ConPool * getConPool() {return conPool;}
	void dispatchCommand(Command & cmd);
	void add2Broadcast(Fragment * frag);
	void sendBroadcast();
	GpioWrapper *io() {return gpio;} // shall we throw on nullptr?
public slots:
	void guiConnectedSl(bool state);
	void newValSl(const NodeValue & newValue, qint64 timestamp);
protected:
	virtual void timerEvent(QTimerEvent *event);
private:
	typedef std::map< CommandTarget* , CommandFilter > Dispatch;
	void process(CommandTarget * target, Command & cmd, Fragment &relevant, bool allowReply);
	QString uid_, buildTimestamp_;
	ConfigFlavor configFlavor=ConfigFlavor::UNDEF;
	QDateTime configTst;
	unsigned long long int luid_ = 0;
	DeviceMP deviceMP;
	Dispatch dispatch; // used for command routing
	NodesMSP nodesByName_; // key is node alias
	NAdrNodesMP nodeAdrMP;
	AlgorithmMQP algsMQP; // key is id - unique
	AlgorithmMMIQP algsPrioMQP; // key is prio - not unique
	QMutex mtxBroadcast;
	Command * broadcast = nullptr;
	int broadcastTimer=0;
	int delayBroadcast_=500, delayHeartbeat_=1000;
	ConPool * conPool=nullptr;
	Platform platform=Platform::UNDEF;
	QString indAliveNm;
	Indicator *indAlive=nullptr, *indConneted=nullptr;
	GpioWrapper *gpio=nullptr;
	DataFile dataFile;
	NodeValueMV logNvs;

};
}

#endif // DATACOND_H
