#include "datacond.h"
#include <QFile>
#include <QTimerEvent>
#include <QProcess>
#include <QCoreApplication>
#include "../piah_common/gpiowrapper.h"
#include "../piah_common/commandtarget.h"
#include "../piah_common/command.h"
#include "piadi.h"

DataConD::DataConD(QObject *parent) : QObject(parent)
{
    //qDebug()<<" ";
	QProcess testerP;
	testerP.start("objdump", QStringList()<<"-swj"<<".buildinfo"<<QCoreApplication::applicationFilePath());
	if(!testerP.waitForFinished(2000))
	{
		qCritical()<<"Command objdump timed out - build timestamp unknown";
	}
	else
	{
		int err=testerP.exitCode();
		if(err)
		{
			qCritical()<<"cannot objdump"<<QCoreApplication::applicationFilePath()<<testerP.errorString();
			qCritical()<<"objdump returned:"<<testerP.readAll();
		}
		else
		{
			for(int i=0; i<4; ++i) // skip  lines
				testerP.readLine();
			QByteArray line=testerP.readLine();
			QString buildtst(line.mid(43, 16));
			line=testerP.readLine();
			buildtst+=line.mid(43, 12);
			if(buildtst.size()<28)
			{
				qCritical()<<"invalid .buildinfo"<<buildtst;
			}
			else
				buildTimestamp_=buildtst;
		}
	}
}

DataConD::~DataConD()
{
}

void DataConD::setup(const QString configFile, const QString dataPath)
{
	luid_=static_cast<unsigned long long>(time(nullptr))*1000000000ULL;
	// platform dependent stuff
	QFile filePI("/proc/cpuinfo");
	if(!filePI.open(QIODevice::ReadOnly))
	{
		qCritical()<<" cannot open /proc/cpuinfo";
		throw Ex("no cpuinfo");
	}
	for(int i=0; i<50; ++i) // atEnd() seems not to work on /proc files
	{
		QByteArray line=filePI.readLine();
		if(line.startsWith("Revision"))
		{ // https://elinux.org/RPi_HardwareHistory
			QString revs=QString::fromUtf8(line.mid(line.indexOf(':')+1));
			if(revs.contains("a01041") || revs.contains("a21041"))
			{
				platform=Platform::PI2B; // Raspberry Pi 2 Model B
			}
			else if(revs.contains("a02082") || revs.contains("a22082"))
			{
				platform=Platform::PI3B; // Raspberry Pi 3 Model B
			}
			else if(revs.contains("a020d3") || revs.contains("a220d3"))
			{
				platform=Platform::PI3BPLUS; // Raspberry Pi 3 Model B plus
			}
			else
			{
				qCritical()<<"cannot determine platform type, Revision says"<<revs;
				throw Ex("no platform");
			}
			break;
		}
	}
	if(platform==Platform::UNDEF)
	{
		qCritical()<<"cannot determine platform type, no Revision found in /proc/cpuinfo";
		throw Ex("no platform");
	}
	qInfo()<<"platform type"<<(platform==Platform::PI2B?"Pi2B":(platform==Platform::PI3B?"Pi3B":"PI3B+"));
	// read config file
	QFile file(configFile);
	if(!file.open(QIODevice::ReadOnly))
	{
		qCritical()<<" cannot open config file"<<configFile;
		throw Ex("XML: no file");
	}
	QDomDocument doc;
	QString errStr;
	int errLine=0;
	if (!doc.setContent(&file, &errStr, &errLine))
	{
		qCritical()<<"config file invalid in line"<<errLine<<errStr;
		throw Ex("XML: invlaid syntax");
	}
	QString configFalvor=doc.documentElement().attribute(QStringLiteral("flavor"));
	if(configFalvor==QStringLiteral("raw"))
		configFlavor=ConfigFlavor::RAW;
	else if(configFalvor==QStringLiteral("normal"))
		configFlavor=ConfigFlavor::NORMAL;
	//else configFlavor=ConfigFlavor::UNDEF is default
	configTst=QDateTime::fromString(doc.documentElement().attribute(QStringLiteral("savetst")), Qt::ISODate);
	{
		QDomNodeList statHosts=doc.elementsByTagName("static_hosts");
		if(statHosts.size())
		{
			QDomElement hsts=statHosts.at(0).toElement();
			conPool=new ConPool(true, &hsts, this);
		}
		else
			conPool=new ConPool(true, nullptr, this);
		connect(conPool, SIGNAL(guiConnectedSig(bool)), this, SLOT(guiConnectedSl(bool)));
	}
	QDomNodeList timing=doc.elementsByTagName("timing");
	if(timing.size()==1)
	{
		QDomElement el=timing.at(0).toElement();
		int i=0;
		bool ok=false;
		if(el.hasAttribute("broadcast_ms"))
		{
			i=el.attribute("broadcast_ms").toInt(&ok);
			if(ok)
				delayBroadcast_=i;
			else
				qWarning()<<"broadcast_ms has non-integer value"<<el.attribute("broadcast_ms")<<"using"<<delayBroadcast_;
		}
		if(el.hasAttribute("heartbeat_ms"))
		{
			i=el.attribute("heartbeat_ms").toInt(&ok);
			if(ok)
				delayHeartbeat_=i;
			else
				qWarning()<<"fast_ms has non-integer value"<<el.attribute("heartbeat_ms")<<"using"<<delayHeartbeat_;
		}
		if(el.hasAttribute("alive_ind"))
			indAliveNm=el.attribute("alive_ind");
	}
	else if(timing.size()>1)
	{
		qCritical()<<"config file has more than one timing tags";
		throw Ex("XML: multiple timing");
	}
	//else use defaults from .h
	qInfo()<<"parsing hardware";
	QDomNodeList hw=doc.elementsByTagName("hardware");
	if(hw.size()==1)
	{
		QDomElement el=hw.item(0).toElement();
		setUid(el.attribute("uid"));
		if(uid_.isEmpty())
		{
			qCritical()<<"no uid defined in hardware tag";
			throw Ex("XML: no uid");
		}
		QDomNodeList devs=el.elementsByTagName("device");
		for(int i=0; i<devs.size(); ++i)
		{
			Device * d=DeviceFactory::produce(devs.item(i).toElement());
			if(d)
			{
				d->setParent(this);
				deviceMP.insert(DeviceMP::value_type(d->id(), d));
			}
		}
	}
	else
	{
		qCritical()<<"config file has none or more than one hardware tag(s)";
		return;
	}
	qInfo()<<"parsing algorithms";
	QDomNodeList alggrp=doc.elementsByTagName("algorithms");
	if(alggrp.size()==1)
	{
		QDomElement el=alggrp.at(0).toElement();
		QDomNodeList algs=el.elementsByTagName("algorithm");
		for(int i=0; i<algs.size(); ++i)
		{
			QDomElement e=algs.item(i).toElement();
			Algorithm * a=qobject_cast<Algorithm *>(DeviceFactory::produce(e));
			if(a)
			{
				a->setParent(this);
				algsMQP.insert(AlgorithmMQP::value_type(a->id(), QPointer<Algorithm>(a)));
				algsPrioMQP.insert(AlgorithmMMIQP::value_type(a->prio(), QPointer<Algorithm>(a)));
			}
			else
			{
				bool musthave=(e.attribute("required")=="true");
				if(musthave)
					throw Ex(QString("required algorithm (%1, id %2) failed to construct").arg(e.attribute("type")).arg(e.attribute("id")));
			}
		}
	}
	else if(alggrp.size()>1)
	{
		qCritical()<<"config file has more than one algorithms tags";
		throw Ex("XML: multiple algorithms-sections");
	}
	qInfo()<<"parsing connections";
	NodeConVV nodeCons;
	QDomNodeList congrp=doc.elementsByTagName("connections");
	if(congrp.size()==1)
	{
		QDomElement el=congrp.at(0).toElement();
		QDomNodeList cons=el.elementsByTagName("con");
		for(int i=0; i<cons.size(); ++i)
		{
			QDomElement e=cons.item(i).toElement();
			// <con src="opto5VGND2" dest="LEDr" ingnore_fail="false" />
			nodeCons.push_back(NodeCon(e.attribute("src"), e.attribute("dest"),
									  e.attribute("ingnore_fail")=="true"));
		}
	}
	else
	{
		if(congrp.size()>1)
		{
			qCritical()<<"config file has more than one connections tags";
			throw Ex("XML: multiple connections");
		}
		else
			qWarning()<<"config file does not specifiy any inter-node connections";
	}
	qInfo()<<"parsing config file completed";
	qInfo()<<"command id base "<<uid_<<" "<<luid_;
	//
	gpio=new GpioWrapper(this);
	qInfo()<<"IO initialized";
	for(DeviceMP::iterator it=deviceMP.begin(); it!=deviceMP.end(); ++it)
	{
		qDebug()<<"initialize"<<it->second->objectName();
		it->second->init();
	}
	// now the hardware nodes shall be regsitered
	qInfo()<<"Devices initialized";
	{
		for(AlgorithmMQP::iterator it=algsMQP.begin(); it!=algsMQP.end(); ++it)
		{
			if(!it->second)
				continue;
			qInfo()<<"initializing"<<it->second->objectName();
			it->second->init();
		}
	}
	qInfo()<<"Algorithms initialized";
	// now _all_ nodes should be registered, including those created by algs
	// setting connections
	NAdrNodesMP allSenders;
	for(auto conIt=nodeCons.begin(); conIt!=nodeCons.end(); ++conIt)
	{
		Node * ns=findNode(conIt->src_, false);
		Node * nd=findNode(conIt->dest_, false);
		int fail=0;
		if(ns && nd)
		{
			allSenders.insert(NAdrNodesMP::value_type(ns->address(), ns));
			/*if(conIt->exclusive_)
			{
				if(nd->getLocker())
					fail=2;
				else
					nd->lock(ns->address().uid(), ns->address().device(), ns->address().alias()); // no need to remember the token - will use slot
			} */
			if(!fail)
			{
				if(!connect(ns, &Node::newValSig, nd, &Node::newValSl, Qt::QueuedConnection))
					fail=3;
			}
		}
		else
			fail=1;
		if(fail)
		{
			//qDebug()<<"connecting"<<(ns?ns->objectName():"nullptr")<<(nd?nd->objectName():"nullptr")<<conIt->ignoreFail_;
			if(conIt->ignoreFail_)
			{
				qWarning()<<"DCON connection failed"<<conIt->src_<<conIt->dest_<<fail<<(ns?ns->objectName():"nullptr")<<(nd?nd->objectName():"nullptr");
				if(nd && nd->getLocker())
					qWarning()<<"DCON connection target locked"<<nd->getLocker()->toStringVerbose();
			}
			else
			{
				qCritical()<<"DCON connection failed"<<conIt->src_<<conIt->dest_<<fail<<(ns?ns->objectName():"nullptr")<<(nd?nd->objectName():"nullptr");
				if(nd && nd->getLocker())
					qWarning()<<"DCON connection target locked"<<nd->getLocker()->toStringVerbose();
				throw Ex("DCON connection failed");
			}
		}
		//else
		//	qDebug()<<"con"<<conIt->src_<<conIt->dest_;
	}
	qInfo()<<"connections set";
	// crank up the algs
	for(AlgorithmMQP::iterator it=algsMQP.begin(); it!=algsMQP.end(); ++it)
	{
		if(!it->second)
			continue;
		qInfo()<<"pre-exec"<<it->second->objectName();
		it->second->preExec();
	}
	qInfo()<<"Algorithms ready for execution";
	// trigger initial updates
	triggerAlgExec(0); // trigger poll_mult=-1 updates
	for(auto it=allSenders.begin(); it!=allSenders.end(); ++it)
	{
		qDebug()<<"trigger initial update for"<<it->second->address().alias();
		it->second->ping();
	}
	// start data-file logging
	if(!dataPath.isEmpty())
	{
		dataFile.setup(dataPath);
		// write initial values
		qint64 ctst=getUsecTst();
		for(NAdrNodesMP::const_iterator it=nodeAdrMP.begin(); it!=nodeAdrMP.end(); ++it)
		{
			if(it->second->doLog(ctst))
			{
				dataFile.write(it->second->address().alias(), it->second->value(), ctst);
			}
		}
	}
	switch(platform)
	{
		case Platform::PI2B:
		case Platform::PI3BPLUS:
			if(indAliveNm.isEmpty())
				indAlive=new Indicator(Indicator::Type::WATCHDOG, Indicator::Target::SYSTEM_LED, "/sys/class/leds/led0", this);
			else
				indAlive=new Indicator(Indicator::Type::WATCHDOG, Indicator::Target::BITPORT, indAliveNm, this);
			indConneted=new Indicator(Indicator::Type::SIMPLE, Indicator::Target::SYSTEM_LED, "/sys/class/leds/led1", this);
			break;
		case Platform::PI3B:
			if(indAliveNm.isEmpty())
				indAlive=nullptr;
			else
				indAlive=new Indicator(Indicator::Type::WATCHDOG, Indicator::Target::BITPORT, indAliveNm, this);
			indConneted=new Indicator(Indicator::Type::SIMPLE, Indicator::Target::SYSTEM_LED, "/sys/class/leds/led0", this);
			break;
		case Platform::UNDEF:
			break;
	}
	qInfo()<<"platform initialized";
}

void DataConD::shutdown()
{
	qInfo()<<"shutting down devices";
	for(DeviceMP::iterator it=deviceMP.begin(); it!=deviceMP.end(); ++it)
	{
		it->second->shutdown();
	}
	qInfo()<<"shutting down networking";
	delete conPool;
	conPool=nullptr;
	qInfo()<<"shutting down hardware";
	if(indAlive)
	{
		delete indAlive;
		indAlive=nullptr;
	}
	if(indConneted)
	{
		delete indConneted;
		indConneted=nullptr;
	}
	//qDebug()<<"shutdown indicators complete";
	delete gpio;
	gpio=nullptr;
	qInfo()<<"DataConD shutdown complete";
}

void DataConD::triggerWatchdog()
{
	if(indAlive)
		indAlive->set(Indicator::State::ACTIVE);
}

void DataConD::triggerAlgExec(int heartbeat)
{
	for(DeviceMP::iterator it=deviceMP.begin(); it!=deviceMP.end(); ++it)
	{
		int mult=it->second->pollMultiplier();
		//qDebug()<<"pollLoop test"<<it->second->objectName()<<mult<<heartbeat;
		if( (mult>0 && !(heartbeat%mult)) || mult==-1 )
		{
			//qDebug()<<"pollLoop on"<<it->second->objectName();
			it->second->pollLoop();
		}
	}
	for(AlgorithmMMIQP::reverse_iterator it=algsPrioMQP.rbegin(); it!=algsPrioMQP.rend(); ++it)
	{
		if(!it->second)
			continue;
		int mult=it->second->pollMultiplier();
		if( (mult>0 && (!heartbeat%mult)) || mult==-1 )
		{
			it->second->execute();
		}
	}
}

void DataConD::triggerAlgShutdown()
{
	qInfo()<<"shutting down algorithms";
	for(AlgorithmMMIQP::iterator it=algsPrioMQP.begin(); it!=algsPrioMQP.end(); ++it)
	{
		if(!it->second)
			continue;
		it->second->shutdown();
	}
}

bool DataConD::algSafePending()
{
	for(AlgorithmMMIQP::reverse_iterator it=algsPrioMQP.rbegin(); it!=algsPrioMQP.rend(); ++it)
	{
		if(!it->second)
			continue;
		if(!it->second->safeState())
		{ // wait for pending shutdowns!
			qWarning()<<"waiting for safe state of "<<it->second->objectName();
			return true;
		}
	}
	return false;
}

qint64 DataConD::getUsecTst() const
{
	struct timespec tst_;
	if(clock_gettime(CLOCK_MONOTONIC_RAW, &tst_))
	{
		qCritical()<<"DCON: cannot read usec timer"<<strerror(errno);
		throw Ex("DCON: cannot read usec timer");
	}
	return static_cast<qint64>(tst_.tv_sec)*Q_INT64_C(1000000)+static_cast<qint64>(tst_.tv_nsec)/Q_INT64_C(1000);
}

/* qint64 DataConD::getMsecTst() const
{
	struct timespec tst_;
	if(clock_gettime(CLOCK_MONOTONIC_RAW, &tst_))
	{
		qCritical()<<"DCON: cannot read usec timer"<<strerror(errno);
		throw Ex("DCON: cannot read msec timer");
	}
	return static_cast<qint64>(tst_.tv_sec)*Q_INT64_C(1000)+static_cast<qint64>(tst_.tv_nsec)/Q_INT64_C(1000000);
} */

void DataConD::registerCmdTarget(const QString & cmd, CommandTarget *target)
{
	auto it=dispatch.find(target);
	if(it==dispatch.end())
	{
		dispatch.insert(Dispatch::value_type(target, CommandFilter(cmd)));
		//qDebug()<<"register cmd target"<<dynamic_cast<QObject *>(target)->objectName()<<cmd;
	}
	else
	{
		it->second.addCmd(cmd);
		//qDebug()<<"register additional cmd target"<<dynamic_cast<QObject *>(target)->objectName()<<it->second.toString();
	}
}

void DataConD::registerCmdTarget(const CommandFilter &filter, CommandTarget *target)
{
	auto it=dispatch.find(target);
	if(it==dispatch.end())
	{
		dispatch.insert(Dispatch::value_type(target, filter));
		qDebug()<<"register target"<<dynamic_cast<QObject *>(target)->objectName()<<filter.toString();
	}
	else
	{
		it->second.merge(filter);
		qDebug()<<"register target (merge)"<<dynamic_cast<QObject *>(target)->objectName()<<filter.toString();
	}
}

void DataConD::registerNode(Node *node)
{
	if(!nodeAdrMP.insert(NAdrNodesMP::value_type(node->address(), node)).second)
		throw Ex(QString("node registration failed, probaly duplicate address %1").arg(node->address().toStringVerbose()));
	QString al=node->address().alias();
	//qDebug()<<"DataConD::registerNode"<<al<<node->address().toStringVerbose();
	if(!al.isEmpty())
	{
		if(!nodesByName_.insert(NodesMSP::value_type(al, node)).second)
			qWarning()<<"non-unique node-alias"<<al<<"find by name will find first registered"<<node->address().toStringVerbose();
	}
	// we have to connect because of logging - the slot will suppress broadcasts
	//if(!node->isHidden())
	if(!connect(node, &Node::newValSig, this, &DataConD::newValSl, Qt::QueuedConnection))
		qCritical()<<"newVal-connection failed for"<<node->address().toStringVerbose();
}

Node *DataConD::findNode(const QString alias, bool throwIfNotFound)
{
	auto it=nodesByName_.find(alias);
	if(it==nodesByName_.end())
	{
		NodeAddress na(alias, false);
		if(na.isValid())
		{
			return findNode(na, throwIfNotFound);
		}
		if(throwIfNotFound)
			throw Ex(QString("node <%1> not found").arg(alias));
		else
		{
			qCritical()<<"no node for"<<alias;
			return nullptr;
		}
	}
	Node * res=it->second;
	if(!res)
	{
		if(throwIfNotFound)
			throw Ex(QString("nodesByName_ conatins dead-Node* for %1").arg(alias));
		else
		{
			qCritical()<<"dead node for"<<alias;
			return nullptr;
		}
	}
	return res;
}

Node *DataConD::findNode(const NodeAddress &adr, bool throwIfNotFound)
{
	NAdrNodesMP::iterator it=nodeAdrMP.find(adr);
	if(it==nodeAdrMP.end())
	{
		if(throwIfNotFound)
			throw Ex(QString("node <%1> not found").arg(adr.toStringVerbose()));
		else
			return nullptr;
	}
	return it->second;
}

void DataConD::addNodeFragments(Command &cmd) const
{
	for(NAdrNodesMP::const_iterator it=nodeAdrMP.begin(); it!=nodeAdrMP.end(); ++it)
		cmd.addFragment(it->second->toFragment());
}

void DataConD::dispatchCommand(pi_at_home::Command &cmd)
{
	if(cmd.isBroadcast())
	{
		//qDebug()<<"dispatch broadcast"<<cmd.cmd()<<cmd.source().uid();
		if(cmd.source().uid()==uid_)
		{ // perform broadcast - we are the source
			conPool->doSend(cmd);
		}
		else
		{ // we received a broadcast, dispatch it
			for(auto dit=dispatch.begin(); dit!=dispatch.end(); ++dit)
			{
				Fragment & relevant=cmd;
				if(dit->second.isMatch(cmd, relevant))
					process(dit->first, cmd, relevant, false); // no replies on broadcast
			}
			// no problem if we don't find a handler
		}
		cmd.deleteLater();
		return;
	}
	const NodeAddress & procTarget=cmd.procTarget();
	if(procTarget.uid()==uid_)
	{ // we should process the command
		bool processed=false;
		NAdrNodesMP::const_iterator nit=nodeAdrMP.find(procTarget);
		if(nit!=nodeAdrMP.end())
		{
			process(nit->second, cmd, cmd, true);
			processed=true;
		}
		for(auto dit=dispatch.begin(); dit!=dispatch.end(); ++dit)
		{
			Fragment & relevant=cmd;
			if(dit->second.isMatch(cmd, relevant))
			{
				process(dit->first, cmd, relevant, true); // no replies on broadcast
				processed=true;
			}
		}
		if(!processed)
			qCritical()<<"no handler for command"<<cmd.cmd()<<procTarget.toStringVerbose();
	}
	else
	{ // we should send or relay the command
		conPool->doSend(cmd, procTarget.uid());
	}
	cmd.deleteLater();
}

void DataConD::add2Broadcast(Fragment * frag)
{
	QMutexLocker lk(&mtxBroadcast);
	if(!broadcast)
	{
		broadcast = new Command("pool");
		broadcast->convertToBroadcast();
	}
	if(!broadcastTimer)
		broadcastTimer=startTimer(delayBroadcast_);
	broadcast->addFragment(frag);
}

void DataConD::sendBroadcast()
{
	QMutexLocker lk(&mtxBroadcast);
	if(broadcastTimer)
	{
		killTimer(broadcastTimer);
		broadcastTimer=0;
	}
	if(broadcast)
	{
		dispatchCommand(*broadcast); // deleted by dispatch
		//qDebug()<<"broadcast dispatched";
	}
	broadcast=nullptr;
}

void DataConD::guiConnectedSl(bool state)
{
	if(indConneted)
		indConneted->set(state?(Indicator::State::ACTIVE):(Indicator::State::INACTIVE));
}

void DataConD::newValSl(const pi_at_home::NodeValue &newValue, qint64 timestamp)
{
	Node * src=qobject_cast<Node * >(sender());
	if(!src)
	{
		qCritical()<<"null sender in newValSl";
		return;
	}
	//qDebug()<<"DCON::newValSl from"<<src->address().toStringVerbose();
	if(src->doLog(timestamp))
	{
		dataFile.write(src->address().alias(), newValue, timestamp);
	}
	if(src->isHidden())
		return;
	add2Broadcast(src->toNodechFrag(newValue, timestamp));
}

void DataConD::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==broadcastTimer)
	{
		sendBroadcast();
	}
	QObject::timerEvent(event);
}

void DataConD::process(CommandTarget *target, Command &cmd, Fragment & relevant, bool allowReply)
{
	bool wasReply=cmd.isReply();
	target->processCommand(cmd, relevant);
	if(allowReply && !wasReply && cmd.isReply())
	{ // send the reply
		const QString & replyTargetUid=cmd.procTarget().uid();
		if(replyTargetUid==uid_)
		{ // we should process the reply? come on...
			//if(cmd.attribute("HTTPsrc")=="true")
			//	return; // do not delete the command!
			qCritical()<<"circular reply"<<cmd.cmd()<<cmd.procTarget().toStringVerbose();
		}
		else
		{
			conPool->doSend(cmd, replyTargetUid);
		}
	}
}
