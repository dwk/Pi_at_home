#include "dwkapp.h"
#include <signal.h>
#include <stdio.h>
#include <iostream> // for --version and --help
#include <unistd.h> // usleep
#include <systemd/sd-daemon.h>
#include <QDir>
#include <QProcess>
#include "../piah_common/command.h"
#include "../piah_common/node.h"
#include "../piah_common/nodeswitchport.h"
#include "../piah_common/algsolarprotector.h"
#include "socketserver.h"
#include "piadi.h"


void pi_at_home_sigh_SIGINT(int sig)
{
	if(sig==SIGINT)
	{
		qInfo()<<"SIGINT shutdown";
		static_cast<DwkApp * >(qApp)->executeServerCmd(DwkApp::ServerCmd::END_PROCESS);
	}
}

DwkApp::ServerCmd DwkApp::serverCmdStr2E(const QString &str)
{
	if(str==QStringLiteral("END_PROCESS"))
		return ServerCmd::END_PROCESS;
	else if(str==QStringLiteral("STOP_SERVICE"))
		return ServerCmd::STOP_SERVICE;
	else if(str==QStringLiteral("RESTART_SERVICE"))
		return ServerCmd::RESTART_SERVICE;
	else if(str==QStringLiteral("SHUTDOWN_MACHINE"))
		return ServerCmd::SHUTDOWN_MACHINE;
	else if(str==QStringLiteral("RESTART_MACHINE"))
		return ServerCmd::RESTART_MACHINE;
	return ServerCmd::NOP;
}

DwkApp::DwkApp(int argc, char *argv[]) : QCoreApplication(argc, argv),
	configFile("config.xml"), dataPath("./")
	/*smtp("homerobot@effektlicht.at", "r8+cycEnie", "smtp.world4you.com", 587)*/
{
	setObjectName("AppObject");
	qInfo()<<"START";
	if (signal(SIGINT, &pi_at_home_sigh_SIGINT) == SIG_ERR)
		qCritical()<<"initiaizing sig-handler failed";
	else
		qInfo()<<"initiaizing sig-handler";
	//
	qInfo()<<"checking for multiple instances";
	QProcess proc;
	proc.start("pidof", QStringList()<<"-o"<<"%PPID"<<"piahd");
	proc.waitForFinished(2000);
	QString procIdStr(proc.readAllStandardOutput());
	//qDebug()<<"pidof returned"<<procIdStr;
	int pid=0;
	bool pidOk=false;
	pid=procIdStr.toInt(&pidOk);
	if(pidOk)
	{
		qCritical()<<"piahd ALREAD RUNNING with pid"<<pid;
		::exit(0);
	}
	//
	const QStringList args=arguments();
	for(int i=0; i<args.size(); ++i)
	{
		if(args.at(i)=="--config_file")
		{
			++i;
			if(i<args.size())
			{
				configFile=args.at(i);
				qInfo()<<"config_file"<<configFile;
			}
		}
		else if(args.at(i)=="--data_path")
		{
			++i;
			if(i<args.size())
			{
				dataPath=args.at(i);
				qInfo()<<"data_path"<<dataPath;
			}
		}
		else if(args.at(i)=="--no_data_log")
		{
			dataPath.clear();
			qInfo()<<"no_data_log";
		}
		else if(args.at(i)=="--use_camera")
		{
			useCamera=true;
			qInfo()<<"use_camera";
		}
		else if(args.at(i)=="--systemd")
		{
			systemd=true;
			qInfo()<<"systemd";
		}
		else if(args.at(i)=="--help")
		{
			cout<<"piahd --config_file /tmp/example.xml --data_path /tmp/example/ --no_data_log --use_camera --systemd --syslog --help --version"<<endl;
			::exit(0);
		}
		else if(args.at(i)=="--version")
		{
			cout<<VERSION_STR<<endl;
			::exit(0);
		}
	}
	// DCON
	try
	{
		DCON.setup(configFile, dataPath);
	}
	catch(Ex &ex)
	{
		qCritical()<<"Exception catched during data setup"<<ex.getCode()<<ex.getMessage();
		::exit(1);
	}
	catch(...)
	{
		qCritical()<<"Exception catched during data setup";
		::exit(1);
	}
	DCON.registerCmdTarget("srvcmd", this);
	DCON.registerCmdTarget("datalist", this);
	DCON.registerCmdTarget("datatrans", this);
	// TCP server
	try
	{
		socketServer=new SocketServer(this);
	}
	catch(Ex &ex)
	{
		qCritical()<<"Exception catched during TCP server setup"<<ex.getCode()<<ex.getMessage();
		::exit(1);
	}
	catch(...)
	{
		qCritical()<<"Exception catched during TCP server setup";
		::exit(1);
	}
	//
	hbCnt=0;
	timerId=startTimer(DCON.delayHeartbeat());
	if(systemd)
		sd_notify(0, "READY=1\nSTATUS=Pi at Home up and running\n");
	qInfo()<<"now running";
}

DwkApp::~DwkApp()
{
	if(systemd)
		sd_notify(0, "STATUS=Pi at Home gracefully stoped\n");
}

bool DwkApp::notify ( QObject * receiver, QEvent * event )
{
	bool res=true;
	try
	{
		res=QCoreApplication::notify(receiver, event);
	}
	catch(Ex & ex)
	{
		qCritical()<<"Exception catched in sync slot"<<ex.getCode()<<ex.getMessage();
	}
	catch(...)
	{
		qCritical()<<"Exception catched in sync slot";
	}
	return res;
}

void DwkApp::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(shutdownPending)
		return;
	if(relevant.cmd()=="srvcmd")
	{
		bool ok=false;
		int scmd=relevant.attribute("cmdid").toInt(&ok);
		if(!ok || scmd<0 || scmd>5)
		{
			cmd.setAttribute(QStringLiteral("success"), QStringLiteral("false"));
			cmd.setAttribute(QStringLiteral("result"), QStringLiteral("ERROR: inavlid cmdid"));
			cmd.convertToReply();
			return;
		}
		qInfo()<<"server command from"<<cmd.source().toStringVerbose()<<scmd;
		QString resStr;
		bool res=executeServerCmd((DwkApp::ServerCmd)scmd, &resStr);
		cmd.setAttribute(QStringLiteral("success"), res?QStringLiteral("true"):QStringLiteral("false"));
		cmd.setAttribute(QStringLiteral("result"), resStr);
		cmd.convertToReply();
		return;
	}
	else if(relevant.cmd()=="datalist")
	{
		QDate fromDate=QDate::fromString(relevant.attribute("from"), Qt::ISODate);
		QDate toDate=QDate::fromString(relevant.attribute("to"), Qt::ISODate);
		QDir ddir(dataPath);
		ddir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
		ddir.setNameFilters(QStringList()<<"data_*.xml");
		//ddir.setSorting(QDir::Name);
		QStringList dfs=ddir.entryList();
		foreach(QString filename, dfs)
		{
			if(filename.size()<13)
				continue;
			QDate testdate=QDate::fromString(filename.mid(5, 8), "yyyyMMdd");
			if(testdate<fromDate || testdate>toDate)
				continue;
			QFileInfo fi(ddir.absoluteFilePath(filename));
			Fragment *frag=new Fragment("datafile");
			frag->setAttribute("name", filename);
			frag->setAttribute("size", QString::number(fi.size()));
			cmd.addFragment(frag);
		}
		cmd.convertToReply();
		return;
	}
	else if(relevant.cmd()=="datatrans")
	{
		//QString fname=dataPath+"/"+relevant.attribute("name");
		QString fname=dataPath+relevant.attribute("name");
		qDebug()<<"datatrans for"<<fname;
		QFile f(fname);
		if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			qWarning()<<"cannot open file for datatrans"<<fname;
			cmd.setAttribute("error", "cannot open file");
			cmd.convertToReply();
			return;
		}
		QByteArray fd=f.readAll();
		f.close();
		qDebug()<<"setting payload with size"<<fd.size();
		cmd.setPayloadEncode(fd);
		cmd.convertToReply();
		qDebug()<<"dispatching with size"<<cmd.payload()->size();
		return;
	}
	else
		qCritical()<<"unknown command for DwkApp"<<relevant.cmd()<<cmd.procTarget().toStringVerbose();
	return;
}

bool DwkApp::executeServerCmd(DwkApp::ServerCmd serverCmd, QString *result)
{
	if(result)
		result->clear();
	switch(serverCmd)
	{
	case ServerCmd::NOP:
		if(result)
			*result=QStringLiteral("A robot may not injure a human being.");
		return true;
	case ServerCmd::END_PROCESS:
		shtdAlg=true;
		shtdAlgSafe=true;
		shtdHw=true;
		shutdownPending=true;
		return true;
	case ServerCmd::STOP_SERVICE:
	case ServerCmd::RESTART_SERVICE:
	{
		if(!systemd)
		{
			if(result)
				*result=QStringLiteral("ERROR: piahd not running as a service");
			return false;
		}
		QProcess proc;
		proc.start(QStringLiteral("systemctl"), QStringList()<<(serverCmd==ServerCmd::STOP_SERVICE?"stop":"restart")<<"piahd");
		proc.waitForFinished(5000); // will not return process goes down
		return true;
	}
	case ServerCmd::SHUTDOWN_MACHINE:
	case ServerCmd::RESTART_MACHINE:
	{
		QProcess proc;
		proc.start(QStringLiteral("shutdown"), QStringList()<<(serverCmd==ServerCmd::SHUTDOWN_MACHINE?"-h":"-r")<<"now");
		proc.waitForFinished(5000); // will not return process goes down
		return true;
	}
	default:
		break;
	}
	return false;
}

void DwkApp::timerEvent(QTimerEvent *event)
{
	Q_UNUSED(event);
	if(shutdownPending)
	{
		if(shtdAlg)
		{
			DCON.triggerAlgShutdown();
			shtdAlg=false;
		}
		if(shtdAlgSafe)
		{
			if(DCON.algSafePending())
				return; // keep event loop running
			shtdAlgSafe=false;
		}
		if(shtdHw)
		{
			DCON.shutdown();
			shtdHw=false;
		}
		qInfo()<<"STOP at HB"<<hbCnt;
		killTimer(timerId);
		qApp->exit(0);
		return;
	}
	DCON.triggerWatchdog();
	DCON.triggerAlgExec(hbCnt);
	++hbCnt;
}


