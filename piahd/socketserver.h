#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

#include "piadh.h"
#include <QObject>

class QTcpServer;

namespace pi_at_home
{

class SocketServer : public QObject
{
	Q_OBJECT
public:
	explicit SocketServer(QObject *parent = 0);
private slots:
	void incomingCon();
private:
	QTcpServer * tcpServer;
};
}
#endif // SOCKETSERVER_H
