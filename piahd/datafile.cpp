#include "datafile.h"
#include <QDateTime>
#include <QDir>
#include <QTimerEvent>
#include "../piah_common/node.h"
#include "piadi.h"

DataFile::DataFile(QObject *parent) : QObject(parent)
{

}

DataFile::~DataFile()
{
	if(f.isOpen())
	{
		f.write("</session>\n</sessions>");
		f.close();
	}
}

bool DataFile::setup(const QString dataPath)
{
	QDir dataDir(dataPath);
	if(dataDir.exists())
	{
		dp=dataPath;
		doLog=true;
		qInfo()<<"data logging in"<<dp;
	}
	else
	{
		qCritical()<<dp<<"does not exist - no data logging";
		return false;
	}
	QMutexLocker lock(&mtx);
	return createFile();
}

bool DataFile::write(const QString label, const QVariant data, qint64 timestamp_us)
{
	if(!doLog || !canLog)
		return false;
	QString truedata;
	switch(data.type())
	{
		case QMetaType::QDateTime:
			truedata=data.toDateTime().toString(Qt::ISODate);
			break;
		case QMetaType::Double:
			truedata=QString::number(data.toDouble()); // avoid the "perfect precission" annoyance
			break;
		default:
			if(data.canConvert(QMetaType::QString))
				truedata=data.toString();
			else
			{
				qWarning()<<"no conversion to string for"<<data.typeName()<<data.type()<<label;
				//truedata="<conversion error>";
				return false;
			}
			break;
	}
	truedata.replace('\"', '\'');
	double sectst=static_cast<double>(timestamp_us/Q_INT64_C(1000)-baseMs)/1000.;
	QMutexLocker lock(&mtx);
	f.write(QString("<d t=\"%2\" l=\"%3\" v=\"%4\"/>\n").arg(QString::number(sectst, 'f', 3), label, truedata).toUtf8());
	f.flush();
	return true;
}

void DataFile::timerEvent(QTimerEvent *event)
{
	killTimer(event->timerId());
	if(!doLog)
		return;
	QMutexLocker lock(&mtx);
	canLog=false;
	if(f.isOpen())
	{
		f.write("</session>\n</sessions>");
		f.close();
	}
	createFile();
}

bool DataFile::createFile()
{
	QDateTime now=QDateTime::currentDateTime();
	qint64 us=DCON.getUsecTst();
	today=now.date();
	QTime nowt=now.time();
	int sec2Run=nowt.secsTo(QTime(23, 59, 59))+5;
	baseMs=us/Q_INT64_C(1000)-static_cast<qint64>(((nowt.hour()*60+nowt.minute())*60+nowt.second())*1000+nowt.msec());
	QString fname=dp+"data_"+today.toString("yyyyMMdd")+".xml";
	f.setFileName(fname);
	if(!f.open(QIODevice::ReadWrite | QIODevice::Append))
	{
		qCritical()<<"cannot open logfile"<<fname;
		return false;
	}
	qInfo()<<"logfile opened"<<fname<<sec2Run;
	if(f.size())
	{
		bool badfile=true;
		if(f.size()>11 && f.seek(f.size()-11))
		{
			QByteArray fend=f.readLine();
			qInfo()<<"re-opened, removing root-end"<<f.pos()<<f.size()<<fend.data();
			if(fend=="</sessions>")
				badfile=false;
		}
		if(badfile)
		{
			f.seek(f.size());
			f.write(QByteArray("\n\n<!-- BAD FILE REOPEN -->\n"));
			qInfo()<<"re-opened of truncated file or unknown filetype";
		}
		else
			f.resize(f.size()-11);
	}
	else
	{
		f.write(QByteArray("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n<sessions>\n"));
	}
	f.write(QString("<session start=\"%1\" version=\"%2\">\n<nodes>\n").arg(today.toString(Qt::ISODate), VERSION_STR).toUtf8());
	const NAdrNodesMP & nds=DCON.allNodes();
	for(NAdrNodesMP::const_iterator it=nds.begin(); it!=nds.end(); ++it)
	{
		f.write(QString("  <node name=\"%1\" log=\"%2\" unit=\"%3\" adr=\"%4\"/>\n").arg(it->second->objectName()).arg(
				it->second->logHoldoffMs()).arg(Node::unit(it->second->unit()), it->second->address().toString()).toUtf8());
		//qDebug()<<"logging"<<it->second->objectName()<<it->second->logHoldoffMs();
	}
	f.write(QString("</nodes>\n").toUtf8());
	f.flush();
	canLog=true;
	startTimer(sec2Run*1000);
	return true;
}
