#include "socketserver.h"
#include <QTcpServer>
#include "../piah_common/netcon.h"
#include "piadi.h"

SocketServer::SocketServer(QObject *parent) :
	QObject(parent), tcpServer(new QTcpServer(this))
{
	connect(tcpServer, SIGNAL(newConnection()), this, SLOT(incomingCon()));
	if (!tcpServer->listen(QHostAddress::Any, 80))
	{
			qCritical()<<"Unable to start the TCP server:"<<tcpServer->errorString();
	}
}

void SocketServer::incomingCon()
{
	QTcpSocket *clientConnection = tcpServer->nextPendingConnection();
	if(!clientConnection)
		return;
	DCON.getConPool()->addClient(new NetCon(clientConnection));
}

