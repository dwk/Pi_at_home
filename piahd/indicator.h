#ifndef INDICATOR_H
#define INDICATOR_H

#include "piadh.h"
#include <QObject>

namespace pi_at_home
{

class Indicator : public QObject
{
	Q_OBJECT
public:
	enum class Target{ INACTIVE=0, // not connected to any hardware; useful if the app expects an indicator although there is no implementation
					   SYSTEM_LED=1, // something in /sys/class/leds/ledx
					   BITPORT=2}; // a node representing a BitPort or other node taking bools as output
	enum class Type{ WATCHDOG=0, // each time you call set wich any parameter the indicator toggles its state
					 SIMPLE=1}; // a turn-on / turn-off lamp...
	enum class State{ INACTIVE=0, ACTIVE=1};
	explicit Indicator(Indicator::Type type, Indicator::Target target, QString resourceName, QObject *parent = 0);
	~Indicator();
	void set(Indicator::State newState);
signals:

private:
	void sysWrite(const QString &fn, const char * val);
	Type type_;
	Target target_;
	bool delayedInit=false;
	QString res, res2;
	Node *node=nullptr;
	bool toggleVal=false;
};
}
#endif // INDICATOR_H
