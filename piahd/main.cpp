#include "dwkapp.h"
#include <QApplication>
#include <QDebug>
#include "../piah_common/ex.h"
#include "../piah_common/command.h"

using namespace pi_at_home;

/* Features and known bugs
 * todo bug: remote alg node connects after power cycling but keeps status "busy" and wont take updates
 * todo BUG: manually created AlgNode which have an entry in the config must be constructed based on DOM element (otherwise they ingnore log parametr, e.g. algpulse.cpp:13 is wrong)
 * todo bug: water flow is logged with unit none - liter
 * todo bug: rain is logged with unit none - newton? or liter / m2?
 * todo bug: ArduBridge does not reset Arduino when systemctl restart is used (although stop and start works) not always? restart too fast?
 * todo bug: DataConD::setup "trigger initial updates": update sequence is not aware of data flow but sorted by
 *		node address which boils down to ID. As a result ID must be engineered in an unobvious way.
 * todo feature: hostdict shall be updated somehow during connect
 * todo feature: warnings and criticals shall be transferred to client and displayed upon connect; get a fast preview of Criticals and Warnings since last boot without browsing the full log
 */

/* log levels
Qt			Syslog		SL#
			LOG_EMERG	0	/ system is unusable - not used in app
qFatal		LOG_ALERT	1	/ action must be taken immediately - app aborts adter a qFatal()
qCritical	LOG_CRIT	2	/ critical conditions - unclear if recovery possible, will try
			LOG_ERR		3	/ error conditions - not used
qWarning	LOG_WARNING	4	/ warning conditions - something bad happend but it is clear how to recover
			LOG_NOTICE	5	/ normal but significant condition - not used
qInfo		LOG_INFO	6	/ informational - main actions
qDebug		LOG_DEBUG	7	/ debug-level messages - all the details
*/
#include <syslog.h>
#include <stdio.h>

bool useSyslog=false;

void debugHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	if(useSyslog)
	{
		QString templ("%1 [%2:%3]"); // no tst and severity in message
		QString msg_=templ.arg(msg, context.file).arg(context.line).toLocal8Bit().constData();
		switch (type)
		{
			case QtDebugMsg:
				syslog(LOG_DAEMON | LOG_DEBUG, "%s\n", msg_.toLocal8Bit().constData());
				break;
			case QtInfoMsg:
				syslog(LOG_DAEMON | LOG_INFO, "%s\n", msg_.toLocal8Bit().constData());
				break;
			case QtWarningMsg:
				syslog(LOG_DAEMON | LOG_WARNING, "%s\n", msg_.toLocal8Bit().constData());
				break;
			case QtCriticalMsg:
				syslog(LOG_DAEMON | LOG_CRIT, "%s\n", msg_.toLocal8Bit().constData());
				break;
			case QtFatalMsg:
				syslog(LOG_DAEMON | LOG_ALERT, "%s\n", msg_.toLocal8Bit().constData());
				abort();
		}
	}
	else
	{
		QString sever;
		switch (type)
		{
			case QtDebugMsg:
				sever="Dbg ";
				break;
			case QtInfoMsg:
				sever="Info";
				break;
			case QtWarningMsg:
				sever="Warn";
				break;
			case QtCriticalMsg:
				sever="Crit";
				break;
			case QtFatalMsg:
				sever="FATAL";
		}
		QString templ("%1 %2 %5 [%3:%4]");
		QString tst=QTime::currentTime().toString("HH:mm:ss.zzz");
		fprintf(stderr, "%s\n", templ.arg(tst, sever, context.file).arg(context.line).arg(msg).toLocal8Bit().constData());
		fflush(stderr);
		if(type==QtFatalMsg)
			abort();
	}
}

int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("dwk");
	QCoreApplication::setOrganizationDomain("effektlict.at");
	QCoreApplication::setApplicationName("PiAtHome");
	int res=0;
	for(int i=1; i<argc; ++i)
	{
		if(!strcmp(argv[i], "--syslog"))
		{
			useSyslog=true;
			break;
		}
	}
	qInstallMessageHandler(debugHandler);
	try
	{
		DwkApp a(argc, argv);
		res=a.exec();
	}
	catch(Ex & ex)
	{
		qCritical()<<"Exception catched in main"<<ex.getCode()<<ex.getMessage();
	}
	catch(...)
	{
		qCritical()<<"Exception catched in main";
	}
	return res;
}


