#ifndef PIADH_H
#define PIADH_H

#include <vector>
#include <list>
#include <map>
#include <set>
#include <QString>
#include "../piah_common/ex.h"

//#define VERSION_STR "piahd1.1" // ?
//#define VERSION_STR "piahd1.2" // 2019-04-xx new datafile format
#define VERSION_STR "piahd1.3" // 2020-09-28 DataConD::findNode by alias uses prefix-qualified alias and thus breaks all "old" config files
							   // CONNECTORs listed as nodes in xml; breaks old configs for AlgBitLogic
							   // AlgNodes use data_type instead of type as attribute name; breaks old configs for AlgDummy

namespace pi_at_home
{
class Command;
class Fragment;
class CommandTarget;
class Node;
class Device;
class GpioWrapper;
}

#endif // PIADH_H
