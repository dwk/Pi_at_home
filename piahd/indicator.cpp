#include "indicator.h"
#include <QFile>
#include "../piah_common/node.h"
#include "piadi.h"

Indicator::Indicator(Indicator::Type type, Indicator::Target target, QString resourceName, QObject *parent) :
	QObject(parent), type_(type), target_(target)
{
	switch(target_)
	{
		case Target::INACTIVE:
			break;
		case Target::SYSTEM_LED:
			res=resourceName+"/trigger";
			switch(type_)
			{
				case Type::SIMPLE:
					res2=resourceName+"/brightness";
					sysWrite(res, "none");
					sysWrite(res2, "0");
					break;
				case Type::WATCHDOG:
					res2=resourceName+"/shot";
					sysWrite(res, "oneshot");
					sysWrite(resourceName+"/delay_on", "100");
					break;
			}
			break;
		case Target::BITPORT:
			res=resourceName;
			delayedInit=true;
			break;
	}
}

Indicator::~Indicator()
{
	switch(target_)
	{
		case Target::INACTIVE:
			break;
		case Target::SYSTEM_LED:
		{
			sysWrite(res, "default-on"); // actually we should restore the original function...
			break;
		}
		case Target::BITPORT:
			break;
	}
}

void Indicator::set(Indicator::State newState)
{
	switch(target_)
	{
		case Target::INACTIVE:
			return;
		case Target::SYSTEM_LED:
			switch(type_)
			{
				case Type::SIMPLE:
					sysWrite(res, newState==State::ACTIVE?"heartbeat":"none");
					break;
				case Type::WATCHDOG:
					if(newState==State::ACTIVE)
						sysWrite(res2, "1");
					break;
			}
			break;
		case Target::BITPORT:
			if(delayedInit)
			{
				delayedInit=false;
				node=DCON.findNode(res, false); // missing indicator doesn't justfy exception
				if(!node)
				{
					target_=Target::INACTIVE;
					qCritical()<<"Indicator set to INACTIVE because outnode not found"<<res;
					return;
				}
			}
			switch(type_)
			{
				case Type::SIMPLE:
					node->setValue(NodeValue(newState==State::ACTIVE));
					break;
				case Type::WATCHDOG:
					toggleVal=(!toggleVal);
					node->setValue(NodeValue(toggleVal));
					break;
			}
			break;
	}
}

void Indicator::sysWrite(const QString &fn, const char *val)
{
	FILE *fd=fopen(fn.toUtf8().data(), "w");
	if(fd)
	{
		fprintf(fd, val);
		fclose(fd);
	}
	else
		qCritical()<<"cannot open "<<res;
}
