#ifndef DWKAPP_H
#define DWKAPP_H

#include "piadh.h"
#include <QCoreApplication>
#include <QTimer>
#include "../piah_common/commandtarget.h"
#include "../piah_common/node.h"
#include "../piah_common/algorithm.h"
//#include "smtp.h"
//#include "system.h"

void pi_at_home_sigh_EOF(int sig);

namespace pi_at_home
{
class SocketServer;

class DwkApp : public QCoreApplication, public CommandTarget
{
	Q_OBJECT
public:
	enum class ServerCmd{NOP=0, END_PROCESS, STOP_SERVICE, RESTART_SERVICE, SHUTDOWN_MACHINE, RESTART_MACHINE};
	static ServerCmd serverCmdStr2E(const QString & str);
	DwkApp(int argc, char *argv[]);
	virtual ~DwkApp();
	virtual bool notify ( QObject * receiver, QEvent * event );
	virtual void processCommand(Command & cmd, Fragment & relevant);
	bool executeServerCmd(ServerCmd serverCmd, QString * result=nullptr); // returns true on success and optionally some text return value / result
protected:
	virtual void timerEvent(QTimerEvent *event);
/*	Smtp smtp;
*/
private:
	int hbCnt=0, timerId=0;
	bool systemd=false, shutdownPending=false, shtdAlg, shtdAlgSafe, shtdHw;
	QString configFile;
	// connections
	SocketServer *socketServer=nullptr;
	// data logging
	QString dataPath;
	// camera
	bool useCamera=false;
};

} // namespace
#endif // DWKAPP_H
