/*
 * Pi@Home; Pi@Home to Ardiuno Bridge
 * Nano V3 (joy-it)
 *  port: ttyUSB0 with CH341SER driver
 *  processor: 328P old bootloader 
 *  programmer: AVRISP mkII
 *  
 * Command syntax:
 *  pos.  val   remark
 *  0     A..Z  channel-index or . for device
 *  1     :     fixed
 *  2-n   any   command
 *  n+1   |     fixed
 *  
 * known device commands
 *  .:E|    enable command echo
 *  .:e|    disable command echo
 *  .:v|    reply with version string
 *  X:anycmd| forward <anycmd> to channel X
 *  X:Idn..| initialize device d on channel X with parameter(s) n.. - every device must implement I cmd
 *  X:i|    un-initialize channel X - restore I/Os, be ready for next I cmd
 */


#define VERSION "pi2Ar1.4"
#define CMDBUFLEN 32
class Command
{
public: 

  Command() : cmd_(' '), cmdTarget_(0), bufOffset(0), cmdecho(false)
  {
    param_[0]=0;
  }
  
  void setup()
  {
    //Serial.setTimeout(0);
    Serial.begin(9600);
    Serial.flush();
    sendMsg(255, VERSION);
  }
  
  bool poll()
  {
    int uartavail=Serial.available();
    if(uartavail<=0)
      return false;
    if(bufOffset+uartavail>64)
    {
      bufOffset=0;
      Serial.flush();
      sendMsg(255, "Ebufovl");
      return false;
    }
    for(int i=0; i<uartavail; ++i)
    {
      int r=Serial.readBytes(commandBuf+bufOffset, 1);
      if(!r)
        return false; // strange... error?
      if(commandBuf[bufOffset]=='|')
        break; // command complete
      ++bufOffset;
    }
    if(commandBuf[bufOffset]!='|')
      return false; // not yet ready
    commandBuf[bufOffset]=0; // replace | with 0, copy till bufOffset will include terminating zero
    if(bufOffset<3 || commandBuf[1]!=':' || (commandBuf[0]!='.' && (commandBuf[0]<'A' || commandBuf[0]>'Z')) )
    {
      bufOffset=0;
      sendMsg(255, "Einvch", commandBuf[0]);
      return false;
    }
    if(commandBuf[0]=='.')
    { // internal command
      if(cmdecho)
        sendMsg(255, &commandBuf[2]);
      switch(commandBuf[2])
      {
        case 'E': cmdecho=true; break;
        case 'e': cmdecho=false; break;
        case 'v': sendMsg(255, VERSION); break;
        default: sendMsg(255, "Ecmd1_", commandBuf[2]); break;
      }
      bufOffset=0;
      return false;
    }
    cmdTarget_=commandBuf[0]-'A';
    cmd_=commandBuf[2];
    int j=0;
    for(int i=3; i<=bufOffset && i<CMDBUFLEN; ++i, ++j)
      param_[j]=commandBuf[i];
    param_[j]=0;
    bufOffset=0;
    if(cmdecho)
      sendMsg(cmdTarget_+32, &(commandBuf[2]));
    return true;
  }
  
  void sendMsg(byte channel, const char * msg)
  {
    Serial.print(channel==255?'.':(char)(channel+'A'));
    Serial.print(':');
    Serial.print(msg);
    Serial.print('|');
  }
  
  void sendMsg(byte channel, const char * msg, long param)
  {
    Serial.print(channel==255?'.':(char)(channel+'A'));
    Serial.print(':');
    Serial.print(msg);
    Serial.print(param);
    Serial.print('|');
  }
  byte cmdTarget() const {return cmdTarget_;}
  const char cmd() const {return cmd_;}
  const char * param() const {return param_;}
private:
  char commandBuf[CMDBUFLEN], cmd_, param_[CMDBUFLEN-3];
  byte cmdTarget_, bufOffset;
  bool cmdecho;
};
Command commander;
unsigned int LCnt;


// ********************
// ValReader begin **** 
// ********************
/* base class for value reading and averaging
 * 
 * known commands (X .. arbitrary channel)
 *  X:Annn| set number of averaged measurmenets to 0<nnn<256 (default 10) will reset the averaging cycle
 *          reply Ainv if nnn out of range
 *  X:Rnnn| round the (averaged) result to the nearest multiple of nnn before sending, nnn>0 (default 1 = no rounding)
 *          reply Rinv if nnn out of range
 *  X:N|    disable measurements
 *  X:S|    trigger single measurment
 *  X:C|    start continous measurements (the default state after boot)
 *  X:D|    direct write of values, so that the Arduino-IDE plotter can display them
 *  X:d|    formated write (default, use for Pi@Home comm)
 *  X:Z|    set next (averaged) value as zero; next value will report as Z instead of V
 *  X:z|    no zero compensation, zero=0 (default)
 *  returned data
 *  X:Vn| n data, corrected for zero
 *  X:Zn| n zero value used from now on (issued once after Z command)
 */
class ValReader
{
public:
  ValReader() : formated(true), zeroUpdate(false), ch(255), 
  avgMaxCount(10), avgCount(0), avgMode(0), result(0), zero(0), roundTo(1)
  {}
  
  void setup(byte channel)
  {
    ch=channel;
    avgMode=2; // start with continous measurements
  }
  
  bool newVal(long v) // true if value complete & sent upstream, false if in progress
  {
    ++avgCount;
    result+=v;
    if(avgCount==avgMaxCount)
    {
      result/=avgMaxCount;
      if(roundTo>1)
      {
        long rem=result%roundTo;
        result-=rem;
        if(abs(rem)>roundTo/2)
        {
          if(rem>0)
            result+=roundTo;
          else
            result-=roundTo;
        }
      }
      if(zeroUpdate)
        zero=result;
      else
        result-=zero;
      if(formated)
        commander.sendMsg(ch, zeroUpdate?"Z":"V", result);
      else
        Serial.println(result);
      zeroUpdate=false;
      avgCount=0;
      result=0;
      if(avgMode==1)
        avgMode=0;
      return true;
    }
    return false;
  }
  
  bool processCmd(bool & avgModeChanged) // returns true if processed
  {
    avgModeChanged=false;
    switch(commander.cmd())
    {
    case 'A':
    {
      String p(commander.param());
      long v=p.toInt();
      if(v>0 && v<256)
      {
        avgMaxCount=v;
        avgCount=0;
        result=0;
      }
      else
        commander.sendMsg(ch, "Ainv", v);
      break;
    }
    case 'R':
    {
      String p(commander.param());
      long v=p.toInt();
      if(v>0)
        roundTo=v;
      else
        commander.sendMsg(ch, "Rinv", v);
      break;
    }
    case 'C': // continous measurement
      avgMode=2;
      avgCount=0;
      result=0;
      avgModeChanged=true;
      break;
    case 'D':
      formated=false;
      break;
    case 'd':
      formated=true;
      break;
    case 'N': // no measurement (stoped)
      avgMode=0;
      avgModeChanged=true;
      break;
    case 'S': // single measurement
      avgMode=1;
      avgCount=0;
      result=0;
      avgModeChanged=true;
      break;
    case 'Z':
      zeroUpdate=true;
      break;
    case 'z':
      zero=0;
      break;
    default:
      return false;
    }
    return true;
  }
protected:
  bool formated, zeroUpdate;
  byte ch, avgMaxCount, avgCount, avgMode;
  long result, zero, roundTo;
};
// ******************
// ValReader end **** 
// ******************

// **************
// HY-SRF05 begin 
// **************
/* deviceID  U   Ultrasonic
 * default pinout direct connection
 * nano   pin   module  pin
 * D5     8     Vcc     5
 * D4     7     Trig    4
 * D3     6     Echo    3
 * D2     NC    out     2 CUT THIS PIN IN THE HEADER
 * GND    4     GND     1
 * 
 * 0 1 2 3 4 5 6 7 8 9 10 11 12
 * A B C D E F G H I J  K  L  M 
 * 
 * known commands (X .. arbitrary channel)
 *  X:IUFED| initialize to default pinout
 *  more commands and returns see base class
 */
class HYSRF05 : public ValReader
{
public:
  HYSRF05() : ValReader(), PORT_VCC(5), PORT_TRIG(4), PORT_ECHO(3) 
  {}
  
  void setup(byte channel, byte portVCC, byte portTrig, byte portEcho)
  {
    ValReader::setup(channel);
    PORT_VCC=portVCC;
    PORT_TRIG=portTrig;
    PORT_ECHO=portEcho;
    // HY-SRF05 power over port
    pinMode(PORT_ECHO, INPUT);
    digitalWrite(PORT_VCC, LOW);
    pinMode(PORT_VCC, OUTPUT);
    digitalWrite(PORT_TRIG, LOW);
    pinMode(PORT_TRIG, OUTPUT);
    // turn on
    digitalWrite(PORT_VCC, HIGH);
  }
  
  bool work() // false if problem
  {
    if(ch==255 || !avgMode) // run every 100ms
      return true;
    digitalWrite(PORT_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(PORT_TRIG, LOW);
    unsigned long echoDur=pulseIn(PORT_ECHO, HIGH, 200000);
    if(echoDur)
    {
      if(newVal(echoDur))
      {
        if(avgMode==0)
        {
          digitalWrite(PORT_VCC, LOW);
        }
      }
      return true;
    }
    else
      return false;
  }
  
  bool processCmd() // returns true if processed
  {
    if(ch==255 && commander.cmd()=='I')
    {
      const char *p=commander.param();
      if(p[0]=='U')
      {
        byte Vcc(p[1]-'A'), Trig(p[2]-'A'), Echo(p[3]-'A');
        if(Vcc<2 || Vcc>12 || Trig<2 || Trig>12 || Echo<2 || Echo>12 || Vcc==Trig || Vcc==Echo || Trig==Echo)
        {
          commander.sendMsg(commander.cmdTarget(), "EUp");
          return true;
        }
        setup(commander.cmdTarget(), Vcc, Trig, Echo);
        commander.sendMsg(ch, "OKU");
        return true;
      }
      return false;
    }
    if(commander.cmdTarget()!=ch)
      return false;
    if(commander.cmd()=='i')
    {
      ch=255;
      digitalWrite(PORT_VCC, LOW);
      pinMode(PORT_VCC, INPUT);
      pinMode(PORT_TRIG, INPUT);
      commander.sendMsg(ch, "OKU_");
      return true;
    }
    bool avgModeChg;
    if(ValReader::processCmd(avgModeChg))
    {
      if(avgModeChg)
      {
        switch(avgMode)
        {
          case 0:
            digitalWrite(PORT_VCC, LOW);
            break;
          case 1:
          case 2:
            digitalWrite(PORT_VCC, HIGH);
            break;
        }
      }
      return true;
    }
    /* switch(commander.cmd())
    {
    case 'X': 
      break;
    default:
        commander.sendMsg(ch, "EUc");
    } */
    commander.sendMsg(ch, "EUc"); // all command handled in base class
    return true;
  }
private:
  byte PORT_VCC, PORT_TRIG, PORT_ECHO;
};
HYSRF05 hysrf05;
// **************
// HY-SRF05 end * 
// **************


// **************
// HX711 begin **
// **************
/* deviceId F   Force sensor
 *  
 * defualt pinout cable connection
 * nano   pin   module  pin
 * GND    ?     GND     _
 * D6     9     DT      _
 * D7    10     SCK     _
 * VCC    ?     VCC     _
 * 
 * 0 1 2 3 4 5 6 7 8 9 10 11 12
 * A B C D E F G H I J  K  L  M 
 * 
 * known commands (X .. arbitrary channel)
 *  X:IFGH| initialize to default pinout
 *  more commands and returns see base class
 * work() may send w upstream if no value available yet
 */
class HX711 : public ValReader
{
public:
  HX711() : ValReader(), pinData(6), pinClk(7)
  {}
  
  void setup(byte channel, byte pinData_, byte pinClk_)
  {
    ValReader::setup(channel);
    pinData=pinData_;
    pinClk=pinClk_;
    pinMode(pinData, INPUT);
    pinMode(pinClk, OUTPUT);
    // turn on
    digitalWrite(pinClk, LOW);
  }
  
  bool work() // false if problem
  {
    if(ch==255 || !avgMode) // 100ms = max speed; || LCnt&0x7 800ms
      return true;
    if(digitalRead(pinData) == HIGH)
    {
      commander.sendMsg(ch, "w");
      return false; // not ready
    }
    noInterrupts();
    // Pulse the clock pin 24 times to read the data.
    val.data[2] = shiftIn(pinData, pinClk, MSBFIRST);
    val.data[1] = shiftIn(pinData, pinClk, MSBFIRST);
    val.data[0] = shiftIn(pinData, pinClk, MSBFIRST);
    byte xclk=1;   // gain 128
    //if(gain==64) xclk=3;
    //if(gain==32) xclk=2;
    while(xclk>0)
    {
      digitalWrite(pinClk, HIGH);
      digitalWrite(pinClk, LOW);
      --xclk;
    }
    interrupts();
    // SIGN extend
    if(val.data[2] & 0x80) 
      val.data[3]=0xff;
    else
      val.data[3]=0;
    if(newVal(val.value))
    {
      if(avgMode==0)
      {
        //digitalWrite(pinClk, LOW);
        digitalWrite(pinClk, HIGH);
      }
    }
    return true;
  }
  
  bool processCmd() // returns true if processed
  {
    if(ch==255 && commander.cmd()=='I')
    {
      const char *p=commander.param();
      if(p[0]=='F')
      {
        byte Data(p[1]-'A'), Clk(p[2]-'A');
        if(Data<2 || Data>12 || Clk<2 || Clk>12 || Data==Clk)
        {
          commander.sendMsg(commander.cmdTarget(), "EFp");
          return true;
        }
        setup(commander.cmdTarget(), Data, Clk);
        commander.sendMsg(ch, "OKF");
        return true;
      }
      return false;
    }
    if(commander.cmdTarget()!=ch)
      return false;
    if(commander.cmd()=='i')
    {
      ch=255;
      digitalWrite(pinClk, HIGH);
      pinMode(pinClk, INPUT);
      commander.sendMsg(ch, "OKF_");
      return true;
    }
    bool avgModeChg;
    if(ValReader::processCmd(avgModeChg))
    {
      if(avgModeChg)
      {
        switch(avgMode)
        {
          case 0:
            //digitalWrite(pinClk, LOW);
            digitalWrite(pinClk, HIGH);
            break;
          case 1:
          case 2:
            digitalWrite(pinClk, LOW);
            break;
        }
      }
      return true;
    }
    /* switch(commander.cmd())
    {
    case 'X': 
      break;
    default:
        commander.sendMsg(ch, "EUc");
    } */
    commander.sendMsg(ch, "EFc"); // all command handled in base class
    return true;
  }
private:
  byte pinData, pinClk;
  union
  {
    long value = 0;
    uint8_t data[4];
  } val;
};
HX711 hx711;
// **************
// HX711 end **** 
// **************


// **************
// DigiOut begin 
// **************
/* deviceID  D   Digital Output
 * 
 * known commands (X .. arbitrary channel)
 *  X:IDx| initialize to default pinout
 *    x = 1..n D-Port code letters according to:
 *  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
 *  A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T
 * D0 D1 D2 D3 D4 D5 D6 D7 B0 B1 B2 B3 B4 B5 C0 C1 C2 C3 C4 C5
 * RX TX I0 I1                   SP SP SP SP A0 A1 A2 A3 A4 A5
 *
 *  X:Vnnnnnnn| lowest bit on nnn goes to first port, second lowest bit to second port... (up to 20 bit = 1048576)
 *  X:Bmn|      m = channel (as code letter) BUT INDEXING the INITIALIZED CHANNELS (i.e A is the first port mentioned in I cmd)
 *              n = 0|1, sets this port
 *
 * 7 segment naming: https://de.wikipedia.org/wiki/Segmentanzeige
 */
class DigiOut
{
public:
  DigiOut() : ch(255), portCnt(0)
  {}
  
  bool processCmd() // returns true if processed
  {
    if(ch==255 && commander.cmd()=='I')
    {
      const char *p=commander.param();
      //Serial.print(p);
      if(p[0]=='D')
      {
        for(; p[portCnt+1] && portCnt<20; ++portCnt)
        {
          byte port=p[portCnt+1];
          //Serial.print(port);
          if(port<'A' || port>'T')
          {
            portCnt=0;
            commander.sendMsg(commander.cmdTarget(), "EDp");
            return true;
          }
          ports[portCnt]=port-'A';
        }
        //Serial.print(portCnt);
        for(int i=0; i<portCnt; ++i)
        {
          pinMode(ports[i], OUTPUT);
          digitalWrite(ports[i], LOW);
        }
        ch=commander.cmdTarget();
        commander.sendMsg(ch, "OKD");
        return true;
      }
      return false;
    }
    if(commander.cmdTarget()!=ch)
      return false;
    switch(commander.cmd())
    {
    case 'i':
    {
      ch=255;
      for(int i=0; i<portCnt; ++i)
      {
        pinMode(ports[i], INPUT);
      }
      portCnt=0;
      commander.sendMsg(ch, "OKD_");
      return true;
    }
    case 'V':
    {
      String p(commander.param());
      long v=p.toInt();
      if(v<0) // no negs
      {
        commander.sendMsg(ch, "Vneg", v);
        break;
      }
      for(int i=0; i<portCnt; ++i)
      {
        digitalWrite(ports[i], v%2);
        v=v>>1;
      }
      if(v>0) // there are bits left
        commander.sendMsg(ch, "Vovl", v);
      break;
    }
    case 'B':
    {
      const char *p=commander.param();
      if(p[0]<'A')
      {
        commander.sendMsg(ch, "Bp", p[0]);
        break;
      }
      byte port=p[0]-'A';
      if(port>=portCnt)
      {
        commander.sendMsg(ch, "BP", p[0]);
        break;
      }
      if(p[1]!='0' && p[1]!='1')
      {
        commander.sendMsg(ch, "Bv", p[1]);
        break;
      }
      digitalWrite(ports[port], p[1]=='1');
      break;
    }
    default:
      commander.sendMsg(ch, "EDc");
    }
    return true;
  }
private:
  byte ch, ports[20], portCnt;
};
DigiOut digiOut;
// **************
// DigiOut end  * 
// **************


void setup() 
{
  LCnt=1; // don't trigger 1s slot in first loop
  commander.setup();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

bool cmdProcessed, problem;

void loop() 
{
  // dispatch commands
  if(commander.poll())
  {
    cmdProcessed=hysrf05.processCmd();
    if(!cmdProcessed)
      cmdProcessed=hx711.processCmd();
    if(!cmdProcessed)
      cmdProcessed=digiOut.processCmd();
    if(!cmdProcessed)
      digitalWrite(LED_BUILTIN, HIGH);
  }
  // main delay
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  // poll devices
  problem=false;
  if(!hysrf05.work())
    problem=true;
  if(!hx711.work())
    problem=true;
  //if(!digiOut.work()) no work function for DigiOut
  //  problem=true;
  if(problem)
    digitalWrite(LED_BUILTIN, HIGH);
  ++LCnt;
}
