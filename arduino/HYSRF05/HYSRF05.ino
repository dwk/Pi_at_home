
/*
 * Pi@Home; ultrasonic distance measurement
 * Nano V3 (joy-it)
 *  port: ttyUSB0 with CH341SER driver
 *  processor: 328P old bootloader 
 *  programmer: AVRISP mkII
 */
//#define OLD_HEADER
/* pinout cable connection
 * nano   pin   module  pin
 * D4     7     Vcc     5
 * D3     6     Trig    4
 * D2     5     Echo    3
 *        NC    out     2
 * GND    4     GND     1
 */

#define DIRECT_HEADER
/* pinout direct connection
 * nano   pin   module  pin
 * D5     8     Vcc     5
 * D4     7     Trig    4
 * D3     6     Echo    3
 * D2     NC    out     2 CUT THIS PIN IN THE HEADER
 * GND    4     GND     1
 */

#ifdef OLD_HEADER
  #ifdef DIRECT_HEADER
    #error DOUBLE DEFINITION OF HEADER OPTION
  #endif
  #define PORT_VCC  4
  #define PORT_TRIG 3
  #define PORT_ECHO 2
  #define VERSION "arah1"
#else
  #ifndef DIRECT_HEADER
    #error NO DEFINITION OF HEADER OPTION
  #endif
  #define PORT_VCC  5
  #define PORT_TRIG 4
  #define PORT_ECHO 3
  #define VERSION "arah2"
#endif
/*
 * Command syntax:
 *  pos.  val   remark
 *  0     A..Z  channel-index or . for device
 *  1     :     fixed
 *  2-n   any   command
 *  n+1   |     fixed
 *  
 *  known commands
 *  .:E|    enable command echo
 *  .:e|    disable command echo
 *  .:v|    reply with version string
 *  A:Annn| set number of averaged measurmenets to 0<nnn<256
 *  A:N|    disable measurements
 *  A:S|    trigger single measurment
 *  A:C|    start continous measurements
 *  returned data
 *  A:Vnnn| nnn microseconds roundtrip time
 */
 #define CMDPARAMBUFLEN 11
class Command
{
public: 
  Command() : cmd_(' '), cmdTarget_(0), bufOffset(0), cmdecho(false)
  {
    param_[0]=0;
  }
  void setup_()
  {
    //Serial.setTimeout(0);
    Serial.begin(9600);
    Serial.flush();
    sendMsg(255, VERSION);
  }
  bool poll()
  {
    int uartavail=Serial.available();
    if(uartavail<=0)
      return false;
    if(bufOffset+uartavail>64)
    {
      bufOffset=0;
      Serial.flush();
      sendMsg(255, "Ebufovl");
      return false;
    }
    for(int i=0; ; )
    {
      int r=Serial.readBytes(commandBuf+bufOffset, 1);
      if(!r)
        return false; // strange... error?
      if(commandBuf[bufOffset++]=='|')
      {
        break; // command complete
      }
      if(++i==uartavail)
        return false; // not yet ready
      //Serial.write(commandBuf, bufOffset);
    }
    commandBuf[bufOffset-1]=0; // copy till bufOffset will include terminating zero
    if(commandBuf[0]=='.' && commandBuf[1]==':')
    { // internal command
      if(cmdecho)
        sendMsg(255, &commandBuf[2]);
      switch(commandBuf[2])
      {
        case 'E': cmdecho=true; break;
        case 'e': cmdecho=false; break;
        case 'v': sendMsg(255, VERSION); break;
        default: sendMsg(255, "Ecmd"); break;
      }
      bufOffset=0;
      return false;
    }
    if( commandBuf[0]<'A' || commandBuf[0]>'Z' || commandBuf[1]!=':')
    {
      bufOffset=0;
      sendMsg(255, "Einvch", commandBuf[0]);
      return false;
    }
    cmdTarget_=commandBuf[0]-'A';
    cmd_=commandBuf[2];
    int j=0;
    for(int i=3; i<bufOffset && j<CMDPARAMBUFLEN; ++i, ++j)
      param_[j]=commandBuf[i];
    bufOffset=0;
    if(cmdecho)
      sendMsg(cmdTarget_+32, &(commandBuf[2]));
    return true;
  }
  void sendMsg(byte channel, const char * msg)
  {
    Serial.print(channel==255?'.':(char)(channel+'A'));
    Serial.print(':');
    Serial.print(msg);
    Serial.print('|');
  }
  void sendMsg(byte channel, const char * msg, long param)
  {
    Serial.print(channel==255?'.':(char)(channel+'A'));
    Serial.print(':');
    Serial.print(msg);
    Serial.print(param);
    Serial.print('|');
  }
  byte cmdTarget() const {return cmdTarget_;}
  const char cmd() const {return cmd_;}
  const char * param() const {return param_;}
private:
  char commandBuf[64], cmd_, param_[CMDPARAMBUFLEN];
  byte cmdTarget_, bufOffset;
  bool cmdecho;
};

Command command;

byte avgMaxCount, avgCount, avgMode;
unsigned long avgEchoDur;

void setup() 
{
  command.setup_();
  // HY-SRF05 power over port
  pinMode(PORT_ECHO, INPUT);
  digitalWrite(PORT_VCC, LOW);
  pinMode(PORT_VCC, OUTPUT);
  digitalWrite(PORT_TRIG, LOW);
  pinMode(PORT_TRIG, OUTPUT);
  //
  avgMaxCount=20;
  avgCount=0;
  avgEchoDur=0;
  avgMode=2; // start with continous measurements
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  // turn on
  digitalWrite(PORT_VCC, HIGH);
}

void loop() 
{
  delay(100);
  if(avgMode)
  {
    digitalWrite(PORT_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(PORT_TRIG, LOW);
    unsigned long echoDur=pulseIn(PORT_ECHO, HIGH, 200000);
    //unsigned long echoDur=random(10, 20000);  dummy read if no HY-SRF05 is connected 
    if(echoDur)
    {
      ++avgCount;
      avgEchoDur+=echoDur;
      if(avgCount==avgMaxCount)
      {
        command.sendMsg(0, "V", avgEchoDur/avgMaxCount);
        avgCount=0;
        avgEchoDur=0;
        if(avgMode==1)
        {
          avgMode=0;
          digitalWrite(PORT_VCC, LOW);
        }
      }
      digitalWrite(LED_BUILTIN, LOW);
    }
    else
    {
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }
  if(command.poll())
  {
    switch(command.cmd())
    {
    case 'A':
    {
      String p(command.param());
      long v=p.toInt();
      if(v>0 && v<256)
      {
        avgMaxCount=v;
        avgCount=0;
        avgEchoDur=0;
      }
      else
        command.sendMsg(command.cmdTarget(), "Ainv", v);
      break;
    }
    case 'C': // continous measurement
      digitalWrite(PORT_VCC, HIGH);
      avgMode=2;
      avgCount=0;
      avgEchoDur=0;
      delay(50); // make sure module is ready
      break;
    case 'N': // no measurement (stoped)
      digitalWrite(PORT_VCC, LOW);
      avgMode=0;
      break;
    case 'S': // single measurement
      digitalWrite(PORT_VCC, HIGH);
      avgMode=1;
      avgCount=0;
      avgEchoDur=0;
      delay(50); // make sure module is ready
      break;
    }
  }
}
