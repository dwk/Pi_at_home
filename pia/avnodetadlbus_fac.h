#ifndef AVNODETADLBUS_FAC_H
#define AVNODETADLBUS_FAC_H
#include "../piah_common/nodetadlbus_nm.h"

namespace
{
pi_at_home::AvNode * AvCreatorSensorT(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeTaDlBusSensorT(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorRpm(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeTaDlBusRpm(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorTst(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeTaDlBusTst(frag, guiParent);
}
bool avatarRegisteredSensorT=pi_at_home::AvatarFactory::registerAvatar(typeNameSensorT, AvCreatorSensorT);
bool avatarRegisteredRpm=pi_at_home::AvatarFactory::registerAvatar(typeNameRpm, AvCreatorRpm);
bool avatarRegisteredTst=pi_at_home::AvatarFactory::registerAvatar(typeNameTst, AvCreatorTst);
}

#endif // AVNODETADLBUS_FAC_H
