#ifndef AVNODEALG_H
#define AVNODEALG_H

#include "piadh.h"
#include "avnode.h"
//#include "../piah_common/nodealg.h"
//#include "../piah_common/device.h"
#include <QDebug>
#include <QDoubleSpinBox>
#include <QLineEdit>

namespace pi_at_home
{

// bool -----------------------------------------------------
class AvNodeAlgBool : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeAlgBool(const Fragment & fragment, QWidget *parent);
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeBool(text);}
	QCheckBox * widVal;
	bool val;
protected slots:
	void valChanged(bool val);
};

// int -----------------------------------------------------
class AvNodeAlgInt : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeAlgInt(const Fragment & fragment, QWidget *parent);
	virtual void processCommand(Command &cmd, Fragment & relevant);
	virtual bool eventFilter(QObject *watched, QEvent *event);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeQString(text);}
	QSpinBox * widVal;
protected slots:
	void valChangedSl(int val);
};

// double -----------------------------------------------------
class AvNodeAlgDbl : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeAlgDbl(const Fragment & fragment, QWidget *parent);
	virtual void processCommand(Command &cmd, Fragment & relevant);
	virtual bool eventFilter(QObject *watched, QEvent *event);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeDouble(text);}
	QDoubleSpinBox * widVal;
protected slots:
	void valChangedSl(double val);
};

// QString -----------------------------------------------------
class AvNodeAlgStr : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeAlgStr(const Fragment & fragment, QWidget *parent);
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeQString(text);}
	QLineEdit * widVal;
protected slots:
	void editingFinishedSl();
};

}
#endif // AVNODEALG_H
