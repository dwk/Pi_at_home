#ifndef WIDDATACOLLECTOR_H
#define WIDDATACOLLECTOR_H

#include "piadh.h"
#include <list>
#include <QWidget>
#include <QThread>
#include <QMutex>
#include "../piah_common/commandtarget.h"
#include "../piah_common/datahistory.h"

class QTreeWidgetItem;

namespace Ui {
class WidDataCollector;
}

namespace pi_at_home
{

class FileLoadWorker : public QObject
{
	Q_OBJECT
public:
	void pushToDcon(); // call from GUI thread for threadsafe transfer of loacl data to DCON
public slots:
	void load(const QStringList & filelist, const QDate & fromDate, const QDate & toDate);
private:
	DataHistoryMP dataHistMP;
signals:
	void loadProgressSig(int precentage);
	void loadDoneSig(int fileCnt, int SkipCnt);
};

struct FileTransferJob
{
	FileTransferJob() {}
	FileTransferJob(const QString &server, const QString & localFullName, const QString & remoteName) :
		server(server), localFullName(localFullName), remoteName(remoteName) {}
	QString server, localFullName, remoteName;
};

class FileTransferWorker : public QObject, public CommandTarget
{
	Q_OBJECT
public:
	FileTransferWorker();
	~FileTransferWorker();
	virtual void processCommand(Command &cmd, Fragment & relevant);
public slots:
	void transfer(QList<pi_at_home::FileTransferJob> jobs);
private:
	QList<pi_at_home::FileTransferJob> jobs_;
	int totalTransfer=0;
	QMutex jobLock;
private slots:
	void transferCmdSl();
signals:
	void dispatchCmdSig(QObject * cmd);
	void transferCmdSig();
	void transferProgressSig(int precentage);
	void transferDoneSig(int fileCnt);
};

class WidDataCollector : public QWidget, public CommandTarget
{
	Q_OBJECT
public:
	explicit WidDataCollector(QWidget *parent = 0);
	~WidDataCollector();
	virtual void processCommand(Command & cmd, Fragment & relevant);
//public slots:
private slots:
	void on_pushScan_clicked();
	void on_pushLoad_clicked();
	void fillTree();
	void itemChangedSl(QTreeWidgetItem *twi, int col);
	void loadProgressSl(int precentage);
	void loadDoneSl(int fileCnt, int skipCnt);
	void transferDoneSl(int fileCnt);
private:
	void recurseTree(QStringList &filelist, QTreeWidgetItem *twi);
	void workstate(bool work);
	Ui::WidDataCollector *ui;
	bool treeParentUpdate=false;
	QThread workerThread;
	FileLoadWorker *lworker=nullptr;
	FileTransferWorker *tworker=nullptr;
signals:
	void loadSig(const QStringList & filelist, const QDate & fromDate, const QDate & toDate);
	//void transferSig(const QString & serverUid, const QStringList & filelist);
	void transferSig(QList<pi_at_home::FileTransferJob> jobs);
	void loadCompleted();
};
}

Q_DECLARE_METATYPE(pi_at_home::FileTransferJob)
Q_DECLARE_METATYPE(QList<pi_at_home::FileTransferJob>)

#endif // WIDDATACOLLECTOR_H
