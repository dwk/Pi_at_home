#ifndef EDITOROBJECTDERIVED_H
#define EDITOROBJECTDERIVED_H
#include "piadh.h"
#include "editorobject.h"
#include "editordict.h"

namespace pi_at_home
{
class EditorAlgNode : public EditorNode
{
public:
	EditorAlgNode(const EditorNodeMetadata *metaData, int channel, QGraphicsItem * parent);
	EditorAlgNode(QDomElement &elem, EditorNodeMetadata *metaData, EditorDict *ed, QGraphicsItem * parent);
	virtual ~EditorAlgNode() {}
	virtual void updateXml(QDomElement &domParent, const EditorBaseConfig & bc);
};

class EOBitLogic : public EditorObject
{
	Q_OBJECT
public:
	EOBitLogic(const EditorObjectMetadata *metaData);
	EOBitLogic(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed);
	virtual ~EOBitLogic() {}
	virtual bool canDeleteNode(int channel, const QString & nodeTypeName) override;
	virtual bool canChangeChannel(int oldChannel, int newChannel) override {return oldChannel==newChannel;} // pin input channels
	//virtual void updateXml(QDomElement &domParent, const EditorBaseConfig & bc) override;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
protected:
	virtual QStringList checkConsistency() override;
};

class EOCompare : public EditorObject
{
	Q_OBJECT
public:
	EOCompare(const EditorObjectMetadata *metaData);
	EOCompare(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed);
	virtual ~EOCompare() {}
	virtual bool canDeleteNode(int channel, const QString & nodeTypeName) override;
	virtual bool canChangeChannel(int oldChannel, int newChannel) override {return oldChannel==newChannel;} // pin channels
protected:
	virtual bool contextMenuAddNodes(QMenu *menu) override; // return=true indicates that all node option are done and EditorObject shall not add default "node add" actions
	virtual void updateInfoStr() override;
	virtual QStringList checkConsistency() override;
	//virtual bool hasExtendedEditor() {return true;}
	//virtual bool updateExtendedEditor(QWidget *parent, KeyValMV &currentKeyVals); // false = no change
protected slots:
	void addNodeExtSl(QString nodeTypeName, bool reindexAndUpdate = true);
protected:
	EditorNodeMetadata::DataType levelDt=EditorNodeMetadata::DataType::ANY; // will be either double or int
};

class EODummy : public EditorObject
{
	Q_OBJECT
public:
	static EditorNode * algNodeFactoryDom(QDomElement &elem, const EditorObjectMetadata *objectMetaData, EditorDict *ed, QGraphicsItem * parent);
	static EditorNode * algNodeFactoryNew(const EditorNodeMetadata *metaData, int channel, QGraphicsItem * parent);
	EODummy(const EditorObjectMetadata *metaData);
	EODummy(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed);
	virtual ~EODummy() {}
	virtual EditorNodeFactoryNew getNodeFactory();
protected:
};

class EOInfoPanel : public EditorObject
{
	Q_OBJECT
public:
	EOInfoPanel(const EditorObjectMetadata *metaData);
	EOInfoPanel(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed);
	virtual ~EOInfoPanel() {}
	virtual bool canDeleteNode(int channel, const QString & nodeTypeName) override;
	virtual bool canChangeChannel(int oldChannel, int newChannel) override {return oldChannel==newChannel;} // pin channels
protected:
	virtual bool contextMenuAddNodes(QMenu *menu) override; // return=true indicates that all node option are done and EditorObject shall not add default "node add" actions
	//virtual void updateInfoStr() override;
	virtual QStringList checkConsistency() override;
protected slots:
	void addNodeExtSl(QString nodeTypeName, bool reindexAndUpdate = true);
protected:
};

class EOTimer : public EditorObject
{
	Q_OBJECT
public:
	EOTimer(const EditorObjectMetadata *metaData);
	EOTimer(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed);
	virtual ~EOTimer() {}
	virtual bool canDeleteNode(int channel, const QString & nodeTypeName) override;
	// QGraphicsItem
	//virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	//virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) Q_DECL_OVERRIDE;
	//
protected:
	//virtual void setupGraphic();
	virtual bool contextMenuAddNodes(QMenu *menu) override; // return=true indicates that all node option are done and EditorObject shall not add default "node add" actions
	virtual void updateInfoStr() override;
	virtual QStringList checkConsistency() override;
	//virtual bool hasExtendedParams() {return true;}
	//virtual bool updateExtendedParams(); // false = no change
	virtual bool hasExtendedEditor() override {return true;}
	virtual bool updateExtendedEditor(QWidget *parent, KeyValMV &currentKeyVals) override; // false = no change
	void updateResetNode();
};
} // namespace pi_at_home

#endif // EDITOROBJECTDERIVED_H
