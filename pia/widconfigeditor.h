#ifndef WIDCONFIGEDITOR_H
#define WIDCONFIGEDITOR_H

#include "piadh.h"
//#include <list>
#include <QWidget>
#include <QDomElement>
#include <QGraphicsScene>
#include <QIcon>
#include "editordict.h"
#include "editorobjectmetadata.h"
#include "editorobject.h"
#include "editorconnection.h"
#include "editorscene.h"
#include "../piah_common/commandtarget.h"

Q_DECLARE_LOGGING_CATEGORY(widconfigeditor)

namespace Ui {
class WidConfigEditor;
}

namespace pi_at_home
{

class WidConfigEditor : public QWidget, public EditorDict
{
	Q_OBJECT
public:
	struct TempCon
	{
		TempCon() : deviceId(-1), channel(-1), startPos(QPointF()) {}
		TempCon(int deviceId, int channel, const QPointF &startScenePos) : deviceId(deviceId), channel(channel), startPos(startScenePos) {}
		bool isActive() const {return deviceId>-1 && channel>-1;}
		int deviceId=-1, channel=-1;
		QPointF startPos;
	};
	explicit WidConfigEditor(QWidget *parent = 0);
	~WidConfigEditor();
	bool canChangeId(int oldId, int newId);
	const TempCon & getTempCon() const {return tempCon;}
	// EditorDict
	virtual EditorNode *getNode(const QString & alias) override;
	virtual EditorNode *getNode(int device, int channel) override;
	virtual NodeAddress getNodeAddress(const QString & alias) override;
	virtual EditorConnectionVP getConnection(int deviceId, int channel, EditorConnection::EndPoint endPointType=EditorConnection::EndPoint::ANY) override;
	virtual EditorConnection *getConnection(int srcId, int srcChannel, int destId, int destChannel) override;
	virtual void pendingConnection(EditorConnection::ConnectorInfo eci) override;
public slots:
	void deleteObjSl(int id);
	void deleteConSl(EditorConnection::ConKey conKey);
	void touchSl();
private slots:
	void fillCombo();
	//void on_radioAlgs_clicked(bool checked);
	//void on_radioConfigs_clicked(bool checked);
	void on_comboObject_currentIndexChanged(int index);
	void on_toolReload_clicked();
	void on_pushBaseConfig_clicked();
	void on_pushSave_clicked();
	void on_pushSaveAs_clicked();
	void saveCommon(const QString &filename, const QString &shortname);
	void parseSection(QDomElement & elem, const QString &elemStr);
	void addObjectSl(const QString &objTypeName);
	void idChangedSl(int oldId, int newId);
	void connectionStartSl(int device, int channel, const QPointF & scPos);
	void connectionCancelSl(int device, int channel);
	void connectionCancelAct();
private:
	void insertNewEditorObject(EditorObject * eo);
	void insertNewEditorConnection(EditorConnection *ec);
	void markAliveInd(EditorNode *oldInd);
	QString createCurrentFilenme(QString *shortname = nullptr);
	Ui::WidConfigEditor *ui;
	QAction *actConnectionCancel;	// scene::rubberBandCancelSig -sig> act::trigger
									// node:: -call> object::connectionCancelInfo;object::connectionCancelSig -sig> editor::connectionCancelSl -call> act::trigger
									// act::triggered -> scene::rubberBandCancelSl
									// act::triggered -> editor::connectionCancelAct;[select object] -call> object::connectionCancelAct;[all] -call> node::connectorCancel
	EditorScene *scene;
	QDomDocument doc;
	QDomElement domTiming, domStaticHosts, domHardware, domAlgorithms, domConnections;
	EditorObjectMetadataMSP objMetadatas;
	EditorObjectMISP editorObjects;
	EditorConnectionMKSP editorConnections;
	EConInfoVV pendingConnections;
	EditorBaseConfig baseConfig;
	QString currentFilename;
	TempCon tempCon;
signals:
};
}

#endif // WIDCONFIGEDITOR_H
