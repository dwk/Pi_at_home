#include "dlgdeploy.h"
#include "piadi.h"
#include <QFileDialog>
#include <QDir>
#include <QHostAddress>
#include <QCheckBox>
//#include <QSignalMapper>
#include "ui_dlgdeploy.h"

DlgDeploy::DlgDeploy(QWidget *parent, QString serverUid) :
	QDialog(parent), ui(new Ui::DlgDeploy), serverUid(serverUid), deployer(new QProcess(this)) //, mapper(new QSignalMapper(this))
{
	ui->setupUi(this);
	QSettings settings;
	ui->lineBinary->setText(settings.value("deploy/binary").toString());
	ui->lineConfigDir->setText(settings.value("deploy/configdir").toString());
	QDialog::accept();
	connect(ui->lineBinary, SIGNAL(editingFinished()), this, SLOT(updateBinary()));
	connect(ui->toolBinary, SIGNAL(clicked(bool)), this, SLOT(toolBinarySl()));
	connect(ui->lineConfigDir, SIGNAL(editingFinished()), this, SLOT(updateConfigDir()));
	connect(ui->toolConfigDir, SIGNAL(clicked(bool)), this, SLOT(toolConfigDirSl()));
	connect(deployer, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(finishedSl(int,QProcess::ExitStatus)));
	//connect(deployer, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(procStateChangedSl(QProcess::ProcessState)));

	HostDict & hd=DCON.hostDict();
	const HostInstanceMV & his=hd.hostInstances();
	ui->tableHosts->setColumnCount(5);
	ui->tableHosts->setRowCount(his.size());
	int row=0;
	for(auto mit=his.begin(); mit!=his.end(); ++mit)
	{
		bool useIt=(serverUid.isEmpty()?true:(serverUid==mit->first));
		QString ipv4c=QHostAddress(mit->second.hadr).toString(); //+":"+QString::number(mit->second.port);
		ui->tableHosts->setItem(row, 0, new QTableWidgetItem(mit->first));
		ui->tableHosts->setItem(row, 1, new QTableWidgetItem(ipv4c));
		QTableWidgetItem *checkBin = new QTableWidgetItem();
		checkBin->setCheckState(useIt?Qt::Checked:Qt::Unchecked);
		//mapper->setMapping(checkBin, mit->first);
		//connect(checkBin, SIGNAL(clicked()), mapper, SLOT(map()));
		ui->tableHosts->setItem(row, 2, checkBin);
		checkBin = new QTableWidgetItem();
		checkBin->setCheckState(useIt?Qt::Checked:Qt::Unchecked);
		ui->tableHosts->setItem(row, 3, checkBin);
		checkBin = new QTableWidgetItem();
		checkBin->setCheckState(useIt?Qt::Checked:Qt::Unchecked);
		ui->tableHosts->setItem(row, 4, checkBin);
		++row;
	}
	ui->tableHosts->resizeColumnsToContents();
	ui->tableHosts->resizeRowsToContents();

	updateBinary();
	updateConfigDir();
}

DlgDeploy::~DlgDeploy() {delete ui;}

void DlgDeploy::accept()
{
	QSettings settings;
	settings.setValue("deploy/binary", ui->lineBinary->text());
	settings.setValue("deploy/configdir", ui->lineConfigDir->text());
	QDialog::accept();
}

void DlgDeploy::timerEvent(QTimerEvent *event)
{
	mtx.lock();
	if(event->timerId()==deployerTimeoutId)
	{
		killTimer(event->timerId());
		deployerTimeoutId=0;
		ui->listLog->addItem(new QListWidgetItem(QString("  ERROR (timeout): %1").arg(deployCmdFull)));
		deployStep=99;
		mtx.unlock();
		finishedSl(0, QProcess::NormalExit);
	}
	else
	{
		mtx.unlock();
		QDialog::timerEvent(event); // not sure who started it..
	}
}

void DlgDeploy::toolBinarySl()
{
	QString binary = QFileDialog::getOpenFileName(this, tr("Set Binary"), ui->lineBinary->text());
	if(binary.isEmpty())
		return;
	ui->lineBinary->setText(binary);
	ui->listLog->clear();
	updateBinary();
}

void DlgDeploy::toolConfigDirSl()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Set Config Directory"), ui->lineConfigDir->text(),
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if(dir.isEmpty())
		return;
	ui->lineConfigDir->setText(dir);
	ui->listLog->clear();
	updateConfigDir();
}

void DlgDeploy::updateBinary()
{
	QString binary=ui->lineBinary->text();
	QString binType, binTst;
	QStringList msgs;
	bool ebin=DCON.readPiahdInfo(binary, binType, binTst, &msgs);
	ui->listLog->addItems(msgs);
	auto fn=ui->tableHosts->item(0, 2)->flags();
	for(int row=0; row<ui->tableHosts->rowCount(); ++row)
	{
		bool useIt=(ebin && (serverUid.isEmpty() || serverUid==ui->tableHosts->item(row, 0)->data(Qt::DisplayRole)));
		ui->tableHosts->item(row, 2)->setCheckState(useIt?Qt::Checked:Qt::Unchecked);
		ui->tableHosts->item(row, 2)->setFlags(ebin?(fn|Qt::ItemIsEnabled):fn&~Qt::ItemIsEnabled);
	}
}

void DlgDeploy::updateConfigDir()
{
	bool ebin=true;
	QDir dir(ui->lineConfigDir->text());
	if(!dir.exists())
	{
		ui->listLog->addItem(new QListWidgetItem(QString("ERROR: config directory does not exist")));
		ebin=false;
	}
	QStringList files=dir.entryList(QStringList()<<"config_*.xml");
	if(ebin)
	{
		if(!files.size())
		{
			ui->listLog->addItem(new QListWidgetItem(QString("ERROR: config directory contains no config files (config_*.xml)")));
			ebin=false;
		}
	}
	if(ebin)
	{
		//ui->listLog->addItem(new QListWidgetItem(files.join('|')));
		QString templ("config_%1.xml"), templraw("config_%1Raw.xml");
		int ccount=0;
		for(int row=0; row<ui->tableHosts->rowCount(); ++row)
		{
			QString uid=ui->tableHosts->item(row, 0)->text();
			bool foundStd=false, foundRaw=false;
			foreach(QString file, files)
			{
				if(file==templ.arg(uid))
				{
					foundStd=true;
					++ccount;
				}
				if(file==templraw.arg(uid))
				{
					foundRaw=true;
					++ccount;
				}
				if(foundRaw && foundStd)
					break;
			}
			//qDebug()<<templ.arg(uid)<<foundStd<<templraw.arg(uid)<<foundRaw;
			bool useIt=(serverUid.isEmpty() || serverUid==ui->tableHosts->item(row, 0)->data(Qt::DisplayRole));
			ui->tableHosts->item(row, 3)->setCheckState((foundStd && useIt)?Qt::Checked:Qt::Unchecked);
			auto fn=ui->tableHosts->item(row, 3)->flags();
			ui->tableHosts->item(row, 3)->setFlags(foundStd?(fn|Qt::ItemIsEnabled):fn&~Qt::ItemIsEnabled);
			ui->tableHosts->item(row, 4)->setCheckState((foundRaw && useIt)?Qt::Checked:Qt::Unchecked);
			fn=ui->tableHosts->item(row, 4)->flags();
			ui->tableHosts->item(row, 4)->setFlags(foundRaw?(fn|Qt::ItemIsEnabled):fn&~Qt::ItemIsEnabled);
		}
		if(ccount)
			ui->listLog->addItem(new QListWidgetItem(QString("OK: found %1 config files.").arg(ccount)));
		else
			ui->listLog->addItem(new QListWidgetItem(QString("WARNING: found no relevant config files.").arg(ccount)));
	}
}

void DlgDeploy::on_pushDeploy_clicked()
{
	if(QMessageBox::question(this, tr("Restart Warning"), tr("Deployment will restart the piah daemon on every system checked in the deployment list, even for config updates. Proceed?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes)==QMessageBox::No)
		return;
	ui->pushClose->setEnabled(false);
	ui->pushDeploy->setEnabled(false);
	ui->pushNone->setEnabled(false);
	ui->pushAll->setEnabled(false);
	ui->listLog->clear();
	deployRow=-1;
	deployStep=99;
	deployError=false;
	finishedSl(0, QProcess::NormalExit);
}

void DlgDeploy::on_pushNone_clicked()
{
	for(int row=0; row<ui->tableHosts->rowCount(); ++row)
	{
		ui->tableHosts->item(row, 2)->setCheckState(Qt::Unchecked);
		ui->tableHosts->item(row, 3)->setCheckState(Qt::Unchecked);
		ui->tableHosts->item(row, 4)->setCheckState(Qt::Unchecked);
	}
}

void DlgDeploy::on_pushAll_clicked()
{
	for(int row=0; row<ui->tableHosts->rowCount(); ++row)
	{
		ui->tableHosts->item(row, 2)->setCheckState(Qt::Checked);
		if(ui->tableHosts->item(row, 3)->flags()&Qt::ItemIsEnabled)
			ui->tableHosts->item(row, 3)->setCheckState(Qt::Checked);
		if(ui->tableHosts->item(row, 4)->flags()&Qt::ItemIsEnabled)
			ui->tableHosts->item(row, 4)->setCheckState(Qt::Checked);
	}
}

void DlgDeploy::finishedSl(int exitCode, QProcess::ExitStatus exitStatus)
{
	//qDebug()<<"start deployStep"<<deployStep;
	{
		QMutexLocker mlock(&mtx);
		if(deployerTimeoutId)
		{
			killTimer(deployerTimeoutId);
			deployerTimeoutId=0;
		}
	}
	if( sender() && (exitCode || exitStatus!=QProcess::NormalExit) )
	{ // error handling when called as slot
		QMutexLocker mlock(&mtx);
		if(ui->checkSystemcltIgnore->isChecked() && (deployStep==1 || deployStep==6 || deployStep==7) )
		{
			ui->listLog->addItem(new QListWidgetItem(QString("  IGNORED ERROR: ""%1"" returned %3, QProcess status %4").arg(deployCmdFull).arg(exitCode).arg((int)exitStatus)));
			ui->listLog->addItem(new QListWidgetItem(QString("  DEBUG: ")+deployer->readAll()));
		}
		else
		{
			ui->listLog->addItem(new QListWidgetItem(QString("  ERROR: ""%1"" returned %3, QProcess status %4").arg(deployCmdFull).arg(exitCode).arg((int)exitStatus)));
			ui->listLog->addItem(new QListWidgetItem(QString("  DEBUG: ")+deployer->readAll()));
			deployStep=99;
			deployError=true;
		}
	}
	switch(deployStep)
	{
		case 1: // copy binary
			++deployStep;
			if(deployBin)
			{
				ui->listLog->addItem(new QListWidgetItem(QString("  transfering executable...")));
				QStringList args;
				args<<ui->lineBinary->text()<<QString("pi@%1:/home/pi/deploy/piahd").arg(deployAdr);
				execCmd("scp", args, 20000);
				break; // 1
			}
			/* FALLTHRU */
		case 2: // copy config
			++deployStep;
			if(deployConfig)
			{
				ui->listLog->addItem(new QListWidgetItem(QString("  transfering config...")));
				QString cfgname=QString("%2config_%1.xml").arg(ui->tableHosts->item(deployRow, 0)->text());
				QStringList args;
				args<<cfgname.arg(ui->lineConfigDir->text()+"/")<<QString("pi@%1:/home/pi/deploy/%2").arg(deployAdr, cfgname.arg(""));
				execCmd("scp", args, 20000);
				break; // 2
			}
			/* FALLTHRU */
		case 3: // copy raw config
			++deployStep;
			if(deployConfigRaw)
			{
				ui->listLog->addItem(new QListWidgetItem(QString("  transfering raw config...")));
				QString cfgname=QString("%2config_%1Raw.xml").arg(ui->tableHosts->item(deployRow, 0)->text());
				QStringList args;
				args<<cfgname.arg(ui->lineConfigDir->text()+"/")<<QString("pi@%1:/home/pi/deploy/%2").arg(deployAdr, cfgname.arg(""));
				execCmd("scp", args, 5000);
				break; // 3
			}
			/* FALLTHRU */
		case 4: // move config
			++deployStep;
			if(deployConfig || deployConfigRaw)
			{
				bool useRaw = deployConfigRaw && !deployConfig;
				const QString cfg=QString("config_%1%2.xml").arg(ui->tableHosts->item(deployRow, 0)->text(), useRaw?"Raw":"");
				ui->listLog->addItem(new QListWidgetItem(QString("  activating %1...").arg(cfg)));
				QString cpcmd="cp ~/deploy/%1 ~/deploy/config.xml";
				execCmd("ssh", QStringList()<<"-lpi"<<deployAdr<<cpcmd.arg(cfg), 5000);
				break; // 4
			}
			/* FALLTHRU */
		case 5: // restart service
			++deployStep;
			if(ui->checkNoStart->isChecked())
				ui->listLog->addItem(new QListWidgetItem(tr("  starting disabled...")));
			else
			{
				ui->listLog->addItem(new QListWidgetItem(tr("  starting piahd...")));
				execCmd("ssh", QStringList()<<"-lroot"<<deployAdr<<"systemctl start piahd");
				break; // 5
			}
			/* FALLTHRU */
		case 6: // verify service
			++deployStep;
			if(ui->checkNoStart->isChecked())
				ui->listLog->addItem(new QListWidgetItem(tr("  check skipped...")));
			else
			{
				execCmd("ssh", QStringList()<<"-lroot"<<deployAdr<<"systemctl is-active piahd");
				break; // 6
			}
			/* FALLTHRU */
		case 7: // verify service output
		{
			deployStep=99;
			QByteArray res=deployer->readAll();
			if(res.startsWith("active"))
				ui->listLog->addItem(new QListWidgetItem(QString("  deployment successfully completed")));
			else
			{
				ui->listLog->addItem(new QListWidgetItem(QString("  WARNING: service not confirmed: ")+QString(res)));
				deployError=true;
			}
		}
		/* FALLTHRU */
		case 99: // increment, setup and stop service
		default:
		{
			bool loop=false;
			do
			{
				loop=false;
				++deployRow;
				deployStep=0;
				if(deployRow>=ui->tableHosts->rowCount())
				{ // we are done!
					if(deployError)
						ui->listLog->addItem(new QListWidgetItem(QString("Finished WITH ERRORs or WARNINGs!")));
					else
						ui->listLog->addItem(new QListWidgetItem(QString("Successfully finished.")));
					ui->listLog->scrollToBottom();
					deployRow=-1;
					deployStep=-1;
					deployError=false;
					ui->pushClose->setEnabled(true);
					ui->pushDeploy->setEnabled(true);
					ui->pushNone->setEnabled(true);
					ui->pushAll->setEnabled(true);
					return;
				}
				deployAdr=ui->tableHosts->item(deployRow, 1)->text();
				ui->listLog->addItem(new QListWidgetItem(ui->tableHosts->item(deployRow, 0)->text()+" / "+deployAdr));
				deployBin=(ui->tableHosts->item(deployRow, 2)->checkState()==Qt::Checked);
				deployConfig=(ui->tableHosts->item(deployRow, 3)->checkState()==Qt::Checked);
				deployConfigRaw=(ui->tableHosts->item(deployRow, 4)->checkState()==Qt::Checked);
				//qDebug()<<ui->tableHosts->item(row, 0)->text()<<deploy<<config<<configraw;
				if(!deployBin && !deployConfig && !deployConfigRaw)
				{
					ui->listLog->addItem(new QListWidgetItem(QString("  nothing to do.")));
					loop=true;
				}
			} while(loop) ;
			deployStep=1;
			ui->listLog->addItem(new QListWidgetItem(QString("  stopping piahd... (wait for safe state)")));
			execCmd("ssh", QStringList()<<"-lroot"<<deployAdr<<"systemctl stop piahd", 20000);
			break; // 99 / default
		}
	} // switch
	ui->listLog->scrollToBottom();
	//qDebug()<<"exit deployStep"<<deployStep;
}

void DlgDeploy::procStateChangedSl(QProcess::ProcessState newstate)
{
	qDebug()<<"procStateChanged"<<(int)newstate;
}

void DlgDeploy::execCmd(const QString & cmd, const QStringList &args, int timeout)
{
	//qDebug()<<"system cmd"<<timeout<<cmd<<args.join(' ');
	deployerTimeoutId=startTimer(timeout);
	deployCmdFull=cmd+args.join(' ');
	deployer->start(cmd, args);
}
