#ifndef EDITOROBJECTMETADATA_H
#define EDITOROBJECTMETADATA_H

#include "piadh.h"
#include <QDomElement>
#include <QColor>
#include <QIcon>
#include <QVariant>

namespace pi_at_home
{
class EditorBaseConfig
{
public:
	void reset();
	typedef std::vector<QStringList> StaticHosts;
	StaticHosts staticHosts; // <static_hosts><host uid="Pi3Kammerl" ip="10.0.1.11" port="80" /></static_hosts>
	int broadcastMs=500, heartbeatMs=1000;
	QString uid, aliveInd;
	bool rawConfig=false;
};

class EditorParamMetadata
{
public:
	typedef std::vector<QVariant> FilterVals;
	enum class Type {REQUIRED=0, DEFAULTVALUE, OMITABLE};
	enum class DataType {UNDEF=0, BOOL, INT, DOUBLE, STRING};
	enum class DataFilter {NONE=0, LIST, MINMAX};
	EditorParamMetadata() : dataType(DataType::UNDEF), dataFilter(DataFilter::NONE), description("Undefined Parameter") {}
	EditorParamMetadata(const QDomElement &elem);
	Type type;
	DataType dataType;
	DataFilter dataFilter;
	QVariant def;
	FilterVals filterVals;
	QString name, description;
	QVariant str2Var(const QString & str);
};
typedef std::map<QString, EditorParamMetadata *> EditorParamMetadataMSP;

class EditorNodeMetadata
{
public:
	enum class Type {UNDEF=0, INPUT, OUTPUT, BIDIR, CONNECTOR};
	enum class DataType {UNDEF=0, BOOL, INT, DOUBLE, STRING, ANY};
	EditorNodeMetadata() : type(Type::UNDEF), dataType(DataType::UNDEF), description("Undefined Node") {}
	EditorNodeMetadata(const QDomElement &elem);
	~EditorNodeMetadata();
	Type type;
	DataType dataType;
	QString typeName, description;
	int baseChannel=0, nodeMin=1, nodeMax=1;
	bool fixed=false;
	EditorParamMetadataMSP editorParamMetadatas;
};
typedef std::map<QString, EditorNodeMetadata *> EditorNodeMetadataMSP;

class EditorObjectMetadata
{
public:
	enum class Category {UNDEF=0, DEVICE, ALGORITHM};
	static QString categoryToStr(Category cat);
	static Category categoryFromStr(const QString & catStr);
	EditorObjectMetadata() : typeName("<templ>"), description("Undefined Object"), icon(":/res/ConfigEditor/icons8-puzzled-48.png"), color(200,100,100) {}
	EditorObjectMetadata(const QDomElement &elem);
	~EditorObjectMetadata();
	//int typeId;
	QString typeName, description;
	QIcon icon;
	QColor color;
	Category category=Category::UNDEF;
	EditorNodeMetadataMSP editorNodeMetadatas;
	EditorParamMetadataMSP editorParamMetadatas;
};
typedef std::map<QString, EditorObjectMetadata *> EditorObjectMetadataMSP;

}

#endif // EDITOROBJECTMETADATA_H
