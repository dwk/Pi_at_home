#include "avnodetadlbus.h"
#include "avnodetadlbus_fac.h"
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QDateTime>
#include <QDateTimeEdit>
#include "ui_widnode.h"
#include "piadi.h"

AvNodeTaDlBusSensorT::AvNodeTaDlBusSensorT(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDoubleSpinBox(this))
{
	defval_=NodeValue((double)0.);
	ui->labelState->setToolTip("TA-DlBusTemp");
	deserialize(fragment.attribute("val"));
	widVal->setValue(value_.toDouble());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(double)), this, SLOT(valChanged(double)));
}

void AvNodeTaDlBusSensorT::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toDouble());
	widVal->blockSignals(false);
}

void AvNodeTaDlBusSensorT::valChanged(double val)
{
	value_=val;
	nodeValueChanged();
}

AvNodeTaDlBusRpm::AvNodeTaDlBusRpm(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QSpinBox(this))
{
	defval_=NodeValue((int)0);
	ui->labelState->setToolTip("TA-DlBusRpm");
	deserialize(fragment.attribute("val"));
	widVal->setValue(value_.toInt());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(int)), this, SLOT(valChanged(int)));
}

void AvNodeTaDlBusRpm::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toInt());
	widVal->blockSignals(false);
}

void AvNodeTaDlBusRpm::valChanged(int val)
{
	value_=val;
	nodeValueChanged();
}

AvNodeTaDlBusTst::AvNodeTaDlBusTst(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDateTimeEdit(this))
{
	defval_=NodeValue(QDateTime());
	ui->labelState->setToolTip("TA-DlBusTst");
	deserialize(fragment.attribute("val"));
	widVal->setDateTime(value_.toDateTime());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(valChanged(QDateTime)));
}

void AvNodeTaDlBusTst::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setDateTime(value_.toDateTime());
	widVal->blockSignals(false);
}

void AvNodeTaDlBusTst::valChanged(QDateTime val)
{
	value_=val;
	nodeValueChanged();
}
