#include "widconfigeditor.h"
#include "widconfigeditor_fac.h"
#include <QDomDocument>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QGraphicsTextItem>
#include "../piah_common/command.h"
#include "dlgeditbaseconfig.h"
#include "ui_widconfigeditor.h"
#include "piadi.h"

Q_LOGGING_CATEGORY(widconfigeditor, "widconfigeditor")

WidConfigEditor::WidConfigEditor(QWidget *parent) :
	QWidget(parent), ui(new ::Ui::WidConfigEditor), actConnectionCancel(new QAction(tr("Cancel Connection"), this)),
	scene(new EditorScene(this))
{
	//qRegisterMetaType<pi_at_home::FileTransferJob>();
	//qRegisterMetaType<QList<pi_at_home::FileTransferJob>>();
	ui->setupUi(this);
	ui->graphicsView->setScene(scene);
	connect(scene, SIGNAL(rubberBandCancelSig()), actConnectionCancel, SLOT(trigger()));
	connect(actConnectionCancel, SIGNAL(triggered(bool)), scene, SLOT(rubberBandCancelSl()));
	connect(actConnectionCancel, SIGNAL(triggered(bool)), this, SLOT(connectionCancelAct()));
	EditorObject::pwid=ui->graphicsView;
	fillCombo();
	QSettings settings;
	QDir dir(settings.value("deploy/configdir").toString());
	QString mrk=dir.absoluteFilePath("ConfigEditorMarkup.xml");
	QFile file(mrk);
	if(!file.open(QIODevice::ReadOnly))
	{
		qCritical()<<" cannot open editor config file"<<mrk;
		//throw Ex("XML: no file");
		return;
	}
	QDomDocument doc;
	QString errStr;
	int errLine=0;
	if (!doc.setContent(&file, &errStr, &errLine))
	{
		qCritical()<<"editor config file invalid in line"<<errLine<<errStr;
		return;
	}
	QDomNodeList objs=doc.elementsByTagName("objects");
	if(objs.size()==1)
	{
		delete ui->toolButton;
		QDomElement el=objs.item(0).toElement();
		QDomNodeList obj=el.elementsByTagName("object");
		for(int i=0; i<obj.size(); ++i)
		{
			QDomElement elem=obj.item(i).toElement();
			EditorObjectMetadata *eomd=new EditorObjectMetadata(elem);
			objMetadatas.insert(EditorObjectMetadataMSP::value_type(eomd->typeName, eomd));
			QToolButton *but=new QToolButton(this);
			//int typeId=objTypeIds.size();
			//objTypeIds.push_back(typeStr);
			but->setIcon(eomd->icon.isNull()?QIcon(":/res/ConfigEditor/icons8-puzzled-48.png"):eomd->icon);
			but->setIconSize(QSize(28,28));
			but->setToolTip(eomd->typeName+": "+eomd->description);
			ui->layoutToolbar->addWidget(but);
			connect(but, &QAbstractButton::clicked, this, [this, eomd](){addObjectSl(eomd->typeName);});
			qCDebug(widconfigeditor)<<"creating tool button"<<elem.nodeName()<<elem.attribute("name");
		}
	}
	else
	{
		qCritical()<<"ConfigEditorMarkup has none or more than objects tag";
	}
}

WidConfigEditor::~WidConfigEditor()
{
	for(auto eit=editorObjects.begin(); eit!=editorObjects.end(); ++eit)
		delete eit->second;
	delete ui;
}

bool WidConfigEditor::canChangeId(int oldId, int newId)
{
	for(auto eit=editorObjects.begin(); eit!=editorObjects.end(); ++eit)
	{
		if(eit->first==oldId)
			continue;
		if(eit->first==newId)
		{
			qCDebug(widconfigeditor)<<"WidConfigEditor::canChangeId FAIL"<<oldId<<newId;
			return false;
		}
	}
	qCDebug(widconfigeditor)<<"WidConfigEditor::canChangeId OK"<<oldId<<newId;
	return true;
}

EditorNode *WidConfigEditor::getNode(const QString &alias)
{
	NodeAddress na(alias, false);
	if(na.isValid())
	{
		if(na.uid().size() && na.uid()!=baseConfig.uid)
			QMessageBox::warning(this, tr("Load Problem"), tr("Imported node refers to UID %1 although this config has UID %2").arg(na.uid(), baseConfig.uid));
		return getNode(na.device(), na.channel());
	}
	else
	{
		for(auto oit=editorObjects.begin(); oit!=editorObjects.end(); ++oit)
		{
			EditorObject *eo=oit->second;
			if(!eo)
				continue;
			EditorNode *en=eo->getNode(alias);
			if(en)
				return en;
		}
		return nullptr;
	}
}

EditorNode *WidConfigEditor::getNode(int device, int channel)
{
	auto oit=editorObjects.find(device);
	if(oit==editorObjects.end())
		return nullptr;
	EditorObject *eo=oit->second;
	return eo?eo->getNode(channel):nullptr;
}

pi_at_home::NodeAddress WidConfigEditor::getNodeAddress(const QString &alias)
{
	NodeAddress na(alias, false);
	if(na.isValid())
		return na;
	else
	{
		for(auto oit=editorObjects.begin(); oit!=editorObjects.end(); ++oit)
		{
			EditorObject *eo=oit->second;
			if(!eo)
				continue;
			EditorNode *en=eo->getNode(alias);
			if(en)
				return NodeAddress(baseConfig.uid, eo->getId(), en->getChannel());
		}
		return NodeAddress();
	}
}

EditorConnectionVP WidConfigEditor::getConnection(int deviceId, int channel, pi_at_home::EditorConnection::EndPoint endPointType)
{
	EditorConnectionVP res;
	for(auto it=editorConnections.begin(); it!=editorConnections.end(); ++it)
	{
		if( (endPointType==EditorConnection::EndPoint::ANY && ((it->first.srcId==deviceId && it->first.srcCh==channel) || (it->first.destId==deviceId && it->first.destCh==channel)) ) ||
			(endPointType==EditorConnection::EndPoint::SRC && it->first.srcId==deviceId && it->first.srcCh==channel ) ||
			(endPointType==EditorConnection::EndPoint::DEST && it->first.destId==deviceId && it->first.destCh==channel) )
			res.push_back(it->second);
	}
	return res;
}

EditorConnection *WidConfigEditor::getConnection(int srcId, int srcChannel, int destId, int destChannel)
{
	EditorConnection::ConKey ck1(srcId, srcChannel, destId, destChannel);
	auto it1=editorConnections.find(ck1);
	if(it1!=editorConnections.end())
		return it1->second;
	return nullptr;
}

void WidConfigEditor::pendingConnection(pi_at_home::EditorConnection::ConnectorInfo eci)
{
	pendingConnections.push_back(eci);
}

void WidConfigEditor::deleteObjSl(int id)
{
	auto it=editorObjects.find(id);
	if(it==editorObjects.end())
		return;
	delete it->second;
	editorObjects.erase(it);
	touchSl();
}

void WidConfigEditor::deleteConSl(EditorConnection::ConKey conKey)
{
	auto it=editorConnections.find(conKey);
	if(it==editorConnections.end())
		return;
	//qDebug()<<"WidConfigEditor::deleteConSl"<<it->second->getName();
	delete it->second;
	editorConnections.erase(it);
	touchSl();
}

void WidConfigEditor::fillCombo()
{
	ui->comboObject->clear();
	ui->comboObject->addItem(tr("<new file>"));
	ui->comboObject->addItem(tr("<non-standard file>"));
	QSettings settings;
	QDir dir(settings.value("deploy/configdir").toString());
	QStringList files=dir.entryList(QStringList()<<"config_*.xml");
	foreach(QString f, files)
	{
		ui->comboObject->addItem(f);
	}
}

void WidConfigEditor::on_comboObject_currentIndexChanged(int index)
{
	qCDebug(editorconnection)<<"on_comboObject_currentIndexChanged"<<index;
	if(ui->pushSave->isEnabled())
	{
		if(QMessageBox::warning(this, tr("Config Save Issue"),
				tr("There are unsaved changed to %1. Discard changes and proceed?").arg(currentFilename),
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
		{
			QString cfsn;
			createCurrentFilenme(&cfsn);
			ui->comboObject->blockSignals(true);
			ui->comboObject->setCurrentText(cfsn);
			ui->comboObject->blockSignals(false);
			return;
		}
	}
	scene->clear();
	editorObjects.clear();
	EditorObject::resetIdGen();
	editorConnections.clear();
	QSettings settings;
	QString selName=ui->comboObject->itemText(index);
	if(selName=="<non-standard file>")
	{
		QSettings settings;
		currentFilename=QFileDialog::getOpenFileName(this, tr("Config Filename"),
			settings.value("deploy/configdir").toString(), "*.xml");
	}
	else
	{
		QDir dir(settings.value("deploy/configdir").toString());
		currentFilename=dir.absoluteFilePath(selName);
	}
	QFile file(currentFilename);
	QPolygonF p;
	p<<QPointF(0.,-8.)<<QPointF(6.,2.)<<QPointF(-6.,2.)<<QPointF(0.,-8.);
	scene->addItem(new QGraphicsPolygonItem(p));
	if(!file.open(QIODevice::ReadOnly))
	{
		qInfo()<<"new config file";
		ui->pushSave->setEnabled(false);
		currentFilename.clear();
		baseConfig.reset();
		doc.clear();
		doc.appendChild(doc.createProcessingInstruction("xml", QString("version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"")));
		QDomElement root=doc.createElement("config");
		root.setAttribute(QStringLiteral("flavor"), QStringLiteral("normal"));
		doc.appendChild(root);
		domStaticHosts=doc.createElement("static_hosts");
		root.appendChild(domStaticHosts);
		domTiming=doc.createElement("timing");
		root.appendChild(domTiming);
		domHardware=doc.createElement("hardware");
		root.appendChild(domHardware);
		domAlgorithms=doc.createElement("algorithms");
		root.appendChild(domAlgorithms);
		domConnections=doc.createElement("connections");
		root.appendChild(domConnections);
		return;
	}
	QString errStr;
	int errLine=0;
	if (!doc.setContent(&file, &errStr, &errLine))
	{
		qCritical()<<"config file invalid in line"<<errLine<<errStr;
		return;
	}
	qInfo()<<"parsing"<<currentFilename;
	pendingConnections.clear();
	int dotpos=currentFilename.lastIndexOf('.');
	if(dotpos>2 && currentFilename.mid(dotpos-3,3)=="Raw")
		baseConfig.rawConfig=true;
	else
		baseConfig.rawConfig=false;
	baseConfig.staticHosts.clear();
	QDomElement rootElem=doc.documentElement();
	for(QDomNode n = rootElem.firstChild(); !n.isNull(); n = n.nextSibling())
	{
		/*if(n.isComment())
			qCDebug(widconfigeditor)<<"COMMENT"<<n.toComment().data();
		if(n.isText())
			qCDebug(widconfigeditor)<<"TEXT"<<n.toText().data();*/
		QDomElement el=n.toElement();
		if(n.nodeName()=="static_hosts")
		{
			domStaticHosts=el;
			for(QDomNode nh = el.firstChild(); !nh.isNull(); nh = nh.nextSibling())
			{
				if(nh.nodeName()!="host")
					continue;
				QDomElement elh=nh.toElement();
				QStringList ha;
				ha<<elh.attribute("uid")<<elh.attribute("ip")<<elh.attribute("port");
				//qCDebug(widconfigeditor)<<"static_host"<<ha;
				baseConfig.staticHosts.push_back(ha);
			}
			continue;
		}
		if(n.nodeName()=="timing")
		{
			domTiming=el;
			int i=0;
			bool ok=false;
			if(el.hasAttribute("broadcast_ms"))
			{
				i=el.attribute("broadcast_ms").toInt(&ok);
				if(ok)
					baseConfig.broadcastMs=i;
				else
					qWarning()<<"broadcast_ms has non-integer value"<<el.attribute("broadcast_ms");
			}
			if(el.hasAttribute("heartbeat_ms"))
			{
				i=el.attribute("heartbeat_ms").toInt(&ok);
				if(ok)
					baseConfig.heartbeatMs=i;
				else
					qWarning()<<"fast_ms has non-integer value"<<el.attribute("heartbeat_ms");
			}
			if(el.hasAttribute("alive_ind"))
				baseConfig.aliveInd=el.attribute("alive_ind");
		}
		if(n.nodeName()=="hardware")
		{
			domHardware=el;
			baseConfig.uid=el.attribute("uid");
			// no! this would change the identity of the client NodeAddress::setDefaultUid(baseConfig.uid);
			parseSection(el, "device");
			continue;
		}
		if(n.nodeName()=="algorithms")
		{
			domAlgorithms=el;
			parseSection(el, "algorithm");
			continue;
		}
		if(n.nodeName()=="connections")
		{
			domConnections=el;
			parseSection(el, "con");
			continue;
		}
	}
	for(auto cit=pendingConnections.begin(); cit!=pendingConnections.end(); ++cit)
	{
		EditorConnection *ec=new EditorConnection(*cit, this);
		insertNewEditorConnection(ec);
	}
	pendingConnections.clear();
	markAliveInd(nullptr);
	ui->pushSave->setEnabled(false);
}

void WidConfigEditor::on_toolReload_clicked()
{
	fillCombo();
}

void WidConfigEditor::on_pushBaseConfig_clicked()
{
	QStringList ports;
	for(auto it=editorObjects.begin(); it!=editorObjects.end(); ++it)
	{
		auto nds=it->second->childItems();
		foreach(QGraphicsItem * nd, nds)
		{
			EditorNode *eond=dynamic_cast<EditorNode *>(nd);
			if(!eond || (eond->md->type!=EditorNodeMetadata::Type::INPUT && eond->md->type!=EditorNodeMetadata::Type::BIDIR) )
				continue;
			ports<<eond->getAlias();
		}
	}
	DlgEditBaseConfig dlg(this, baseConfig, ports);
	QString oalis=baseConfig.aliveInd;
	if(dlg.exec()==QDialog::Accepted)
	{
		touchSl();
		markAliveInd(getNode(oalis));
	}
}

void WidConfigEditor::on_pushSave_clicked()
{
	QString newShortname;
	QString newFilename=createCurrentFilenme(&newShortname);
	if(!currentFilename.isEmpty() && newFilename!=currentFilename)
	{
		if(QMessageBox::warning(this, tr("Config Save Issue"),
				tr("The base config changed so that you will save to %1 although you loaded %2. Proceed?").arg(newFilename, currentFilename),
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
			return;
		if(QFileInfo::exists(newFilename))
		{
			if(QMessageBox::warning(this, tr("Config Save Issue"),
					tr("This will overwrite the existing file %1. Proceed?").arg(newFilename),
					QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
				return;
		}
	}
	currentFilename=newFilename;
	qCDebug(widconfigeditor)<<"saving to"<<currentFilename;
	saveCommon(currentFilename, newShortname);
}

void WidConfigEditor::on_pushSaveAs_clicked()
{
	QSettings settings;
	QString specialFilename=QFileDialog::getSaveFileName(this, tr("Config Filename"),
		settings.value("deploy/specialdir").toString(), "*.xml");
	if(specialFilename.isEmpty())
		return;
	QFileInfo d(specialFilename);
	settings.setValue("deploy/specialdir", d.absolutePath());
	saveCommon(specialFilename, QString());
}

void WidConfigEditor::saveCommon(const QString &filename, const QString & shortname)
{
	QStringList issues;
	for(auto eoit=editorObjects.begin(); eoit!=editorObjects.end(); ++eoit)
	{
		issues<<eoit->second->checkConsistency();
	}
	for(auto coit=editorConnections.begin(); coit!=editorConnections.end(); ++coit)
	{
		issues<<coit->second->checkConsistency();
	}
	if(issues.size())
	{
		if(QMessageBox::warning(this, tr("Validation Problems"), tr("There are problems which may (will) affect startup or behaviour at runtime. Save anyway?\n%1").arg(issues.join("\n")),
							 QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
			return;
	}
	//
	QFile file(filename);
	if(!file.open(QIODevice::WriteOnly))
	{
		qCritical()<<"cannot open config file"<<filename;
		QMessageBox::critical(this, tr("Config Save Issue"),
				tr("Cannot open or create config file %1.").arg(filename), QMessageBox::Ok);
		return;
	}
	doc.documentElement().setAttribute(QStringLiteral("flavor"), baseConfig.rawConfig?QStringLiteral("raw"):QStringLiteral("normal"));
	doc.documentElement().setAttribute(QStringLiteral("savetst"), QDateTime::currentDateTime().toString(Qt::ISODate));
	// update static hosts
	QDomNodeList hsts=domStaticHosts.childNodes();
	for(int hi=hsts.count()-1; hi>=0; --hi)
	{
		QDomNode n=hsts.at(hi);
		if(n.nodeName()=="host")
			domStaticHosts.removeChild(n);
	}
	for(int r=0; r<(int)baseConfig.staticHosts.size(); ++r)
	{
		const QStringList slh=baseConfig.staticHosts.at(r);
		if(slh.size()<3)
			continue;
		QDomElement h=doc.createElement("host");
		h.setAttribute("uid", slh.at(0));
		h.setAttribute("ip", slh.at(1));
		h.setAttribute("port", slh.at(2));
		domStaticHosts.appendChild(h);
	}
	// update timing
	domTiming.setAttribute("broadcast_ms", QString::number(baseConfig.broadcastMs));
	domTiming.setAttribute("heartbeat_ms", QString::number(baseConfig.heartbeatMs));
	if(baseConfig.aliveInd.isEmpty())
		domTiming.removeAttribute("alive_ind");
	else
		domTiming.setAttribute("alive_ind", baseConfig.aliveInd);
	domHardware.setAttribute("uid", baseConfig.uid);
	for(auto eoit=editorObjects.begin(); eoit!=editorObjects.end(); ++eoit)
	{
		if(eoit->second->md->category==EditorObjectMetadata::Category::DEVICE)
			eoit->second->updateXml(domHardware, baseConfig);
		else if(eoit->second->md->category==EditorObjectMetadata::Category::ALGORITHM)
			eoit->second->updateXml(domAlgorithms, baseConfig);
	}
	for(auto coit=editorConnections.begin(); coit!=editorConnections.end(); ++coit)
	{
		coit->second->updateXml(domConnections, baseConfig); // connection knows if it's a CONNECTOR and thus silent
	}
	file.write(doc.toString(2).toUtf8());
	file.close();
	if(!shortname.isEmpty())
	{
		ui->pushSave->setEnabled(false);
		ui->comboObject->setCurrentText(shortname);
		if(ui->comboObject->currentIndex()<0)
		{
			fillCombo();
			ui->comboObject->setCurrentText(shortname);
		}
	}
}

void WidConfigEditor::parseSection(QDomElement &elem, const QString & elemStr)
{
	for(QDomNode n = elem.firstChild(); !n.isNull(); n = n.nextSibling())
	{
		if(n.nodeName()!=elemStr)
			continue;
		QDomElement el=n.toElement();
		if(elemStr==QStringLiteral("con"))
		{
			EditorConnection *ec=new EditorConnection(el, this);
			insertNewEditorConnection(ec);
			continue;
		}
		qCDebug(widconfigeditor)<<"WidConfigEditor::parseSection"<<elem.nodeName()<<el.attribute("type");
		insertNewEditorObject(EditorObject::createEditorObject(el, objMetadatas, this));
	}
}

void WidConfigEditor::addObjectSl(const QString & objTypeName)
{
	qCDebug(widconfigeditor)<<"addObjectSig"<<objTypeName;
	EditorObject *eo=EditorObject::createEditorObject(objTypeName, objMetadatas);
	insertNewEditorObject(eo);
	eo->ensureVisible();
}

void WidConfigEditor::idChangedSl(int oldId, int newId)
{
	auto oit=editorObjects.find(oldId);
	if(oit==editorObjects.end())
	{
		qWarning()<<"WidConfigEditor::idChangedS no old ID"<<oldId<<newId;
		return;
	}
	EditorObject *eo=oit->second;
	if(eo->getId()!=newId)
		qWarning()<<"WidConfigEditor::idChangedS inconsistent newId"<<eo->getId()<<newId;
	editorObjects.erase(oit);
	editorObjects.insert(EditorObjectMISP::value_type(eo->getId(), QPointer<EditorObject>(eo)));
}

void WidConfigEditor::connectionStartSl(int device, int channel, const QPointF &scPos)
{
	if(tempCon.deviceId>=0)
	{ // actually not a start but a completion
		scene->rubberBand(QPointF());
		auto itSrc=editorObjects.find(tempCon.deviceId);
		auto itDest=editorObjects.find(device);
		if(itSrc==editorObjects.end() || itDest==editorObjects.end())
		{
			tempCon=TempCon();
			qWarning()<<"WidConfigEditor::connectionStartSl missing source or destination";
			return;
		}
		EditorNode *ndSrc=itSrc->second->getNode(tempCon.channel);
		ndSrc->toDefaultState();
		EditorNode *ndDest=itDest->second->getNode(channel);
		ndDest->toDefaultState();
		if(EditorNode::isValidConnection(ndSrc, ndDest))
		{
			// never duplicate a connection
			EditorConnection *ecExists1=getConnection(tempCon.deviceId, tempCon.channel, device, channel);
			EditorConnection *ecExists2=getConnection(device, channel, tempCon.deviceId, tempCon.channel);
			if(ecExists1 || ecExists2)
				qApp->beep();
			else
			{
				EditorConnection *ec=new EditorConnection(ndSrc, tempCon.startPos, ndDest, scPos);
				insertNewEditorConnection(ec);
			}
		}
		else
			qApp->beep();
		tempCon=TempCon();
		return;
	}
	// no it's a real start
	actConnectionCancel->setData(device);
	tempCon=TempCon(device, channel, scPos);
	scene->rubberBand(scPos);
}

void WidConfigEditor::connectionCancelSl(int device, int channel)
{
	Q_UNUSED(channel)
	actConnectionCancel->setData(device);
	actConnectionCancel->trigger();
}

void WidConfigEditor::connectionCancelAct()
{
	auto it=editorObjects.find(tempCon.deviceId);
	if(it==editorObjects.end())
		return;
	it->second->connectionCancelAct(tempCon.channel);
	tempCon=TempCon();
}

void WidConfigEditor::insertNewEditorObject(EditorObject *eo)
{
	qCDebug(widconfigeditor)<<"insertNewEditorObject"<<eo->getName();
	scene->addItem(eo);
	editorObjects.insert(EditorObjectMISP::value_type(eo->getId(), QPointer<EditorObject>(eo)));
	connect(eo, &EditorObject::idChangedSig, this, &WidConfigEditor::idChangedSl);
	connect(eo, &EditorObject::changedSig, this, &WidConfigEditor::touchSl);
	connect(eo, &EditorObject::connectionStartSig, this, &WidConfigEditor::connectionStartSl);
	connect(eo, &EditorObject::connectionCancelSig, this, &WidConfigEditor::connectionCancelSl);
	touchSl();
}

void WidConfigEditor::insertNewEditorConnection(EditorConnection *ec)
{
	qCDebug(widconfigeditor)<<"insertNewEditorConnection"<<ec->getName()<<ec->isHealthy();
	if(!ec->isHealthy())
	{
		qCritical()<<"won't add connection because not healthy";
		return;
	}
	EditorObject *srcP=ec->getSource()->getParentObject();
	EditorObject *destP=ec->getDest()->getParentObject();
	scene->addItem(ec);
	editorConnections.insert(EditorConnectionMKSP::value_type(ec->getKey(), QPointer<EditorConnection>(ec)));
	connect(srcP, &EditorObject::connectionReposSig, ec, &EditorConnection::reposSl);
	connect(destP, &EditorObject::connectionReposSig, ec, &EditorConnection::reposSl);
	connect(ec, &EditorConnection::changedSig, this, &WidConfigEditor::touchSl);
	touchSl();
}

void WidConfigEditor::markAliveInd(EditorNode *oldInd)
{
	if(oldInd)
		oldInd->setDecoration(oldInd->decoration() & ~EditorNode::DECO_ALIVE_IND);
	EditorNode *ai=getNode(baseConfig.aliveInd);
	if(ai)
		ai->setDecoration(ai->decoration() | EditorNode::DECO_ALIVE_IND);
}

QString WidConfigEditor::createCurrentFilenme(QString * shortname)
{
	QSettings settings;
	QDir dir(settings.value("deploy/configdir").toString());
	QString newShortname=QStringLiteral("config_%1%2.xml").arg(baseConfig.uid, baseConfig.rawConfig?"Raw":"");
	if(shortname)
		*shortname=newShortname;
	return dir.absoluteFilePath(newShortname);
}

void WidConfigEditor::touchSl()
{
	ui->pushSave->setEnabled(true);
}

