#ifndef AVNODEARDUBRIDGE_H
#define AVNODEARDUBRIDGE_H

#include "piadh.h"
#include "avnode.h"
//#include "../piah_common/nodealg.h"
//#include "../piah_common/device.h"
#include <QDebug>
#include <QDoubleSpinBox>
#include <QLineEdit>

namespace pi_at_home
{

class AvNodeArduBridge : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeArduBridge(const Fragment & fragment, QWidget *parent);
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeQString(text);}
	QLineEdit * widVal;
	QToolButton * tbut;
protected slots:
	void editingFinishedSl();
};

}
#endif // AVNODEARDUBRIDGE_H
