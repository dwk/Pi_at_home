#ifndef EDITORCONNECTION_H
#define EDITORCONNECTION_H
#include "piadh.h"
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QGraphicsItem>
#include <QPolygonF>
#include <QDomElement>
#include "editorobjectmetadata.h"


Q_DECLARE_LOGGING_CATEGORY(editorconnection)

namespace pi_at_home
{
class EditorDict;
class EditorNode;

// EditorNode is not a QObject -> use signals to EditorObject as proxy
class EditorConnection : public QObject, public QGraphicsItem
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsItem)
	friend class DlgEditObject;
	friend class WidConfigEditor;
public:
	struct ConKey
	{
		ConKey(int srcId, int srcCh, int destId, int destCh) : srcId(srcId), srcCh(srcCh), destId(destId), destCh(destCh) {}
		bool operator<(const ConKey & o) const;
		int srcId, srcCh, destId, destCh;
	};
	struct ConnectorInfo
	{
		ConnectorInfo(const QString &srcStr, const QString &destStr, QDomElement &hints) : srcStr(srcStr), destStr(destStr), hints(hints) {}
		QString srcStr, destStr;
		QDomElement hints;
	};
	enum class EndPoint	{ANY=0, SRC, DEST, NONE};
	static QDomElement domDummy;
	//static EditorConnection * createEditorConnection(QDomElement &elem);
	EditorConnection(EditorNode * source, const QPointF &sceneStart, EditorNode * dest, const QPointF &sceneEnd);
	EditorConnection(QDomElement &elem, pi_at_home::EditorDict *editorDict);
	EditorConnection(const ConnectorInfo &info, pi_at_home::EditorDict *editorDict);
	virtual ~EditorConnection();
	EditorConnection::ConKey getKey() const;
	bool isHealthy() const {return healthy;}
	virtual void updateXml(QDomElement &domParent, const EditorBaseConfig & bc);
	virtual void removeXml();
	virtual void readHints(const QDomElement &editorHint);
	virtual void updateHints(QDomElement &editorHint);
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE;
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	QString getName() const;
	EditorNode * getSource() const {return source;}
	EditorNode * getDest() const {return dest;}
public slots:
	void reposSl(void *node);
	void deleteSl();
protected:
	// QGraphicsItem
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	//
	virtual void setupGraphic(const QPointF endp);
	virtual QStringList checkConsistency();
	void updatePainterUtils(QPainter *painter, const QStyleOptionGraphicsItem *option);
	// graphic representation
	QPen outlinePen, backpen;
	double levelOfDetail;
	QPainterPath boundingShape;
	QPolygonF body, tbody, arrowHead;
	double center=0.5;
	//
	bool healthy=false;
	QDomElement elem;
	EditorNode *source, *dest;
signals:
	void changedSig();
};
typedef std::vector<EditorConnection *> EditorConnectionVP;
typedef std::vector<EditorConnection::ConKey> EConKeyVV;
typedef std::vector<EditorConnection::ConnectorInfo> EConInfoVV;
typedef std::multimap<QString, QPointer<EditorConnection> > EditorConnectionMMSSP; // key is src or dest
typedef std::map<EditorConnection::ConKey, QPointer<EditorConnection> > EditorConnectionMKSP; // unique key
} // namespace pi_at_home

#endif // EDITORCONNECTION_H
