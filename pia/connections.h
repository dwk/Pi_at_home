#ifndef CONNECTIONS_H
#define CONNECTIONS_H
#include "piadh.h"
#include <QObject>
#include <QTableWidget>
#include <QAction>
#include "../piah_common/netcon.h"

namespace pi_at_home
{
class MainWindow;

class Connections : public QObject
{
	Q_OBJECT
public:
	explicit Connections(QObject *parent, MainWindow &mwin);
	~Connections();
	QWidget * widget(QWidget *guiParent);
	NetConMSP & hostConMP() {return hostConMP_;}
	NetCon * hostCon(const QString & uid);
public slots:
	bool addHost(const QString & host, int port);
	void closeHost();
	void closeAllHosts();
	void srvcmdSl(int srvcmd);
	void cleanupList();
private slots:
	void sockErrorSig(int errNo);
	void sockReady(bool ready);
	void contextMenu(QPoint pos);
	void itemSelectionChanged();
private:
	int rowByHc(NetCon * hc, bool useAddress = false);
	QTableWidget * wid=nullptr;
	NetConMSP hostConMP_; // key is uid or (during startup) address:port
	QAction *actCloseAllConnections=nullptr, *actCleanupConnectionList=nullptr,
		*actCloseHost=nullptr, *actionAutoClose=nullptr;
	QAction *actEndProcess=nullptr, *actStopService=nullptr, *actRestartService=nullptr, *actShutdownMachine=nullptr, *actRestartMachine=nullptr;
signals:
	void connectionListChanged();
	void connectionNameChanged(QString, QString);
};
}
#endif // CONNECTIONS_H
