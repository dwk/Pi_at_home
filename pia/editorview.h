#ifndef EDITORVIEW_H
#define EDITORVIEW_H

#include <QGraphicsView>

//namespace pi_at_home - not supportd for promoted Widgets?
//{

class EditorView : public QGraphicsView
{
    Q_OBJECT
public:
	EditorView(QWidget *parent = Q_NULLPTR);

protected:
#ifndef QT_NO_WHEELEVENT
    void wheelEvent(QWheelEvent *) Q_DECL_OVERRIDE;
#endif
	void transformMatrix();
private:
	double zoom=1.;
};
//} // namespace pi_at_home

#endif // EDITORVIEW_H
