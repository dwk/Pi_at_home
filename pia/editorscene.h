#ifndef EDITORSCENE_H
#define EDITORSCENE_H

#include <QGraphicsScene>
#include <QGraphicsLineItem>

namespace pi_at_home
{

class EditorScene : public QGraphicsScene
{
    Q_OBJECT
public:
	EditorScene(QObject *parent = Q_NULLPTR);
	void rubberBand(const QPointF &start);
public slots:
	void rubberBandCancelSl();
protected:
	virtual void keyPressEvent(QKeyEvent *keyEvent);
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
private:
	QPointF rubberStart;
	QGraphicsLineItem *rubBand = nullptr;
signals:
	void rubberBandCancelSig();
};
} // namespace pi_at_home

#endif // EDITORSCENE_H
