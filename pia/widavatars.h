#ifndef WIDAVATARS_H
#define WIDAVATARS_H

#include "piadh.h"
//#include <QLabel>
#include <QtWidgets/QTabWidget>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QWidget>
#include "../piah_common/commandtarget.h"

namespace pi_at_home
{

class WidAvatars : public QTabWidget, public CommandTarget
{
	Q_OBJECT
public:
	explicit WidAvatars(QWidget *parent = 0);
	~WidAvatars() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
public slots:
	void connectionsChanged();
private:
	QTabWidget *tabWidget;
	QVBoxLayout *verticalLayout;
};

}

#endif // WIDAVATARS_H
