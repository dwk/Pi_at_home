#ifndef AVNODE__H
#define AVNODE__H

#include "piadh.h"
#include "../piah_common/enum_result.h"
#include <QVariant>
#include <QPointer>
#include <QDomElement>
#include <QDebug>
#include <QDateTime>

#include <QWidget>
#include <QLabel>
#include <QCheckBox>
#include "../piah_common/node.h"
#include "../piah_common/nodeaddress.h"
#include "../piah_common/commandtarget.h"

namespace Ui
{
	class WidNode;
}

namespace pi_at_home
{

class AvNode : public QWidget, public CommandTarget
{
	Q_OBJECT
public:
	explicit AvNode(const Fragment & fragment, QWidget *parent, bool removeWidDummy = true);
	virtual ~AvNode();
	virtual void processCommand(Command & cmd, Fragment & relevant);
	const NodeAddress & nodeAddress() const {return adr_;}
protected:
	virtual QString serialize() {return value_.toString();}
	virtual void deserialize(const QString & text) =0;
	void deserializeBool(const QString & text) {value_=NodeValue(text=="true");}
	void deserializeInt(const QString & text) {bool ok; value_=NodeValue(text.toInt(&ok)); if(!ok) qCritical()<<"invalid int deseri"<<text;}
	void deserializeDouble(const QString & text) {bool ok; value_=NodeValue(text.toDouble(&ok)); if(!ok) qCritical()<<"invalid double deseri"<<text;}
	void deserializeQString(const QString & text) {value_=NodeValue(text);}
	void deserializeQDateTime(const QString & text) {value_=NodeValue(QDateTime::fromString(text, Qt::ISODate));}
	Ui::WidNode *ui;
	NodeAddress adr_;
	NodeValue defval_, value_;
	bool isHidden_=false;
protected slots:
	void clickedOnline(bool onl);
	void clickedSim(bool sim);
	void nodeValueChanged();
	void updateState(Node::State state);
	void on_labelAlias_customContextMenuRequested(const QPoint &pos);
	void copyAdrSl();
};

class AvatarFactory
{
public:
	typedef AvNode * (*AvatarCreator)(const Fragment &, QWidget *);
	static bool registerAvatar(const QString & typeId, AvatarCreator creator);
	static AvNode * produce(const Fragment &elem, QWidget * guiParent);
private:
	typedef std::map< QString, AvatarCreator> AvatarCreatorMP;
	static AvatarCreatorMP avatarCreatorMP;
};

class AvNodeDummy : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeDummy(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeDummy() {}
	virtual void processCommand(Command & cmd, Fragment & relevant) {Q_UNUSED(cmd); Q_UNUSED(relevant); return;}
protected:
};

}
#endif // AVNODE__H
