#ifndef EDITORDICT_H
#define EDITORDICT_H
#include "piadh.h"
#include "../piah_common/nodeaddress.h"
#include "editorconnection.h"

namespace pi_at_home
{
class EditorNode;
class EditorConnection;

class EditorDict
{
public:
	virtual EditorNode *getNode(const QString & alias) =0;
	virtual EditorNode *getNode(int device, int channel) =0;
	virtual NodeAddress getNodeAddress(const QString & alias) =0;
	virtual EditorConnectionVP getConnection(int deviceId, int channel, pi_at_home::EditorConnection::EndPoint endPointType=EditorConnection::EndPoint::ANY) =0;
	virtual EditorConnection *getConnection(int srcId, int srcChannel, int destId, int destChannel) =0;
	virtual void pendingConnection(EditorConnection::ConnectorInfo eci) =0;
};

} // namespace pi_at_home

#endif // EDITORDICT_H
