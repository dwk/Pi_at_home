#include "editorscene.h"
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include "piadi.h"

EditorScene::EditorScene(QObject *parent) : QGraphicsScene(parent)
{
}

void EditorScene::rubberBand(const QPointF &start)
{
	rubberStart=start;
	if(start.isNull())
	{
		rubberBandCancelSl();
		return;
	}
	delete rubBand;
	rubBand=new QGraphicsLineItem();
	rubBand->setZValue(1000000.);
	addItem(rubBand);
}

void EditorScene::rubberBandCancelSl()
{
	delete rubBand;
	rubBand=nullptr;
}

void EditorScene::keyPressEvent(QKeyEvent *keyEvent)
{
	if(keyEvent->key()==Qt::Key_Escape)
	{
		emit rubberBandCancelSig();
		keyEvent->accept();
	}
	QGraphicsScene::keyPressEvent(keyEvent);
}

void EditorScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	if(rubBand)
	{
		QLineF line(rubberStart, mouseEvent->scenePos());
		//qDebug()<<"updating rubber band"<<line;
		rubBand->setLine(line);
		//rubBand->update();
	}
	QGraphicsScene::mouseMoveEvent(mouseEvent);
}

