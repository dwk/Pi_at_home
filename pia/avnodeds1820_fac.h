#ifndef AVNODEDS1820_FAC_H
#define AVNODEDS1820_FAC_H
#include "../piah_common/nodeds1820_nm.h"

namespace
{
pi_at_home::AvNode * AvCreator(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeDS1820(frag, guiParent);
}
bool avatarRegistered=pi_at_home::AvatarFactory::registerAvatar(typeName, AvCreator);
}

#endif // AVNODEDS1820_FAC_H
