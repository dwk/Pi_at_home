#ifndef AVNODELCD84X48_FAC_H
#define AVNODELCD84X48_FAC_H

#include "../piah_common/nodelcd84x48_nm.h"

namespace
{
pi_at_home::AvNode * AvCreatorStr(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeLcd84x48Line(frag, guiParent);
}
bool avatarStrRegistered=pi_at_home::AvatarFactory::registerAvatar(typeName, AvCreatorStr);
}

#endif // AVNODELCD84X48_FAC_H
