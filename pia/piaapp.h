#ifndef DWKAPP_H
#define DWKAPP_H

#include "piadh.h"
#include <QApplication>

class QSharedMemory;

namespace pi_at_home
{

class PiaApp : public QApplication
{
	Q_OBJECT
public:
	PiaApp(int & argc, char **argv);
	virtual ~PiaApp();
    virtual bool notify ( QObject * receiver, QEvent * event );
	bool attachIpc();
private:
	QSharedMemory *shm;
};

} // namespace
#endif // DWKAPP_H
