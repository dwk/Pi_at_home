#ifndef DLGEDITNODE_H
#define DLGEDITNODE_H

#include "piadh.h"
#include <QDialog>
#include <QGridLayout>
#include "editorobject.h"

namespace Ui
{
	class DlgEditNode;
}

namespace pi_at_home
{

class DlgEditNode : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEditNode(QWidget *parent, EditorNode & node);
	virtual ~DlgEditNode();
public slots:
	virtual void accept();
private slots:
	void defOmChangedSl(int checkState, int row);
private:
	Ui::DlgEditNode *ui;
	QGridLayout *grid;
	EditorNode & node;
	int oldChannel, channelRow;
};
}
#endif // DLGEDITNODE_H
