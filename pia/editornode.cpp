#include "editornode.h"
#include <QtWidgets>
#include <QToolButton>
#include "editorobjectderived.h"
#include "widconfigeditor.h"
#include "dlgeditnode.h"
#include "dlgeditobject.h"
#include "piadi.h"

Q_LOGGING_CATEGORY(editornode, "editornode")

EditorNodeMetadata EditorNode::dummyMD;

bool EditorNode::isValidConnection(const EditorNode *en1, const EditorNode *en2, bool *conversionWarning)
{
	if(!en1 ||!en2)
	{
		if(conversionWarning)
			*conversionWarning=true;
		return false;
	}
	bool res=false;
	switch(en1->md->type)
	{
	case EditorNodeMetadata::Type::INPUT:
	case EditorNodeMetadata::Type::CONNECTOR:
		res=(en2->md->type==EditorNodeMetadata::Type::OUTPUT ||
			   en2->md->type==EditorNodeMetadata::Type::BIDIR);
		break;
	case EditorNodeMetadata::Type::OUTPUT:
		res=(en2->md->type==EditorNodeMetadata::Type::CONNECTOR ||
			   en2->md->type==EditorNodeMetadata::Type::INPUT ||
			   en2->md->type==EditorNodeMetadata::Type::BIDIR);
		break;
	case EditorNodeMetadata::Type::BIDIR:
		res=(en2->md->type==EditorNodeMetadata::Type::CONNECTOR ||
			   en2->md->type==EditorNodeMetadata::Type::INPUT ||
			   en2->md->type==EditorNodeMetadata::Type::OUTPUT ||
			   en2->md->type==EditorNodeMetadata::Type::BIDIR);
		break;
	case EditorNodeMetadata::Type::UNDEF:
		res=false;
		break;
	}
	if(conversionWarning)
	{
		if(res)
			*conversionWarning=(en1->md->dataType!=en2->md->dataType);
		else
			*conversionWarning=true;
	}
	return res;
}

EditorNode::EditorNode(const pi_at_home::EditorNodeMetadata *metaData, int channel, QGraphicsItem *parent) :
	QGraphicsItem(parent), md(metaData), elem(EditorObject::domDummy), alias(md->typeName), channel(channel)
{
	setFlags(ItemIsSelectable);
	setAcceptHoverEvents(true);
	for(auto it=md->editorParamMetadatas.begin(); it!=md->editorParamMetadatas.end(); ++it)
	{
		if(it->second->type==EditorParamMetadata::Type::REQUIRED)
		{
			if(it->second->name=="channel")
				keyVals.insert(KeyValMV::value_type(it->second->name, QString::number(channel)));
			else
				keyVals.insert(KeyValMV::value_type(it->second->name, it->second->def.toString()));
		}
	}
	/* name / alias will be used as parameter name in object
	if(md->type==EditorNodeMetadata::Type::CONNECTOR)
	{
		alias=QStringLiteral("(con)");
	}*/
	keyvalsChanged();
}

EditorNode::EditorNode(QDomElement &elem, const pi_at_home::EditorObjectMetadata *objectMetaData, pi_at_home::EditorDict *ed, QGraphicsItem *parent) :
	QGraphicsItem(parent), elem(elem)
{
	alias=elem.attribute("name");
	if(objectMetaData->editorNodeMetadatas.size()==1)
	{
		md=objectMetaData->editorNodeMetadatas.begin()->second;
	}
	else
	{
		QString typeName=elem.attribute("type_name");
		auto it=objectMetaData->editorNodeMetadatas.find(typeName);
		if(it==objectMetaData->editorNodeMetadatas.end())
			it=objectMetaData->editorNodeMetadatas.find(alias);
		if(it==objectMetaData->editorNodeMetadatas.end())
			it=objectMetaData->editorNodeMetadatas.find(QString(""));
		if(it==objectMetaData->editorNodeMetadatas.end())
		{
			qWarning()<<"EditorNode can't identify metadata"<<alias<<channel;
			md=&EditorNode::dummyMD;
		}
		else
			md=it->second;
	}
	commonCtor(elem, ed);
}

EditorNode::EditorNode(QDomElement &elem, const EditorNodeMetadata *metaData, pi_at_home::EditorDict *ed, QGraphicsItem *parent) :
	QGraphicsItem(parent), md(metaData), elem(elem)
{
	commonCtor(elem, ed);
}

EditorNode::~EditorNode()
{
	removeXml();
	elem.clear();
}

EditorObject *EditorNode::getParentObject() const
{
	return dynamic_cast<EditorObject *>(parentItem());
}

void EditorNode::updateXml(QDomElement &domParent, const pi_at_home::EditorBaseConfig &bc)
{
	Q_UNUSED(bc)
	// CONNECTOR does need an xml element!
	//if(md->type==EditorNodeMetadata::Type::CONNECTOR)
	//	return; // connector type will never change -> update or remove never required
	if(md->fixed && md->type!=EditorNodeMetadata::Type::CONNECTOR)
		return; // this node can't change; so we never have to update or delete a node-DOM element
	if(elem.isNull())
	{
		elem=domParent.ownerDocument().createElement("node");
		domParent.appendChild(elem);
	}
	if(md->type==EditorNodeMetadata::Type::CONNECTOR)
	{
		QDomNode eh=elem.firstChild();
		QDomElement ehe;
		for(; ehe.isNull() && !eh.isNull(); eh=eh.nextSibling())
		{
			if(eh.isElement() && eh.nodeName()=="editor-hints")
				ehe=eh.toElement();
		}
		if(ehe.isNull())
		{
			ehe=domParent.ownerDocument().createElement("editor-hints");
			elem.appendChild(ehe);
		}
		EditorDict *ce=dynamic_cast<EditorDict *>(scene()->parent());
		if(!ce)
		{
			qCritical()<<"EditorNode::updateXml no EditorDict"<<alias<<getAddress().toStringVerbose();
			return;
		}
		EditorConnectionVP ecs=ce->getConnection(getParentId(), channel);
		if(ecs.size()==1)
		{
			EditorNode *nd=ecs.at(0)->getDest();
			//qDebug()<<"EOBitLogic::updateXml 1"<<nd->getAddress().toString();
			if(nd==this)
				nd=ecs.at(0)->getSource();
			//qDebug()<<"EOBitLogic::updateXml 2"<<nd->getAddress().toString();
			NodeAddress nad=nd->getAddress();
			//if(nad.uid().isEmpty()) always update to current uid, it may have changed
			nad.setUid(bc.uid);
			alias=nad.toString();
			ecs.at(0)->updateHints(ehe);
		}
		else
			qCritical()<<"EOBitLogic::updateXml no or multiple connections on CONNECTOR"<<alias<<getAddress().toStringVerbose()<<ecs.size();
	}
	elem.setAttribute("name", alias);
	elem.setAttribute("type_name", md->typeName);
	// elem.setAttribute("channel", channel); is a keyval
	for(auto it=keyVals.begin(); it!=keyVals.end(); ++it)
	{
		elem.setAttribute(it->first, it->second);
	}
	auto atts=elem.attributes();
	for(int i=0; i<atts.count(); ++i)
	{
		QString an=atts.item(i).nodeName();
		if(an=="name" || an=="type_name") // have no keyval
			continue;
		auto it=keyVals.find(an);
		if(it==keyVals.end())
			elem.removeAttribute(an);
	}
}

void EditorNode::removeXml()
{
	if(elem.isNull())
		return;
	QDomNode p=elem.parentNode();
	//if(md->type==EditorNodeMetadata::Type::CONNECTOR)
	//	return; // connector type will never change -> remove never required because there is no xml
	p.removeChild(elem);
	elem.clear();
}

QString EditorNode::getAlias() const
{
	EditorObject * eop=dynamic_cast<EditorObject *>(parentItem());
	return (eop?eop->getPrefix():QStringLiteral("NULL"))+alias;
}

void EditorNode::setChannel(int newChannel)
{
	channel=newChannel;
	auto kit=keyVals.find("channel");
	if(kit!=keyVals.end())
		kit->second=QString::number(channel);
}

int EditorNode::getParentId() const
{
	EditorObject *pi=dynamic_cast<EditorObject*>(parentItem());
	if(pi)
		return pi->getId();
	return -1;
}

NodeAddress EditorNode::getAddress() const
{
	EditorObject *pi=dynamic_cast<EditorObject*>(parentItem());
	if(pi)
		return NodeAddress("", pi->getId(), channel, alias); // uid wildcard, has to be remapped during save
	return NodeAddress();
}

void EditorNode::setIndex(int index)
{
	prepareGeometryChange();
	this->index=index;
	setPos(0., EditorObject::objectHeaderHeight+nodeHeight*index);
}

void EditorNode::setDecoration(EditorNode::Decos decos)
{
	this->decos=decos;
	setupGraphic();
	update();
}

void EditorNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget)
	double levelOfDetail=option->levelOfDetailFromTransform(painter->worldTransform());
	if(levelOfDetail<0.2)
		return; // nodes will not paint at all at this resolution
	if (option->state & QStyle::State_MouseOver)
	{
		WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
		EditorObject *pi=dynamic_cast<EditorObject*>(parentItem());
		if(ce && pi)
		{
			auto tc=ce->getTempCon();
			bool rb(tc.isActive()), self=(tc.deviceId==pi->getId() && tc.channel==channel);
			if(!rb)
				painter->fillRect(body, QColor(Qt::gray));
			else if(self)
				painter->fillRect(body, QColor(Qt::darkMagenta));
			else
			{
				if(md->type==EditorNodeMetadata::Type::CONNECTOR)
				{
					EditorConnectionVP ecs=ce->getConnection(pi->getId(), channel);
					painter->fillRect(body, (!ecs.size())?colHoverOk:QColor(Qt::darkRed));
				}
				else
				{
					bool warn;
					bool valid=EditorNode::isValidConnection(this, ce->getNode(tc.deviceId, tc.channel), &warn);
					painter->fillRect(body, valid?(warn?QColor(Qt::yellow):colHoverOk):QColor(Qt::red));
				}
			}
		}
	}
	if(decos & DECO_ALIVE_IND)
	{
		QPen p=painter->pen();
		painter->setPen(Qt::red);
		painter->drawLine(body.left(), body.top(), body.right(), body.bottom());
		painter->drawLine(body.left(), body.bottom(), body.right(), body.top());
		painter->setPen(p);
	}
	if(decos & DECO_LOGGED)
	{
		auto it=nodeMarkerMV.find((int)DECO_LOGGED);
		if(it!=nodeMarkerMV.end())
		{
			painter->fillRect(it->second.area, it->second.col);
		}
	}
	painter->drawRect(body);
	if(levelOfDetail>=2)
	{
		QFont font("Courier", 5);
		painter->setFont(font);
		painter->drawText(body.x()+2, nodeHeight-9, alias);
		painter->drawText(body.x()+2, nodeHeight-2, QStringLiteral("channel %1").arg(channel));
	}
	else if(levelOfDetail>0.8)
	{
		painter->drawText(body.x()+2, nodeHeight/2+3, md->type==EditorNodeMetadata::Type::CONNECTOR?QStringLiteral("(con)"):alias);
	}
	if(decos & DECO_ALIVE_IND)
	{
		QPen p=painter->pen();
		painter->setPen(Qt::red);
		painter->drawLine(body.left(), body.top(), body.right(), body.bottom());
		painter->drawLine(body.left(), body.bottom(), body.right(), body.top());
		painter->setPen(p);
	}
}

void EditorNode::toDefaultState()
{
	if(tempCon)
	{
		tempCon=0;
		update();
	}
}

void EditorNode::keyvalsChanged()
{
	bool unsetLogHoldoff=true;
	for(auto it=keyVals.begin(); it!=keyVals.end(); ++it)
	{
		if(it->first==QStringLiteral("channel"))
			channel=it->second.toInt();
		else if(it->first==QStringLiteral("log_holdoff_ms"))
		{
			unsetLogHoldoff=false;
			int v=it->second.toInt();
			if(v<0)
				decos &= ~EditorNode::DECO_LOGGED;
			else
				decos |= EditorNode::DECO_LOGGED;
		}
	}
	if(unsetLogHoldoff)
		decos &= ~EditorNode::DECO_LOGGED;
	setupGraphic();
}

void EditorNode::commonCtor(QDomElement &elem, pi_at_home::EditorDict *ed)
{
	setFlags(ItemIsSelectable);
	setAcceptHoverEvents(true);
	//setAcceptedMouseButtons(Qt::LeftButton | Qt::MidButton | Qt::RightButton);
	//setZValue(parentItem()->zValue()-0.5);
	bool nameOk=false, idOk=false;
	channel=md->baseChannel;
	QDomNamedNodeMap attrs=elem.attributes();
	for(int i=0; i<attrs.size(); ++i)
	{
		QDomAttr attr=attrs.item(i).toAttr();
		QString aname=attr.name();
		QString v=attr.value();
		if(aname=="name")
		{
			alias=v;
			nameOk=true;
		}
		else if(aname=="channel")
		{
			channel=v.toInt(&idOk);
			keyVals.insert(KeyValMV::value_type(aname, v));
		}
		else
			keyVals.insert(KeyValMV::value_type(aname, v));
	}
	if(!nameOk || (md->type!=EditorNodeMetadata::Type::CONNECTOR && !idOk && md->nodeMax>1) )
		qCritical()<<"no Node channel/alias"<<alias<<channel;
	//else
	//	qInfo()<<"Creating node"<<alias<<channel;
	if(md->type==EditorNodeMetadata::Type::CONNECTOR)
	{
		QDomElement eh;
		QDomNodeList markupList=elem.elementsByTagName("editor-hints");
		if(markupList.size())
			eh=markupList.item(0).toElement();
		ed->pendingConnection(EditorConnection::ConnectorInfo(alias, getAddress().toString(), eh));
	}
	keyvalsChanged();
}

void EditorNode::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	EditorObject *pi=dynamic_cast<EditorObject*>(parentItem());
	if(!pi)
	{
		qWarning()<<"EditorNode::contextMenuEvent with no parent";
		return;
	}
	if(tempCon && pi)
		pi->connectionCancelInfo(channel);
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction *act=menu->addAction(QIcon(":/res/cross_red.png"), QString("Delete node '%1'").arg(alias));
	act->setEnabled(pi->canDeleteNode(channel, md->typeName));
	pi->connect(act, &QAction::triggered, pi, [pi, this](){pi->deleteNodeSl(channel);});
	act=menu->addAction(QIcon(":/res/star.png"), QString("%1 node '%2'").arg(md->fixed?"View":"Edit", alias));
	//if(md->type==EditorNodeMetadata::Type::CONNECTOR)
	//	act->setEnabled(false); is readonly anyway
	pi->connect(act, &QAction::triggered, pi, [pi, this](){pi->editNodeSl(channel);});
	menu->popup(event->screenPos());
}

void EditorNode::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	qDebug()<<"dblclk at"<<(int)event->button()<<event->buttonDownScenePos(Qt::MidButton)<<event->scenePos();
	EditorObject *pi=dynamic_cast<EditorObject*>(parentItem());
	if(tempCon && pi)
		pi->connectionCancelInfo(channel);
	if(event->button()==Qt::LeftButton)
	{
		if(!pi)
		{
			qWarning()<<"EditorNode::mouseDoubleClickEvent with no parent";
			return;
		}
		pi->editNodeSl(channel);
	}
	// intentionally not called QGraphicsItem::mouseDoubleClickEvent(event);
}

void EditorNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	Q_UNUSED(event)
	//qDebug()<<"node press"<<channel<<alias<<(int)event->button()<<event->scenePos();
	tempCon=1;
	EditorObject *pi=dynamic_cast<EditorObject*>(parentItem());
	if(pi)
		pi->connectionStart(channel, scenePos()+conOffset); //event->scenePos()
	update();
	// DON'T call QGraphicsItem::mousePressEvent(event); - will supress release event for middle Button (very strange)...
}

void EditorNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"node release"<<channel<<alias<<(int)event->button()<<event->scenePos();
	if(tempCon==1)
		tempCon=2;
	// if(event->button()==Qt::MidButton)
	update();
	QGraphicsItem::mouseReleaseEvent(event);
}

#define EditorNode_LOGDECO_RGB 20,0,110
#define EditorNode_LOGDECO_WIDTH 10.
void EditorNode::setupGraphic()
{
	nodeMarkerMV.clear();
	switch(md->type)
	{
	case EditorNodeMetadata::Type::INPUT:
	case EditorNodeMetadata::Type::CONNECTOR:
		body=QRectF(0., 0., nodeWidth, nodeHeight);
		conOffset.setX(0.);
		conOffset.setY(nodeHeight/2.);
		nodeMarkerMV.insert(NodeMarkerMV::value_type((int)Deco::DECO_LOGGED, NodeMarker(QRectF(nodeWidth-EditorNode_LOGDECO_WIDTH, 0., EditorNode_LOGDECO_WIDTH, nodeHeight), QColor(EditorNode_LOGDECO_RGB))));
		break;
	case EditorNodeMetadata::Type::OUTPUT:
		body=QRectF(EditorObject::objectWidth-nodeWidth, 0., nodeWidth, nodeHeight);
		conOffset.setX(EditorObject::objectWidth-1.);
		conOffset.setY(nodeHeight/2.);
		nodeMarkerMV.insert(NodeMarkerMV::value_type((int)Deco::DECO_LOGGED, NodeMarker(QRectF(EditorObject::objectWidth-nodeWidth, 0., EditorNode_LOGDECO_WIDTH, nodeHeight), QColor(EditorNode_LOGDECO_RGB))));
		break;
	case EditorNodeMetadata::Type::BIDIR:
		body=QRectF(0., 0., EditorObject::objectWidth, nodeHeight);
		conOffset.setX(EditorObject::objectWidth/2.);
		conOffset.setY(nodeHeight/2.);
		nodeMarkerMV.insert(NodeMarkerMV::value_type((int)Deco::DECO_LOGGED, NodeMarker(QRectF(EditorObject::objectWidth/2.-EditorNode_LOGDECO_WIDTH/2., 0., EditorNode_LOGDECO_WIDTH, nodeHeight), QColor(EditorNode_LOGDECO_RGB))));
		break;
	case EditorNodeMetadata::Type::UNDEF:
		body=QRectF(20., 0., EditorObject::objectWidth-40, nodeHeight);
		conOffset.setX(EditorObject::objectWidth/2.);
		conOffset.setY(nodeHeight/2.);
		nodeMarkerMV.insert(NodeMarkerMV::value_type((int)Deco::DECO_LOGGED, NodeMarker(QRectF(EditorObject::objectWidth/2.-EditorNode_LOGDECO_WIDTH/2., 0., EditorNode_LOGDECO_WIDTH, nodeHeight), QColor(EditorNode_LOGDECO_RGB))));
		break;
	}
	switch(md->type)
	{
	case EditorNodeMetadata::Type::INPUT:
	case EditorNodeMetadata::Type::OUTPUT:
	case EditorNodeMetadata::Type::BIDIR:
		colHoverOk.setRgb(65,250,11);
		break;
	case EditorNodeMetadata::Type::CONNECTOR:
		colHoverOk.setRgb(11,180,205);
		break;
	case EditorNodeMetadata::Type::UNDEF:
		colHoverOk.setRgb(255,255,0);
		break;
	}
	//penHoverOk.setColor(colHoverOk.darker());
	//penHoverOk.setWidth(3);
	setPos(0., EditorObject::objectHeaderHeight+nodeHeight*index);
	setToolTip(md->description);
}


