#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QListWidget>
#include "../piah_common/commandtarget.h"
#include "widconnectioninspector.h"

namespace Ui
{
	class MainWindow;
}

namespace pi_at_home
{
class MainWindow : public QMainWindow, public CommandTarget
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	QAction * getAction(const QString & name);
	virtual void processCommand(Command &cmd, Fragment & relevant);
	void relayDebug(QString text, int type, long long threadId);
protected:
	virtual void closeEvent(QCloseEvent *event);
private slots:
	void about();
	void newHostCon();
	void editHostsSl();
	void deploymentSl();
private:
	Ui::MainWindow *ui;
	WidConnectionInspector *conInspect=nullptr;
	QObjectList actors;
signals:
	void relayDebugSig(QString text, int type, long long threadId);
};
}
#endif // MAINWINDOW_H
