#include "worldobject.h"
#include <QtWidgets>
#include <QToolButton>
#include <QDomNode>
#include <QDomNodeList>
#include <QDomElement>
#include "widconfigworld.h"
#include "dlgdeploy.h"
#include "../piah_common/command.h"
#include "piadi.h"

Q_LOGGING_CATEGORY(worldobject, "worldobject")

double WorldObject::defaultPosX=0.;
double WorldObject::defaultPosY=0.;
QWidget * WorldObject::pwid=nullptr;
QDomElement WorldObject::domDummy;


void WorldObject::Conf::readConf()
{
	QFile file(filepath);
	if(!file.open(QIODevice::ReadOnly))
	{
		qWarning()<<"config file not found"<<filepath;
		src=Source::LOCMISS;
		return;
	}
	QDomDocument doc;
	QString errStr;
	int errLine=0;
	if (!doc.setContent(&file, &errStr, &errLine))
	{
		qCritical()<<"config file invalid in line"<<errLine<<errStr<<filepath;
		src=Source::LOCMISS;
		return;
	}
	QDomElement de=doc.documentElement();
	tst=de.attribute(QStringLiteral("savetst"));
	if(de.attribute(QStringLiteral("flavor"))==QStringLiteral("raw"))
		src=Source::LOCRAW;
	else if(de.attribute(QStringLiteral("flavor"))==QStringLiteral("normal"))
		src=Source::LOC;
	else
	{
		qWarning()<<"config file has no flavor (old or manual file?)";
		src=Source::LOCMISS;
		return;
	}
	QDomNodeList algs=doc.elementsByTagName("algorithm");
	for(int i=0; i<algs.count(); ++i)
	{
		QDomElement rootElem=algs.item(i).toElement();
		if(rootElem.attribute("type")!=QStringLiteral("Dummy"))
			continue;
		for(QDomNode n = rootElem.firstChild(); !n.isNull(); n = n.nextSibling())
		{
			QString src=n.toElement().attribute(QStringLiteral("source"));
			if(n.nodeName()==QStringLiteral("node") && !src.isEmpty())
			{
				sourceNodes<<src;
			}
		}
	}
	states=vector<SourceState>(sourceNodes.size(), SourceState::NONE);
}


WorldObject::WorldObject(const QString &uid, const pi_at_home::HostInstance &hi, const QString & binaryTst) :
	QObject(), hostInst(hi), binTst(binaryTst), currentConf(conf) //elem(domDummy), , md(metaData)
{
	hostInst.uid=uid;
	ip=QHostAddress(hostInst.hadr).toString();
	setPos(defaultPosX, defaultPosY);
	defaultPosX+=objectWidth*1.5;
	if(defaultPosX>objectWidth*4.)
	{
		defaultPosY+=objectHeaderHeight*2.;
		defaultPosX=0.;
	}
	setFlags(ItemIsSelectable | ItemIsMovable | ItemSendsGeometryChanges);
    setAcceptHoverEvents(true);
	QString templ("config_%1.xml"), templraw("config_%1Raw.xml");
	QSettings settings;
	QDir dir(settings.value("deploy/configdir").toString());
	conf.filepath=dir.absoluteFilePath(templ.arg(uid));
	conf.readConf();
	confRaw.filepath=dir.absoluteFilePath(templraw.arg(uid));
	confRaw.readConf();
	if(conf.src==Source::LOC)
		currentConf=conf;
	else if(confRaw.src==Source::LOCRAW)
		currentConf=confRaw;
	else
		currentConf=conf;
	setupGraphic(); // final height for port section
}

WorldObject::~WorldObject()
{
	DCON.deregisterCmdTarget(this);
	if(nc)
		delete nc;
}

bool WorldObject::hasNode(const QString &nadr) const
{
	bool res=nodes.contains(nadr);
	if(!res)
		qDebug()<<"hasNode miss"<<nadr<<nodes;
	return res;
}

void WorldObject::markSource(int index, WorldObject::SourceState state)
{
	if(index<0 || index>=(int)currentConf.states.size())
	{
		qCritical()<<"WorldObject::markSource range"<<index<<currentConf.states.size();
		return;
	}
	currentConf.states[index]=state;
	update();
}

QPointF WorldObject::getSourcePos(int index) const
{
	if(index<0 || index>=nodes.size())
		return pos(); //+QPointF(boundingBox.width()/2., boundingBox.height()/2.);
	return pos()+QPointF(2, 1+objectHeaderHeight+index*10);
}

void WorldObject::processCommand(Command &cmd, Fragment &relevant)
{
	Q_UNUSED(& relevant);
	//qDebug()<<"WorldObject"<<cmd.cmd();
	if(cmd.cmd()=="contest" && hostInst.uid==cmd.destination().uid())
	{
		//qDebug()<<"WorldObject::processCommand"<<toString();
		auto nds=cmd.fragments();
		for(auto fit=nds.begin(); fit!=nds.end(); ++fit)
		{
			nodes<<(*fit)->attribute(QStringLiteral("addr"));
		}
		DCON.deregisterCmdTarget(this);
		//qDebug()<<"WorldObject nodes"<<hostInst.uid<<nodes;
	}
}

QRectF WorldObject::boundingRect() const
{
	return boundingBox;
}

QPainterPath WorldObject::shape() const
{
	QPainterPath path;
	path.addRect(body);
	return path;
}

void WorldObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);
	QPen oldPen = painter->pen();
	QBrush oldBrush = painter->brush();
	updatePainterUtils(painter, option);
	if(levelOfDetail<0.2)
	{
		painter->setPen(outlinePen);
		painter->setBrush(backgroundBrush);
		if (option->state & QStyle::State_Selected)
			painter->drawRect(body);
		else
			painter->fillRect(body, backgroundColor);
		painter->setPen(oldPen);
		painter->setBrush(oldBrush);
		return;
	}

	painter->setPen(outlinePen);
	painter->setBrush(backgroundBrush);
	painter->drawRect(body);
	painter->setPen(oldPen);
	painter->setBrush(oldBrush);

	if (levelOfDetail>0.8)
	{ // Draw text
		QFont font("Courier", 7);
		painter->setFont(font);
		painter->drawText(4, 12, QStringLiteral("%1: %2").arg(hostInst.uid, ip));
		painter->drawText(4, 21, itext);
		painter->drawText(4, 30, iitext);
		for(int i=0; i<currentConf.sourceNodes.size(); ++i)
		{
			switch(currentConf.states[i])
			{
			case SourceState::NONE:
				painter->setPen(Qt::black);
				break;
			case SourceState::NONODE:
				painter->setPen(Qt::red);
				break;
			case SourceState::NOSRV:
				painter->setPen(Qt::darkRed);
				break;
			case SourceState::OK:
				painter->setPen(Qt::darkGreen);
				break;
			}
			painter->drawText(4, 6+objectHeaderHeight+i*10, currentConf.sourceNodes[i]);
		}
		painter->setPen(oldPen);
	}
}

void WorldObject::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act=menu->addAction(QIcon(":/res/arrow_rotate_clockwise.png"), tr("Test Server"));
	if(nc)
		act->setEnabled(false);
	connect(act, &QAction::triggered, this, &WorldObject::testSrvSl);
	act=menu->addAction(QIcon(":/res/deploy.png"), tr("Deploy to this server"));
	connect(act, &QAction::triggered, this, &WorldObject::deploySrvSl);
	/*for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
	{
		QString nodeTypeName=nit->second->typeName;
		QAction *act=menu->addAction(QIcon(":/res/ConfigEditor/icons8-add-node-48.png"), QString("Add node %1").arg(nodeTypeName));
		if(nodesOfType(nodeTypeName)<nit->second->nodeMax)
			connect(act, &QAction::triggered, this, [this, nodeTypeName](){addNodeSl(nodeTypeName);});
		else
			act->setEnabled(false);
	}
	QAction * act=menu->addAction(QIcon(":/res/star.png"),
		QStringLiteral("Edit %2").arg(EditorObjectMetadata::categoryToStr(md->category)));
	connect(act, &QAction::triggered, this, &EditorObject::editObjSl);
	act=menu->addAction(QIcon(":/res/cross_red.png"), tr("Delete"));
	WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
	connect(act, &QAction::triggered, ce, [ce, this](){ce->deleteObjSl(id);});
	//act->setEnabled(false);*/
	menu->popup(event->screenPos());
}

QVariant WorldObject::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change==ItemPositionHasChanged)
	{
		wasMoved=true;
		//qDebug()<<"EditorObject::itemChange"<<value.toPointF();
	}
	return QGraphicsItem::itemChange(change, value);
}

void WorldObject::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	qDebug()<<"obj dblclk at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseDoubleClickEvent(event);
	//if(event->button()==Qt::LeftButton)
	//	; //editObjSl();
}

void WorldObject::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj press at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mousePressEvent(event);
	wasMoved=false;
	//update();
}

void WorldObject::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	/*if (event->modifiers() & Qt::ShiftModifier) {
        stuff << event->pos();
        update();
        return;
	}*/
    QGraphicsItem::mouseMoveEvent(event);
}

void WorldObject::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj release at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseReleaseEvent(event);
	if(wasMoved)
	{
		wasMoved=false;
		QPointF p=scenePos();
		setPos(floor((p.x()+8)/16)*16, floor((p.y()+8)/16)*16);
		emit objectReposSig(hostInst.uid);
/*		emit changedSig();
		auto nds=childItems();
		foreach(QGraphicsItem * nd, nds)
		{
			EditorNode *eond=dynamic_cast<EditorNode *>(nd);
			if(eond)
			{
				//qDebug()<<"EditorObject connectionReposSig"<<id<<eond->getChannel()<<eond->scenePos()<<eond->getConOffset();
				emit connectionReposSig(eond);
			}
		}*/
	}
	//update();
}

void WorldObject::setupGraphic()
{
	switch(currentConf.src)
	{
	case Source::NONE:
		baseColor=QColor(170, 180, 170);
		itext=tr("uninitialized");
		break;
	case Source::LOC:
		baseColor=QColor(180, 220, 220);
		itext=tr("local");
		break;
	case Source::LOCRAW:
		baseColor=QColor(180, 220, 220);
		itext=tr("RAW local");
		break;
	case Source::LOCMISS:
		baseColor=QColor(255, 200, 180);
		itext=tr("NO local config");
		break;
	case Source::REMUPT:
		baseColor=QColor(200, 255, 180);
		itext=tr("current");
		break;
	case Source::REMRAWUPT:
		baseColor=QColor(180, 255, 230);
		itext=tr("RAW current");
		break;
	case Source::REMOTD:
		baseColor=QColor(255, 255, 100);
		itext=tr("outdated");
		break;
	case Source::REMRAWOTD:
		baseColor=QColor(255, 255, 190);
		itext=tr("RAW outdated");
		break;
	}
	backgroundColor=baseColor;
	backgroundBrush=QBrush(backgroundColor);
	switch(bintst)
	{
	case BinTst::NOCHECK:
		outlinePen=QPen(Qt::black);
		break;
	case BinTst::UNKNOWN:
		outlinePen=QPen(Qt::darkGray);
		break;
	case BinTst::OUTDATED:
		outlinePen=QPen(Qt::red);
		outlinePen.setWidth(3);
		break;
	case BinTst::UPTODATE:
		outlinePen=QPen(Qt::green);
		outlinePen.setWidth(3);
		break;
	}
	prepareGeometryChange();
	body=QRectF(0.,0., WorldObject::objectWidth, WorldObject::objectHeaderHeight+10*currentConf.sourceNodes.size()); //+WorldObject::nodeHeight*nodeSectionHeightCnt);
	boundingBox=body.adjusted(-3.,-3.,3.,3.);
	//setToolTip("tooltip");
}

void WorldObject::updatePainterUtils(QPainter *painter, const QStyleOptionGraphicsItem *option)
{
	levelOfDetail=option->levelOfDetailFromTransform(painter->worldTransform());
	// (option->state & QStyle::State_Selected) ? color.dark(150) : color;
	if (option->state & QStyle::State_MouseOver)
		backgroundColor=baseColor.lighter(110);
	else
		backgroundColor=baseColor;
	backgroundBrush=QBrush(backgroundColor);
	if (option->state & QStyle::State_Selected)
		outlinePen.setStyle(Qt::DashLine);
	else
		outlinePen.setStyle(Qt::SolidLine);
}


void WorldObject::testSrvSl()
{
	if(nc)
	{
		QApplication::beep();
		return;
	}
	nodes.clear();
	iitext=tr("testing...");
	nc=new NetCon(QHostAddress(hostInst.hadr), hostInst.port, true, true);
	connect(nc, SIGNAL(errorSig(int)), this, SLOT(sockErrorSl(int)));
	connect(nc, SIGNAL(readySig(bool)), this, SLOT(sockReadySl(bool)));
	DCON.registerCmdTarget(QStringLiteral("contest"), this);
	nc->start();
	setupGraphic();
	update();
}

void WorldObject::sockErrorSl(int err)
{
	DCON.deregisterCmdTarget(this);
	if(!nc)
	{
		iitext=tr("nc?");
		return;
	}
	iitext=tr("E(%1) %2").arg(err).arg(nc->lastErrorStr());
	nc->deleteLater();
	nc=nullptr;
	setupGraphic();
	update();
}

void WorldObject::sockReadySl(bool ready)
{
	if(!nc)
	{
		iitext=tr("nc?");
		return;
	}
	if(ready)
	{
		iitext=tr("%1 %2").arg(nc->version(), nc->build());
		if(binTst.isEmpty())
			bintst=BinTst::NOCHECK;
		else
		{
			if(nc->build()==binTst)
				bintst=BinTst::UPTODATE;
			else
				bintst=BinTst::OUTDATED;
		}
		qDebug()<<"WorldObject netcon reply"<<(int)bintst<<nc->build()<<binTst;
		if(nc->configFlavor()==QStringLiteral("raw"))
		{
			currentConf=confRaw;
			if(nc->configTst()==currentConf.tst)
				currentConf.src=Source::REMRAWUPT;
			else
				currentConf.src=Source::REMRAWOTD;
		}
		else if(nc->configFlavor()==QStringLiteral("normal"))
		{
			currentConf=conf;
			qDebug()<<"WorldObject::sockReadySl"<<nc->address().toString()<<nc->configTst()<<currentConf.tst;
			if(nc->configTst()==currentConf.tst)
				currentConf.src=Source::REMUPT;
			else
				currentConf.src=Source::REMOTD;
		}
		else
		{
			qWarning()<<"WorldObject::sockReadySl host with old signature"<<nc->address().toString();
			iitext=tr("old signature?");
			currentConf=conf;
			currentConf.src=Source::NONE;
		}
	}
	nc->deleteLater();
	nc=nullptr;
	setupGraphic();
	update();
}

void WorldObject::deploySrvSl()
{
	DlgDeploy dlg(qobject_cast<QWidget *>(scene()->parent()), hostInst.uid);
	dlg.exec();
}


