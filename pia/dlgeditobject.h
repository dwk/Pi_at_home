#ifndef DLGEDITOBJECT_H
#define DLGEDITOBJECT_H

#include "piadh.h"
#include <QDialog>
#include <QGridLayout>
#include "editorobject.h"

namespace Ui
{
	class DlgEditObject;
}

namespace pi_at_home
{

class DlgEditObject : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEditObject(QWidget *parent, EditorObject & obj);
	virtual ~DlgEditObject();
public slots:
	virtual void accept();
private slots:
	void defOmChangedSl(int checkState, int row);
	void on_pushEditExt_clicked();
private:
	void extractValues(KeyValMV &targetKeyVals);
	Ui::DlgEditObject *ui;
	QGridLayout *grid;
	EditorObject & obj;
	KeyValMV currentKeyVals;
	int idRow;
};
}
#endif // DLGEDITOBJECT_H
