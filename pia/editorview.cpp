#include "editorview.h"
#include <QtWidgets>
#include <qmath.h>

EditorView::EditorView(QWidget *parent) : QGraphicsView(parent)
{
	setViewport(new QWidget);
	setRenderHint(QPainter::Antialiasing, false);
	setDragMode(QGraphicsView::RubberBandDrag);
	setOptimizationFlags(QGraphicsView::DontSavePainterState);
	setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
}

#ifndef QT_NO_WHEELEVENT
void EditorView::wheelEvent(QWheelEvent *e)
{
	if (e->modifiers() & Qt::ControlModifier)
	{
		if (e->angleDelta().y() > 0) // formerly delta
			zoom+=.5;
        else
			zoom-=.5;
        e->accept();
		transformMatrix();
	}
	else
	{
        QGraphicsView::wheelEvent(e);
    }
}
#endif

void EditorView::transformMatrix()
{
	qreal scale = qPow(qreal(2), zoom);
	//QMatrix matrix;
	QTransform matrix;
	matrix.scale(scale, scale);
	//setMatrix(matrix);
	setTransform(matrix);
}

