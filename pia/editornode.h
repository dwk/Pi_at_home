#ifndef EDITORNODE_H
#define EDITORNODE_H
#include "piadh.h"
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QIcon>
#include <QGraphicsItem>
#include <QDomElement>
#include "editorobjectmetadata.h"
#include "editordict.h"
#include "../piah_common/nodeaddress.h"

Q_DECLARE_LOGGING_CATEGORY(editornode)

namespace pi_at_home
{
class EditorObject;

struct NodeMarker
{
	NodeMarker(QRectF area, QColor col) : area(area), col(col) {}
	QRectF area;
	QColor col;
};
typedef std::map<int, NodeMarker> NodeMarkerMV;

class EditorNode : public QGraphicsItem
{
	friend class DlgEditNode;
public:
	static const int nodeHeight=16;
	static const int nodeWidth=60;
	static EditorNodeMetadata dummyMD;
	static bool isValidConnection(const EditorNode *en1, const EditorNode *en2, bool *conversionWarning=nullptr);
	enum Deco {DECO_NONE=0x0, DECO_ALIVE_IND=0x1, DECO_LOGGED=0x2};
	Q_DECLARE_FLAGS(Decos, Deco)
	EditorNode(const EditorNodeMetadata *metaData, int channel, QGraphicsItem * parent);
	EditorNode(QDomElement &elem, const EditorObjectMetadata *objectMetaData, pi_at_home::EditorDict *ed, QGraphicsItem * parent);
	EditorNode(QDomElement &elem, const EditorNodeMetadata *metaData, pi_at_home::EditorDict *ed, QGraphicsItem * parent);
	virtual ~EditorNode();
	EditorObject * getParentObject() const;
	bool hasSilentCon() const {return md->type==EditorNodeMetadata::Type::CONNECTOR;}
	virtual void updateXml(QDomElement &domParent, const EditorBaseConfig & bc);
	virtual void removeXml();
	QString getAlias() const;
	const QString & getNodeAlias() {return alias;}
	void setNodeAlias(const QString & newAlias) {alias=newAlias;}
	int getChannel() const {return channel;}
	void setChannel(int newChannel);
	int getParentId() const;
	NodeAddress getAddress() const;
	KeyValMV & getKeyVals() {return keyVals;}
	int getIndex() const {return index;}
	void setIndex(int index);
	EditorNode::Decos decoration() const {return decos;}
	void setDecoration(EditorNode::Decos decos);
	const QPointF & getConOffset() const {return conOffset;} // relative to own origin
	QPointF getHotSpot() const {return scenePos()+conOffset;} // absolute, = scene coordinates
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE {return body;}
	virtual QPainterPath shape() const Q_DECL_OVERRIDE {QPainterPath path; path.addRect(body); return path;}
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	void toDefaultState();
	const EditorNodeMetadata *md;
protected slots:
	void keyvalsChanged();
protected:
	void commonCtor(QDomElement &elem, EditorDict *ed);
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE; 
	virtual void setupGraphic();
	QDomElement elem;
	QString alias;
	int channel=0, index=0;
	KeyValMV keyVals;
	// graphic representation
	QRectF body;
	QPointF conOffset;
	QColor colHoverOk;
	EditorNode::Decos decos=DECO_NONE;
	NodeMarkerMV nodeMarkerMV;
	//QPen penHoverOk;
	int tempCon=0; // 1: user holds mousebutton to start a connection; 2: we are the source of an active rubber band
};
typedef EditorNode* (*EditorNodeFactoryDom)(QDomElement &elem, const EditorObjectMetadata *objectMetaData, EditorDict *ed, QGraphicsItem * parent);
typedef EditorNode* (*EditorNodeFactoryNew)(const EditorNodeMetadata *metaData, int channel, QGraphicsItem * parent);
Q_DECLARE_OPERATORS_FOR_FLAGS(EditorNode::Decos)

} // namespace pi_at_home

#endif // EDITORNODE_H
