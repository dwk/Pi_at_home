#include "dlgeditbaseconfig.h"
#include <QSpinBox>
#include <QCheckBox>
#include "piadi.h"
#include "ui_dlgeditbaseconfig.h"

DlgEditBaseConfig::DlgEditBaseConfig(QWidget *parent, pi_at_home::EditorBaseConfig &bc, const QStringList &ports) :
	QDialog(parent), ui(new Ui::DlgEditBaseConfig), bc(bc)
{
	ui->setupUi(this);
	ui->lineUid->setText(bc.uid);
	ui->spinHbMs->setValue(bc.heartbeatMs);
	ui->spinBroadcastMs->setValue(bc.broadcastMs);
	ui->comboAliveInd->addItem("");
	foreach(QString port, ports)
		ui->comboAliveInd->addItem(port);
	ui->comboAliveInd->setCurrentText(bc.aliveInd);
	ui->checkRawConfig->setChecked(bc.rawConfig);
	//ui->tableWidget->setColumnCount(3);
	ui->tableWidget->setRowCount(bc.staticHosts.size());
	for(int r=0; r<(int)bc.staticHosts.size(); ++r)
	{
		const QStringList sh=bc.staticHosts.at(r);
		if(sh.size()<3)
		{
			ui->tableWidget->setItem(r, 0, new QTableWidgetItem("invalid entry"));
			continue;
		}
		ui->tableWidget->setItem(r, 0, new QTableWidgetItem(sh.at(0)));
		ui->tableWidget->setItem(r, 1, new QTableWidgetItem(sh.at(1)));
		ui->tableWidget->setItem(r, 2, new QTableWidgetItem(sh.at(2)));
	}
	ui->tableWidget->resizeColumnsToContents();
	ui->tableWidget->resizeRowsToContents();
	on_tableWidget_itemSelectionChanged();
}

DlgEditBaseConfig::~DlgEditBaseConfig() {delete ui;}

void DlgEditBaseConfig::on_tableWidget_itemSelectionChanged()
{
	QList<QTableWidgetItem *> sels=ui->tableWidget->selectedItems();
	if(!sels.size())
	{
		ui->pushDelete->setEnabled(false);
	}
	else
	{
		ui->pushDelete->setEnabled(true);
	}
}

void DlgEditBaseConfig::on_pushAdd_clicked()
{
	int rc=ui->tableWidget->rowCount();
	ui->tableWidget->setRowCount(rc+1);
	ui->tableWidget->setItem(rc, 0, new QTableWidgetItem(tr("newUID")));
	ui->tableWidget->setItem(rc, 1, new QTableWidgetItem(tr("0.0.0.0")));
	ui->tableWidget->setItem(rc, 2, new QTableWidgetItem(tr("80")));
	ui->tableWidget->resizeColumnsToContents();
}

void DlgEditBaseConfig::on_pushDelete_clicked()
{
	QList<QTableWidgetItem *> sels=ui->tableWidget->selectedItems();
	if(!sels.size())
	{
		on_tableWidget_itemSelectionChanged();
		return;
	}
	ui->tableWidget->clearSelection();
	ui->tableWidget->removeRow(sels.at(0)->row());
}

void DlgEditBaseConfig::accept()
{
	bc.uid=ui->lineUid->text();
	bc.heartbeatMs=ui->spinHbMs->value();
	bc.broadcastMs=ui->spinBroadcastMs->value();
	bc.aliveInd=ui->comboAliveInd->currentText();
	bc.rawConfig=ui->checkRawConfig->isChecked();
	int rc=ui->tableWidget->rowCount();
	int hc=(int)bc.staticHosts.size();
	bool *ticks=new bool[hc];
	for(int hi=0; hi<hc; ++hi)
		ticks[hi]=false;
	for(int r=0; r<rc; ++r)
	{
		QString h=ui->tableWidget->item(r,0)->data(Qt::DisplayRole).toString();
		bool updated=false;
		for(int hi=0; hi<hc; ++hi)
		{
			if(bc.staticHosts.at(hi).at(0)==h)
			{
				bc.staticHosts.at(hi)[1]=ui->tableWidget->item(r,1)->data(Qt::DisplayRole).toString();
				bc.staticHosts.at(hi)[2]=ui->tableWidget->item(r,2)->data(Qt::DisplayRole).toString();
				updated=true;
				ticks[hi]=true;
				break;
			}
		}
		if(!updated)
		{
			QStringList sl;
			sl<<h;
			sl<<ui->tableWidget->item(r,1)->data(Qt::DisplayRole).toString();
			sl<<ui->tableWidget->item(r,2)->data(Qt::DisplayRole).toString();
			bc.staticHosts.push_back(sl);
		}
	}
	for(int hi=hc-1; hi>=0; --hi)
	{
		if(!ticks[hi])
			bc.staticHosts.erase(bc.staticHosts.begin()+hi);
	}
	delete[] ticks;
	QDialog::accept();
}

