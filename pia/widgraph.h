#ifndef WIDGRAPH_H
#define WIDGRAPH_H

#include "piadh.h"
#include <list>
#include <QWidget>
#include "widactor.h"
#include "../piah_common/nodeaddress.h"
#include "../qcustomplot/qcustomplot.h"

class QListWidget;
namespace Ui {
class WidGraph;
}

namespace pi_at_home
{
struct Scaler
{
	void initX(const double & x) {vx=false; xmin=x; xmax=x;}
	void initY(const double & y, bool left) {if(left) {vy1=false; y1min=y; y1max=y;} else {vy2=false; y2min=y; y2max=y;}}
	void updateX(const double & x) {if(std::isnan(x)) return; if(vx) {initX(x); return;} if(x<xmin) xmin=x; else if(x>xmax) xmax=x;}
	void updateY(const double & y, bool left)
	{
		if(std::isnan(y))
			return;
		if(left)
		{
			if(vy1)
			{
				initY(y, left);
				return;
			}
			if(y<y1min)
				y1min=y;
			else if(y>y1max)
				y1max=y;
		}
		else
		{
			if(vy2)
			{
				initY(y, left);
				return;
			}
			if(y<y2min)
				y2min=y;
			else if(y>y2max)
				y2max=y;
		}
	}
	double xmin=0., xmax=0., y1min=0., y1max=0., y2min=0., y2max=0.;
	bool vx=true, vy1=true, vy2=true;
};

class WidGraph : public QWidget, public WidActor
{
	Q_OBJECT
public:
	typedef std::map <NodeAddress , bool> DispMap;
	explicit WidGraph(QWidget *parent = 0);
	~WidGraph();
	virtual void connectAll(QObjectList & otherActors);
public slots:
private slots:
	void fillCombo();
	void pushUpdateGraph();
	void pushConfigureSl();
	void addPlot(const pi_at_home::NodeAddress &adr, bool leftAxis, Scaler &scaler);
	void selectedSomethingSl();
	void selectAllSl(DispMap & dispMap);
	void selectFavSl(DispMap & dispMap);
	void selectDlgSl(DispMap & dispMap);
	void xStyleChangeSl(int index);
	void xRangeChangedSl(const QCPRange &newRange);
private:
	void comboChanged(QVariant vnamelist, DispMap & dispMap);
	void activateGraph();
	Ui::WidGraph *ui;
	DispMap leftMap, rightMap;
};
}

#endif // WIDGRAPH_H
