#ifndef AVNODEBITPORT_H
#define AVNODEBITPORT_H

#include "piadh.h"
#include "avnode.h"
#include "../piah_common/nodebitport.h"

namespace pi_at_home
{

class AvNodeBitPort : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeBitPort(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeBitPort() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeBool(text);}
	QCheckBox * widVal;
	//bool val;
protected slots:
	void valChanged(bool val);
};
}

#endif // AVNODEBITPORT_H
