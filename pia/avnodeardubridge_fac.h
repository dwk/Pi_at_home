#ifndef AVNODEARDUBRIDGE_FAC_H
#define AVNODEARDUBRIDGE_FAC_H

#include "../piah_common/nodeardubridge_nm.h"

namespace
{
pi_at_home::AvNode * AvCreatorStr(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeArduBridge(frag, guiParent);
}
bool avatarStrRegistered=pi_at_home::AvatarFactory::registerAvatar(typeName, AvCreatorStr);
}

#endif // AVNODEARDUBRIDGE_FAC_H
