#ifndef DLGDATA_H
#define DLGDATA_H

#include "piadh.h"
#include <QDialog>
#include "../piah_common/datahistory.h"
#include "datahistoryextgui.h"

namespace Ui
{
	class DlgData;
}

namespace pi_at_home
{

class DlgData : public QDialog
{
	Q_OBJECT
public:
	explicit DlgData(QWidget *parent, DataHistory & data);
	~DlgData();
public slots:
	virtual void accept();
private slots:
	void selectColorSl();
	void updatePreviewSl();
private:
	Ui::DlgData *ui;
	DataHistory & data;
	DataHistoryExtGui * dhExt;
	QColor newColor;
};
}
#endif // DLGDATA_H
