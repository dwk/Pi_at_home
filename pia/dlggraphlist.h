#ifndef DLGGRAPHLIST_H
#define DLGGRAPHLIST_H

#include "piadh.h"
#include <QDialog>
#include <QSignalMapper>
#include "widgraph.h"

//#include "../piah_common/datahistory.h"

namespace Ui
{
	class DlgGraphList;
}

namespace pi_at_home
{

class DlgGraphList : public QDialog
{
	Q_OBJECT
public:
	explicit DlgGraphList(QWidget *parent, WidGraph::DispMap & dispMap);
	~DlgGraphList();
public slots:
	virtual void accept();
private slots:
	void clicked(QString adrStr);
private:
	Ui::DlgGraphList *ui;
	WidGraph::DispMap &dispMap;
	QSignalMapper *mapper;
};
}
#endif // DLGGRAPHLIST_H
