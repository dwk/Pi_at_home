#ifndef AVNODESWITCHPORT_FAC_H
#define AVNODESWITCHPORT_FAC_H
#include "../piah_common/nodeswitchport_nm.h"

namespace
{
pi_at_home::AvNode * AvCreator(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeSwitchPort(frag, guiParent);
}
bool avatarRegistered=pi_at_home::AvatarFactory::registerAvatar(typeName, AvCreator);
}

#endif // AVNODESWITCHPORT_FAC_H
