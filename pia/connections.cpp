#include "connections.h"
#include <QHeaderView>
#include <QMenu>
#include "mainwindow.h"
#include "piadi.h"

Connections::Connections(QObject *parent, pi_at_home::MainWindow &mwin) :
	QObject(parent)
{
	actCloseAllConnections=mwin.getAction("action_CloseAllConnections");
	connect(actCloseAllConnections, SIGNAL(triggered(bool)), this, SLOT(closeAllHosts()));
	actCleanupConnectionList=mwin.getAction("action_CleanupConnectionList");
	connect(actCleanupConnectionList, SIGNAL(triggered(bool)), this, SLOT(cleanupList()));
	actCloseHost=mwin.getAction("action_CloseConnection");
	actCloseHost->setEnabled(false);
	connect(actCloseHost, SIGNAL(triggered(bool)), this, SLOT(closeHost()));
	actionAutoClose=mwin.getAction("action_AutoRemoveHosts");
	// enum class ServerCmd{NOP=0, END_PROCESS, STOP_SERVICE, RESTART_SERVICE, SHUTDOWN_MACHINE, RESTART_MACHINE};
	actEndProcess=mwin.getAction("svrcmdEndProcess");
	if(actEndProcess)
	{
		actEndProcess->setEnabled(false);
		connect(actEndProcess, &QAction::triggered, this, [this](){srvcmdSl(1);});
	}
	else
		qCritical()<<"svrcmdEndProcess not available";
	actStopService=mwin.getAction("svrcmdStopService");
	if(actStopService)
	{
		actStopService->setEnabled(false);
		connect(actStopService, &QAction::triggered, this, [this](){srvcmdSl(2);});
	}
	else
		qCritical()<<"svrcmdStopService not available";
	actRestartService=mwin.getAction("svrcmdRestartService");
	if(actRestartService)
	{
		actRestartService->setEnabled(false);
		connect(actRestartService, &QAction::triggered, this, [this](){srvcmdSl(3);});
	}
	else
		qCritical()<<"svrcmdRestartService not available";
	actShutdownMachine=mwin.getAction("svrcmdShutdownMachine");
	if(actShutdownMachine)
	{
		actShutdownMachine->setEnabled(false);
		connect(actShutdownMachine, &QAction::triggered, this, [this](){srvcmdSl(4);});
	}
	else
		qCritical()<<"svrcmdShutdownMachine not available";
	actRestartMachine=mwin.getAction("svrcmdRestartMachine");
	if(actRestartMachine)
	{
		actRestartMachine->setEnabled(false);
		connect(actRestartMachine, &QAction::triggered, this, [this](){srvcmdSl(5);});
	}
	else
		qCritical()<<"svrcmdRestartMachine not available";
}

Connections::~Connections()
{
}

QWidget *Connections::widget(QWidget *guiParent)
{
	if(wid)
		return wid;
	wid=new QTableWidget(guiParent);
	wid->setColumnCount(5);
	wid->setRowCount(0);
	wid->setHorizontalHeaderLabels(QStringList()<<tr("Host")<<tr("Address")<<tr("Version")<<tr("Flavor")<<tr("State"));
	wid->verticalHeader()->hide();
	wid->setSelectionBehavior(QAbstractItemView::SelectRows);
	wid->resizeColumnsToContents();
	wid->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(wid, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenu(QPoint)));
	connect(wid, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));
	return wid;
}

NetCon *Connections::hostCon(const QString &uid)
{
	NetConMSP::iterator it=hostConMP_.find(uid);
	if(it==hostConMP_.end())
		return nullptr;
	return it->second;
}

bool Connections::addHost(const QString &host, int port)
{
	NetCon *hc=new NetCon(QHostAddress(host), port);
	QString adrStr=hc->address().toString()+":"+QString::number(hc->port());
	pair<NetConMSP::iterator,bool> res=hostConMP_.insert(NetConMSP::value_type(adrStr, hc));
	if(!res.second)
	{
		QMessageBox::critical(qApp->activeWindow(), tr("Duplicate Host Address"), tr("The host address <%1> is already used. Connection closed.").arg(adrStr));
		delete hc;
		return false;
	}
	int newRow=wid->rowCount();
	wid->insertRow(newRow);
	wid->setItem(newRow, 0, new QTableWidgetItem(""));
	wid->setItem(newRow, 1, new QTableWidgetItem(adrStr));
	wid->setItem(newRow, 2, new QTableWidgetItem(""));
	wid->setItem(newRow, 3, new QTableWidgetItem(""));
	wid->setItem(newRow, 4, new QTableWidgetItem(QIcon("://res/hourglass.png"), ""));
	wid->resizeRowsToContents();
	emit connectionListChanged();
	connect(hc, SIGNAL(errorSig(int)), this, SLOT(sockErrorSig(int)));
	connect(hc, SIGNAL(readySig(bool)), this, SLOT(sockReady(bool)));
	hc->start();
	return true;
}

void Connections::closeHost()
{
	int row=wid->currentRow();
	if(row<0)
		return;
	QString uid=wid->item(row, 0)->text();
	NetConMSP::iterator it=hostConMP_.find(uid);
	if(it==hostConMP_.end())
		return;
	it->second->stop();
	emit connectionListChanged();
}

void Connections::closeAllHosts()
{
	std::vector< NetCon * > h;
	for(NetConMSP::iterator it=hostConMP_.begin(); it!=hostConMP_.end(); ++it)
		h.push_back(it->second);
	for(auto it=h.begin(); it!=h.end(); ++it)
		if(*it)
			(*it)->stop();
	emit connectionListChanged();
}

void Connections::srvcmdSl(int srvcmd)
{
	int row=wid->currentRow();
	if(row<0)
		return;
	QString uid=wid->item(row, 0)->text();
	QString msg;
	// enum class ServerCmd{NOP=0, END_PROCESS, STOP_SERVICE, RESTART_SERVICE, SHUTDOWN_MACHINE, RESTART_MACHINE};
	switch(srvcmd)
	{
	case 0:
		break;
	case 1:
		msg=tr("This will end the server process on the host %1. If run as service systemd will try to restart. Proceed?").arg(uid);
		break;
	case 2:
		msg=tr("This will stop the server process on the host %1 using systemd. Proceed?").arg(uid);
		break;
	case 3:
		msg=tr("This will restart the server process on the host %1 using systemd. Proceed?").arg(uid);
		break;
	case 4:
		msg=tr("This will SHUTDOWN the host %1. You will have to power-cycle to restart. Proceed?").arg(uid);
		break;
	case 5:
		msg=tr("This will RESTART the host %1. Proceed?").arg(uid);
		break;
	default: // unknown command or NOP
		return;
	}
	if(QMessageBox::warning(nullptr, tr("Server Command"), msg, QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
		return;
	qWarning()<<"server command"<<uid<<srvcmd;
	Command *cmd=new Command("srvcmd");
	cmd->setAttribute(QStringLiteral("cmdid"), QString::number(srvcmd));
	cmd->setDestination(NodeAddress(uid, 0, 0));
	DCON.dispatchCommand(*cmd);
}

void Connections::cleanupList()
{
	bool doSignal=false;
	for(NetConMSP::iterator it=hostConMP_.begin(); it!=hostConMP_.end(); )
	{
		if(it->second->state()==QAbstractSocket::UnconnectedState)
		{
			NetConMSP::iterator delit=it;
			NetCon *ncp=delit->second;
			++it;
			int r=rowByHc(ncp);
			if(!hostConMP_.erase(ncp->uid()))
				hostConMP_.erase(ncp->address().toString()+":"+QString::number(ncp->port())); // was still connecting
			wid->removeRow(r);
			ncp->deleteLater();
			doSignal=true;
		}
		else
			++it;
	}
	if(doSignal)
		emit connectionListChanged();
}

void Connections::sockErrorSig(int errNo)
{
	Q_UNUSED(errNo);
	int myrow=rowByHc(dynamic_cast<NetCon * >(sender()));
	if(myrow<0)
		return;
	wid->item(myrow, 4)->setIcon(QIcon("://res/exclamation.png"));
}

void Connections::sockReady(bool ready)
{
	NetCon * hc=dynamic_cast<NetCon * >(sender());
	int myrow=rowByHc(hc);
	if(myrow<0)
		return;
	if(ready)
	{
		DCON.hostDict().confirm(hc->uid(), hc->address(), hc->port());
		hostConMP_.erase(hc->address().toString()+":"+QString::number(hc->port()));
		pair<NetConMSP::iterator,bool> res=hostConMP_.insert(NetConMSP::value_type(hc->uid(), hc));
		if(!res.second)
		{
			QMessageBox::critical(qApp->activeWindow(), tr("Duplicate Host UID"), tr("The host UID <%1> is already used. Connection to %2 closed.").arg(hc->uid(), hc->address().toString()));
			// list removal by sockReady(false) from the dtor would hit the wrong host - the one already open.
			disconnect(hc, 0, this, 0);
			myrow=rowByHc(hc, true);
			wid->removeRow(myrow);
			hc->deleteLater();
			emit connectionListChanged();
			return;
		}
		//
		wid->item(myrow, 0)->setText(hc->uid());
		wid->item(myrow, 2)->setText(hc->version());
		wid->item(myrow, 3)->setText(hc->configFlavor());
		wid->item(myrow, 4)->setIcon(QIcon("://res/accept.png"));
		wid->resizeColumnsToContents();
		emit connectionNameChanged(hc->address().toString(), hc->uid());
	}
	else
	{
		if(actionAutoClose->isChecked())
		{
			if(!hostConMP_.erase(hc->uid()))
				hostConMP_.erase(hc->address().toString()+":"+QString::number(hc->port())); // was still connecting
			hc->deleteLater();
			wid->removeRow(myrow);
			emit connectionListChanged();
		}
		else
			wid->item(myrow, 4)->setIcon(QIcon("://res/cross.png"));
	}
}

void Connections::contextMenu(QPoint pos)
{
	QTableWidgetItem *twi=wid->itemAt(pos);
	if(!twi)
		return;
	qDebug()<<"contextMenu"<<pos<<twi->row()<<twi->column();
	QMenu *menu=new QMenu(wid);
	if(actCloseHost)
		menu->addAction(actCloseHost);
	if(actEndProcess)
		menu->addAction(actEndProcess);
	if(actStopService)
		menu->addAction(actStopService);
	if(actRestartService)
		menu->addAction(actRestartService);
	if(actShutdownMachine)
		menu->addAction(actShutdownMachine);
	if(actRestartMachine)
		menu->addAction(actRestartMachine);
	menu->popup(wid->viewport()->mapToGlobal(pos));
}

void Connections::itemSelectionChanged()
{
	bool ena=wid->selectedItems().size();
	if(actCloseHost)
		actCloseHost->setEnabled(ena);
	if(actEndProcess)
		actEndProcess->setEnabled(ena);
	if(actStopService)
		actStopService->setEnabled(ena);
	if(actRestartService)
		actRestartService->setEnabled(ena);
	if(actShutdownMachine)
		actShutdownMachine->setEnabled(ena);
	if(actRestartMachine)
		actRestartMachine->setEnabled(ena);
}

int Connections::rowByHc(NetCon * hc, bool useAddress)
{
	if(!hc)
	{
		qDebug()<<"!!!Connections::rowByHc sender not a HostCon!";
		return -1;
	}
	QList<QTableWidgetItem *> liu=wid->findItems(hc->uid(), Qt::MatchFixedString);
	if(useAddress || !liu.size())
	{
		QString adr=hc->address().toString()+":"+QString::number(hc->port());
		QList<QTableWidgetItem *> lia=wid->findItems(adr, Qt::MatchFixedString);
		if(!lia.size())
		{
			qDebug()<<"!!!Connections::rowByHc cannot find row for "<<hc->uid()<<" / "<<adr;
			return -1;
		}
		if(useAddress)
		{
			foreach(QTableWidgetItem * twi, lia)
			{
				if(wid->item(twi->row(), 0)->text().isEmpty())
					return twi->row();
			}
		}
		return lia.front()->row();
	}
	return liu.front()->row();
}
