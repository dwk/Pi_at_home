#ifndef AVNODEDS1820_H
#define AVNODEDS1820_H

#include "piadh.h"
#include "avnode.h"
#include "../piah_common/nodeds1820.h"
#include <QDoubleSpinBox>

namespace pi_at_home
{
class AvNodeDS1820 : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeDS1820(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeDS1820() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeDouble(text);}
	QDoubleSpinBox * widVal;
	//double val;
protected slots:
	void valChangedSl(double val);
};
}

#endif // AVNODEDS1820_H
