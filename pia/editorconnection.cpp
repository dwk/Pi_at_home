#include "editorconnection.h"
#include <QtWidgets>
#include "editordict.h"
#include "dlgeditnode.h"
#include "dlgeditobject.h"
#include "widconfigeditor.h"
#include "piadi.h"

Q_LOGGING_CATEGORY(editorconnection, "editorconnection")

QDomElement EditorConnection::domDummy;

bool EditorConnection::ConKey::operator<(const EditorConnection::ConKey &o) const
{
	if(srcId==o.srcId)
	{
		if(srcCh==o.srcCh)
		{
			if(destId==o.destId)
			{
				if(destCh==o.destCh)
				{
					return false;
				}
				else
					return destCh<o.destCh;
			}
			else
				return destId<o.destId;
		}
		else
			return srcCh<o.srcCh;
	}
	else
		return srcId<o.srcId;
}

EditorConnection::EditorConnection(pi_at_home::EditorNode *source, const QPointF & sceneStart, pi_at_home::EditorNode *target, const QPointF & sceneEnd) :
	backpen(Qt::white), elem(domDummy), source(source), dest(target)
{
	backpen.setWidth(4);
	healthy=true;
	setPos(sceneStart);
	setFlags(ItemIsSelectable);
    setAcceptHoverEvents(true);
	setZValue(1000000.); // like always on top
	setupGraphic(sceneEnd-sceneStart);
	// wouldn't work - not connected yet
	//emit connectionStateSig(source.device(), source.channel(), 1);
	//emit connectionStateSig(target.device(), target.channel(), 1);
}

EditorConnection::EditorConnection(QDomElement &elem, pi_at_home::EditorDict *editorDict) :
	backpen(Qt::white), elem(elem)
{
	backpen.setWidth(4);
	setFlags(ItemIsSelectable);
	setAcceptHoverEvents(true);
	setZValue(1000000.); // like always on top
	QString srcStr, destStr;
	QDomNamedNodeMap attrs=elem.attributes();
	for(int i=0; i<attrs.size(); ++i)
	{
		QDomAttr attr=attrs.item(i).toAttr();
		QString aname=attr.name();
		QString v=attr.value();
		if(aname=="src")
			srcStr=v;
		else if(aname=="dest")
			destStr=v;
	}
	QDomNodeList markupList=elem.elementsByTagName("editor-hints");
	for(int i=0; i<markupList.size(); ++i)
	{
		readHints(markupList.item(i).toElement());
	}
	source=editorDict->getNode(srcStr);
	dest=editorDict->getNode(destStr);
	if(source && dest)
	{
		healthy=true;
		setPos(source->getHotSpot());
		setupGraphic(dest->getHotSpot()-source->getHotSpot());
	}
	else
	{
		qCritical()<<"EditorConnection Node not found"<<srcStr<<(source?source->getAddress().toString():"NULL")<<destStr<<(dest?dest->getAddress().toString():"NULL");
		setupGraphic(QPointF(0.,0.));
	}
}

EditorConnection::EditorConnection(const pi_at_home::EditorConnection::ConnectorInfo &info, pi_at_home::EditorDict *editorDict) :
	backpen(Qt::white)
{
	backpen.setWidth(4);
	setFlags(ItemIsSelectable);
	setAcceptHoverEvents(true);
	setZValue(1000000.); // like always on top
	source=editorDict->getNode(info.srcStr);
	dest=editorDict->getNode(info.destStr);
	readHints(info.hints);
	if(source && dest)
	{
		healthy=true;
		setPos(source->getHotSpot());
		setupGraphic(dest->getHotSpot()-source->getHotSpot());
	}
	else
	{
		qCritical()<<"EditorConnection Node not found"<<info.srcStr<<(source?source->getAddress().toString():"NULL")<<info.destStr<<(dest?dest->getAddress().toString():"NULL");
		setupGraphic(QPointF(0.,0.));
	}
}

EditorConnection::~EditorConnection()
{
	removeXml();
	elem.clear();
}

EditorConnection::ConKey EditorConnection::getKey() const
{
	return ConKey(source->getParentObject()->getId(), source->getChannel(), dest->getParentObject()->getId(), dest->getChannel());
}

void EditorConnection::updateXml(QDomElement &domParent, const EditorBaseConfig &bc)
{
	if(!healthy)
		return;
	if(source->hasSilentCon() || dest->hasSilentCon())
		return;
	if(elem.isNull())
	{
		elem=domParent.ownerDocument().createElement("con");
		domParent.appendChild(elem);
	}
	NodeAddress nas=source->getAddress();
	if(nas.uid().isEmpty())
		nas.setUid(bc.uid);
	NodeAddress nad=dest->getAddress();
	if(nad.uid().isEmpty())
		nad.setUid(bc.uid);
	elem.setAttribute("src", nas.toString());
	elem.setAttribute("dest", nad.toString());
	QDomNode eh=elem.firstChild();
	QDomElement ehe;
	for(; ehe.isNull() && !eh.isNull(); eh=eh.nextSibling())
	{
		if(eh.isElement() && eh.nodeName()=="editor-hints")
			ehe=eh.toElement();
	}
	if(ehe.isNull())
	{
		ehe=domParent.ownerDocument().createElement("editor-hints");
		elem.appendChild(ehe);
	}
	updateHints(ehe);
}

void EditorConnection::removeXml()
{
	qCDebug(editorconnection)<<"removeXml";
	if(!healthy ||elem.isNull())
		return;
	QDomNode p=elem.parentNode();
	p.removeChild(elem);
	//elem.setTagName("DELETED");
	elem.clear();
}

void EditorConnection::readHints(const QDomElement &editorHint)
{
	if(editorHint.isNull())
		return;
	if(editorHint.hasAttribute("center"))
	{
		bool ok=false;
		center=editorHint.attribute("center").toDouble(&ok);
		if(!ok)
			center=0.5;
	}
}

void EditorConnection::updateHints(QDomElement &editorHint)
{
	// <editor-hints center="0.5" />
	editorHint.setAttribute("center", QString::number(center));
}

QRectF EditorConnection::boundingRect() const
{
	if(tbody.size())
		return tbody.boundingRect();
	return boundingShape.boundingRect();
}

QPainterPath EditorConnection::shape() const
{
	return boundingShape;
}

void EditorConnection::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);
	QPen oldPen = painter->pen();
	updatePainterUtils(painter, option);

	if(tbody.size())
	{
		painter->setPen(outlinePen);
		painter->drawPolyline(tbody);
	}
	else
	{
		painter->setPen(backpen);
		painter->drawPolyline(body);
		painter->setPen(outlinePen);
		painter->drawPolyline(body);
		painter->drawEllipse(body.at(0), 3. ,3.);
		painter->drawPolyline(arrowHead);
	}
	painter->setPen(oldPen);
}

QString EditorConnection::getName() const
{
	QString templ("(%1 -> %2)");
	return templ.arg(source?source->getAlias():QStringLiteral("?"), dest?dest->getAlias():QStringLiteral("?"));
}

void EditorConnection::reposSl(void * node)
{
	EditorNode *nd=reinterpret_cast<EditorNode *>(node);
	QPointF newScPos=nd->getHotSpot();
	if(source==nd)
	{
		prepareGeometryChange();
		QPointF np2=body.last()+pos()-newScPos;
		setPos(newScPos);
		setupGraphic(np2);
		update();
	}
	else if(dest==nd)
	{
		prepareGeometryChange();
		setupGraphic(newScPos-pos());
		update();
	}
	//else quite annoying - repos sig goes to all connections of this object but only one will match
	//	qWarning()<<"EditorConnection::reposSl"<<getName()<<"irrelevant node"<<nd->getAddress().toStringVerbose();
}

void EditorConnection::deleteSl()
{
	//actually noone really cares...
	//emit connectionStateSig(source, 0);
	//emit connectionStateSig(dest, 0);
	deleteLater();
}

void EditorConnection::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	if(!healthy)
		return;
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act;//=menu->addAction(QIcon(":/res/star.png"), QStringLiteral("Edit connection"));
	//connect(act, &QAction::triggered, this, &EditorObject::editObjSl);
	act=menu->addAction(QIcon(":/res/cross_red.png"), tr("Delete"));
	connect(act, &QAction::triggered, this, &EditorConnection::deleteSl);
	WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
	connect(act, &QAction::triggered, ce, [ce, this](){ce->deleteConSl(getKey());});
	menu->popup(event->screenPos());
}

void EditorConnection::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	qCDebug(editorconnection)<<"obj dblclk at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseDoubleClickEvent(event);
	//if(event->button()==Qt::LeftButton)
	//	editObjSl();
}

void EditorConnection::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	//qCDebug(editorconnection)<<"mouseMoveEvent"<<(int)event->button()<<event->scenePos();
	// if(event->button()==Qt::LeftButton) seems to be not set
	if(tbody.size()<3)
		return;
	int centerOffset=event->pos().x()-event->buttonDownPos(Qt::LeftButton).x();
	const QPointF &e=body.last();
	tbody[1]=QPointF(e.x()*center+centerOffset,0.);
	tbody[2]=QPointF(e.x()*center+centerOffset,e.y());
	update();
	QGraphicsItem::mouseMoveEvent(event);
}

void EditorConnection::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	qCDebug(editorconnection)<<"obj press at"<<(int)event->button()<<event->scenePos();
	if(event->button()==Qt::LeftButton)
		tbody=body;
	QGraphicsItem::mousePressEvent(event);
	//update();
}

void EditorConnection::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	qCDebug(editorconnection)<<"obj release at"<<(int)event->button()<<event->scenePos();
	if(event->button()==Qt::LeftButton)
	{
		if(tbody.size()>=4)
			center=tbody.at(1).x()/tbody.at(3).x();
		tbody.clear();
		setupGraphic(body.last());
		update();
		emit changedSig();
	}
	QGraphicsItem::mouseReleaseEvent(event);
}

#define SELW 5
void EditorConnection::setupGraphic(const QPointF endp)
{
	body.clear();
	body<<QPointF(0.,0.)<<QPointF(endp.x()*center,0.)<<QPointF(endp.x()*center,endp.y())<<endp;
	outlinePen=QPen(Qt::black);
	boundingShape=QPainterPath();
	boundingShape.addRect(QRectF(body.at(0)-QPointF(10.,10.), body.at(0)+QPointF(10.,10.)));
	boundingShape.addRect(QRectF(body.at(0), body.at(1)).normalized().adjusted(-SELW, -SELW, SELW, SELW));
	boundingShape.addRect(QRectF(body.at(1), body.at(2)).normalized().adjusted(-SELW, -SELW, SELW, SELW));
	boundingShape.addRect(QRectF(body.at(2), body.at(3)).normalized().adjusted(-SELW, -SELW, SELW, SELW));
	boundingShape.addRect(QRectF(body.at(3)-QPointF(10.,10.), body.at(3)+QPointF(10.,10.)));
	arrowHead.clear();
	QPointF dir=endp-body.at(2);
	double len=sqrt(dir.x()*dir.x()+dir.y()*dir.y());
	if(fabs(len)<0.0001)
		len=1.;
	dir.rx()*=5./len;
	dir.ry()*=5./len;
	QPointF dirw(dir.y(), -dir.x());
	dirw.rx()/=2.;
	dirw.ry()/=2.;
	QPointF p=endp;
	arrowHead<<p;
	p=p-dir-dirw;
	arrowHead<<p;
	p=p+2.*dirw;
	arrowHead<<p;
	arrowHead<<endp;
	//setToolTip(md->description);
}

QStringList EditorConnection::checkConsistency()
{
	QStringList problems;
	return problems;
}

void EditorConnection::updatePainterUtils(QPainter *painter, const QStyleOptionGraphicsItem *option)
{
	levelOfDetail=option->levelOfDetailFromTransform(painter->worldTransform());
	if(option->state & QStyle::State_Selected)
	{
		outlinePen=QPen(Qt::red);
	}
	else if(option->state & QStyle::State_MouseOver)
	{
		outlinePen=QPen(Qt::darkRed);
	}
	else
	{
		if(source->hasSilentCon() || dest->hasSilentCon())
			outlinePen=QPen(QColor(11,180,205));
		else
			outlinePen=QPen(Qt::black);
	}
	outlinePen.setWidth(2);
}

