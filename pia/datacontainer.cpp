#include "datacontainer.h"
#include <QNetworkInterface>
#include <QFileInfo>
#include <QProcess>
//#include "xmlconfig.h"
#include <time.h>
#include "piaapp.h"
#include "piadi.h"

DataContainer::DataContainer(QObject *parent) : QObject(parent), hostDict_(new HostDict(false, nullptr))
{
	//qDebug()<<" ";
}

DataContainer::~DataContainer()
{
	for(DataHistoryMP::iterator it=dataHistMP.begin(); it!=dataHistMP.end(); ++it)
		delete it->second;
}

void DataContainer::setup(const QString configFile, MainWindow &mwin)
{
	Q_UNUSED(configFile);
	PiaApp * a=dynamic_cast<PiaApp*>qApp;
	if(a)
	{
		if(!a->attachIpc())
			//qDebug()<<"You run more than one instance of the Pi@Home client!";
			QMessageBox::critical(nullptr, "Multiple Instances", "You run more than one instance of the Pi@Home client on your machine simultanously, which is an unsupported configuration. This may lead to unpredictable behavior of network commands.");
	}
	else
		qDebug()<<"qApp not a PiaApp!";
		//QMessageBox::critical(nullptr, tr("Undefined Instances"), tr("Strange things happen. You are lost..."));

	foreach(QNetworkInterface interface, QNetworkInterface::allInterfaces())
	{
		if ((interface.flags() & QNetworkInterface::IsLoopBack) || !(interface.flags() & QNetworkInterface::IsUp) || interface.hardwareAddress().isEmpty())
			continue;
		setUid(interface.hardwareAddress());
		break;
	}
	if(uid_.isEmpty())
		qDebug()<<"no MAC!";
		//QMessageBox::critical(nullptr, tr("Host address unknown"), tr("The progamm cannot read the network hardware address (usually MAC) of this computer. Network communication may fail."));
	luid_=(unsigned long long)time(nullptr)*1000000000ULL;
	qDebug()<<"command id base "<<uid_<<" "<<luid_;
	// uid_ + luid_ is unique as long as there is only one instance of pia running on a given machine

	connections_=new Connections(this, mwin);

}

void DataContainer::shutdown()
{
	connections_->closeAllHosts();
	if(hostDict_)
	{
		delete hostDict_;
		hostDict_=nullptr;
	}
}

bool DataContainer::readPiahdInfo(QString binaryFilename, QString &fileType, QString &fileTst, QStringList *messages)
{
	if(binaryFilename.isEmpty())
	{
		QSettings settings;
		binaryFilename=settings.value("deploy/binary").toString();
	}
	fileType.clear();
	fileTst.clear();
	QFileInfo inf(binaryFilename);
	if(!inf.exists())
	{
		if(messages)
			(*messages)<<tr("ERROR: Binary file not found.");
		return false;
	}
	QProcess testerP;
	testerP.start("file", QStringList()<<binaryFilename);
	if(!testerP.waitForFinished(5000))
	{
		if(messages)
			(*messages)<<tr("ERROR: Command (file) timed out");
		return false;
	}
	if(testerP.exitCode())
	{
		if(messages)
		{
			(*messages)<<tr("ERROR: cannot read file type: %1").arg(testerP.errorString());
			(*messages)<<QString("DEBUG: %1").arg(QString(testerP.readAll()));
		}
		return false;
	}
	QString filetyperaw(testerP.readAll());
	QStringList filetype=filetyperaw.mid(filetyperaw.indexOf(':')+2).split(", ");
	if(filetype.size()>1 && filetype.at(0)=="ELF 32-bit LSB executable" && filetype.at(1)=="ARM")
	{
		fileType=filetype.at(0);
		if(messages)
			(*messages)<<tr("OK: ARM 32bit ELF executable");
	}
	else
	{
		fileType=filetype.at(0);
		if(messages)
			(*messages)<<tr("ERROR: not an ARM 32bit ELF executable (%1)").arg(filetype.join('|'));
		return false;
	}
	// check version
	QProcess testerD;
	testerD.start("objdump", QStringList()<<"-swj"<<".buildinfo"<<binaryFilename);
	if(!testerD.waitForFinished(5000))
	{
		if(messages)
			(*messages)<<tr("ERROR: Command (objdump) timed out");
		return false;
	}
	if(testerD.exitCode())
	{
		if(messages)
		{
			(*messages)<<tr("ERROR: cannot objdump file: %1").arg(testerD.errorString());
			(*messages)<<QString("DEBUG: %1").arg(QString(testerD.readAll()));
		}
		return false;
	}
	//expected output
	//
	//piahd:     file format elf32-little
	//
	//Contents of section .buildinfo:
	//0         1         2         3         4         5
	//012345678901234567890123456789012345678901234567890123456789
	// 0000 32303231 2d30362d 32362031 383a3133  2021-06-26 18:13
	// 0010 3a303120 43455354 0a                 :01 CEST.
	for(int i=0; i<4; ++i) // skip  lines
		testerD.readLine();
	QByteArray line=testerD.readLine();
	fileTst=line.mid(43, 16);
	line=testerD.readLine();
	int s=5; // timezone abbreviations can be one to 5 chars long, output ends with 0x0a
	while(s<9 && line.at(43+s)>='A')
		++s;
	fileTst+=line.mid(43, s);
	if(fileTst.size()<21)
	{
		if(messages)
			(*messages)<<tr("ERROR: invalid .buildinfo (%1)").arg(fileTst);
		return false;
	}
	if(messages)
		(*messages)<<tr("Build %1").arg(fileTst);
	return true;
}

void DataContainer::deregisterCmdTarget(CommandTarget *target)
{
	for(Dispatch::iterator it=dispatch.begin(); it!=dispatch.end(); )
	{
		if(it->second==target)
		{
			Dispatch::iterator delit=it;
			++it;
			dispatch.erase(delit);
		}
		else
			++it;
	}
}

void DataContainer::addNodeFragments(Command &cmd) const
{
	Q_UNUSED(cmd)
	//for(NodeAdrMP::const_iterator it=nodeAdrMP.begin(); it!=nodeAdrMP.end(); ++it)
	//	cmd.addFragment(it->second->toFragment());
}

void DataContainer::dispatchCommand(Command &cmd)
{
	if(cmd.isBroadcast())
	{ // handle broadcasts
		if(cmd.source().uid()==uid_)
		{
			NetConMSP & ncs=connections_->hostConMP();
			for(auto it=ncs.begin(); it!=ncs.end(); ++it)
			{
				if(it->second)
					it->second->send(cmd);
			}
		}
		else
		{ // we received a broadcast, dispatch to avatars
			if(cmd.cmd()=="pool")
			{
				const FragmentLP & frags=cmd.fragments();
				for(FragmentLP::const_iterator it=frags.begin(); it!=frags.end(); ++it)
				{
					Avatars::iterator avit=avatars.find(NodeAddress((*it)->attribute("addr"))); // ignores avatar-uid!
					if(avit!=avatars.end())
						avit->second->processCommand(cmd, **it); // never process replies on broadcasts
				}
			}
			else
			{
				Avatars::iterator it=avatars.find(cmd.source()); // ignores avatar-uid!
				if(it!=avatars.end())
					it->second->processCommand(cmd, cmd); // never process replies on broadcasts
			}
		}
		cmd.deleteLater();
		return;
	}
	// non-broadcasts
	const NodeAddress & procTarget=cmd.procTarget();
	if(procTarget.uid()==uid_)
	{ // we should process the command
		vector<CommandTarget * > trgts=findTarget(cmd.cmd());
		if(trgts.size())
		{
			qDebug()<<"dispatchCommand specific target(s)"<<cmd.cmd()<<trgts.size();
			bool wasReply=cmd.isReply(), doReply=false;
			for(auto cit=trgts.begin(); cit!=trgts.end(); ++cit)
			{
				(*cit)->processCommand(cmd, cmd);
				if(!wasReply && cmd.isReply())
					doReply=true; // if anyone wants to reply, we reply
			}
			if(doReply)
			{ // send the reply
				const NodeAddress & replyTarget=cmd.procTarget();
				if(replyTarget.uid()==uid_)
				{ // we should process the reply? come on...
					qCritical()<<"circular reply"<<cmd.cmd()<<procTarget.toStringVerbose();
				}
				else
				{
					NetCon *host=connections_->hostCon(replyTarget.uid());
					if(host)
						host->send(cmd);
					else
						qCritical()<<"no reply route"<<cmd.cmd()<<replyTarget.toStringVerbose();
				}
			}
		}
		else if(cmd.isReply())
		{
			Avatars::iterator it=avatars.find(cmd.destination()); // ignores avatar-uid!
			if(it==avatars.end())
				qWarning()<<"no one to handle reply on"<<cmd.cmd()<<procTarget.alias();
			else
				it->second->processCommand(cmd, cmd); // never process replies on replies
		}
		else
			qCritical()<<"no handler for command"<<cmd.cmd()<<procTarget.toStringVerbose();
	}
	else
	{ // we should send or relay the command
		NetCon *host=connections_->hostCon(procTarget.uid());
		if(host)
			host->send(cmd);
		else
			qCritical()<<"no route"<<cmd.cmd()<<procTarget.toStringVerbose();
	}
	cmd.deleteLater();
}

/*
Command & DataContainer::getPendingBroadcast()
{
	QMutexLocker lk(&mtxBroadcast);
	if(!broadcast)
	{
		broadcast = new Command("pool");
		broadcast->convertToBroadcast();
	}
	return *broadcast;
}*/

void DataContainer::sendBroadcast()
{
	QMutexLocker lk(&mtxBroadcast);
	if(broadcast)
		dispatchCommand(*broadcast); // deleted by dispatch
	broadcast=nullptr;
}

DataHistory *DataContainer::getDataHistory(const pi_at_home::NodeAddress &adr, bool createOnMiss)
{
	DataHistoryMP::const_iterator it=dataHistMP.find(adr);
	if(it==dataHistMP.end())
	{
		if(createOnMiss)
			return dataHistMP.insert(DataHistoryMP::value_type(adr, new DataHistory(adr))).first->second;
		return nullptr;
	}
	return it->second;
}

DataHistory *DataContainer::getDataHistory(const QString &uidName)
{
	QString alias, uid;
	NodeAddress::uidAlias(uidName, uid, alias);
	for(DataHistoryMP::iterator dit=dataHistMP.begin(); dit!=dataHistMP.end(); ++dit)
	{
		if(dit->first.alias()==alias && dit->first.uid()==uid)
		{
			return dit->second;
		}
	}
	return nullptr;
}

void DataContainer::purgeEmptyHistories()
{
	for(DataHistoryMP::iterator dit=dataHistMP.begin(); dit!=dataHistMP.end(); )
	{
		if(dit->second->isEmpty())
		{
			//qDebug()<<"purging"<<dit->first;
			DataHistoryMP::iterator ddit=dit;
			++dit;
			dataHistMP.erase(ddit);
		}
		else
			++dit;
	}
}

QObject *DataContainer::extendDataHistory(const pi_at_home::DataHistory &dh) const
{
	if(facDH_)
		return (*facDH_)(dh);
	return nullptr;
}

void DataContainer::dispatchCmdSl(QObject *cmd)
{
	Command * cmd_=dynamic_cast<Command *>(cmd);
	if(!cmd)
	{
		qWarning()<<"dispatchCmdSl cannot cast to Command";
		return;
	}
	//qDebug()<<"slot dispatch command"<<cmd_->cmd();
	dispatchCommand(*cmd_);
}

void DataContainer::timerEvent(QTimerEvent *event)
{
	QObject::timerEvent(event);
}

std::vector<CommandTarget * > DataContainer::findTarget(const QString & cmd)
{
	std::vector<CommandTarget * > trgts;
	auto dit=dispatch.equal_range(cmd);
	for(auto xit=dit.first; xit!=dit.second; ++xit)
		trgts.push_back(xit->second);
	return trgts;
}


