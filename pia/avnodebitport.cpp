#include "avnodebitport.h"
#include "avnodebitport_fac.h"
#include "ui_widnode.h"
#include "piadi.h"

AvNodeBitPort::AvNodeBitPort(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QCheckBox(this))
{
	defval_=NodeValue(false);
	ui->labelState->setToolTip(typeName);
	deserialize(fragment.attribute("val"));
	widVal->setChecked(value_.toBool());
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(clicked(bool)), this, SLOT(valChanged(bool)));
}

void AvNodeBitPort::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->setChecked(value_.toBool());
}

void AvNodeBitPort::valChanged(bool val)
{
	value_=NodeValue(val);
	nodeValueChanged();
}
