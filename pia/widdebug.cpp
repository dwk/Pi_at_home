#include "widdebug.h"
#include <QFontDatabase>
#include "piadi.h"


WidDebug::WidDebug(QWidget *parent) : QListWidget(parent)
{
	setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
}

void WidDebug::addLine(QString text, int type, long long threadId)
{
	QListWidgetItem *lwi=new QListWidgetItem(text);
	switch (type)
	{
		case QtDebugMsg:
			lwi->setForeground(QBrush(Qt::black)); break;
		case QtInfoMsg:
			lwi->setForeground(QBrush(Qt::darkBlue)); break;
		case QtWarningMsg:
			lwi->setForeground(QBrush(Qt::darkYellow)); break;
		case QtCriticalMsg:
			lwi->setForeground(QBrush(Qt::darkRed)); break;
		case QtFatalMsg:
			lwi->setForeground(QBrush(Qt::red)); break;
	}
	if(threadId!=(long long)QThread::currentThreadId())
		lwi->setBackground(QBrush(Qt::lightGray));
	this->addItem(lwi);
	if(this->count()>100)
	{
		delete this->takeItem(0);
		this->item(0)->setText("...");
	}
	this->scrollToBottom();
}
