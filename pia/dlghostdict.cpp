#include "dlghostdict.h"
#include "ui_dlghostdict.h"
#include <QDateTime>
#include "../piah_common/hostdict.h"
#include "piadi.h"

DlgHostDict::DlgHostDict(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgHostDict)
{
	ui->setupUi(this);
	HostDict & hd=DCON.hostDict();
	const HostInstanceMV & his=hd.hostInstances();
	ui->tableWidget->setColumnCount(5);
	ui->tableWidget->setRowCount(his.size());
	int row=0;
	for(auto it=his.begin(); it!=his.end(); ++it)
	{
		QString ipv4c=QHostAddress(it->second.hadr).toString();
		QTableWidgetItem *twi=new QTableWidgetItem(it->first);
		twi->setFlags(twi->flags()&~Qt::ItemIsEditable);
		// 0=clean, 1=modified, 2=new
		twi->setData(Qt::UserRole, 0);
		NetCon *hc=DCON.connections()->hostCon(it->first);
		//twi->setBackground(hc?QBrush(QColor(210,255,210)):QBrush(QColor(230,230,230)));
		ui->tableWidget->setItem(row, 0, twi);
		ui->tableWidget->setItem(row, 1, new QTableWidgetItem(ipv4c));
		ui->tableWidget->setItem(row, 2, new QTableWidgetItem(QString::number(it->second.port)));
		twi=new QTableWidgetItem(it->second.lastConfirm.isValid()?it->second.lastConfirm.toString(Qt::ISODate):tr("never"));
		twi->setFlags(twi->flags()&~Qt::ItemIsEditable);
		ui->tableWidget->setItem(row, 3, twi);
		if(hc)
			ui->tableWidget->setItem(row, 4, new QTableWidgetItem(hc->version()+" build "+hc->build()));
		else
			ui->tableWidget->setItem(row, 4, new QTableWidgetItem(tr("not connected")));
		++row;
	}
	ui->tableWidget->resizeColumnsToContents();
	if(row)
		ui->tableWidget->setCurrentItem(ui->tableWidget->item(0,0));
	ignoreCellChange=false;
}

DlgHostDict::~DlgHostDict()
{
	delete ui;
}

void DlgHostDict::accept()
{
	if(!allClean)
	{
		if(QMessageBox::warning(this, tr("Unsaved Changes"), tr("It seems that there are unsaved edits. Close anyway and discard?"), \
								QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
			return;
	}
	QDialog::accept();
}

void DlgHostDict::on_toolAdd_clicked()
{
	ignoreCellChange=true;
	int nr=ui->tableWidget->rowCount();
	ui->tableWidget->setRowCount(nr+1);
	QTableWidgetItem *twi=new QTableWidgetItem("Pi3xxx");
	//twi->setFlags(twi->flags()&~Qt::ItemIsEditable); keep it editable while new
	twi->setData(Qt::UserRole, 2);
	twi->setIcon(QIcon(":/res/exclamation.png"));
	ui->tableWidget->setItem(nr, 0, twi);
	ui->tableWidget->setItem(nr, 1, new QTableWidgetItem(tr("10.0.1.99")));
	ui->tableWidget->setItem(nr, 2, new QTableWidgetItem(tr("80")));
	twi=new QTableWidgetItem(tr("never"));
	twi->setFlags(twi->flags()&~Qt::ItemIsEditable);
	ui->tableWidget->setItem(nr, 3, twi);
	ui->tableWidget->setItem(nr, 4, new QTableWidgetItem(tr("new")));
	ignoreCellChange=false;
	allClean=false;
	ui->pushDeployAll->setEnabled(allClean);
	ui->tableWidget->setCurrentCell(nr,0);
	setToolbuttons(2);
}

void DlgHostDict::on_toolDelete_clicked()
{
	int cr=ui->tableWidget->currentRow();
	QTableWidgetItem *twi=ui->tableWidget->item(cr, 0);
	if(!twi)
	{
		setToolbuttons(-1);
		return;
	}
	HostDict & hd=DCON.hostDict();
	hd.remove(twi->data(Qt::DisplayRole).toString());
	ui->tableWidget->removeRow(cr);
	checkAllClean();
	if(ui->tableWidget->rowCount())
		ui->tableWidget->setCurrentItem(ui->tableWidget->item(0,0));
	else
		setToolbuttons(-1);
}

void DlgHostDict::on_toolSave_clicked()
{
	int cr=ui->tableWidget->currentRow();
	QTableWidgetItem *twi=ui->tableWidget->item(cr, 0);
	if(!twi)
	{
		setToolbuttons(-1);
		return;
	}
	HostDict & hd=DCON.hostDict();
	QHostAddress hadr(ui->tableWidget->item(cr, 1)->data(Qt::DisplayRole).toString());
	QDateTime dt;
	bool ok=false;
	int port=ui->tableWidget->item(cr, 2)->data(Qt::DisplayRole).toInt(&ok);
	HostInstance hi(hadr.toIPv4Address(), port, dt);
	hi.uid=twi->data(Qt::DisplayRole).toString();
	int state=twi->data(Qt::UserRole).toInt();
	if(hadr.isNull() || !ok)
	{
		if(QMessageBox::warning(this, tr("Invalid Data"), tr("It seems that either the IP address or the port are ionvalid. Save anyway?"), \
								QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
			return;
	}
	//qDebug()<<"on_toolSave_clicked"<<state<<hi.uid<<hi.hadr<<hi.port<<hi.lastConfirm;
	if(state==1)
	{
		if(!hd.update(hi))
			qWarning()<<"DlgHostDict::on_toolSave_clicked update failed"<<twi->data(Qt::DisplayRole).toString();
	}
	else if(state==2)
	{
		if(!hd.create(hi))
			qWarning()<<"DlgHostDict::on_toolSave_clicked create failed"<<twi->data(Qt::DisplayRole).toString();
	}
	ignoreCellChange=true;
	twi->setData(Qt::UserRole, 0);
	twi->setIcon(QIcon());
	ignoreCellChange=false;
	checkAllClean();
	setToolbuttons(0);
}

void DlgHostDict::on_pushDeployAll_clicked()
{
	const HostInstanceMV & himv=DCON.hostDict().hostInstances();
	if(!himv.size())
	{
		QMessageBox::warning(this, tr("No Data"), tr("The HostDict does not contain any entries. Perform successful connects to add entries."));
		return;
	}
	int ch=DCON.connections()->hostConMP().size();
	if(!ch)
	{
		QMessageBox::warning(this, tr("No Hosts"), tr("You are not connected to any host - cannot broadcast."));
		return;
	}
	Command *broadcast = new Command("hostdict");
	broadcast->convertToBroadcast();
	for(auto it=himv.begin(); it!=himv.end(); ++it)
	{
		Fragment *fr=new Fragment("host");
		fr->setAttribute("uid", it->first);
		fr->setAttribute("ip", QHostAddress(it->second.hadr).toString());
		fr->setAttribute("port", QString::number(it->second.port));
		fr->setAttribute("verified", it->second.lastConfirm.toString(Qt::ISODate));
		broadcast->addFragment(fr);
	}
	DCON.dispatchCommand(*broadcast);
	QMessageBox::information(this, tr("Host Broadcast"), tr("%1 host addresses were sent.").arg(himv.size()));
}

void DlgHostDict::on_tableWidget_currentCellChanged(int currentRow, int , int , int )
{
	QTableWidgetItem *twi=ui->tableWidget->item(currentRow, 0);
	if(twi)
	{
		int state=twi->data(Qt::UserRole).toInt();
		setToolbuttons(state);
	}
	else
		setToolbuttons(-1);
}

void DlgHostDict::on_tableWidget_itemChanged(QTableWidgetItem *item)
{
	if(ignoreCellChange)
		return;
	int r=item->row();
	//qDebug()<<"on_tableWidget_itemChanged"<<r;
	QTableWidgetItem *twi=ui->tableWidget->item(r, 0);
	if(twi->data(Qt::UserRole).toInt()==2)
		return; // new stays new
	ignoreCellChange=true;
	twi->setData(Qt::UserRole, 1);
	twi->setIcon(QIcon(":/res/exclamation.png"));
	ignoreCellChange=false;
	allClean=false;
	ui->pushDeployAll->setEnabled(allClean);
	setToolbuttons(1);
}

void DlgHostDict::setToolbuttons(int currentState)
{
	if(currentState<0)
	{
		ui->toolAdd->setEnabled(true);
		ui->toolDelete->setEnabled(false);
		ui->toolSave->setEnabled(false);
		return;
	}
	ui->toolAdd->setEnabled(true);
	//qDebug()<<"setToolbuttons"<<currentState;
	ui->toolDelete->setEnabled(currentState>=0);
	ui->toolSave->setEnabled(currentState>0);
}

void DlgHostDict::checkAllClean()
{
	allClean=true;
	for(int r=0, rm=ui->tableWidget->rowCount(); r<rm; ++r)
	{
		QTableWidgetItem *twi=ui->tableWidget->item(r, 0);
		if(twi->data(Qt::UserRole).toInt())
		{
			allClean=false;
			break;
		}
	}
	ui->pushDeployAll->setEnabled(allClean);
}
