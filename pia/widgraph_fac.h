#ifndef WIDGRAPHFAC_H
#define WIDGRAPHFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new pi_at_home::WidGraph(parent);
}
bool dockWidRegistered=pi_at_home::DockWidFactory::get().registerDockWid("Graphs", "dockwid_graph", Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}

#endif // WIDGRAPHFAC_H
