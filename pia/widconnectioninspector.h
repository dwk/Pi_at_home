#ifndef WIDCONNECTIONINSPECTOR_H
#define WIDCONNECTIONINSPECTOR_H

#include "piadh.h"
#include <list>
#include <QWidget>

namespace Ui {
class WidConnectionInspector;
}

namespace pi_at_home
{

class WidConnectionInspector : public QWidget
{
	Q_OBJECT

public:
	explicit WidConnectionInspector(QWidget *parent = 0);
	~WidConnectionInspector();
public slots:
	void connectionListChanged();
	void connectionNameChanged(QString oldName, QString newName);
	void rawData(QString host, QByteArray data, bool sending, bool error);
private slots:
	void fillList();
	void clearCurrent();
	void clearAll();
	void unfreezeSl(bool checked);
private:
	struct Line {Line() {} Line(QString txt, QBrush br) : txt(txt), br(br) {} QString txt; QBrush br;};
	typedef std::list<Line> LineL;
	typedef std::map<QString, LineL> Texts;
	Ui::WidConnectionInspector *ui;
	Texts texts;
	int maxLines = 500;
};
}

#endif // WIDCONNECTIONINSPECTOR_H
