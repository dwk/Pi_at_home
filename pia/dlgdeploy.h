#ifndef DLGDEPLOY_H
#define DLGDEPLOY_H

#include "piadh.h"
#include <QDialog>
#include <QProcess>
#include <QMutex>

namespace Ui
{
	class DlgDeploy;
}

namespace pi_at_home
{

class DlgDeploy : public QDialog
{
	Q_OBJECT
public:
	explicit DlgDeploy(QWidget *parent, QString serverUid = QString());
	~DlgDeploy();
public slots:
	virtual void accept();
protected:
	void timerEvent(QTimerEvent *event);
private slots:
	void toolBinarySl();
	void toolConfigDirSl();
	void updateBinary();
	void updateConfigDir();
	void on_pushDeploy_clicked();
	void on_pushNone_clicked();
	void on_pushAll_clicked();
	void finishedSl(int exitCode, QProcess::ExitStatus exitStatus);
	void procStateChangedSl(QProcess::ProcessState newstate);
private:
	void execCmd(const QString & cmd, const QStringList &args, int timeout=5000);
	Ui::DlgDeploy *ui;
	int deployRow=-1, deployStep=-1;
	QString deployAdr, deployCmdFull, serverUid;
	bool deployBin=false, deployConfig=false, deployConfigRaw=false, deployError=false;
	QProcess *deployer;
	int deployerTimeoutId=0;
	QMutex mtx;
};
}
#endif // DLGDEPLOY_H
