#ifndef WIDACTOR_H
#define WIDACTOR_H

#include "piadh.h"
#include <QObject>

namespace pi_at_home
{

class WidActor
{
public:
	virtual void connectAll(QObjectList & otherActors) =0;
};
}

#endif // WIDACTOR_H
