#include "editorobject.h"
#include <QtWidgets>
#include <QToolButton>
#include <QDomNode>
#include "editorobjectderived.h"
#include "widconfigeditor.h"
#include "dlgeditnode.h"
#include "dlgeditobject.h"
#include "piadi.h"

Q_LOGGING_CATEGORY(editorobject, "editorobject")

int EditorObject::highestId=0;
double EditorObject::defaultPosY=0.;
QWidget * EditorObject::pwid=nullptr;
EditorObjectMetadata EditorObject::dummyMD;
QDomElement EditorObject::domDummy;

EditorObject *EditorObject::createEditorObject(const QString &typeName, const EditorObjectMetadataMSP &metaDatas)
{
	auto it=metaDatas.find(typeName);
	EditorObject *res=nullptr;
	if(typeName=="BitLogic")
	{
		res=new EOBitLogic(it==metaDatas.end()?&dummyMD:it->second);
	}
	else if(typeName=="Compare")
	{
		res=new EOCompare(it==metaDatas.end()?&dummyMD:it->second);
	}
	else if(typeName=="Dummy")
	{
		res=new EODummy(it==metaDatas.end()?&dummyMD:it->second);
	}
	else if(typeName=="InfoPanel")
	{
		res=new EOInfoPanel(it==metaDatas.end()?&dummyMD:it->second);
	}
	else if(typeName=="Timer")
	{
		res=new EOTimer(it==metaDatas.end()?&dummyMD:it->second);
	}
	if(!res) // SwitchPort, BitPort
		res=new EditorObject(typeName, it==metaDatas.end()?&dummyMD:it->second);
	return res;
}

EditorObject *EditorObject::createEditorObject(QDomElement &elem, const EditorObjectMetadataMSP &metaDatas, pi_at_home::EditorDict *ed)
{
	QString aname=elem.attribute("type");
	auto it=metaDatas.find(aname);
	EditorObject *res=nullptr;
	//qDebug()<<"EditorObject::createEditorObject"<<aname;
	if(aname=="BitLogic")
	{
		res=new EOBitLogic(elem, it==metaDatas.end()?&dummyMD:it->second, ed);
	}
	else if(aname=="Compare")
	{
		res=new EOCompare(elem, it==metaDatas.end()?&dummyMD:it->second, ed);
	}
	else if(aname=="Dummy")
	{
		res=new EODummy(elem, it==metaDatas.end()?&dummyMD:it->second, ed);
	}
	else if(aname=="InfoPanel")
	{
		res=new EOInfoPanel(elem, it==metaDatas.end()?&dummyMD:it->second, ed);
	}
	else if(aname=="Timer")
	{
		res=new EOTimer(elem, it==metaDatas.end()?&dummyMD:it->second, ed);
	}
	if(!res) // SwitchPort, BitPort
		res=new EditorObject(elem, it==metaDatas.end()?&dummyMD:it->second, ed);
	return res;
}

EditorObject::EditorObject(const QString &typeName, const EditorObjectMetadata *metaData) : elem(domDummy), name(typeName), md(metaData)
{
	setPos(0., defaultPosY);
	defaultPosY+=120.;
	setFlags(ItemIsSelectable | ItemIsMovable | ItemSendsGeometryChanges);
    setAcceptHoverEvents(true);
	for(auto it=md->editorParamMetadatas.begin(); it!=md->editorParamMetadatas.end(); ++it)
	{
		if(it->second->type==EditorParamMetadata::Type::REQUIRED)
			keyVals.insert(KeyValMV::value_type(it->second->name, it->second->def.toString()));
	}
	kvUpdate(true);
	id=++highestId;
	setZValue(id);
	kvUpdate(false); // write back id to kv
	for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
	{
		for(int cnt=0; cnt<nit->second->nodeMin; ++cnt)
			addNodeSl(nit->second->typeName, false);
	}
	reindexChildren();
	setupGraphic(); // final height for port section
}

EditorObject::EditorObject(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed, pi_at_home::EditorNodeFactoryDom editorNodeFactory) :
	elem(elem), md(metaData)
{
	Q_UNUSED(ed)
	setFlags(ItemIsSelectable | ItemIsMovable | ItemSendsGeometryChanges);
	setAcceptHoverEvents(true);

	// read stuff from xml
	bool nameOk=false, posOk=false, zorderOk=false;
	QDomNamedNodeMap attrs=elem.attributes();
	for(int i=0; i<attrs.size(); ++i)
	{
		QDomAttr attr=attrs.item(i).toAttr();
		QString aname=attr.name();
		QString v=attr.value();
		if(aname=="type")
		{
			name=v;
			nameOk=true;
		}
		else
		{
			keyVals.insert(KeyValMV::value_type(aname, v));
			//qDebug()<<"reading KV"<<aname<<v;
		}
	}
	kvUpdate(true);
	if(!nameOk)
	{
		qCritical()<<"no Device type"<<name<<id;
		return;
	}
	else
	{
		//qInfo()<<"Creating object"<<name<<id;
		if(id>highestId)
			highestId=id;
	}
	double x=0., y=0.;
	QDomNodeList markupList=elem.elementsByTagName("editor-hints");
	for(int i=0; i<markupList.size(); ++i)
	{
		QDomElement el=markupList.item(i).toElement();
		if(el.hasAttribute("pos_x"))
		{
			bool ok=false;
			double v=el.attribute("pos_x").toDouble(&ok);
			if(ok)
			{
				x=v;
				posOk=true;
			}
		}
		if(el.hasAttribute("pos_y"))
		{
			bool ok=false;
			double v=el.attribute("pos_y").toDouble(&ok);
			if(ok)
			{
				y=v;
				posOk=true;
			}
		}
		if(el.hasAttribute("z_order"))
		{
			bool ok=false;
			double v=el.attribute("z_order").toDouble(&ok);
			if(ok)
			{
				setZValue(v);
				zorderOk=true;
			}
		}
	}
	if(posOk)
		setPos(x, y);
	else
	{
		setPos(0., defaultPosY);
		defaultPosY+=120.;
	}
	if(!zorderOk)
	{
		setZValue(id);
	}
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement el=nodes.item(i).toElement();
		if(editorNodeFactory)
			editorNodeFactory(el, md, ed, this);
		else
			new EditorNode(el, md, ed, this);
	}
	// create nodes if EditorNodeMetadata::fixed==true if required (these may not show up in xml)
	// warn if node count does not match metadata
	bool warn=false;
	for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
	{
		int cnt=nodesOfType(nit->second->typeName);
		if(nit->second->fixed && cnt<nit->second->nodeMin)
		{
			for(int i=cnt; i<nit->second->nodeMin; ++i)
				addNodeSl(nit->second->typeName, false);
			cnt=nit->second->nodeMin;
		}
		if(cnt<nit->second->nodeMin || cnt>nit->second->nodeMax)
		{
			qCritical()<<"EditorObject(xml): node count outside range"<<nit->second->typeName<<cnt<<nit->second->nodeMin<<nit->second->nodeMax;
			warn=true;
		}
	}
	if(warn)
		QMessageBox::warning(nullptr, tr("XML Data Issue"), tr("Invalid node count at %1 (id %2). See log for more information.").arg(name).arg(id));
	reindexChildren();
	setupGraphic();
}

EditorObject::~EditorObject()
{
	removeXml();
	// we can't move this to ~EditorNode because the node won't find out the Id of its
	// parent object - which is already destroyed
	// delete the nodes while the object is still alive - safer
	WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
	auto nds=childItems();
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(!eond)
			continue;
		EditorConnectionVP ecs=ce->getConnection(id, eond->getChannel());
		for(auto it=ecs.begin(); it!=ecs.end(); ++it)
		{
			if(*it)
				ce->deleteConSl((*it)->getKey());
		}
	}
	elem.clear();
}

bool EditorObject::canDeleteNode(int channel, const QString &nodeTypeName)
{
	Q_UNUSED(channel)
	auto it=md->editorNodeMetadatas.find(nodeTypeName);
	return it==md->editorNodeMetadatas.end()?false:nodesOfType(nodeTypeName)>it->second->nodeMin;
}

bool EditorObject::canChangeChannel(int oldChannel, int newChannel)
{
	auto nds=childItems();
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(eond)
		{
			if(eond->getChannel()==oldChannel)
				continue;
			if(eond->getChannel()==newChannel)
				return false;
		}
	}
	return true;
}

void EditorObject::updateXml(QDomElement &domParent, const pi_at_home::EditorBaseConfig &bc)
{
	if(elem.isNull())
	{
		elem=domParent.ownerDocument().createElement(EditorObjectMetadata::categoryToStr(md->category));
		domParent.appendChild(elem);
	}
	elem.setAttribute("type", md->typeName);
	//elem.setAttribute("id", QString::number(id)); // is a keyval
	for(auto it=keyVals.begin(); it!=keyVals.end(); ++it)
	{
		elem.setAttribute(it->first, it->second);
	}
	auto atts=elem.attributes();
	for(int i=0; i<atts.count(); ++i)
	{
		QString an=atts.item(i).nodeName();
		if(an=="type") // has noe keyval
			continue;
		auto it=keyVals.find(an);
		if(it==keyVals.end())
			elem.removeAttribute(an);
	}
	QDomElement ehe=getEditorHintsElem(domParent);
	// <editor-hints pos_x="230" pos_y="-50" z_order="0" />
	ehe.setAttribute("pos_x", QString::number(pos().x()));
	ehe.setAttribute("pos_y", QString::number(pos().y()));
	ehe.setAttribute("z_order", QString::number(zValue()));
	auto nds=childItems();
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(eond)
			eond->updateXml(elem, bc);
	}
}

void EditorObject::removeXml()
{
	//qDebug()<<"removeXml"<<name<<elem.attribute("type");
	if(elem.isNull())
		return;
	QDomNode p=elem.parentNode();
	p.removeChild(elem);
	//elem.setTagName("DELETED");
	elem.clear();
}

QRectF EditorObject::boundingRect() const
{
	return boundingBox;
}

QPainterPath EditorObject::shape() const
{
	QPainterPath path;
	path.addRect(body);
	return path;
}

void EditorObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);
	QPen oldPen = painter->pen();
	QBrush oldBrush = painter->brush();
	updatePainterUtils(painter, option);
	if(levelOfDetail<0.2)
	{
		drawDummy(painter, option->state & QStyle::State_Selected);
		painter->setPen(oldPen);
		painter->setBrush(oldBrush);
		return;
	}

	painter->setPen(outlinePen);
	painter->setBrush(backgroundBrush);
	painter->drawRect(body);
	painter->fillRect(2, 2, 34, 34, backgroundColor.lighter(135));
	md->icon.paint(painter, 3, 3, 32, 32);
	painter->setPen(oldPen);
	painter->setBrush(oldBrush);

	if (levelOfDetail>0.8)
	{ // Draw text
		QFont font("Courier", 7);
		painter->setFont(font);
		painter->drawText(36, 12, QStringLiteral("%1: %2").arg(name, prefix));
		painter->drawText(36, 21, QStringLiteral("id %1").arg(id));
		painter->drawText(36, 30, extInfoStr);
	}
	/*
	if (stuff.size() > 1)
	{
		QPen p = painter->pen();
		painter->setPen(QPen(Qt::red, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter->setBrush(Qt::NoBrush);
		QPainterPath path;
		path.moveTo(stuff.first());
		for (int i = 1; i < stuff.size(); ++i)
			path.lineTo(stuff.at(i));
		painter->drawPath(path);
		painter->setPen(p);
		painter->setPen(oldPen);
		painter->setBrush(oldBrush);
	} */
}

bool EditorObject::setId(int newId)
{
	if(newId==id)
		return true;
	WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
	if(ce->canChangeId(id, newId))
	{
		int oldId=id;
		id=newId;
		kvUpdate(false);
		/*auto it=keyVals.find("id");
		if(it==keyVals.end())
			qWarning()<<"EditorObject::setId no id kv"<<name;
		else
			it->second=QString::number(id);*/
		emit idChangedSig(oldId, newId);
		return true;
	}
	return false;
}

EditorNode *EditorObject::getNode(int channel)
{
	auto nds=childItems();
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(eond && eond->getChannel()==channel)
			return eond;
	}
	return nullptr;
}

EditorNode *EditorObject::getNode(QString alias)
{
	auto nds=childItems();
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(eond && eond->getAlias()==alias)
			return eond;
	}
	return nullptr;
}

void EditorObject::connectionStart(int channel, const QPointF &scPos)
{
	emit connectionStartSig(id, channel, scPos);
}

void EditorObject::connectionCancelInfo(int channel)
{
	emit connectionCancelSig(id, channel);
}

void EditorObject::connectionCancelAct(int channel)
{
	EditorNode *eond=getNode(channel);
	if(eond)
		eond->toDefaultState();
}

void EditorObject::editObjSl()
{
	DlgEditObject dlg(pwid, *this);
	if(dlg.exec()==QDialog::Accepted)
	{
		kvUpdate(true);
		reportProblems(checkConsistency());
		reindexChildren();
		setupGraphic();
		update();
		emit changedSig();
	}
}

void EditorObject::deleteNodeSl(int channel)
{
	//qDebug()<<"deleteNodeSl"<<prefix<<channel;
	WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
	EditorNode *eond=getNode(channel);
	if(eond)
	{
		EditorConnectionVP ecs=ce->getConnection(id, channel);
		for(auto it=ecs.begin(); it!=ecs.end(); ++it)
		{
			if(*it)
				ce->deleteConSl((*it)->getKey());
		}
		delete eond;
		checkConsistency();
		reindexChildren();
		setupGraphic();
		update();
		emit changedSig();
	}
	else
		qWarning()<<"deleteNodeSl channel not found"<<prefix<<channel;
}

void EditorObject::editNodeSl(int channel)
{
	//qDebug()<<"editNodeSl"<<prefix<<channel;
	EditorNode *eond=getNode(channel);
	if(eond)
	{
		DlgEditNode dlg(pwid, *eond);
		dlg.setWindowModality(Qt::WindowModal);
		int oldChannel=eond->getChannel();
		if(dlg.exec()==QDialog::Accepted)
		{
			reportProblems(checkConsistency());
			if(eond->getChannel()!=oldChannel)
			{
				reindexChildren();
				setupGraphic();
			}
			update();
			emit changedSig();
		}
	}
	else
		qWarning()<<"editNodeSl channel not found"<<prefix<<channel;
}

void EditorObject::addNodeSl(QString nodeTypeName, bool reindexAndUpdate)
{
	//qDebug()<<"addNodeSl"<<prefix<<nodeTypeName;
	auto it=md->editorNodeMetadatas.find(nodeTypeName);
	EditorNodeMetadata *mdn=nullptr;
	if(it==md->editorNodeMetadatas.end())
	{
		mdn=&EditorNode::dummyMD;
		qCritical()<<"adding node with dummy metadata, request was for"<<nodeTypeName;
	}
	else
		mdn=it->second;
	EditorNode *res=nullptr;
	int ncnt=nodesOfType(nodeTypeName);
	auto nodeFac=getNodeFactory();
	if(nodeFac)
		res=nodeFac(mdn, ncnt+mdn->baseChannel, this);
	else
		res=new EditorNode(mdn, ncnt+mdn->baseChannel, this);
	if(mdn->nodeMax>1) // why -mdn->nodeMin ?
		res->setNodeAlias(res->getNodeAlias()+QString::number(ncnt));
	if(reindexAndUpdate)
	{
		//reportProblems(checkConsistency()); not here, quite annoying
		reindexChildren();
		setupGraphic();
		update();
	}
}

/*void EditorObject::connectionStateSl(void *editorNode, int state)
{
	Q_UNUSED(state) // doesn't matter
	EditorNode *eond=reinterpret_cast<EditorNode * >(editorNode);
	if(!eond || eond->getAddress().device()!=id)
		return;
	qDebug()<<"connectionStateSl"<<id<<eond->getAddress().toStringVerbose()<<state;
	eond->toDefaultState();
}*/

void EditorObject::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	if(!contextMenuAddNodes(menu))
	{
		for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
		{
			QString nodeTypeName=nit->second->typeName;
			QAction *act=menu->addAction(QIcon(":/res/ConfigEditor/icons8-add-node-48.png"), QString("Add node %1").arg(nodeTypeName));
			if(nodesOfType(nodeTypeName)<nit->second->nodeMax)
				connect(act, &QAction::triggered, this, [this, nodeTypeName](){addNodeSl(nodeTypeName);});
			else
				act->setEnabled(false);
		}
	}
	QAction * act=menu->addAction(QIcon(":/res/star.png"),
		QStringLiteral("Edit %2").arg(EditorObjectMetadata::categoryToStr(md->category)));
	connect(act, &QAction::triggered, this, &EditorObject::editObjSl);
	act=menu->addAction(QIcon(":/res/cross_red.png"), tr("Delete"));
	WidConfigEditor *ce=dynamic_cast<WidConfigEditor *>(scene()->parent());
	connect(act, &QAction::triggered, ce, [ce, this](){ce->deleteObjSl(id);});
	//act->setEnabled(false);
	menu->popup(event->screenPos());
}

QVariant EditorObject::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change==ItemPositionHasChanged)
	{
		wasMoved=true;
		//qDebug()<<"EditorObject::itemChange"<<value.toPointF();
	}
	return QGraphicsItem::itemChange(change, value);
}

void EditorObject::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	qDebug()<<"obj dblclk at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseDoubleClickEvent(event);
	if(event->button()==Qt::LeftButton)
		editObjSl();
}

void EditorObject::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj press at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mousePressEvent(event);
	wasMoved=false;
	//update();
}

void EditorObject::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	/*if (event->modifiers() & Qt::ShiftModifier) {
        stuff << event->pos();
        update();
        return;
	}*/
    QGraphicsItem::mouseMoveEvent(event);
}

void EditorObject::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj release at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseReleaseEvent(event);
	if(wasMoved)
	{
		wasMoved=false;
		QPointF p=scenePos();
		setPos(floor((p.x()+8)/16)*16, floor((p.y()+8)/16)*16);
		emit changedSig();
		auto nds=childItems();
		foreach(QGraphicsItem * nd, nds)
		{
			EditorNode *eond=dynamic_cast<EditorNode *>(nd);
			if(eond)
			{
				//qDebug()<<"EditorObject connectionReposSig"<<id<<eond->getChannel()<<eond->scenePos()<<eond->getConOffset();
				emit connectionReposSig(eond);
			}
		}
	}
	//update();
}

void EditorObject::setupGraphic()
{
	backgroundColor=QColor(Qt::white);
	backgroundBrush=QBrush(backgroundColor);
	outlinePen=QPen(Qt::black);
	prepareGeometryChange();
	body=QRectF(0.,0., EditorObject::objectWidth, EditorObject::objectHeaderHeight+EditorNode::nodeHeight*nodeSectionHeightCnt);
	boundingBox=body.adjusted(-3.,-3.,3.,3.);
	setToolTip(md->description);
}

QStringList EditorObject::checkConsistency()
{
	QStringList problems;
	auto nds=childItems();
	set<QString> aliases;
	set<int> channels;
	EditorDict *ce=dynamic_cast<EditorDict *>(scene()->parent());
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(eond)
		{
			if(eond->md->type==EditorNodeMetadata::Type::CONNECTOR)
			{
				EditorConnectionVP ecs=ce->getConnection(id, eond->getChannel());
				if(ecs.size()!=1)
					problems<<QString("Object %1 (id %2, channel %3): no or multiple connections (exactely one required)").arg(name).arg(id).arg(eond->getChannel());
			}
			else
			{
				if(!aliases.insert(eond->getAlias()).second)
					problems<<QString("Object %1 (id %3): duplicate alias (name) '%2' in channel %4").arg(name, eond->getAlias()).arg(id).arg(eond->getChannel());
				if(!channels.insert(eond->getChannel()).second)
					problems<<QString("Object %1 (id %3): duplicate channel %4 for alias %2").arg(name, eond->getAlias()).arg(id).arg(eond->getChannel());
			}
		}
	}
	return problems;
}

void EditorObject::kvUpdate(bool kv2Member)
{
	bool ok=false;
	auto it=keyVals.find("id");
	if(it!=keyVals.end())
	{
		if(kv2Member)
		{
			int i=it->second.toInt(&ok);
			if(ok)
				id=i;
			else
				qWarning()<<"EditorObject::kvUpdate id invalid"<<it->second;
		}
		else
			it->second=QString::number(id);
	}
	it=keyVals.find("prefix");
	if(it!=keyVals.end())
	{
		if(kv2Member)
			prefix=it->second;
		else
			it->second=prefix;
	}
}

void EditorObject::reindexChildren()
{
	auto nds=childItems();
	//int indexL=0, indexR=0;
	map<int, EditorNode * > ndsL, ndsR, ndsBd;
	foreach(QGraphicsItem * nd, nds)
	{
		EditorNode *eond=dynamic_cast<EditorNode *>(nd);
		if(eond)
		{
			int i=0;
			switch(eond->md->type)
			{
			case EditorNodeMetadata::Type::INPUT:
			case EditorNodeMetadata::Type::CONNECTOR:
				ndsL.insert(map<int, EditorNode * >::value_type(eond->getChannel(), eond));
				//i=indexL++;
			break;
			case EditorNodeMetadata::Type::OUTPUT:
				ndsR.insert(map<int, EditorNode * >::value_type(eond->getChannel(), eond));
				//i=indexR++;
			break;
			case EditorNodeMetadata::Type::UNDEF:
			case EditorNodeMetadata::Type::BIDIR:
				ndsBd.insert(map<int, EditorNode * >::value_type(eond->getChannel(), eond));
				//i=max(indexL, indexR);
				//indexL=i+1;
				//indexR=indexL;
			break;
			}
			eond->setIndex(i);
		}
	}
	int line=0, llr=0;
	for(auto nit=ndsBd.begin(); nit!=ndsBd.end(); ++nit, ++line)
		nit->second->setIndex(line);
	for(auto nit=ndsL.begin(); nit!=ndsL.end(); ++nit, ++llr)
		nit->second->setIndex(line+llr);
	llr=0;
	for(auto nit=ndsR.begin(); nit!=ndsR.end(); ++nit, ++llr)
		nit->second->setIndex(line+llr);
	nodeSectionHeightCnt=max(ndsL.size(), ndsR.size())+ndsBd.size();
}

int EditorObject::nodesOfType(const QString &nodeTypeName)
{
	auto nds=childItems();
	int cnt=0;
	foreach(QGraphicsItem *gi, nds)
	{
		EditorNode *en=dynamic_cast<EditorNode *>(gi);
		if(en && en->md->typeName==nodeTypeName)
			++cnt;
	}
	return cnt;
}

void EditorObject::reportProblems(const QStringList &probs)
{
	if(probs.size())
	{
		QMessageBox::warning(pwid, tr("Validation Problems"), tr("This object may have problems at runtime due to:\n%1").arg(probs.join("\n")));
	}
}

void EditorObject::updatePainterUtils(QPainter *painter, const QStyleOptionGraphicsItem *option)
{
	levelOfDetail=option->levelOfDetailFromTransform(painter->worldTransform());
	// (option->state & QStyle::State_Selected) ? color.dark(150) : color;
	if (option->state & QStyle::State_MouseOver)
		backgroundColor=md->color.lighter(110);
	else
		backgroundColor=md->color;
	backgroundBrush=QBrush(backgroundColor);
	if (option->state & QStyle::State_Selected)
	{
		outlinePen=QPen(Qt::red);
		outlinePen.setWidth(3);
	}
	else
		outlinePen=QPen(Qt::black);
}

void EditorObject::drawDummy(QPainter *painter, bool highlighted)
{
	painter->setPen(outlinePen);
	painter->setBrush(backgroundBrush);
	if (highlighted)
		painter->drawRect(body);
	else
		painter->fillRect(body, backgroundColor);
}

QDomElement EditorObject::getEditorHintsElem(QDomElement &domParent)
{
	QDomElement ehe;
	QDomNode eh=elem.firstChild();
	for(; ehe.isNull() && !eh.isNull(); eh=eh.nextSibling())
	{
		if(eh.isElement() && eh.nodeName()=="editor-hints")
			ehe=eh.toElement();
	}
	if(ehe.isNull())
	{
		ehe=domParent.ownerDocument().createElement("editor-hints");
		elem.appendChild(ehe);
	}
	return ehe;
}
