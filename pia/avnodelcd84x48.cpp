#include "avnodelcd84x48.h"
#include "avnodelcd84x48_fac.h"
#include "ui_widnode.h"
#include "piadi.h"


// QString -----------------------------------------------------
AvNodeLcd84x48Line::AvNodeLcd84x48Line(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QLineEdit(this))
{
	defval_=NodeValue(QString(""));
	ui->labelState->setToolTip(typeName);
	widVal->setMaxLength(14);
	widVal->setText(fragment.attribute("val")); // Fragment does unmarshaling
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(editingFinished()), this, SLOT(editingFinishedSl()));
}

void AvNodeLcd84x48Line::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setText(value_.toString());
	widVal->blockSignals(false);
}

void AvNodeLcd84x48Line::editingFinishedSl()
{
	value_=NodeValue(widVal->text()); // Fragment does marshaling
	nodeValueChanged();
}


