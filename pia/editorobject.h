#ifndef EDITOROBJECT_H
#define EDITOROBJECT_H
#include "piadh.h"
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QIcon>
#include <QGraphicsItem>
#include <QDomElement>
#include "editorobjectmetadata.h"
#include "editordict.h"
#include "editornode.h"

Q_DECLARE_LOGGING_CATEGORY(editorobject)

namespace pi_at_home
{

class EditorObject : public QObject, public QGraphicsItem
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsItem)
	friend class DlgEditObject;
	friend class WidConfigEditor;
public:
	static const int objectWidth=140;
	static const int objectHeaderHeight=40;
	static QWidget * pwid;
	static EditorObjectMetadata dummyMD;
	static QDomElement domDummy;
	static EditorObject * createEditorObject(const QString & typeName, const EditorObjectMetadataMSP &metaDatas);
	static EditorObject * createEditorObject(QDomElement &elem, const EditorObjectMetadataMSP &metaDatas, pi_at_home::EditorDict *ed);
	static void resetIdGen() {highestId=0; defaultPosY=0.;}
	EditorObject(const QString & typeName, const EditorObjectMetadata *metaData);
	EditorObject(QDomElement &elem, const EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed, EditorNodeFactoryDom editorNodeFactory= nullptr);
	virtual ~EditorObject();
	virtual EditorNodeFactoryNew getNodeFactory() {return nullptr;}
	virtual bool canDeleteNode(int channel, const QString & nodeTypeName);
	virtual bool canChangeChannel(int oldChannel, int newChannel);
	virtual void updateXml(QDomElement &domParent, const EditorBaseConfig & bc);
	virtual void removeXml();
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE;
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	const QString & getName() const {return name;}
	const QString & getPrefix() const {return prefix;}
	int getId() const {return id;}
	bool setId(int newId);
	KeyValMV & getKeyVals() {return keyVals;}
	EditorNode * getNode(int channel);
	EditorNode * getNode(QString alias);
	void setKeyVals(const KeyValMV & newKeyVals) {keyVals=newKeyVals;}
	void connectionStart(int channel, const QPointF & scPos);
	void connectionCancelInfo(int channel);
	void connectionCancelAct(int channel);
public slots:
	void editObjSl();
	void deleteNodeSl(int channel);
	void editNodeSl(int channel);
	void addNodeSl(QString nodeTypeName, bool reindexAndUpdate = true);
	//void connectionStateSl(void * editorNode, int state);
protected:
	// QGraphicsItem
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
	virtual bool contextMenuAddNodes(QMenu *) {return false;} // return=true indicates that all node option are done and EditorObject shall not add default "node add" actions
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	//
	virtual void setupGraphic();
	virtual void updateInfoStr() {}
	virtual QStringList checkConsistency();
	virtual bool hasExtendedParams() {return false;}
	virtual bool updateExtendedParams() {return false;} // false = no change
	virtual bool hasExtendedEditor() {return false;}
	virtual bool updateExtendedEditor(QWidget *parent, KeyValMV &currentKeyVals) {Q_UNUSED(parent) Q_UNUSED(currentKeyVals) return false;} // false = no change
	void kvUpdate(bool kv2Member);
	void reindexChildren();
	int nodesOfType(const QString & nodeTypeName);
	void reportProblems(const QStringList &probs);
	void updatePainterUtils(QPainter *painter, const QStyleOptionGraphicsItem *option);
	void drawDummy(QPainter *painter, bool highlighted);
	QDomElement getEditorHintsElem(QDomElement &domParent);
	static int highestId;
	static double defaultPosY;
	// PainterUtils
	// graphic representation
	QColor backgroundColor;
	QBrush backgroundBrush;
	QPen outlinePen;
	double levelOfDetail;
	QRectF boundingBox, body;
	int nodeSectionHeightCnt=1;
	bool wasMoved=false;
	//
	QDomElement elem;
	QString name, prefix;
	const EditorObjectMetadata *md;
	int id=-1;
	KeyValMV keyVals;
	QString extInfoStr; // derived class may add info text here
signals:
	void idChangedSig(int oldId, int newId);
	void changedSig();
	void connectionStartSig(int device, int channel, const QPointF & scPos);
	void connectionCancelSig(int device, int channel);
	void connectionReposSig(void * node);
};
typedef std::vector<EditorObject *> EditorObjectVP;
typedef std::map<int, QPointer<EditorObject> > EditorObjectMISP; // key is ID

} // namespace pi_at_home

#endif // EDITOROBJECT_H
