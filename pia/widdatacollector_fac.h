#ifndef WIDDATACOLLECTORFAC_H
#define WIDDATACOLLECTORFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new pi_at_home::WidDataCollector(parent);
}
bool dockWidRegistered=pi_at_home::DockWidFactory::get().registerDockWid("Data Collector", "dockwid_datacollector", Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::LeftDockWidgetArea, DockCreator);
}

#endif // WIDDATACOLLECTORFAC_H
