#include "editorobjectderived.h"
#include <QWidget>
#include <QPen>
#include <QBrush>
#include <QPainter>
#include <QMenu>
#include "dlgeotimer.h"
#include <QGraphicsScene>
#include "editordict.h"
#include "piadi.h"


EditorAlgNode::EditorAlgNode(const pi_at_home::EditorNodeMetadata *metaData, int channel, QGraphicsItem *parent) :
	EditorNode(metaData, channel, parent)
{
}

EditorAlgNode::EditorAlgNode(QDomElement &elem, EditorNodeMetadata *metaData, EditorDict *ed, QGraphicsItem *parent) :
	EditorNode(elem, metaData, ed, parent)
{
}

void EditorAlgNode::updateXml(QDomElement &domParent, const EditorBaseConfig & bc)
{
	EditorNode::updateXml(domParent, bc);
	//qDebug()<<"EditorAlgNode::updateXml";
	switch(md->dataType)
	{
	case EditorNodeMetadata::DataType::BOOL:
		elem.setAttribute(QStringLiteral("data_type"), QStringLiteral("bool"));
		break;
	case EditorNodeMetadata::DataType::INT:
		elem.setAttribute(QStringLiteral("data_type"), QStringLiteral("int"));
		break;
	case EditorNodeMetadata::DataType::DOUBLE:
		elem.setAttribute(QStringLiteral("data_type"), QStringLiteral("double"));
		break;
	case EditorNodeMetadata::DataType::STRING:
		elem.setAttribute(QStringLiteral("data_type"), QStringLiteral("string"));
		break;
	default:
		elem.setAttribute(QStringLiteral("data_type"), QStringLiteral("int"));
		qCritical()<<"EditorAlgNode unknown data type, defaulting to int"<<(int)md->dataType;
		break;
	}
}



EOBitLogic::EOBitLogic(const EditorObjectMetadata *metaData) :
	EditorObject("BitLogic", metaData)
{
}

EOBitLogic::EOBitLogic(QDomElement &elem, const EditorObjectMetadata *metaData, EditorDict *ed) :
	EditorObject(elem, metaData, ed)
{
}

bool EOBitLogic::canDeleteNode(int channel, const QString &nodeTypeName)
{
	bool res=EditorObject::canDeleteNode(channel, nodeTypeName);
	qDebug()<<"EOBitLogic::canDeleteNode"<<res<<nodeTypeName<<channel<<nodesOfType(nodeTypeName)-1;
	if(res && nodeTypeName==QStringLiteral("input"))
	{
		auto mit=md->editorNodeMetadatas.find(nodeTypeName);
		if(mit!=md->editorNodeMetadatas.end() && channel-mit->second->baseChannel<nodesOfType(nodeTypeName)-1)
			res=false; // disable delete on input nodes except last (don't mess up channel numbering)
	}
	return res;
}

void EOBitLogic::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	EditorObject::paint(painter, option, widget);
	auto it=keyVals.find("op");
	if(it==keyVals.end())
		return;
	painter->fillRect(2, 2, 34, 34, backgroundColor.lighter(135));
	QString op=it->second;
	// true|false|unity|not|and|or|parity|nand|nor|notparity
	if(op==QStringLiteral("true"))
		QIcon(":/res/ConfigEditor/logic-gate-TRUE48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("false"))
		QIcon(":/res/ConfigEditor/logic-gate-FALSE48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("unity"))
		QIcon(":/res/ConfigEditor/logic-gate-BUF48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("not"))
		QIcon(":/res/ConfigEditor/logic-gate-NOT48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("and"))
		QIcon(":/res/ConfigEditor/logic-gate-AND48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("or"))
		QIcon(":/res/ConfigEditor/logic-gate-OR48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("parity"))
		QIcon(":/res/ConfigEditor/logic-gate-XOR48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("nand"))
		QIcon(":/res/ConfigEditor/logic-gate-NAND48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("nor"))
		QIcon(":/res/ConfigEditor/logic-gate-NOR48.png").paint(painter, 3, 3, 32, 32);
	else if(op==QStringLiteral("notparity"))
		QIcon(":/res/ConfigEditor/logic-gate-XNOR48.png").paint(painter, 3, 3, 32, 32);
}

QStringList EOBitLogic::checkConsistency()
{
	QStringList res=EditorObject::checkConsistency();
	auto kit=keyVals.find(QStringLiteral("op"));
	// true|false|unity|not|and|or|parity|nand|nor|notparity
	if(kit==keyVals.end())
	{
		qCritical()<<"no op parameter for EOBitLogic";
		res<<tr("BitLogic %1 (id %2) misses op paramter.").arg(prefix).arg(id);
	}
	int cnt=nodesOfType(QStringLiteral("input"));
	if(kit->second==QStringLiteral("true") || kit->second==QStringLiteral("false"))
	{
		if(cnt>0)
			res<<tr("BitLogic %1 (id %2): operation requires no inputs but there are %3.").arg(prefix).arg(id).arg(cnt);
	}
	else if(kit->second==QStringLiteral("unity") || kit->second==QStringLiteral("not"))
	{
		if(cnt!=1)
			res<<tr("BitLogic %1 (id %2): operation requires one input but there are %3.").arg(prefix).arg(id).arg(cnt);
	}
	else
	{
		if(cnt<2)
			res<<tr("BitLogic %1 (id %2): operation requires at least two inputs but there are %3.").arg(prefix).arg(id).arg(cnt);
	}
	return res;
}



EOCompare::EOCompare(const pi_at_home::EditorObjectMetadata *metaData) : EditorObject("Compare", metaData)
{
	updateInfoStr();
}

EOCompare::EOCompare(QDomElement &elem, const pi_at_home::EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed) :
	EditorObject(elem, metaData, ed)
{
	updateInfoStr();
	if(nodesOfType(QStringLiteral("level(int)")))
		levelDt=EditorNodeMetadata::DataType::INT;
	else if(nodesOfType(QStringLiteral("level(dbl)")))
		levelDt=EditorNodeMetadata::DataType::DOUBLE;
	else if(nodesOfType(QStringLiteral("input(int)")))
		levelDt=EditorNodeMetadata::DataType::INT;
	else if(nodesOfType(QStringLiteral("input(dbl)")))
		levelDt=EditorNodeMetadata::DataType::DOUBLE;
}

bool EOCompare::canDeleteNode(int channel, const QString &nodeTypeName)
{
	bool res=EditorObject::canDeleteNode(channel, nodeTypeName);
	//qDebug()<<"EOBitLogic::canDeleteNode"<<res<<nodeTypeName<<channel<<nodesOfType(nodeTypeName)-1;
	if(res && nodeTypeName==QStringLiteral("out"))
		res=false; // out nodes are created and removed programatically
	if(res && (nodeTypeName==QStringLiteral("level(int)") || nodeTypeName==QStringLiteral("level(dbl)")))
	{
		auto mit=md->editorNodeMetadatas.find(nodeTypeName);
		if(mit!=md->editorNodeMetadatas.end() && channel-mit->second->baseChannel<nodesOfType(nodeTypeName)-1)
			res=false; // disable delete on input nodes except last (don't mess up channel numbering)
	}
	return res;
}

void EOCompare::updateInfoStr()
{
	auto it=keyVals.find(QStringLiteral("op"));
	if(it==keyVals.end())
		extInfoStr=QStringLiteral("NOP?");
	else
		extInfoStr=it->second;
}

bool EOCompare::contextMenuAddNodes(QMenu *menu)
{
	for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
	{
		QString nodeTypeName=nit->second->typeName;
		if( nodeTypeName==QStringLiteral("out") ||
			(levelDt==EditorNodeMetadata::DataType::DOUBLE &&
				(nodeTypeName==QStringLiteral("input(int)") || nodeTypeName==QStringLiteral("level(int)"))) ||
			(levelDt==EditorNodeMetadata::DataType::INT &&
				(nodeTypeName==QStringLiteral("input(dbl)") || nodeTypeName==QStringLiteral("level(dbl)"))) )
			continue;
		QAction *act=menu->addAction(QIcon(":/res/ConfigEditor/icons8-add-node-48.png"), QString("Add node %1").arg(nodeTypeName));
		if(nodesOfType(nodeTypeName)<nit->second->nodeMax)
			connect(act, &QAction::triggered, this, [this, nodeTypeName](){addNodeExtSl(nodeTypeName);});
		else
			act->setEnabled(false);
	}
	return true;
}

QStringList EOCompare::checkConsistency()
{
	QStringList res=EditorObject::checkConsistency();
	auto nds=childItems();
	EditorDict *ed=dynamic_cast<EditorDict *>(scene()->parent());
	map<int, double > defs; // channel -> default value
	foreach(QGraphicsItem *gi, nds)
	{
		EditorNode *en=dynamic_cast<EditorNode *>(gi);
		if(en && en->md->typeName.startsWith("level"))
		{
			int concnt=ed->getConnection(id, en->getChannel(), EditorConnection::EndPoint::DEST).size();
			if(!concnt)
			{
				KeyValMV &nkv=en->getKeyVals();
				auto it=nkv.find(QStringLiteral("default_value"));
				if(it==nkv.end())
					res<<QString("EOCompare: Level (ch %1) has neither default value nor connection").arg(en->getChannel());
				else
					defs.insert(pair<int,double>(en->getChannel(), it->second.toDouble()));
			}
		}
	}
	if(defs.size())
	{
		auto vit=defs.begin();
		double v=vit->second;
		++vit;
		for(; vit!=defs.end(); ++vit)
		{
			if(v>=vit->second)
				res<<QString("EOCompare: Default value of level (ch %1) has is not strictly ascending: %2 %3").arg(vit->first).arg(v).arg(vit->second);
			v=vit->second;
		}
	}
	auto it=keyVals.find(QStringLiteral("op"));
	if(it==keyVals.end())
	{
		qCritical()<<"EOCompare no keyval for op";
		res<<"EOCompare no keyval for op";
	}
	else
	{
		if(levelDt==EditorNodeMetadata::DataType::DOUBLE || levelDt==EditorNodeMetadata::DataType::INT)
		{
			int lcnt=nodesOfType(levelDt==EditorNodeMetadata::DataType::DOUBLE?QStringLiteral("level(dbl)"):QStringLiteral("level(int)"));
			if(it->second.startsWith(QStringLiteral("i")))
				++lcnt; // intervall test, N+1 out's
			int ocnt=nodesOfType(QStringLiteral("out"));
			while(ocnt>lcnt)
			{
				int ch=200;
				auto nds=childItems();
				foreach(QGraphicsItem *gi, nds)
				{
					EditorNode *en=dynamic_cast<EditorNode *>(gi);
					if(en && en->md->typeName==QStringLiteral("out") && en->getChannel()>ch)
						ch=en->getChannel();
				}
				deleteNodeSl(ch);
				--ocnt;
			}
			while(ocnt<lcnt)
			{
				EditorObject::addNodeSl(QStringLiteral("out"), true);
				++ocnt;
			}
		}
	}
	updateInfoStr();
	return res;
}

void EOCompare::addNodeExtSl(QString nodeTypeName, bool reindexAndUpdate)
{
	if(levelDt==EditorNodeMetadata::DataType::ANY)
	{
		if(nodeTypeName==QStringLiteral("input(int)") || nodeTypeName==QStringLiteral("level(int)"))
			levelDt=EditorNodeMetadata::DataType::INT;
		else if(nodeTypeName==QStringLiteral("input(dbl)") || nodeTypeName==QStringLiteral("level(dbl)"))
			levelDt=EditorNodeMetadata::DataType::DOUBLE;
		else
			qCritical()<<"EOCompare::addNodeExtSl type conflict"<<(int)levelDt<<nodeTypeName;
	}
	bool addOut=(nodeTypeName==QStringLiteral("level(dbl)") || nodeTypeName==QStringLiteral("level(int)"));
	EditorObject::addNodeSl(nodeTypeName, !addOut && reindexAndUpdate);
	if(addOut)
		EditorObject::addNodeSl(QStringLiteral("out"), reindexAndUpdate);
}

/*bool EOCompare::updateExtendedEditor(QWidget *parent, KeyValMV &currentKeyVals)
{
	DlgEOCompare dlg(this, currentKeyVals, parent);
	return dlg.exec()==QDialog::Accepted;
}*/


EditorNode *EODummy::algNodeFactoryDom(QDomElement &elem, const EditorObjectMetadata *objectMetaData, EditorDict *ed, QGraphicsItem *parent)
{
	// selection through data_type is stronger than the usual mechanism
	QString ntype=elem.attribute("data_type");
	auto it=objectMetaData->editorNodeMetadatas.find(ntype);
	if(it==objectMetaData->editorNodeMetadatas.end())
		return new EditorNode(elem, objectMetaData, ed, parent);
	return new EditorAlgNode(elem, it->second, ed, parent);
}

EditorNode *EODummy::algNodeFactoryNew(const pi_at_home::EditorNodeMetadata *metaData, int channel, QGraphicsItem *parent)
{
	return new EditorAlgNode(metaData, channel, parent);
}

EODummy::EODummy(const pi_at_home::EditorObjectMetadata *metaData) : EditorObject("Dummy", metaData)
{
}

EODummy::EODummy(QDomElement &elem, const pi_at_home::EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed) :
	EditorObject(elem, metaData, ed, &EODummy::algNodeFactoryDom)
{
}

EditorNodeFactoryNew EODummy::getNodeFactory()
{
	return &EODummy::algNodeFactoryNew;
}


EOInfoPanel::EOInfoPanel(const pi_at_home::EditorObjectMetadata *metaData) : EditorObject("InfoPanel", metaData)
{
	updateInfoStr();
}

EOInfoPanel::EOInfoPanel(QDomElement &elem, const pi_at_home::EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed) :
	EditorObject(elem, metaData, ed)
{
	updateInfoStr();
}

bool EOInfoPanel::canDeleteNode(int channel, const QString &nodeTypeName)
{
	bool res=EditorObject::canDeleteNode(channel, nodeTypeName);
	//qDebug()<<"EOBitLogic::canDeleteNode"<<res<<nodeTypeName<<channel<<nodesOfType(nodeTypeName)-1;
	if(res && nodeTypeName==QStringLiteral("lineText"))
		res=false; // out nodes are created and removed programatically
	return res;
}

/*void EOInfoPanel::updateInfoStr()
{
	auto it=keyVals.find(QStringLiteral("op"));
	if(it==keyVals.end())
		extInfoStr=QStringLiteral("NOP?");
	else
		extInfoStr=it->second;
}*/

bool EOInfoPanel::contextMenuAddNodes(QMenu *menu)
{
	for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
	{
		QString nodeTypeName=nit->second->typeName;
		if( nodeTypeName==QStringLiteral("lineText") )
			continue;
		QAction *act=menu->addAction(QIcon(":/res/ConfigEditor/icons8-add-node-48.png"), QString("Add node %1").arg(nodeTypeName));
		if(nodesOfType(nodeTypeName)<nit->second->nodeMax)
			connect(act, &QAction::triggered, this, [this, nodeTypeName](){addNodeExtSl(nodeTypeName);});
		else
			act->setEnabled(false);
	}
	return true;
}

QStringList EOInfoPanel::checkConsistency()
{
	QStringList res=EditorObject::checkConsistency();
	int lcnt=nodesOfType(QStringLiteral("lineValue"));
	int ocnt=nodesOfType(QStringLiteral("lineText"));
	while(ocnt>lcnt)
	{
		int ch=64;
		auto nds=childItems();
		foreach(QGraphicsItem *gi, nds)
		{
			EditorNode *en=dynamic_cast<EditorNode *>(gi);
			if(en && en->md->typeName==QStringLiteral("lineText") && en->getChannel()>ch)
				ch=en->getChannel();
		}
		deleteNodeSl(ch);
		--ocnt;
	}
	while(ocnt<lcnt)
	{
		EditorObject::addNodeSl(QStringLiteral("lineText"), true);
		++ocnt;
	}
	updateInfoStr();
	return res;
}

void EOInfoPanel::addNodeExtSl(QString nodeTypeName, bool reindexAndUpdate)
{
	bool addOut=(nodeTypeName==QStringLiteral("lineValue"));
	EditorObject::addNodeSl(nodeTypeName, !addOut && reindexAndUpdate);
	if(addOut)
		EditorObject::addNodeSl(QStringLiteral("lineText"), reindexAndUpdate);
}


EOTimer::EOTimer(const pi_at_home::EditorObjectMetadata *metaData) : EditorObject("Timer", metaData)
{
	EditorObject::addNodeSl(QStringLiteral("reset"), true);
	updateResetNode();
}

EOTimer::EOTimer(QDomElement &elem, const pi_at_home::EditorObjectMetadata *metaData, pi_at_home::EditorDict *ed) :
	EditorObject(elem, metaData, ed)
{
	auto dsit=keyVals.find("delay_style");
	if(dsit!=keyVals.end())
	{
		if(dsit->second!=QStringLiteral("time_of_day"))
		{
			EditorObject::addNodeSl(QStringLiteral("reset"), true);
		}
	}
	updateInfoStr();
	// todo feature piahd always creates time Input nodes - make visible in editor!
}

bool EOTimer::canDeleteNode(int channel, const QString &nodeTypeName)
{
	if(nodeTypeName==QStringLiteral("reset"))
		return false;
	return EditorObject::canDeleteNode(channel, nodeTypeName);
}

bool EOTimer::contextMenuAddNodes(QMenu *menu)
{
	for(auto nit=md->editorNodeMetadatas.begin(); nit!=md->editorNodeMetadatas.end(); ++nit)
	{
		QString nodeTypeName=nit->second->typeName;
		if( nodeTypeName==QStringLiteral("reset") )
			continue;
		QAction *act=menu->addAction(QIcon(":/res/ConfigEditor/icons8-add-node-48.png"), QString("Add node %1").arg(nodeTypeName));
		if(nodesOfType(nodeTypeName)<nit->second->nodeMax)
			connect(act, &QAction::triggered, this, [this, nodeTypeName](){addNodeSl(nodeTypeName);});
		else
			act->setEnabled(false);
	}
	return true;
}

void EOTimer::updateInfoStr()
{
	auto dsit=keyVals.find("delay_style");
	if(dsit==keyVals.end())
		extInfoStr=QStringLiteral("UNDEF:");
	else if(dsit->second=="absolute")
		extInfoStr=QStringLiteral("abs:");
	else if(dsit->second=="incremental")
		extInfoStr=QStringLiteral("inc:");
	else if(dsit->second=="time_of_day")
		extInfoStr=QStringLiteral("day:");
	else
		extInfoStr=dsit->second;
	QString templ="timer%1";
	int tcnt=0;
	for(; true; ++tcnt)
	{
		dsit=keyVals.find(templ.arg(tcnt));
		if(dsit==keyVals.end())
			break;
		if(dsit->second.endsWith(QStringLiteral("000000")))
		{
			extInfoStr+=dsit->second.mid(0, dsit->second.size()-6);
			extInfoStr+="M";
		}
		else if(dsit->second.endsWith(QStringLiteral("000")))
		{
			extInfoStr+=dsit->second.mid(0, dsit->second.size()-3);
			extInfoStr+="k";
		}
		else
			extInfoStr+=dsit->second;
		extInfoStr+="/";
	}
	//qDebug()<<"updating extInfoStr to"<<extInfoStr;
}

QStringList EOTimer::checkConsistency()
{
	updateResetNode();
	QStringList res=EditorObject::checkConsistency();
	bool isToggle=false;
	auto dsit=keyVals.find("op");
	if(dsit!=keyVals.end() && dsit->second==QStringLiteral("toggle"))
	{
		isToggle=true;
	}
	QString templ="timer%1";
	int tcnt=0;
	for(int idx=0; true; ++idx)
	{
		dsit=keyVals.find(templ.arg(idx));
		if(dsit==keyVals.end())
			break;
		if(dsit->first!=QStringLiteral("timer0") || dsit->second!=QStringLiteral("0"))
			++tcnt; // do not count leading 0-interval
	}
	if(!tcnt)
		res<<tr("Timer %1 must use at least one timing.").arg(prefix);
	if(isToggle && tcnt%2)
		res<<tr("Timer %1 uses toggle operation with an uneven number of timings. First and last delay will add!").arg(prefix);
	updateInfoStr();
	return res;
}

bool EOTimer::updateExtendedEditor(QWidget *parent, KeyValMV &currentKeyVals)
{
	DlgEOTimer dlg(this, currentKeyVals, parent);
	return dlg.exec()==QDialog::Accepted;
}

void EOTimer::updateResetNode()
{
	auto dsit=keyVals.find("delay_style");
	if(dsit!=keyVals.end())
	{
		bool isdod=(dsit->second==QStringLiteral("time_of_day"));
		int rcnt=nodesOfType(QStringLiteral("reset"));
		if(isdod && rcnt)
		{
			auto nds=childItems();
			foreach(QGraphicsItem *gi, nds)
			{
				EditorNode *en=dynamic_cast<EditorNode *>(gi);
				if(en && en->md->typeName==QStringLiteral("reset"))
					deleteNodeSl(en->getChannel());
			}
		}
		if(!isdod && !rcnt)
		{
			EditorObject::addNodeSl(QStringLiteral("reset"), true);
		}
	}
}

