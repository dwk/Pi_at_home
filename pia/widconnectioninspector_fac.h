#ifndef WIDCONNECTIONINSPECTORFAC_H
#define WIDCONNECTIONINSPECTORFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new pi_at_home::WidConnectionInspector(parent);
}
bool dockWidRegistered=pi_at_home::DockWidFactory::get().registerDockWid("Connection Inspector", "dockwid_connectioninspector", Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // WIDCONNECTIONINSPECTORFAC_H
