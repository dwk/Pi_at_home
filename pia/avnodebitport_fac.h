#ifndef AVNODEBITPORT_FAC_H
#define AVNODEBITPORT_FAC_H
#include "../piah_common/nodebitport_nm.h"

namespace
{
pi_at_home::AvNode * AvCreator(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeBitPort(frag, guiParent);
}
bool avatarRegistered=pi_at_home::AvatarFactory::registerAvatar(typeName, AvCreator);
}

#endif // AVNODEBITPORT_FAC_H
