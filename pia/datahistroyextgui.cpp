#include "datahistoryextgui.h"
#include "piadi.h"

QObject * pi_at_home::ExtGuiFactory(const DataHistory & dh)
{
	return new DataHistoryExtGui(dh);
}

DataHistoryExtGui::DataHistoryExtGui(const DataHistory & dh) :
	QObject(), baskey(dh.getAdr().toSettingsKey())
{
	QSettings settings;
	graphColor_=settings.value(baskey+"/color").value<QColor>();
	if(!graphColor_.isValid())
		graphColor_=QColor(80,80,80);
	graphLineWidth_=settings.value(baskey+"/linewidth", 3).toInt();
	fav_=settings.value(baskey+"/is_fav", false).toBool();
	//qDebug()<<"DataHistoryExtGui"<<baskey<<"loaded with"<<graphColor_<<graphLineWidth_<<fav_;
}

void DataHistoryExtGui::setGraphColor(const QColor &color)
{
	if(graphColor_!=color)
	{
		//qDebug()<<"DataHistoryExtGui"<<baskey<<"change to"<<graphColor_;
		graphColor_=color;
		QSettings settings;
		settings.setValue(baskey+"/color", graphColor_);
	}
}

void DataHistoryExtGui::setGraphLineWidth(int width)
{
	if(graphLineWidth_!=width)
	{
		//qDebug()<<"DataHistoryExtGui"<<baskey<<"change to"<<graphLineWidth_;
		graphLineWidth_=width;
		QSettings settings;
		settings.setValue(baskey+"/linewidth", graphLineWidth_);
	}
}

void DataHistoryExtGui::setFav(bool fav)
{
	if(fav_!=fav)
	{
		//qDebug()<<"DataHistoryExtGui"<<baskey<<"change to"<<fav_;
		fav_=fav;
		QSettings settings;
		settings.setValue(baskey+"/is_fav", fav_);
	}
}

