#include "editorobjectmetadata.h"
#include "piadi.h"


void EditorBaseConfig::reset()
{
	staticHosts.clear();
	broadcastMs=500;
	heartbeatMs=1000;
	uid=QStringLiteral("Pi3x");
	aliveInd.clear();
	rawConfig=false;
}

EditorParamMetadata::EditorParamMetadata(const QDomElement &elem)
{
	name=elem.attribute("name");
	QString typeStr=elem.attribute("type");
	if(typeStr=="required")
		type=Type::REQUIRED;
	else if(typeStr=="default_value")
		type=Type::DEFAULTVALUE;
	else if(typeStr=="omitable")
		type=Type::OMITABLE;
	else
		qWarning()<<"EditorParamMetadata undefined type"<<typeStr<<"use required, default_value or omitable";
	typeStr=elem.attribute("data_type");
	if(typeStr=="bool")
		dataType=DataType::BOOL;
	else if(typeStr=="int")
		dataType=DataType::INT;
	else if(typeStr=="double")
		dataType=DataType::DOUBLE;
	else if(typeStr=="string")
		dataType=DataType::STRING;
	else
		qWarning()<<"EditorParamMetadata undefined data_type"<<typeStr<<"use bool, int, double or string";
	typeStr=elem.attribute("data_filter");
	if(typeStr=="none" || typeStr.isEmpty())
		dataFilter=DataFilter::NONE;
	else if(typeStr=="list")
		dataFilter=DataFilter::LIST;
	else if(typeStr=="minmax")
		dataFilter=DataFilter::MINMAX;
	else
		qWarning()<<"EditorParamMetadata undefined data_filter"<<typeStr<<"use none, list or minmax";
	if(dataFilter!=DataFilter::NONE)
	{
		QStringList vls=elem.attribute("values").split("|", Qt::SkipEmptyParts);
		foreach(QString vl, vls)
		{
			filterVals.push_back(str2Var(vl));
		}
		if(dataFilter==DataFilter::MINMAX && filterVals.size()!=2)
		{
			qWarning()<<"EditorParamMetadata invaid filter values for minmax"<<elem.attribute("values");
		}
	}
	if(elem.hasAttribute("default"))
		def=str2Var(elem.attribute("default"));
	description=elem.attribute("description");
	if(description.size()>40)
		description=QString("<html><head/><body><p>%1</p></body></html>").arg(description);
}

QVariant EditorParamMetadata::str2Var(const QString &str)
{
	switch(dataType)
	{
	case DataType::BOOL:
	{
		bool v=(str=="true");
		return QVariant(v);
	}
	case DataType::INT:
	{
		bool ok=false;
		int v=(str.toInt(&ok));
		if(!ok)
		{
			qWarning()<<"EditorParamMetadata (int) invalid"<<str;
			return QVariant(0);
		}
		return QVariant(v);
	}
	case DataType::DOUBLE:
	{
		bool ok=false;
		double v=(str.toDouble(&ok));
		if(!ok)
		{
			qWarning()<<"EditorParamMetadata (double) invalid"<<str;
			return QVariant(0.);
		}
		return QVariant(v);
	}
	case DataType::STRING:
	case DataType::UNDEF:
	{
		return QVariant(str);
	}
	}
	return QVariant();
}


EditorNodeMetadata::EditorNodeMetadata(const QDomElement &elem) : type(Type::UNDEF), dataType(DataType::UNDEF)
{
	typeName=elem.attribute("name");
	if(elem.hasAttribute("base_channel"))
	{
		bool ok=false;
		int iv=elem.attribute("base_channel").toInt(&ok);
		if(ok)
			baseChannel=iv;
		else
			qWarning()<<"Node: invalid base_channel"<<typeName<<elem.attribute("base_channel");
	}
	fixed=(elem.attribute("style")=="fixed");
	QString typeStr=elem.attribute("type");
	if(typeStr=="input")
		type=Type::INPUT;
	else if(typeStr=="output")
		type=Type::OUTPUT;
	else if(typeStr=="bidirectional")
		type=Type::BIDIR;
	else if(typeStr=="connector")
	{
		type=Type::CONNECTOR;
		fixed=true; // supress editing von CONNECTOR channel, alias etc.
	}
	else
		qWarning()<<"EditorNodeMetadata undefined type"<<typeStr;
	typeStr=elem.attribute("data_type");
	if(typeStr=="bool")
		dataType=DataType::BOOL;
	else if(typeStr=="int")
		dataType=DataType::INT;
	else if(typeStr=="double")
		dataType=DataType::DOUBLE;
	else if(typeStr=="string")
		dataType=DataType::STRING;
	else if(typeStr=="any")
		dataType=DataType::ANY;
	else
		qWarning()<<"EditorNodeMetadata undefined data_type"<<typeStr<<"use bool, int, double, string or any";
	if(elem.hasAttribute("node_cnt"))
	{
		QStringList ndcnt=elem.attribute("node_cnt").split("-", Qt::SkipEmptyParts);
		bool minOk=false, maxOk=false;
		if(ndcnt.size()==1)
		{
			nodeMin=ndcnt.at(0).toInt(&minOk);
			nodeMax=nodeMin;
			if(!minOk)
			{
				qWarning()<<"EditorNodeMetadata node_cnt invalid (1)"<<elem.attribute("node_cnt");
				nodeMin=1, nodeMax=1;
			}
		}
		else if(ndcnt.size()==2)
		{
			nodeMin=ndcnt.at(0).toInt(&minOk);
			nodeMax=ndcnt.at(1).toInt(&maxOk);
			if(!minOk || !maxOk)
			{
				qWarning()<<"EditorNodeMetadata node_cnt invalid (2)"<<elem.attribute("node_cnt");
				nodeMin=1, nodeMax=1;
			}
		}
		else
		{
			qWarning()<<"EditorNodeMetadata node_cnt invalid (3)"<<elem.attribute("node_cnt");
			nodeMin=1, nodeMax=1;
		}
	}
	description=elem.attribute("description");
	if(description.size()>40)
		description=QString("<html><head/><body><p>%1</p></body></html>").arg(description);
	for(QDomNode n = elem.firstChild(); !n.isNull(); n = n.nextSibling())
	{
		if(n.nodeName()!="parameter")
			continue;
		QDomElement elem=n.toElement();
		EditorParamMetadata *epmd=new EditorParamMetadata(elem);
		editorParamMetadatas.insert(EditorParamMetadataMSP::value_type(epmd->name, epmd));
	}
}

EditorNodeMetadata::~EditorNodeMetadata()
{
	for(auto it=editorParamMetadatas.begin(); it!=editorParamMetadatas.end(); ++it)
		delete it->second;
}

QString EditorObjectMetadata::categoryToStr(EditorObjectMetadata::Category cat)
{
	switch(cat)
	{
	case EditorObjectMetadata::Category::DEVICE:
		return QStringLiteral("device");
	case EditorObjectMetadata::Category::ALGORITHM:
		return QStringLiteral("algorithm");
	default:
		return QStringLiteral("object");
	}
}

EditorObjectMetadata::Category EditorObjectMetadata::categoryFromStr(const QString &catStr)
{
	if(catStr=="device")
		return Category::DEVICE;
	else if(catStr=="algorithm")
		return Category::ALGORITHM;
	return Category::UNDEF;
}

EditorObjectMetadata::EditorObjectMetadata(const QDomElement &elem)
{
	typeName=elem.attribute("name");
	category=EditorObjectMetadata::categoryFromStr(elem.attribute("category"));
	description=elem.attribute("description");
	if(description.size()>40)
		description=QString("<html><head/><body><p>%1</p></body></html>").arg(description);
	icon=QIcon(elem.attribute("icon"));
	if(icon.isNull())
		icon=QIcon(":/res/ConfigEditor/icons8-puzzled-48.png");
	color=QColor(elem.attribute("color"));
	if(!color.isValid())
		color=QColor(255,200,200);
	for(QDomNode n = elem.firstChild(); !n.isNull(); n = n.nextSibling())
	{
		QDomElement ele=n.toElement();
		if(n.nodeName()=="node")
		{
			EditorNodeMetadata *enmd=new EditorNodeMetadata(ele);
			editorNodeMetadatas.insert(EditorNodeMetadataMSP::value_type(enmd->typeName, enmd));
		}
		else if(n.nodeName()=="parameter")
		{
			EditorParamMetadata *epmd=new EditorParamMetadata(ele);
			editorParamMetadatas.insert(EditorParamMetadataMSP::value_type(epmd->name, epmd));
		}
	}
}

EditorObjectMetadata::~EditorObjectMetadata()
{
	for(auto it=editorNodeMetadatas.begin(); it!=editorNodeMetadatas.end(); ++it)
		delete it->second;
	for(auto it=editorParamMetadatas.begin(); it!=editorParamMetadatas.end(); ++it)
		delete it->second;
}


