#include "dlghostcon.h"
#include "ui_dlghostcon.h"
#include "../piah_common/netcon.h"
#include <QProcess>
#include <QDir>
#include "piadi.h"

DlgHostCon::DlgHostCon(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgHostCon)
{
	ui->setupUi(this);
	connect(ui->pushTest, SIGNAL(clicked(bool)), this, SLOT(test()));
	connect(ui->pushRemoteConsole, SIGNAL(clicked(bool)), this, SLOT(remoteConSl()));
	connect(ui->pushViewLog, SIGNAL(clicked(bool)), this, SLOT(viewLogSl()));
	//connect(ui->pushRemove, SIGNAL(clicked(bool)), this, SLOT(removeSl()));
	HostDict & hd=DCON.hostDict();
	const HostInstanceMV & his=hd.hostInstances();
	std::multimap< QDateTime , QString > mmh; // lastcon -> uid
	for(auto it=his.begin(); it!=his.end(); ++it)
		mmh.insert(std::pair<QDateTime , QString>(it->second.lastConfirm, it->first));
	ui->tableKnownHosts->setColumnCount(5);
	ui->tableKnownHosts->setRowCount(his.size());
	int row=0;
	for(auto mit=mmh.rbegin(); mit!=mmh.rend(); ++mit)
	{
		auto it=his.find(mit->second);
		if(it==his.end())
		{
			qCritical()<<"what the hell? no"<<mit->second;
			continue;
		}
		QString ipv4c=QHostAddress(it->second.hadr).toString();
		if(!row)
			ui->lineIpAddress->setText(ipv4c);
		ui->tableKnownHosts->setItem(row, 0, new QTableWidgetItem(it->first));
		ui->tableKnownHosts->setItem(row, 1, new QTableWidgetItem(ipv4c));
		ui->tableKnownHosts->setItem(row, 2, new QTableWidgetItem(QString::number(it->second.port)));
		ui->tableKnownHosts->setItem(row, 3, new QTableWidgetItem(it->second.lastConfirm.toString(Qt::ISODate)));
		++row;
	}
	ui->tableKnownHosts->resizeColumnsToContents();
	ui->tableKnownHosts->resizeRowsToContents();
	connect(ui->tableKnownHosts, SIGNAL(clicked(QModelIndex)), this, SLOT(hostSl(QModelIndex)));
	buttonState();
}

DlgHostCon::~DlgHostCon()
{
	if(hc)
	{
		disconnect(hc,0,this,0); // avoid the sockReady(false) due to shutdown
		delete hc;
		hc=nullptr;
	}
	if(worker)
	{
		disconnect(worker,0,this,0);
		worker->quit();
		if(!worker->wait(5000))
			worker->terminate();
		delete worker;
		worker=nullptr;
	}
	delete ui;
}

QString DlgHostCon::hostAddress() const
{
	return ui->lineIpAddress->text();
}

int DlgHostCon::port() const
{
	return  ui->spinBox->value();
}

void DlgHostCon::accept()
{
	DCON.connections()->addHost(hostAddress(), port());
	QDialog::accept();
}

void DlgHostCon::test()
{
	ui->textBrowser->clear();
	hc=new NetCon(QHostAddress(ui->lineIpAddress->text()), ui->spinBox->value(), true);
	buttonState();
	ui->progressBar->setMaximum(hc->timeout()*2/100);
	connect(hc, SIGNAL(errorSig(int)), this, SLOT(sockErrorSig(int)));
	connect(hc, SIGNAL(readySig(bool)), this, SLOT(sockReady(bool)));
	progress=0;
	ui->progressBar->setMaximum(hc->timeout()*2/100);
	ui->progressBar->setValue(0);
	tid=startTimer(100);
	hc->start();
}

void DlgHostCon::on_tableKnownHosts_doubleClicked(const QModelIndex &index)
{
	hostSl(index);
	DCON.connections()->addHost(hostAddress(), port()); // add connection but keep the dialog open
}

/*void DlgHostCon::removeSl(const QString netAdr)
{
	//int row=ui->tableKnownHosts->currentRow();
	QStringList sl=netAdr.split(':');
	if(sl.size()!=2)
		return;
	QHostAddress adr(sl.at(0));
	int port=sl.at(1).toInt();
	//qDebug()<<"DlgHostCon::removeSls"<<netAdr<<adr<<port;
	DCON.hostDict().remove(adr, port);
	QList<QTableWidgetItem *> wil=ui->tableKnownHosts->findItems(sl.at(0), Qt::MatchExactly);
	foreach(QTableWidgetItem *twi, wil)
	{
		int r=twi->row();
		if(ui->tableKnownHosts->item(r, 2)->text()==sl.at(1))
			ui->tableKnownHosts->removeRow(r);
	}
	//qDebug()<<ui->tableKnownHosts->rowCount();
}*/

void DlgHostCon::sockErrorSig(int errNo)
{
	Q_UNUSED(errNo);
	if(!hc)
		return;
	ui->textBrowser->append(hc->lastErrorStr());
	//qDebug()<<"DlgHostCon::sockErrorSig";
	sockReady(false);
	//qDebug()<<"DlgHostCon::sockErrorSig2";
}

void DlgHostCon::sockReady(bool ready)
{
	if(tid)
	{
		killTimer(tid);
		tid=0;
	}
	disconnect(hc,0,this,0); // avoid the sockReady(false) due to shutdown
	hc->stop();
	if(ready)
	{
		ui->textBrowser->append(tr("Successfully tested!"));
		ui->textBrowser->append(QString("host %1 running %2 build %3 flavor %4 %5").arg(hc->uid(), hc->version(), hc->build(), hc->configFlavor(), hc->configTst()));
		DCON.hostDict().confirm(hc->uid(), hc->address(), hc->port());
	}
	else
	{
		ui->textBrowser->append(tr("TEST FAILED!"));
	}
	hc->deleteLater();
	hc=nullptr;
	ui->progressBar->setValue(ui->progressBar->maximum());
	buttonState();
}

void DlgHostCon::timerEvent(QTimerEvent *event)
{
	Q_UNUSED(event);
	ui->progressBar->setValue(++progress);
}

void DlgHostCon::hostSl(QModelIndex idx)
{
	int row=idx.row();
	ui->lineIpAddress->setText(ui->tableKnownHosts->item(row, 1)->text());
	ui->spinBox->setValue(ui->tableKnownHosts->item(row, 2)->text().toInt());
	buttonState();
}

void DlgHostCon::remoteConSl()
{
	WorkerCmd wc={.cmd=QStringLiteral("gnome-terminal"), .args=QStringList()<<"--"<<"ssh"<<"-lpi"<<ui->lineIpAddress->text(), .timeoutMs=5000};
	workerCmds.push_back(wc);
	workerNext();
}

void DlgHostCon::viewLogSl()
{ // like scp pi@10.0.1.14:/var/log/piahd.log .
	int row=ui->tableKnownHosts->currentRow();
	QString fromTempl("pi@%1:/var/log/piahd.log");
	QString hostname=ui->tableKnownHosts->item(row, 0)->text();
	QSettings settings;
	QDir dir(settings.value("dataPath").toString());
	if(!dir.exists(hostname))
		dir.mkdir(hostname);
	QString targetFile=dir.absoluteFilePath(hostname);
	QStringList args;
	args<<fromTempl.arg(ui->tableKnownHosts->item(row, 1)->text())<<targetFile;
	WorkerCmd wc={.cmd=QStringLiteral("scp"), .args=args, .timeoutMs=10000};
	workerCmds.push_back(wc);
	targetFile+=QStringLiteral("/piahd.log");
	WorkerCmd wc2={.cmd=QStringLiteral("gedit"), .args=QStringList()<<targetFile, .timeoutMs=-1};
	workerCmds.push_back(wc2);
	workerNext();
}

void DlgHostCon::workerDoneSl()
{
	killTimer(tid);
	tid=0;
	ui->progressBar->setValue(ui->progressBar->maximum());
	if(worker)
	{
		ui->textBrowser->append(worker->getResult());
		ui->textBrowser->setStyleSheet("color:red");
		ui->textBrowser->append(worker->getError());
		ui->textBrowser->setStyleSheet("");
		delete worker;
		worker=nullptr;
	}
	workerNext();
}

void DlgHostCon::workerNext()
{
	bool loop=true;
	while(loop)
	{
		if(workerCmds.size())
		{
			WorkerCmd wc=workerCmds.front();
			workerCmds.pop_front();
			ui->textBrowser->append(tr("executing %1 %2").arg(wc.cmd, wc.args.join(" ")));
			if(wc.timeoutMs>0)
			{
				worker=new WorkerThread(wc.cmd, wc.args, wc.timeoutMs, this);
				connect(worker, &QThread::finished, this, &DlgHostCon::workerDoneSl);
				progress=0;
				ui->progressBar->setMaximum(wc.timeoutMs/100);
				ui->progressBar->setValue(0);
				tid=startTimer(100);
				worker->start();
				loop=false;
			}
			else
				QProcess::startDetached(wc.cmd, wc.args);
		}
		else
			loop=false;
	}
	buttonState();
}

void DlgHostCon::buttonState()
{
	bool ena=(!worker && !hc); // manual IP address is OK && ui->tableKnownHosts->currentRow()>=0);
	ui->pushRemoteConsole->setEnabled(ena);
	ui->pushTest->setEnabled(ena);
	ui->pushViewLog->setEnabled(ena && ui->tableKnownHosts->currentRow()>=0);
}

void WorkerThread::run()
{
	//qDebug()<<"executing command"<<cmd<<args;
	if(timeoutMs>0)
	{
		QProcess proc;
		proc.start(cmd, args);
		proc.waitForFinished(timeoutMs);
		resultStr=QString::fromUtf8(proc.readAllStandardOutput());
		errStr=QString::fromUtf8(proc.readAllStandardError());
	}
	else
		QProcess::startDetached(cmd, args);
}
