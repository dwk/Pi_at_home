#ifndef AVNODETADLBUS_H
#define AVNODETADLBUS_H

#include "piadh.h"
#include "avnode.h"
#include "../piah_common/nodetadlbus.h"

class QDoubleSpinBox;
class QSpinBox;
class QDateTimeEdit;

namespace pi_at_home
{
class AvNodeTaDlBusSensorT : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeTaDlBusSensorT(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeTaDlBusSensorT() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeDouble(text);}
	QDoubleSpinBox * widVal;
	//double val;
protected slots:
	void valChanged(double val);
};

class AvNodeTaDlBusRpm : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeTaDlBusRpm(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeTaDlBusRpm() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeInt(text);}
	QSpinBox * widVal;
	//int val;
protected slots:
	void valChanged(int val);
};

class AvNodeTaDlBusTst : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeTaDlBusTst(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeTaDlBusTst() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	virtual QString serialize() {return value_.toDateTime().toString(Qt::ISODate);}
	virtual void deserialize(const QString & text) {return deserializeQDateTime(text);}
	QDateTimeEdit * widVal;
	//QDateTime val;
protected slots:
	void valChanged(QDateTime val);
};
}

#endif // AVNODETADLBUS_H
