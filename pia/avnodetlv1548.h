#ifndef AVNODETLV1548_H
#define AVNODETLV1548_H

#include "piadh.h"
#include <limits>
#include "avnode.h"
#include "../piah_common/nodetlv1548.h"
#include <QDoubleSpinBox>

namespace pi_at_home
{
class AvNodeTLV1548Rw : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeTLV1548Rw(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeTLV1548Rw() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeDouble(text);}
	QDoubleSpinBox * widVal;
protected slots:
	void valChanged(double val);
};

class AvNodeTLV1548Sc : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeTLV1548Sc(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeTLV1548Sc() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeDouble(text);}
	QDoubleSpinBox * widVal;
protected slots:
	void valChanged(double val);
};

class AvNodeTLV1548Pt : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeTLV1548Pt(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeTLV1548Pt() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeDouble(text);}
	QDoubleSpinBox * widVal;
protected slots:
	void valChanged(double val);
};
}
#endif // AVNODETLV1548_H
