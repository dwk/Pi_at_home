#ifndef AVNODESWITCHPORT_H
#define AVNODESWITCHPORT_H

#include "piadh.h"
#include "avnode.h"
#include "../piah_common/nodeswitchport.h"

namespace pi_at_home
{
class AvNodeSwitchPort : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeSwitchPort(const Fragment & fragment, QWidget *parent);
	virtual ~AvNodeSwitchPort() {}
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeBool(text);}
	void updateWid();
	QCheckBox * widVal;
	//bool val;
protected slots:
	void valChanged(bool val);
};
}

#endif // AVNODESWITCHPORT_H
