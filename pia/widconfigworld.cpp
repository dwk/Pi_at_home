#include "widconfigworld.h"
#include "widconfigworld_fac.h"
#include <QDomDocument>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QGraphicsTextItem>
#include "../piah_common/command.h"
#include "ui_widconfigworld.h"
#include "worldobject.h"
#include "piadi.h"

Q_LOGGING_CATEGORY(widconfigworld, "widconfigworld")

WidConfigWorld::WidConfigWorld(QWidget *parent) :
	QWidget(parent), ui(new ::Ui::WidConfigWorld), scene(new EditorScene(this))
{
	ui->setupUi(this);
	ui->graphicsView->setScene(scene);
	on_toolReload_clicked();
}

WidConfigWorld::~WidConfigWorld()
{
	for(auto eit=worldObjects.begin(); eit!=worldObjects.end(); ++eit)
		delete eit->second;
	delete ui;
}

void WidConfigWorld::on_toolReload_clicked()
{
	scene->clear();
	for(auto eit=worldObjects.begin(); eit!=worldObjects.end(); ++eit)
		delete eit->second;
	worldObjects.clear();
	WorldObject::defaultPosX=0.;
	WorldObject::defaultPosY=0.;
	QString binType, binTst;
	bool ebin=DCON.readPiahdInfo(QString(), binType, binTst, nullptr);
	if(!ebin)
	{
		QMessageBox::warning(this, tr("No Binary Info"), tr("Cannot read the timestamp of the deploy binary. Make sure you entered a valid binary in the deploy dialog and exited with the close button."));
		binTst.clear(); // supress testing
	}
	const HostInstanceMV & his=DCON.hostDict().hostInstances();
	for(auto hisit=his.begin(); hisit!=his.end(); ++hisit)
	{
		//qCDebug(widconfigworld)<<"toolReload"<<hisit->first<<hisit->second.uid;
		WorldObject *wo=new WorldObject(hisit->first, hisit->second, binTst);
		insertNewWorldObject(wo);
	}
	ui->toolConTest->setEnabled(true);
	ui->toolNodeTest->setEnabled(false);
}

void WidConfigWorld::on_toolConTest_clicked()
{
	for(auto eit=worldObjects.begin(); eit!=worldObjects.end(); ++eit)
		eit->second->testSrvSl();
	ui->toolNodeTest->setEnabled(true);
}

void WidConfigWorld::on_toolNodeTest_clicked()
{
	QPen goodPen(Qt::darkGreen);
	goodPen.setWidth(3);
	QPen badPen(Qt::red);
	badPen.setWidth(3);
	for(auto eit=worldObjects.begin(); eit!=worldObjects.end(); ++eit)
	{
		int idx=0;
		const QStringList & srcs=eit->second->getSourceAddrs();
		for(auto sit=srcs.begin(); sit!=srcs.end(); ++sit, ++idx)
		{
			NodeAddress na(*sit);
			QPointF startP=eit->second->getSourcePos(idx);
			auto woit=worldObjects.find(na.uid());
			if(woit==worldObjects.end())
			{
				//qCritical()<<"unknown server"<<na.uid();
				eit->second->markSource(idx, WorldObject::SourceState::NOSRV);
				scene->addEllipse(startP.x(), startP.y(), 5., 5., QPen(Qt::darkRed), QBrush(Qt::darkRed));
				continue;
			}
			if(woit->second->hasNode(*sit))
			{
				//qInfo()<<"OK"<<eit->first<<*sit;
				eit->second->markSource(idx, WorldObject::SourceState::OK);
				QPointF endP=woit->second->getSourcePos(-1);
				scene->addLine(startP.x(), startP.y(), endP.x(), endP.y(), goodPen);
				continue;
			}
			else
			{
				//qWarning()<<"NOK"<<eit->first<<woit->first<<*sit;
				eit->second->markSource(idx, WorldObject::SourceState::NONODE);
				QPointF endP=woit->second->getSourcePos(-1);
				scene->addLine(startP.x(), startP.y(), endP.x(), endP.y(), badPen);
				continue;
			}
		}
	}
}

void WidConfigWorld::objectReposSl(QString uid)
{
	Q_UNUSED(uid)

	QList<QGraphicsItem *> gis=scene->items();
	foreach(QGraphicsItem *gi, gis)
	{
		QGraphicsLineItem *gli=dynamic_cast< QGraphicsLineItem * >(gi);
		if(gli)
			scene->removeItem(gli);
		else
		{
			QGraphicsEllipseItem *gei=dynamic_cast< QGraphicsEllipseItem * >(gi);
			if(gei)
				scene->removeItem(gei);
		}
	}
}

void WidConfigWorld::insertNewWorldObject(WorldObject * wo)
{
	qCDebug(widconfigworld)<<"insertNewWorldObject"<<wo->getName();
	scene->addItem(wo);
	worldObjects.insert(WorldObjectMSSP::value_type(wo->getName(), QPointer<WorldObject>(wo)));
	connect(wo, &WorldObject::objectReposSig, this, &WidConfigWorld::objectReposSl);
}

