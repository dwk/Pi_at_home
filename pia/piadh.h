#ifndef PIAH_H
#define PIAH_H

#include <vector>
#include <list>
#include <map>
#include <set>
#include <QString>
#include <QLoggingCategory>
#include "../piah_common/ex.h"

#define VERSION_STR "piahGUI1.0"
#define GUI_ONLY

namespace pi_at_home
{
class Command;
class Fragment;
class CommandTarget;
class Node;
class Device;
}

typedef std::map<QString,QString> KeyValMV;

#endif // PIAH_H
