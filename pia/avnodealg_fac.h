#ifndef AVNODEALG_FAC_H
#define AVNODEALG_FAC_H

#include "../piah_common/nodealg_nm.h"

namespace
{
pi_at_home::AvNode * AvCreatorBool(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeAlgBool(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorInt(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeAlgInt(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorDbl(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeAlgDbl(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorStr(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeAlgStr(frag, guiParent);
}
bool avatarBoolRegistered=pi_at_home::AvatarFactory::registerAvatar(typeNameBool, AvCreatorBool);
bool avatarIntIRegistered=pi_at_home::AvatarFactory::registerAvatar(typeNameInt, AvCreatorInt);
bool avatarDblORegistered=pi_at_home::AvatarFactory::registerAvatar(typeNameDbl, AvCreatorDbl);
bool avatarStrIRegistered=pi_at_home::AvatarFactory::registerAvatar(typeNameStr, AvCreatorStr);
}

#endif // AVNODEALG_FAC_H
