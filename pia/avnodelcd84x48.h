#ifndef AVNODELCD84X48_H
#define AVNODELCD84X48_H

#include "piadh.h"
#include "avnode.h"
//#include "../piah_common/nodealg.h"
//#include "../piah_common/device.h"
#include <QDebug>
#include <QDoubleSpinBox>
#include <QLineEdit>

namespace pi_at_home
{

class AvNodeLcd84x48Line : public AvNode
{
	Q_OBJECT
public:
	explicit AvNodeLcd84x48Line(const Fragment & fragment, QWidget *parent);
	virtual void processCommand(Command &cmd, Fragment & relevant);
protected:
	//virtual QString serialize() works fine
	virtual void deserialize(const QString & text) {return deserializeQString(text);}
	QLineEdit * widVal;
protected slots:
	void editingFinishedSl();
};

}
#endif // AVNODELCD84X48_H
