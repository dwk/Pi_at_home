#ifndef DLGEDITBASECONFIG_H
#define DLGEDITBASECONFIG_H

#include "piadh.h"
#include <QDialog>
#include <QGridLayout>
#include "editorobjectmetadata.h"
#include "editorobject.h"

namespace Ui
{
	class DlgEditBaseConfig;
}

namespace pi_at_home
{

class DlgEditBaseConfig : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEditBaseConfig(QWidget *parent, EditorBaseConfig & bc, const QStringList & ports);
	virtual ~DlgEditBaseConfig();
public slots:
	void on_tableWidget_itemSelectionChanged();
	void on_pushAdd_clicked();
	void on_pushDelete_clicked();
	virtual void accept();
private slots:
private:
	Ui::DlgEditBaseConfig *ui;
	EditorBaseConfig & bc;
};
}
#endif // DLGEDITBASECONFIG_H
