#include "dlgabout.h"
#include "dockwidfactory.h"
#include <QFileDialog>
#include "../piah_common/buildtst.h"
#include "piadi.h"
#include "ui_dlgabout.h"

DlgAbout::DlgAbout(QWidget *parent) : QDialog(parent), ui(new Ui::DlgAbout)
{
	ui->setupUi(this);
	ui->label_ProgVersion->setText(VERSION_STR);
	ui->label_ProgBuild->setText(QString("Build timestamp: ")+buildTimestamp());
	ui->label_QtVersion->setText(QString("Qt Version: ")+QT_VERSION_STR+" (open source)");
	ui->label_QCustomPlotVersion->setText("QCustomPlot 2.0.1");
	ui->comboCentral->addItem(tr("none"), QVariant());
	QStringList dws=DockWidFactory::get().allDockWids();
	foreach(QString dwname, dws)
	{
		DockWidFactory::DWInfo *dwi=DockWidFactory::get().produce(dwname);
		if(!dwi)
			continue;
		ui->comboCentral->addItem(dwi->guiName, QVariant(dwi->techName));
	}
	QSettings settings;
	QString cent=settings.value("layout/central").toString();
	int ind=ui->comboCentral->findData(QVariant(cent));
	ui->comboCentral->setCurrentIndex(ind<0?0:ind);
	ui->lineDataPath->setText(settings.value("dataPath").toString());
}

DlgAbout::~DlgAbout() {delete ui;}

void DlgAbout::accept()
{
	QSettings settings;
	settings.setValue("layout/central", ui->comboCentral->currentData().toString());
	settings.setValue("dataPath", ui->lineDataPath->text());
	QDialog::accept();
}

void DlgAbout::on_toolDataPath_clicked()
{
	QSettings settings;
	QString dir = QFileDialog::getExistingDirectory(this, tr("Set Data Directory"), ui->lineDataPath->text(),
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if(dir.isEmpty())
		return;
	ui->lineDataPath->setText(dir);
}
