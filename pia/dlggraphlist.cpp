#include "dlggraphlist.h"
#include <QTableWidgetItem>
#include <QPushButton>
#include "datahistoryextgui.h"
#include "dlgdata.h"
#include "piadi.h"
#include "ui_dlggraphlist.h"

DlgGraphList::DlgGraphList(QWidget *parent, WidGraph::DispMap & dispMap) :
	QDialog(parent), ui(new Ui::DlgGraphList), dispMap(dispMap), mapper(new QSignalMapper(this))
{
	ui->setupUi(this);
	ui->tableWidget->setRowCount(dispMap.size());
	ui->tableWidget->setColumnCount(4);
	int r=0;
	for(WidGraph::DispMap::iterator it=dispMap.begin(); it!=dispMap.end(); ++it, ++r)
	{
		DataHistory * dh = DCON.getDataHistory(it->first, false);
		if(!dh)
			continue;
		DataHistoryExtGui * dhe=qobject_cast< DataHistoryExtGui * >(dh->guiData().data());
		QTableWidgetItem * twi=new QTableWidgetItem(dh->getName());
		ui->tableWidget->setItem(r, 0, twi);
		twi=new QTableWidgetItem("");
		twi->setFlags(twi->flags() | Qt::ItemIsUserCheckable);
		twi->setCheckState(it->second?Qt::Checked:Qt::Unchecked);
		ui->tableWidget->setItem(r, 1, twi);
		twi=new QTableWidgetItem("");
		twi->setFlags(twi->flags() | Qt::ItemIsUserCheckable);
		twi->setCheckState(dhe->isFav()?Qt::Checked:Qt::Unchecked);
		ui->tableWidget->setItem(r, 2, twi);
		QPushButton * tb=new QPushButton(tr("configure"));
		mapper->setMapping(tb, it->first.toString());
		connect(tb, SIGNAL(clicked()), mapper, SLOT(map()));
		ui->tableWidget->setCellWidget(r, 3, tb);
	}
	ui->tableWidget->resizeColumnsToContents();
	ui->tableWidget->resizeRowsToContents();
	connect(mapper, SIGNAL(mapped(QString)), this, SLOT(clicked(QString)));
}

DlgGraphList::~DlgGraphList() {delete ui;}

void DlgGraphList::accept()
{
	int r=0;
	for(WidGraph::DispMap::iterator it=dispMap.begin(); it!=dispMap.end(); ++it, ++r)
	{
		DataHistory * dh = DCON.getDataHistory(it->first, false);
		if(!dh)
			continue;
		DataHistoryExtGui * dhe=qobject_cast< DataHistoryExtGui * >(dh->guiData().data());
		it->second=(ui->tableWidget->item(r, 1)->checkState()==Qt::Checked);
		dhe->setFav(ui->tableWidget->item(r, 2)->checkState()==Qt::Checked);
	}
	QDialog::accept();
}

void DlgGraphList::clicked(QString adrStr)
{
	NodeAddress adr(adrStr);
	DataHistory * dh = DCON.getDataHistory(adr, false);
	qDebug()<<"editiging"<<adrStr<<adr.toStringVerbose()<<dh;
	if(dh)
	{
		DlgData dlg(this, *dh);
		dlg.exec();
	}
	else
		qCritical()<<"no DataHistory for"<<adrStr;
}

