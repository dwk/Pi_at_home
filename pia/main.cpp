#include "mainwindow.h"
#include "piaapp.h"
#include <QLoggingCategory>

//using namespace pi_at_home;

// todo bug
//	* EditorObject::addNodeSl ignores highest channel number (and push pull offset for BitPort) but uses number of channels as new channel number -> overlapping nodes!
//	* failed connect attempt due to "already exists" leaves dead additional tab for remote nodes
//  * config editor: move of multiple selected devices or algs does not relocate connections except for the device grabed.
// todo feature
//  * make EditorNode a command target - register always or just when connected? - display values in ConfigEditor when connected
//	* AvNode::clickedSim: Simulation requied? enable / disable widgets, send command to node - what for?
//	* undo in config editor
//	* Do we need a channel parameter in nodes with a fixed channel? Could always be the base_channel from meta data. Omit from ConfigEditorMarkup.xml?
int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("dwk");
	QCoreApplication::setOrganizationDomain("effektlict.at");
	QCoreApplication::setApplicationName("PiAtHome");
	QLoggingCategory::setFilterRules(QStringLiteral(
//			"*=false\n"
			"editorconnection=false\n"
			"editornode=false\n"
			"editorobject=false\n"
			"widconfigeditor=false\n"
										 ));
	pi_at_home::PiaApp a(argc, argv);
	pi_at_home::MainWindow w;
	w.show();
	return a.exec();
}
