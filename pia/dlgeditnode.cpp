#include "dlgeditnode.h"
#include <QSpinBox>
#include <QCheckBox>
#include "piadi.h"
#include "ui_dlgeditnode.h"

DlgEditNode::DlgEditNode(QWidget *parent, EditorNode & node) :
	QDialog(parent), ui(new Ui::DlgEditNode), grid(new QGridLayout()), node(node), oldChannel(node.getChannel())
{
	ui->setupUi(this);
	ui->lineAlias->setText(node.getNodeAlias());
	/*ui->spinChannel->setValue(node.getChannel());
	ui->checkHidden->setChecked(node.isHidden());
	ui->spinLogHoldoffMs->setValue(node.getLogHoldoffMs());
	delete ui->labelParam;
	delete ui->lineParam;
	delete ui->checkParam;
	*/
	ui->comboType->setCurrentIndex((int)node.md->type);
	ui->comboDataType->setCurrentIndex((int)node.md->dataType);
	QFrame *frame=new QFrame();
	ui->scrollArea->setWidget(frame);
	frame->setLayout(grid);
	KeyValMV & kv=node.getKeyVals();
	int r=0;
	for(auto mit=node.md->editorParamMetadatas.begin(); mit!=node.md->editorParamMetadatas.end(); ++mit, ++r)
	{
		auto kit=kv.find(mit->second->name);
		bool markit=false;
		if(kit==kv.end())
		{
			if(mit->second->type==EditorParamMetadata::Type::REQUIRED)
				qWarning()<<"DlgEditNode missing required"<<mit->second->name;
			else
				markit=true;
			//qDebug()<<"no kv for"<<mit->second->name<<markit;
		}
		if(mit->first=="channel")
			channelRow=r;
		grid->addWidget(new QLabel(mit->first, this), r, 0);
		QWidget *w=nullptr;
		if(mit->second->dataFilter==EditorParamMetadata::DataFilter::LIST)
		{
			QComboBox *wid=new QComboBox(this);
			w=wid;
			for(auto vit=mit->second->filterVals.begin(); vit!=mit->second->filterVals.end(); ++vit)
				wid->addItem(vit->toString());
			if(kit!=kv.end())
				wid->setCurrentText(kit->second);
			grid->addWidget(wid, r, 1);
		}
		else
		{
			switch(mit->second->dataType)
			{
			case EditorParamMetadata::DataType::BOOL:
			{
				QCheckBox *wid=new QCheckBox(this);
				w=wid;
				//qDebug()<<"Creating checkbox for"<<mit->first<<"value"<<(kit==kv.end()?"<noval!>":kit->second);
				if(kit!=kv.end())
				{
					//bool b=(kit->second=="true");
					//qDebug()<<"Creating checkbox"<<b;
					wid->setChecked(kit->second=="true");
				}
				grid->addWidget(w, r, 1);
				break;
			}
			case EditorParamMetadata::DataType::INT:
			{
				QSpinBox *wid=new QSpinBox(this);
				w=wid;
				if(mit->second->dataFilter==EditorParamMetadata::DataFilter::MINMAX)
				{
					wid->setMinimum(mit->second->filterVals.at(0).toInt());
					wid->setMaximum(mit->second->filterVals.at(1).toInt());
				}
				else
				{
					wid->setMinimum(std::numeric_limits<int>::min());
					wid->setMaximum(std::numeric_limits<int>::max());
				}
				if(kit!=kv.end())
					wid->setValue(kit->second.toInt());
				grid->addWidget(wid, r, 1);
				break;
			}
			case EditorParamMetadata::DataType::DOUBLE:
			case EditorParamMetadata::DataType::STRING:
			case EditorParamMetadata::DataType::UNDEF:
			{
				QLineEdit *wid=new QLineEdit(this);
				w=wid;
				if(kit!=kv.end())
					wid->setText(kit->second);
				grid->addWidget(w, r, 1);
				break;
			}
			}
		}
		if(mit->second->type==EditorParamMetadata::Type::DEFAULTVALUE || mit->second->type==EditorParamMetadata::Type::OMITABLE)
		{
			QCheckBox * doWid=new QCheckBox(mit->second->type==EditorParamMetadata::Type::DEFAULTVALUE?tr("Default"):tr("Omit"), this);
			if(mit->second->type==EditorParamMetadata::Type::DEFAULTVALUE)
				doWid->setToolTip(mit->second->def.toString());
			doWid->setChecked(markit);
			connect(doWid, &QCheckBox::stateChanged, this, [this, r](int checkState){defOmChangedSl(checkState, r);});
			grid->addWidget(doWid, r, 2);
			defOmChangedSl(doWid->isChecked()?Qt::Checked:Qt::Unchecked, r); // resets value to default
		}
		if(w)
			w->setToolTip(mit->second->description);
	}
	grid->setRowStretch(r, 1);
	if(node.md->fixed)
	{
		ui->pushOk->setDisabled(true);
		setWindowTitle(tr("Node Detais (fixed)"));
	}
}

DlgEditNode::~DlgEditNode() {delete ui;}

void DlgEditNode::accept()
{
	EditorObject *eo=dynamic_cast<EditorObject *>(node.parentItem());
	auto li=grid->itemAtPosition(channelRow, 1);
	if(li)
	{
		int newChannel=dynamic_cast<QSpinBox *>(li->widget())->value();
		if(!eo->canChangeChannel(oldChannel, newChannel))
		{
			QMessageBox::warning(this, tr("Invalid Channel"), tr("Channel %1 is already in use. Channels must be unique.").arg(newChannel));
			return;
		}
		// node.channel=newChannel; handeled in node.keyvalsChanged()
	}
	node.alias=ui->lineAlias->text();
	int rcnt=grid->rowCount();
	KeyValMV & kv=node.getKeyVals();
	for(int r=0; r<rcnt; ++r)
	{
		auto li=grid->itemAtPosition(r, 0);
		if(!li)
			continue; // the last line usually which has only a spacer
		QLabel * l=dynamic_cast<QLabel *>(li->widget());
		if(!l)
		{
			qWarning()<<"DlgEditNode::accept no label"<<r;
			continue;
		}
		QCheckBox * ch=dynamic_cast<QCheckBox *>(grid->itemAtPosition(r, 2)?grid->itemAtPosition(r, 2)->widget():nullptr);
		bool dowrite=true;
		if(ch && ch->isChecked())
			dowrite=false; // doesn't matter if omitable or default_value
		auto it=kv.find(l->text());
		QString data;
		bool dok=false;
		QLineEdit * led=dynamic_cast<QLineEdit *>(grid->itemAtPosition(r, 1)->widget());
		if(led)
		{
			dok=true;
			data=led->text();
		}
		if(!dok)
		{
			QComboBox * cbd=dynamic_cast<QComboBox *>(grid->itemAtPosition(r, 1)->widget());
			if(cbd)
			{
				dok=true;
				data=cbd->currentText();
			}
			if(!dok)
			{
				QCheckBox * chd=dynamic_cast<QCheckBox *>(grid->itemAtPosition(r, 1)->widget());
				if(chd)
				{
					dok=true;
					data=(chd->isChecked()?"true":"false");
				}
				if(!dok)
				{
					QSpinBox * spd=dynamic_cast<QSpinBox *>(grid->itemAtPosition(r, 1)->widget());
					if(spd)
					{
						dok=true;
						data=QString::number(spd->value());
					}
				}
			}
		}
		if(!dok)
		{
			qWarning()<<"DlgEditNode::accept no data"<<r<<l->text();
			continue;
		}
		if(dowrite)
		{
			if(it==kv.end())
				kv.insert(KeyValMV::value_type(l->text(), data));
			else
				it->second=data;
		}
		else
		{
			if(it==kv.end())
				continue; // it's fine
			else
				kv.erase(it);
		}
	}
	node.keyvalsChanged();
	QDialog::accept();
}

void DlgEditNode::defOmChangedSl(int checkState, int row)
{
	QWidget *w=grid->itemAtPosition(row, 1)->widget();
	if(!w)
		return;
	w->setEnabled(checkState!=Qt::Checked);
	if(checkState!=Qt::Checked)
		return; // don't set default if do-box is unchecked
	QWidget *w2=grid->itemAtPosition(row, 2)->widget();
	if(!w2)
		return;
	QString d=w2->toolTip();
	//qDebug()<<"tooltip as default"<<d;
	QLineEdit * led=dynamic_cast<QLineEdit *>(w);
	if(led)
		led->setText(d);
	else
	{
		QComboBox * cbd=dynamic_cast<QComboBox *>(w);
		if(cbd)
			cbd->setCurrentText(d);
		else
		{
			QCheckBox * chd=dynamic_cast<QCheckBox *>(w);
			if(chd)
				chd->setChecked(d=="true");
			else
			{
				QSpinBox * spd=dynamic_cast<QSpinBox *>(w);
				if(spd)
					spd->setValue(d.toInt());
			}
		}
	}
}
