#include "dockwidfactory.h"
#include "piadi.h"

DockWidFactory * DockWidFactory::inst=nullptr;

bool DockWidFactory::registerDockWid(QString guiName, QString techName, Qt::DockWidgetAreas allowedAreas, Qt::DockWidgetArea defaultArea, DockWidCreator factory)
{
	bool res=dwInfoMV.insert(DWInfoMV::value_type(techName, DWInfo(guiName, techName, allowedAreas, defaultArea, factory))).second;
	qInfo()<<"registring DockWid"<<techName<<res;
	return res;
}

pi_at_home::DockWidFactory::DWInfo *DockWidFactory::produce(const QString &techName)
{
	DWInfoMV::iterator it=dwInfoMV.find(techName);
	if(it==dwInfoMV.end())
	{
		qCritical()<<"unknown dock widget type"<<techName;
		return nullptr;
	}
	return &(it->second);
}

QStringList DockWidFactory::allDockWids()
{
	QStringList res;
	for(DWInfoMV::iterator it=dwInfoMV.begin(); it!=dwInfoMV.end(); ++it)
		res<<it->first;
	return res;
}
