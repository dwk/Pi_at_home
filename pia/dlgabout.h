#ifndef DLGABOUT_H
#define DLGABOUT_H

#include "piadh.h"
#include <QDialog>

namespace Ui
{
	class DlgAbout;
}

namespace pi_at_home
{

class DlgAbout : public QDialog
{
	Q_OBJECT
public:
	explicit DlgAbout(QWidget *parent);
	~DlgAbout();
public slots:
	virtual void accept();
private slots:
	void on_toolDataPath_clicked();
private:
	Ui::DlgAbout *ui;
};
}
#endif // DLGABOUT_H
