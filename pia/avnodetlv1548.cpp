#include "avnodetlv1548.h"
#include "avnodetlv1548_fac.h"
#include "ui_widnode.h"
#include "piadi.h"

AvNodeTLV1548Rw::AvNodeTLV1548Rw(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDoubleSpinBox(this))
{
	defval_=NodeValue((double)0.);
	ui->labelState->setToolTip("TLV1548");
	widVal->setMinimum(0);
	widVal->setMaximum(1023);
	deserialize(fragment.attribute("val"));
	widVal->setValue(value_.toInt());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(double)), this, SLOT(valChanged(double)));
}

void AvNodeTLV1548Rw::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toDouble());
	widVal->blockSignals(false);
}

void AvNodeTLV1548Rw::valChanged(double val)
{
	value_=val;
	nodeValueChanged();
}

AvNodeTLV1548Sc::AvNodeTLV1548Sc(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDoubleSpinBox(this))
{
	defval_=NodeValue((double)0.);
	ui->labelState->setToolTip("TLV1548-scaled");
	widVal->setMinimum(0.);
	widVal->setMaximum(2000.);
	deserialize(fragment.attribute("val"));
	widVal->setValue(value_.toDouble());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(double)), this, SLOT(valChanged(double)));
}

void AvNodeTLV1548Sc::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toDouble());
	widVal->blockSignals(false);
}

void AvNodeTLV1548Sc::valChanged(double val)
{
	value_=val;
	nodeValueChanged();
}

AvNodeTLV1548Pt::AvNodeTLV1548Pt(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDoubleSpinBox(this))
{
	defval_=NodeValue((double)0.);
	ui->labelState->setToolTip("TLV1548-Pt1000");
	widVal->setMinimum(-273.14);
	widVal->setMaximum(2000.);
	deserialize(fragment.attribute("val"));
	widVal->setValue(value_.toDouble());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(double)), this, SLOT(valChanged(double)));
}

void AvNodeTLV1548Pt::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toDouble());
	widVal->blockSignals(false);
}

void AvNodeTLV1548Pt::valChanged(double val)
{
	value_=val;
	nodeValueChanged();
}
