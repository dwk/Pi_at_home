#include "avnodeswitchport.h"
#include "avnodeswitchport_fac.h"
#include "ui_widnode.h"
#include "piadi.h"

AvNodeSwitchPort::AvNodeSwitchPort(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent, false) //, widVal(new QCheckBox(this))
{
	defval_=NodeValue(false);
	ui->labelState->setToolTip(typeName);
	deserialize(fragment.attribute("val"));
	updateWid();
	/*
	widVal->setEnabled(false);
	ui->horizontalLayout->insertWidget(3, widVal, 1);
	connect(widVal, SIGNAL(clicked(bool)), this, SLOT(valChanged(bool)));
	*/
}

void AvNodeSwitchPort::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	AvNode::processCommand(cmd, relevant);
	updateWid();
}

void AvNodeSwitchPort::updateWid()
{
	if(isHidden_)
		return;
	bool v=value_.toBool();
	ui->valueDummy->setStyleSheet(v?"background: yellow":"background: blue");
	ui->valueDummy->setText(v?tr("active"):tr("inactive"));
	//widVal->setChecked(value_.toBool());

}

void AvNodeSwitchPort::valChanged(bool val)
{
	value_=val;
	nodeValueChanged();
}
