#include "avnodeardubridge.h"
#include "avnodeardubridge_fac.h"
#include "ui_widnode.h"
#include "piadi.h"


// QString -----------------------------------------------------
AvNodeArduBridge::AvNodeArduBridge(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent, false), widVal(new QLineEdit(this)), tbut(new QToolButton(this))
{
	defval_=NodeValue(QString(""));
	ui->labelState->setToolTip(typeName);
	ui->valueDummy->setStyleSheet("background: #e0e0e0");
	ui->valueDummy->setText(fragment.attribute("val"));
	ui->valueDummy->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	QHBoxLayout *l=new QHBoxLayout();
	QIcon icon1;
	icon1.addFile(QStringLiteral(":/res/arrow_rotate_clockwise.png"), QSize(), QIcon::Normal, QIcon::Off);
	icon1.addFile(QStringLiteral(":/res/arrow_rotate_clockwise.png"), QSize(), QIcon::Normal, QIcon::On);
	tbut->setIcon(icon1);
	l->insertWidget(0, widVal, 1);
	l->insertWidget(1, tbut);
	ui->mainLayout->addLayout(l, 1, 3, 1, 2);
	connect(tbut, SIGNAL(clicked()), this, SLOT(editingFinishedSl()));
}

void AvNodeArduBridge::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	ui->valueDummy->setText(value_.toString());
	//widVal->blockSignals(true);
	//widVal->setText(value_.toString());
	//widVal->blockSignals(false);
}

void AvNodeArduBridge::editingFinishedSl()
{
	value_=NodeValue(widVal->text()); // Fragment does marshaling
	nodeValueChanged();
}


