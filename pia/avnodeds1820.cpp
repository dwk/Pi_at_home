#include "avnodeds1820.h"
#include "avnodeds1820_fac.h"
#include "ui_widnode.h"
#include "piadi.h"

AvNodeDS1820::AvNodeDS1820(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDoubleSpinBox(this))
{
	defval_=NodeValue((double)0.);
	ui->labelState->setToolTip(typeName);
	deserialize(fragment.attribute("val"));
	widVal->setValue(value_.toDouble());
	widVal->setEnabled(false);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(double)), this, SLOT(valChangedSl(double)));
}

void AvNodeDS1820::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	AvNode::processCommand(cmd, relevant);
	//qDebug()<<"update"<<widVal->value()<<value_.toDouble()
	widVal->blockSignals(true);
	widVal->setValue(value_.toDouble());
	widVal->blockSignals(false);
}

void AvNodeDS1820::valChangedSl(double val)
{
	value_=NodeValue(val);
	nodeValueChanged();
}
