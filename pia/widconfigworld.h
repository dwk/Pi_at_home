#ifndef WIDCONFIGWORLD_H
#define WIDCONFIGWORLD_H

#include "piadh.h"
#include <QWidget>
#include <QDomElement>
#include <QGraphicsScene>
#include <QIcon>
#include "worldobject.h"
//#include "editorconnection.h"
#include "editorscene.h"
#include "../piah_common/commandtarget.h"

Q_DECLARE_LOGGING_CATEGORY(widconfigworld)

namespace Ui {
class WidConfigWorld;
}

namespace pi_at_home
{

class WidConfigWorld : public QWidget
{
	Q_OBJECT
public:
	struct TempCon
	{
		TempCon() : deviceId(-1), channel(-1), startPos(QPointF()) {}
		TempCon(int deviceId, int channel, const QPointF &startScenePos) : deviceId(deviceId), channel(channel), startPos(startScenePos) {}
		bool isActive() const {return deviceId>-1 && channel>-1;}
		int deviceId=-1, channel=-1;
		QPointF startPos;
	};
	explicit WidConfigWorld(QWidget *parent = 0);
	~WidConfigWorld();
private slots:
	void on_toolReload_clicked();
	void on_toolConTest_clicked();
	void on_toolNodeTest_clicked();
	void objectReposSl(QString uid);
private:
	void insertNewWorldObject(WorldObject * wo);
	Ui::WidConfigWorld *ui;
	EditorScene *scene;
	WorldObjectMSSP worldObjects;
	TempCon tempCon;
signals:
};
}

#endif // WIDCONFIGWORLD_H
