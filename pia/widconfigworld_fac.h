#ifndef WIDCONFIGWORLDFAC_H
#define WIDCONFIGWORLDFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new pi_at_home::WidConfigWorld(parent);
}
bool dockWidRegistered=pi_at_home::DockWidFactory::get().registerDockWid("Config World Viewer", "dockwid_configworld", Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::LeftDockWidgetArea, DockCreator);
}

#endif // WIDCONFIGWORLDFAC_H
