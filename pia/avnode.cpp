#include "avnode.h"
#include "../piah_common/command.h"
#include <QCryptographicHash>
#include <QIcon>
#include <QMenu>
#include <QClipboard>
#include "ui_widnode.h"
#include "piadi.h"

AvNode::AvNode(const pi_at_home::Fragment &fragment, QWidget *parent, bool removeWidDummy) :
	QWidget(parent), ui(new Ui::WidNode)
{
	ui->setupUi(this);
	connect(ui->toolOnline, SIGNAL(clicked(bool)), this, SLOT(clickedOnline(bool)));
	connect(ui->toolSim, SIGNAL(clicked(bool)), this, SLOT(clickedSim(bool)));
	QString noad=fragment.attribute("addr");
	setObjectName(noad);
	adr_=NodeAddress(noad);
	adr_.convertToAvatar();
	ui->labelAlias->setText(fragment.attribute("name"));
	ui->labelAlias->setToolTip(noad);
	ui->labelAlias->setContextMenuPolicy(Qt::CustomContextMenu);
	if(fragment.attribute("hidden")=="true")
		isHidden_=true;
	QString locker=fragment.attribute("lock");
	if(!locker.isEmpty())
		ui->toolSim->setToolTip(locker);
	updateState(Node::state(fragment.attribute("state")));
	ui->toolSim->setChecked(fragment.attribute("sim")=="true");
	ui->toolSim->setEnabled(!isHidden_ && locker.isEmpty());
	ui->toolOnline->setChecked(!isHidden_);
	ui->toolOnline->setEnabled(!isHidden_);
	if(isHidden_)
	{
		ui->valueDummy->setText("hidden node");
	}
	else if(removeWidDummy)
	{
		ui->valueDummy->hide();
		ui->mainLayout->removeWidget(ui->valueDummy);
	}
	if(!DCON.registerAvatar(adr_, this))
		throw Ex("duplicate avatar");
}

AvNode::~AvNode()
{
	delete ui;
	DCON.deregisterAvatar(adr_);
}

void AvNode::processCommand(Command &cmd, Fragment &relevant)
{
	Q_UNUSED(cmd);
	if(relevant.cmd()=="nodech" || relevant.cmd()=="nodeset") // nodeset may reply with a modified value
	{
		deserialize(relevant.attribute("val"));
		updateState(Node::state(relevant.attribute("state")));
	}
	else
		qCritical()<<"avatar can't handle"<<relevant.cmd();
}

void AvNode::clickedOnline(bool onl)
{
	if(onl)
		nodeValueChanged();
}

void AvNode::clickedSim(bool sim)
{
	if(sim)
	{
		ui->toolOnline->setChecked(false);
		ui->toolOnline->setEnabled(false);
		//ui->comboState->setEnabled(true);
	}
	else
	{
		ui->toolOnline->setEnabled(true);
		//ui->comboState->setEnabled(false);
	}
}

void AvNode::nodeValueChanged()
{
	Command *cmd=new Command("nodeset");
	cmd->setDestination(adr_);
	cmd->setAttribute("val", value_.toString());
	DCON.dispatchCommand(*cmd);
}

void AvNode::updateState(Node::State state)
{
	switch(state)
	{
	case Node::State::READY:
		ui->labelState->setPixmap(QIcon(QStringLiteral(":/res/accept.png")).pixmap(ui->toolOnline->height()));
		break;
	case Node::State::BUSY:
		ui->labelState->setPixmap(QIcon(QStringLiteral(":/res/hourglass.png")).pixmap(ui->toolOnline->height()));
		break;
	case Node::State::ERROR:
		ui->labelState->setPixmap(QIcon(QStringLiteral(":/res/cross_red.png")).pixmap(ui->toolOnline->height()));
		break;
	case Node::State::LOCKED:
		ui->labelState->setPixmap(QIcon(QStringLiteral(":/res/lock.png")).pixmap(ui->toolOnline->height()));
		break;
	case Node::State::UNDEF:
		ui->labelState->setPixmap(QIcon(QStringLiteral(":/res/icons8-query-48.png")).pixmap(ui->toolOnline->height()));
		break;
	}
}

void AvNode::on_labelAlias_customContextMenuRequested(const QPoint &pos)
{
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act=menu->addAction(QStringLiteral("Copy"));
	connect(act, &QAction::triggered, this, &AvNode::copyAdrSl);
	menu->popup(ui->labelAlias->mapToGlobal(pos));
}

void AvNode::copyAdrSl()
{
	QClipboard *clipboard = QGuiApplication::clipboard();
	clipboard->setText(ui->labelAlias->toolTip());
}

AvatarFactory::AvatarCreatorMP AvatarFactory::avatarCreatorMP;

bool AvatarFactory::registerAvatar(const QString &typeId, AvatarFactory::AvatarCreator creator)
{
	bool res=avatarCreatorMP.insert(AvatarCreatorMP::value_type(typeId, creator)).second;
	qInfo()<<"registring avatar"<<typeId<<res;
	return res;
}

AvNode *AvatarFactory::produce(const Fragment &elem, QWidget * guiParent)
{
	QString type=elem.attribute("type");
	AvatarCreatorMP::iterator it=avatarCreatorMP.find(type);
	if(it==avatarCreatorMP.end())
	{
		qCritical()<<"no avatar for type"<<type;
		return nullptr;
	}
	return (it->second)(elem, guiParent);
}

AvNodeDummy::AvNodeDummy(const pi_at_home::Fragment &fragment, QWidget *parent) : AvNode(fragment, parent)
{
	ui->valueDummy->setText(tr("unknwon type ")+fragment.attribute("type"));
}
