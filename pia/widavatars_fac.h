#ifndef WIDAVATARSFAC_H
#define WIDAVATARSFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new pi_at_home::WidAvatars(parent);
}
bool dockWidRegistered=pi_at_home::DockWidFactory::get().registerDockWid("Remote Nodes", "dockwid_avatars", Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::LeftDockWidgetArea, DockCreator);
}

#endif // WIDAVATARSFAC_H
