#include "dlgeotimer.h"
#include "piadi.h"
#include "ui_dlgeotimer.h"

DlgEOTimer::DlgEOTimer(pi_at_home::EditorObject *obj, KeyValMV &currentKeyVals, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgEOTimer), obj(dynamic_cast<EOTimer * >(obj)), currentKeyVals(currentKeyVals)
{
	ui->setupUi(this);
	auto dsit=currentKeyVals.find("delay_style");
	if(dsit==currentKeyVals.end())
	{
		hms=false;
		setWindowTitle(tr("Timer Intervals (unknown delay_style)"));
	}
	else
	{
		hms=(dsit->second=="time_of_day");
		delayStyle=dsit->second;
		setWindowTitle(tr("Timer Intervals (%1)").arg(delayStyle));
	}
	map<int, long long > tms_;
	for(auto it=currentKeyVals.begin(); it!=currentKeyVals.end(); ++it)
	{
		if(it->first.indexOf("timer")==0)
		{
			bool ok=false;
			int index=it->first.mid(5).toInt(&ok);
			if(!ok)
			{
				qWarning()<<"misformed timerN argument"<<it->first;
				continue;
			}
			long long tm=it->second.toLongLong(&ok);
			if(!ok)
			{
				QTime testt=QTime::fromString(it->second, "hh:mm:ss");
				if(testt.isValid())
				{
					ok=true;
					tm=testt.msecsSinceStartOfDay();
				}
				if(!ok)
				{
					qWarning()<<"misformed timerN value"<<it->second;
					continue;
				}
			}
			tms_.insert(map<int, long long>::value_type(index, tm));
		}
	}
	int r=0;
	for(auto it=tms_.begin(); it!=tms_.end(); ++it, ++r)\
	{
		QListWidgetItem *item=new QListWidgetItem(tm2Str(it->second));
		item->setData(Qt::UserRole, it->second); // QVariant is long long
		ui->listWidget->insertItem(r, item);
	}
	connect(ui->spinDay, SIGNAL(valueChanged(int)), this, SLOT(spinChangedSl(int)));
	connect(ui->spinHour, SIGNAL(valueChanged(int)), this, SLOT(spinChangedSl(int)));
	connect(ui->spinMin, SIGNAL(valueChanged(int)), this, SLOT(spinChangedSl(int)));
	connect(ui->spinSec, SIGNAL(valueChanged(int)), this, SLOT(spinChangedSl(int)));
	connect(ui->spinMSec, SIGNAL(valueChanged(int)), this, SLOT(spinChangedSl(int)));
	on_listWidget_currentRowChanged(-1);
}

DlgEOTimer::~DlgEOTimer()
{
	delete ui;
}

void DlgEOTimer::accept()
{
	if(!checkConsistency())
	{
		if(QMessageBox::warning(this, tr("Wrong sequence"),
			tr("The timer values ypu eneterd are not valid for the current delay_style. piahd will fail if you download this config! proceed anyway?"),
			QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
			return;
	}
	for(auto it=currentKeyVals.begin(); it!=currentKeyVals.end(); )
	{
		if(it->first.indexOf("timer")==0)
		{
			auto delit=it;
			++it;
			currentKeyVals.erase(delit);
		}
		else
			++it;
	}
	QString templ("timer%1");
	//for(int i=0; i<(int)tms.size(); ++i)
	for(int i=0; i<ui->listWidget->count(); ++i)
	{
		currentKeyVals.insert(KeyValMV::value_type(templ.arg(i), ui->listWidget->item(i)->data(Qt::UserRole).toString()));
	}
	QDialog::accept();
}

void DlgEOTimer::on_toolAdd_clicked()
{
	int r=ui->listWidget->currentRow();
	QListWidgetItem *item=new QListWidgetItem("0");
	item->setData(Qt::UserRole, QVariant(Q_INT64_C(0))); // QVariant is long long
	if(r<0) // nothing selected
	{
		ui->listWidget->insertItem(0, item);
		ui->listWidget->setCurrentRow(0);
	}
	else
	{
		ui->listWidget->insertItem(r+1, item);
		ui->listWidget->setCurrentRow(r+1);
	}
	ui->spinDay->setFocus();
	checkConsistency();
}

void DlgEOTimer::on_toolDelete_clicked()
{
	int r=ui->listWidget->currentRow();
	if(r<0)
		return;
	delete ui->listWidget->takeItem(r);
	//tms.erase(tms.begin()+r);
	ui->listWidget->setCurrentRow(r>=ui->listWidget->count()?ui->listWidget->count()-1:r);
	checkConsistency();
}

void DlgEOTimer::on_toolUp_clicked()
{
	int r=ui->listWidget->currentRow();
	if(r<1)
		return;
	QVariant d=ui->listWidget->item(r-1)->data(Qt::DisplayRole);
	QVariant u=ui->listWidget->item(r-1)->data(Qt::UserRole);
	ui->listWidget->item(r-1)->setData(Qt::DisplayRole, ui->listWidget->item(r)->data(Qt::DisplayRole));
	ui->listWidget->item(r-1)->setData(Qt::UserRole, ui->listWidget->item(r)->data(Qt::UserRole));
	ui->listWidget->item(r)->setData(Qt::DisplayRole, d);
	ui->listWidget->item(r)->setData(Qt::UserRole, u);
	ui->listWidget->setCurrentRow(r-1);
	checkConsistency();
}

void DlgEOTimer::on_toolDown_clicked()
{
	int r=ui->listWidget->currentRow();
	if(r<0)
		return;
	QVariant d=ui->listWidget->item(r)->data(Qt::DisplayRole);
	QVariant u=ui->listWidget->item(r)->data(Qt::UserRole);
	ui->listWidget->item(r)->setData(Qt::DisplayRole, ui->listWidget->item(r+1)->data(Qt::DisplayRole));
	ui->listWidget->item(r)->setData(Qt::UserRole, ui->listWidget->item(r+1)->data(Qt::UserRole));
	ui->listWidget->item(r+1)->setData(Qt::DisplayRole, d);
	ui->listWidget->item(r+1)->setData(Qt::UserRole, u);
	ui->listWidget->setCurrentRow(r+1);
	checkConsistency();
}

void DlgEOTimer::on_listWidget_currentRowChanged(int currentRow)
{
	if(currentRow<0)
	{
		ui->toolDelete->setEnabled(false);
		ui->toolUp->setEnabled(false);
		ui->toolDown->setEnabled(false);
		ui->spinDay->setValue(0);
		ui->spinHour->setValue(0);
		ui->spinMin->setValue(0);
		ui->spinSec->setValue(0);
		ui->spinMSec->setValue(0);
		return;
	}
	ui->toolDelete->setEnabled(true);
	ui->toolUp->setEnabled(currentRow);
	ui->toolDown->setEnabled(currentRow<ui->listWidget->count()-1);
	long long tm=ui->listWidget->item(currentRow)->data(Qt::UserRole).toLongLong();
	int d=tm/(3600000*24);
	tm-=d*3600000*24;
	int h=tm/3600000;
	tm-=h*3600000;
	int m=tm/60000;
	tm-=m*60000;
	int s=tm/1000;
	tm-=s*1000;
	ui->spinDay->setValue(d);
	ui->spinHour->setValue(h);
	ui->spinMin->setValue(m);
	ui->spinSec->setValue(s);
	ui->spinMSec->setValue(tm);
}

void DlgEOTimer::updateTimerVal()
{
	int r=ui->listWidget->currentRow();
	if(r<0)
		return;
	long long tm=ui->spinDay->value()*3600000*24+ui->spinHour->value()*3600000+ui->spinMin->value()*60000+ui->spinSec->value()*1000+ui->spinMSec->value();
	QListWidgetItem *lwi=ui->listWidget->currentItem();
	lwi->setData(Qt::DisplayRole, tm2Str(tm));
	lwi->setData(Qt::UserRole, QVariant(tm));
	checkConsistency();
}

void DlgEOTimer::spinChangedSl(int v)
{
	Q_UNUSED(v)
	updateTimerVal();
}

QString DlgEOTimer::tm2Str(long long tm)
{
	if(hms)
	{
		if(tm>=Q_INT64_C(86399999))
			return tr("> one day!");
		else
			return QTime::fromMSecsSinceStartOfDay(tm).toString("hh:mm:ss.zzz");
	}
	if(!tm)
		return "0";
	int d=tm/(3600000*24);
	tm-=d*3600000*24;
	int h=tm/3600000;
	tm-=h*3600000;
	int m=tm/60000;
	tm-=m*60000;
	int s=tm/1000;
	tm-=s*1000;
	QStringList res;
	if(d)
		res<<QString::number(d)<<tr("days ");
	if(h)
		res<<QString::number(h)<<tr("h ");
	if(m)
		res<<QString::number(m)<<tr("min ");
	if(s)
		res<<QString::number(s)<<tr("s ");
	if(tm)
		res<<QString::number(tm)<<tr("ms ");
	return res.join(QStringLiteral(""));
}

bool DlgEOTimer::checkConsistency()
{
	bool res=true;
	long long tmsequ=-1;
	bool cs=(hms || delayStyle==QStringLiteral("absolute")); // hms is time_of_day
	for(int i=0; i<ui->listWidget->count(); ++i)
	{
		ui->listWidget->item(i)->setForeground(Qt::black);
		long long tm=ui->listWidget->item(i)->data(Qt::UserRole).toLongLong();
		if(i && !tm)
		{
			res=false;
			ui->listWidget->item(i)->setForeground(Qt::red);
		}
		if(cs)
		{
			if(tm<=tmsequ || (hms && tm>Q_INT64_C(86399999)) )
			{
				res=false;
				ui->listWidget->item(i)->setForeground(Qt::red);
			}
			tmsequ=tm;
		}
	}
	return res;
}
