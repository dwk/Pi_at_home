#ifndef TINYLISTWIDGET_H
#define TINYLISTWIDGET_H

#include "piadh.h"
#include <QListWidget>

class TinyListWidget : public QListWidget
{
	Q_OBJECT
public:
	explicit TinyListWidget(QWidget *parent = 0) : QListWidget(parent) {}
	virtual QSize minimumSizeHint() const {return QSize(100, 30);}
	virtual QSize sizeHint() const {return QSize(100, 50);}
};

#endif // TINYLISTWIDGET_H
