#ifndef WORLDOBJECT_H
#define WORLDOBJECT_H
#include "piadh.h"
#include "../piah_common/commandtarget.h"
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QIcon>
#include <QGraphicsItem>
#include <QDomElement>
#include "../piah_common/hostdict.h"
#include "../piah_common/netcon.h"

Q_DECLARE_LOGGING_CATEGORY(worldobject)

namespace pi_at_home
{

class WorldObject : public QObject, public QGraphicsItem, public CommandTarget
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsItem)
	//friend class DlgEditObject;
	friend class WidConfigWorld;
public:
	enum class BinTst {NOCHECK=0, UNKNOWN, OUTDATED, UPTODATE};
	enum class Source {NONE=0, LOC, LOCRAW, LOCMISS, REMUPT, REMRAWUPT, REMOTD, REMRAWOTD};
	enum class SourceState {NONE=0, OK, NOSRV, NONODE};
	class Conf
	{
	public:
		void readConf();
		QString filepath, tst;
		Source src=Source::NONE;
		QStringList sourceNodes;
		std::vector<SourceState> states;
	};
	static const int objectWidth=180;
	static const int objectHeaderHeight=40;
	static QWidget * pwid;
	static QDomElement domDummy;
	WorldObject(const QString & uid, const HostInstance & hi, const QString &binaryTst);
	virtual ~WorldObject();
	const QStringList & getNodeList() const {return nodes;}
	bool hasNode(const QString & nadr) const;
	const QStringList & getSourceAddrs() const {return currentConf.sourceNodes;}
	void markSource(int index, SourceState state);
	QPointF getSourcePos(int index) const;
	// CommandTarget
	virtual void processCommand(Command & cmd, Fragment & relevant) override;
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE;
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	const QString & getName() const {return hostInst.uid;}
protected:
	// QGraphicsItem
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	//
	virtual void setupGraphic();
	void updatePainterUtils(QPainter *painter, const QStyleOptionGraphicsItem *option);
protected slots:
	void testSrvSl();
	void sockErrorSl(int err);
	void sockReadySl(bool ready);
	void deploySrvSl();
protected:
	static double defaultPosX, defaultPosY;
	// PainterUtils
	// graphic representation
	QColor baseColor, backgroundColor;
	QBrush backgroundBrush;
	QPen outlinePen;
	double levelOfDetail;
	QRectF boundingBox, body;
	QString itext, iitext;
	bool wasMoved=false;
	//
	HostInstance hostInst;
	QString binTst;
	QString ip;
	Conf conf, confRaw, &currentConf;
	BinTst bintst=BinTst::UNKNOWN;
	NetCon *nc=nullptr;
	QStringList nodes;
signals:
	void objectReposSig(QString uid);
};
typedef std::vector<WorldObject *> WorldObjectVP;
typedef std::map<QString, QPointer<WorldObject> > WorldObjectMSSP; // key is UID

} // namespace pi_at_home

#endif // WORLDOBJECT_H
