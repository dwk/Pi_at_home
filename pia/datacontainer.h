#ifndef DATACONTAINER_H
#define DATACONTAINER_H

#include "piadh.h"
#include <QObject>
#include <QMutex>
#include "../piah_common/nodeaddress.h"
#include "connections.h"
#include "../piah_common/hostdict.h"
#include "../piah_common/command.h"
#include "../piah_common/commandtarget.h"
#include "../piah_common/datahistory.h"

namespace pi_at_home
{
class MainWindow;

class DataContainer : public QObject
{
	Q_OBJECT
public:
	explicit DataContainer(QObject *parent = nullptr);
	~DataContainer();
	DataContainer *self() {return this;}
	void setup(const QString configFile, MainWindow &mwin);
	void shutdown();
	QString getConfigFlavorStr() const {return QStringLiteral("none");}
	QDateTime getConfigTst() const {return QDateTime();}
	const QString & uid() const {return uid_;}
	unsigned long long int nextId() {return ++luid_;}
	void setUid(const QString uid) {uid_=uid; uid_.replace(':', '-'); NodeAddress::setDefaultUid(uid_);}
	qint64 getUsecTst() {return 0;}
	bool readPiahdInfo(QString binaryFilename, QString &fileType, QString &fileTst, QStringList * messages);
	Connections * connections() {return connections_;}
	void registerCmdTarget(const QString & cmd, CommandTarget * target) {dispatch.insert(Dispatch::value_type(cmd, target));}
	//bool registerCmdTarget(const CommandFilter & filter, CommandTarget * target) {Q_UNUSED(filter); Q_UNUSED(target); return true;} // do we need it?
	void deregisterCmdTarget(CommandTarget * target);
	bool registerAvatar(const NodeAddress & adr, CommandTarget * target) {return avatars.insert(Avatars::value_type(adr, target)).second;}
	void deregisterAvatar(const NodeAddress & adr) {avatars.erase(adr);}
	void addNodeFragments(Command & cmd) const;
	void dispatchCommand(Command &cmd);
	//Command & getPendingBroadcast();
	void add2Broadcast(Fragment * frag) {Q_UNUSED(frag);}
	void sendBroadcast();
	HostDict & hostDict() {if(!hostDict_) throw Ex("hostDict not available"); return *hostDict_;}
	//
	DataHistory * getDataHistory(const NodeAddress & adr, bool createOnMiss);
	DataHistory * getDataHistory(const QString & uidName);
	const DataHistoryMP & getAllDataHistory() const {return dataHistMP;}
	void purgeEmptyHistories();
	void setDataHistoryExtFactory(DataHistory::ExtFactory fac){facDH_=fac;}
	QObject * extendDataHistory(const DataHistory &dh) const;
public slots:
	void dispatchCmdSl(QObject * cmd);
protected:
	virtual void timerEvent(QTimerEvent *event);
private:
	typedef std::multimap<QString, CommandTarget*> Dispatch;
	typedef std::map<NodeAddress, CommandTarget*> Avatars;
	std::vector<CommandTarget *> findTarget(const QString & cmd); // const NodeAddress & procTarget
	QString uid_;
	unsigned long long int luid_ = 0;
	Connections * connections_=nullptr;
	HostDict *hostDict_;
	Dispatch dispatch;
	Avatars avatars;
//	NodeAdrMP nodeAdrMP;
//	NodeNameMP nodeNameMP;
	QMutex mtxBroadcast;
	Command * broadcast = nullptr;
	int delayBroadcast=200;
	//
	DataHistoryMP dataHistMP;
	DataHistory::ExtFactory facDH_=nullptr;
};
}

#endif // DATACONTAINER_H
