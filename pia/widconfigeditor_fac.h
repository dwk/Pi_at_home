#ifndef WIDCONFIGEDITORFAC_H
#define WIDCONFIGEDITORFAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new pi_at_home::WidConfigEditor(parent);
}
bool dockWidRegistered=pi_at_home::DockWidFactory::get().registerDockWid("Config Editor", "dockwid_configeditor", Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::LeftDockWidgetArea, DockCreator);
}

#endif // WIDCONFIGEDITORFAC_H
