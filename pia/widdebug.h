#ifndef WIDDEBUG_H
#define WIDDEBUG_H

#include "piadh.h"
#include <list>
#include <QListWidget>

//namespace Ui {
//class WidConnectionInspector;
//}

namespace pi_at_home
{

class WidDebug : public QListWidget
{
	Q_OBJECT
public:
	explicit WidDebug(QWidget *parent = 0);
	~WidDebug() {}
public slots:
	void addLine(QString text, int type, long long threadId);
};
}

#endif // WIDCONNECTIONINSPECTOR_H
