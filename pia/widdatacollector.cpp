#include "widdatacollector.h"
#include "widdatacollector_fac.h"
#include <QFileDialog>
#include <QDir>
#include "../piah_common/command.h"
#include "../piah_common/datafilereader.h"
#include "ui_widdatacollector.h"
#include "piadi.h"


void FileLoadWorker::pushToDcon()
{
	for(DataHistoryMP::iterator dit=dataHistMP.begin(); dit!=dataHistMP.end(); ++dit)
	{
		DataHistory *dh=DCON.getDataHistory(dit->first, true);
		dh->addData(*(dit->second));
	}
}

void FileLoadWorker::load(const QStringList &filelist, const QDate &fromDate, const QDate &toDate)
{
	int dfcnt=0, skcnt=0, i=0;
	foreach(QString filename, filelist)
	{
		++i;
		emit loadProgressSig(i*100/filelist.size());
		if(filename.size()<13)
		{
			continue;
		}
		QDate testdate=QDate::fromString(filename.mid(filename.size()-12, 8), "yyyyMMdd");
		if(!testdate.isValid())
		{
			continue;
		}
		if(testdate<fromDate || testdate>toDate)
		{
			++skcnt;
			continue;
		}
		//qDebug()<<"loading datafile"<<filename;
		try
		{
			DataFileReader dfr(filename, dataHistMP);
			dfr.parse();
			++dfcnt;
		}
		catch(Ex &ex)
		{
			qCritical()<<"creation of WidNode failed"<<ex.getMessage();
		}
		catch(...)
		{
			qCritical()<<"unknwon exception catched";
		}
	}
	for(DataHistoryMP::iterator dit=dataHistMP.begin(); dit!=dataHistMP.end(); ++dit)
		dit->second->moveToThread(DCON.thread());
	emit loadDoneSig(dfcnt, skcnt);
}

FileTransferWorker::FileTransferWorker()
{
	connect(this, &FileTransferWorker::transferCmdSig, this, &FileTransferWorker::transferCmdSl);
	connect(this, &FileTransferWorker::dispatchCmdSig, DCON.self(), &DataContainer::dispatchCmdSl);
	DCON.registerCmdTarget("datatrans", this);
}

FileTransferWorker::~FileTransferWorker()
{
	DCON.deregisterCmdTarget(this);
}

void FileTransferWorker::processCommand(Command &cmd, Fragment &relevant)
{
	Q_UNUSED(&relevant);
	if(cmd.cmd()!="datatrans" || !cmd.isReply())
	{
		qWarning()<<"unexpected command"<<cmd.cmd();
		return;
	}
	if(cmd.hasPayload())
	{
		QString fname=jobs_.begin()->localFullName;
		QFile f(fname);
		if(!f.open(QIODevice::WriteOnly | QIODevice::Text))
		{
			qWarning()<<"cannot open file for datatrans"<<fname;
			return;
		}
		f.write(*cmd.payload());
		f.close();
		//qDebug()<<"DF"<<cmd.attribute("name")<<cmd.payload()->left(80);
	}
	else
	{
		QString err=cmd.attribute("error");
		qWarning()<<"datatrans error"<<cmd.attribute("name")<<err;
	}
	jobLock.lock();
	jobs_.erase(jobs_.begin());
	jobLock.unlock();
	emit transferCmdSig();
}

void FileTransferWorker::transfer(QList<pi_at_home::FileTransferJob> jobs)
{
	bool triggerTrans=false;
	jobLock.lock();
	triggerTrans=!jobs_.size();
	jobs_.append(jobs);
	totalTransfer=jobs_.size();
	jobLock.unlock();
	if(triggerTrans)
		transferCmdSl();
}

void FileTransferWorker::transferCmdSl()
{
	jobLock.lock();
	if(!jobs_.size())
	{
		jobLock.unlock();
		emit transferDoneSig(totalTransfer);
		return;
	}
	int perc=(totalTransfer-jobs_.size())*100/totalTransfer;
	FileTransferJob j=*(jobs_.begin());
	jobLock.unlock();
	emit transferProgressSig(perc);
	Command *cmd=new Command("datatrans");
	cmd->setDestination(NodeAddress(j.server, 0, 0));
	cmd->setAttribute("name", j.remoteName);
	//qDebug()<<"dispatchCommand";
	cmd->moveToThread(DCON.self()->thread());
	emit dispatchCmdSig(cmd);
}

WidDataCollector::WidDataCollector(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::WidDataCollector)
{
	qRegisterMetaType<pi_at_home::FileTransferJob>();
	qRegisterMetaType<QList<pi_at_home::FileTransferJob>>();
	QSettings settings;settings.value("dataPath").toString();
	ui->setupUi(this);
	ui->progressBar->setVisible(false);
	ui->dateFromSrv->setDate(QDate::currentDate().addMonths(-12));
	ui->dateToSrv->setDate(QDate::currentDate());
	ui->dateFromLoad->setDate(QDate::currentDate().addDays(-7));
	ui->dateToLoad->setDate(QDate::currentDate());
	connect(ui->treeDataFiles, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChangedSl(QTreeWidgetItem*,int)));
	DCON.registerCmdTarget("datalist", this);
	fillTree();
	workerThread.start();
}

WidDataCollector::~WidDataCollector()
{
	workerThread.quit();
	workerThread.wait();
	if(lworker)
		delete lworker;
	if(tworker)
		delete tworker;
	delete ui;
}

void WidDataCollector::processCommand(Command &cmd, Fragment &relevant)
{
	Q_UNUSED(&relevant);
	if(cmd.cmd()!="datalist" || !cmd.isReply())
		return;
	const FragmentLP & frgs=cmd.fragments();
	QString serverUid=cmd.destination().uid();
	QString pathBase=QSettings().value("dataPath").toString()+"/"+serverUid+"/";
	if(!QDir(pathBase).exists())
		QDir().mkdir(pathBase);
	//qDebug()<<pathBase;
	QDir baseDir(pathBase);
	//baseDir.setNameFilters(QStringList()<<QStringLiteral("*.xml"));
	QStringList toTrans;
	QList<FileTransferJob> tjs;
	for(FragmentLP::const_iterator fit=frgs.begin(); fit!=frgs.end(); ++fit)
	{
		if((*fit)->cmd()!="datafile")
			continue;
		QString fname=(*fit)->attribute("name");
		bool ok=false;
		int fsize=(*fit)->attribute("size").toInt(&ok);
		//qDebug()<<fname<<fsize<<ok;
		if(!ok)
			continue;
		QString testname=pathBase+fname;
		bool isLocal=baseDir.exists(fname);
		if(!isLocal || (isLocal && QFileInfo(testname).size()!=fsize))
		{
			toTrans<<fname;
			tjs<<FileTransferJob(serverUid, testname, fname);
		}
	}
	//qDebug()<<toTrans;
	QString fileNames;
	if(toTrans.size()>10)
		fileNames=QStringLiteral("%1 files (%2 to %3)").arg(toTrans.size()).arg(toTrans.first(), toTrans.last());
	else
		fileNames=toTrans.join(' ');
	if(QMessageBox::question(this, tr("File Transfer"), tr("Transfer %1 from %2?").arg(fileNames, serverUid), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes)==QMessageBox::Yes)
	{
		workstate(true);
		if(tworker)
		{
			qDebug()<<"worker: adding jobs"<<tjs.size();
		}
		else
		{
			qDebug()<<"worker: starting with"<<tjs.size();
			tworker = new FileTransferWorker;
			tworker->moveToThread(&workerThread);
			connect(this, &pi_at_home::WidDataCollector::transferSig, tworker, &pi_at_home::FileTransferWorker::transfer);
			connect(tworker, &FileTransferWorker::transferProgressSig, this, &WidDataCollector::loadProgressSl);
			connect(tworker, &FileTransferWorker::transferDoneSig, this, &WidDataCollector::transferDoneSl);
		}
		emit transferSig(tjs);
	}
	return;
}

void WidDataCollector::on_pushScan_clicked()
{
	Connections *cons=DCON.connections();
	NetConMSP &ncs=cons->hostConMP();
	for(NetConMSP::const_iterator cit=ncs.begin(); cit!=ncs.end(); ++cit)
	{
		NetCon *nc=cit->second;
		if(!nc)
			continue;
		Command *cmd=new Command("datalist");
		cmd->setDestination(NodeAddress(nc->uid(), 0, 0));
		cmd->setAttribute("from", ui->dateFromSrv->date().toString(Qt::ISODate));
		cmd->setAttribute("to", ui->dateToSrv->date().toString(Qt::ISODate));
		DCON.dispatchCommand(*cmd);
	}
}

void WidDataCollector::on_pushLoad_clicked()
{
	if(lworker)
	{
		qCritical()<<"worker not deleted!";
		return;
	}
	workstate(true);
	QStringList filelist;
	for(int i=0; i<ui->treeDataFiles->topLevelItemCount(); ++i)
		recurseTree(filelist, ui->treeDataFiles->topLevelItem(i));
	//qDebug()<<"files to load"<<filelist; // This msg seems corrupt the Debug Widget... thread safety?
	lworker = new FileLoadWorker;
	lworker->moveToThread(&workerThread);
	connect(this, &WidDataCollector::loadSig, lworker, &FileLoadWorker::load);
	connect(lworker, &FileLoadWorker::loadProgressSig, this, &WidDataCollector::loadProgressSl);
	connect(lworker, &FileLoadWorker::loadDoneSig, this, &WidDataCollector::loadDoneSl);
	emit loadSig(filelist, ui->dateFromLoad->date(), ui->dateToLoad->date());
}

void WidDataCollector::fillTree()
{
	ui->treeDataFiles->clear();
	QDir dir(QSettings().value("dataPath").toString());
	dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
	QStringList dds=dir.entryList();
	foreach(QString dirname, dds)
	{
		QTreeWidgetItem *ttwi=new QTreeWidgetItem(QStringList()<<""<<dirname);
		ttwi->setCheckState(0, Qt::Checked);
		QFont font=ttwi->font(1);
		font.setBold(true);
		ttwi->setFont(1,font);
		ui->treeDataFiles->addTopLevelItem(ttwi);
		QDir ddir(dir.absoluteFilePath(dirname));
		ddir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
		ddir.setNameFilters(QStringList()<<"data_*.xml");
		ddir.setSorting(QDir::Name);
		QStringList dfs=ddir.entryList();
		int lastMonth=0;
		QTreeWidgetItem *mtwi=nullptr;
		foreach(QString filename, dfs)
		{
			if(filename.size()<13)
				continue;
			QDate testdate=QDate::fromString(filename.mid(5, 8), "yyyyMMdd");
//			if(testdate<ui->dateFrom->date() || testdate>ui->dateTo->date())
//				continue;
			int thisMonth=testdate.year()*100+testdate.month();
			if(thisMonth!=lastMonth)
			{
				lastMonth=thisMonth;
				mtwi=new QTreeWidgetItem(QStringList()<<""<<testdate.toString("yyyy-MM"));
				mtwi->setCheckState(0, Qt::Checked);
				ttwi->addChild(mtwi);
			}
			QString fullpath=ddir.absoluteFilePath(filename);
			QFileInfo fi(fullpath);
			QTreeWidgetItem *twi=new QTreeWidgetItem(QStringList()<<""<<testdate.toString(Qt::ISODate)<<QString::number(fi.size()));
			twi->setCheckState(0, Qt::Checked);
			twi->setData(0, Qt::UserRole, QVariant(fullpath));
			mtwi->addChild(twi);
		}
	}
	ui->treeDataFiles->resizeColumnToContents(0);
}

void WidDataCollector::itemChangedSl(QTreeWidgetItem *twi, int col)
{
	if(col)
		return;
	Qt::CheckState chst=twi->checkState(0);
	if(chst==Qt::PartiallyChecked)
	{
		QTreeWidgetItem *ptwi=twi->parent();
		if(ptwi)
			ptwi->setCheckState(0, Qt::PartiallyChecked);
	}
	else
	{
		if(!treeParentUpdate)
		{
			QTreeWidgetItem *ptwi=twi->parent();
			if(ptwi)
				ptwi->setCheckState(0, Qt::PartiallyChecked);
		}
		bool treeParentUpdateSave=treeParentUpdate;
		treeParentUpdate=true;
		int ccnt=twi->childCount();
		for(int i=0; i<ccnt; ++i)
		{
			QTreeWidgetItem *xtwi=twi->child(i);
			xtwi->setCheckState(0, chst);
		}
		treeParentUpdate=treeParentUpdateSave;
	}
}

void WidDataCollector::loadProgressSl(int precentage)
{
	ui->progressBar->setValue(precentage);
}

void WidDataCollector::loadDoneSl(int fileCnt, int skipCnt)
{
	lworker->pushToDcon();
	delete lworker;
	lworker=nullptr;
	workstate(false);
	DCON.purgeEmptyHistories();
	QMessageBox::information(this, tr("Datafile"), tr("%1 file(s) loaded, %2 skiped.").arg(fileCnt).arg(skipCnt));
	emit loadCompleted();
}

void WidDataCollector::transferDoneSl(int fileCnt)
{
	delete tworker;
	tworker=nullptr;
	workstate(false);
	fillTree();
	QMessageBox::information(this, tr("Datafile"), tr("%1 file(s) transferred.").arg(fileCnt));
}

void WidDataCollector::recurseTree(QStringList &filelist, QTreeWidgetItem *twi)
{
	QString fullpath=twi->data(0, Qt::UserRole).toString();
	if(fullpath.isEmpty())
	{
		if(twi->checkState(0)!=Qt::Unchecked)
		{
			for(int i=0; i<twi->childCount(); ++i)
				recurseTree(filelist, twi->child(i));
		}
	}
	else
	{
		if(twi->checkState(0)==Qt::Checked)
			filelist<<fullpath;
	}
}

void WidDataCollector::workstate(bool work)
{
	if(work)
	{
		ui->progressBar->setValue(0);
		ui->progressBar->setVisible(true);
		ui->pushLoad->setEnabled(false);
		ui->pushScan->setEnabled(false);
	}
	else
	{
		ui->progressBar->setVisible(false);
		ui->pushLoad->setEnabled(true);
		ui->pushScan->setEnabled(true);
	}
}

