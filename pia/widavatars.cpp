#include "widavatars.h"
#include "widavatars_fac.h"
#include "avnode.h"
#include "piadi.h"

WidAvatars::WidAvatars(QWidget *parent) :
	QTabWidget(parent)
{
	verticalLayout=new QVBoxLayout(this);
	verticalLayout->setSpacing(0);
	verticalLayout->setContentsMargins(0, 0, 0, 0);
	tabWidget = new QTabWidget(this);
	verticalLayout->addWidget(tabWidget);
	DCON.registerCmdTarget("connect", this);
	connect(DCON.connections(), SIGNAL(connectionListChanged()), this, SLOT(connectionsChanged()));
}

/* WidAvatarsPriv::WidAvatarsPriv(QWidget *parent) :
	QWidget(parent)
{
	centralLay=(new QVBoxLayout(this));
	centralLay->setSizeConstraint(QLayout::SetMinAndMaxSize);
	//centralLay->addWidget(new QLabel("test1test1test1test1test1test1test1test1test1test1test1test1test1", central));
	//centralLay->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding)); not for scroll area!
	DCON.registerCmdTarget("connect", this);
	connect(DCON.connections(), SIGNAL(connectionListChanged()), this, SLOT(connectionsChanged()));
} */

void WidAvatars::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	Q_UNUSED(& relevant);
	if(cmd.cmd()=="connect")
	{
		//qDebug()<<"connect avatar list";
		QWidget *tab = new QWidget();
		QVBoxLayout *verticalLayout_2 = new QVBoxLayout(tab);
		verticalLayout_2->setSpacing(0);
		verticalLayout_2->setContentsMargins(0, 0, 0, 0);
		QScrollArea *scrollArea = new QScrollArea(tab);
		scrollArea->setFrameShape(QFrame::NoFrame);
		scrollArea->setLineWidth(0);
		scrollArea->setWidgetResizable(true);
		QWidget *saWid = new QWidget();
		saWid->setGeometry(QRect(0, 0, 288, 260));
		QVBoxLayout *layout = new QVBoxLayout(saWid);
		scrollArea->setWidget(saWid);
		verticalLayout_2->addWidget(scrollArea);
		//tabWids.insert(TabWids::value_type(cmd.destination().uid(), TabWid(tab, saWid, layout)));
		const FragmentLP & frags=cmd.fragments();
		for(FragmentLP::const_iterator it=frags.begin(); it!=frags.end(); ++it)
		{
			//qDebug()<<"avatar"<<(*it)->attribute("addr");
			QWidget * w=nullptr;
			try
			{
				w=AvatarFactory::produce(**it, saWid);
			}
			catch(Ex &ex)
			{
				qCritical()<<"creation of WidNode failed"<<ex.getMessage();
			}
			if(!w)
				continue;
			//qDebug()<<"ading widget"<<w->objectName();
			layout->addWidget(w);
		}
		QSpacerItem *verticalSpacer = new QSpacerItem(20, 177, QSizePolicy::Minimum, QSizePolicy::Expanding);
		layout->addItem(verticalSpacer);
		tabWidget->addTab(tab, cmd.destination().uid());
	}
	return;
}

void WidAvatars::connectionsChanged()
{
	//remove widegt during disconnect
	NetConMSP & cons=DCON.connections()->hostConMP();
	//int cnt=verticalLayout_3->count();
	for(int i=tabWidget->count()-1; i>=0; --i)
	{
		if(cons.find(tabWidget->tabText(i))!=cons.end())
			continue;
		QWidget *w=tabWidget->widget(i);
		tabWidget->removeTab(i);
		delete w;
	}
}

