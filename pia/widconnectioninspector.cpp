#include "widconnectioninspector.h"
#include "widconnectioninspector_fac.h"
#include "ui_widconnectioninspector.h"
#include "piadi.h"

WidConnectionInspector::WidConnectionInspector(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::WidConnectionInspector)
{
	ui->setupUi(this);
	connect(ui->comboCon, SIGNAL(currentIndexChanged(int)), this, SLOT(fillList()));
	connect(ui->pushClearCurrent, SIGNAL(clicked(bool)), this, SLOT(clearCurrent()));
	connect(ui->pushClearAll, SIGNAL(clicked(bool)), this, SLOT(clearAll()));
	connect(DCON.connections(), SIGNAL(connectionListChanged()), this, SLOT(connectionListChanged()));
	connect(DCON.connections(), SIGNAL(connectionNameChanged(QString,QString)), this, SLOT(connectionNameChanged(QString,QString)));
	connect(ui->checkFreeze, SIGNAL(toggled(bool)), this, SLOT(unfreezeSl(bool)));
}

WidConnectionInspector::~WidConnectionInspector()
{
	delete ui;
}

void WidConnectionInspector::connectionListChanged()
{
	QString ct=ui->comboCon->currentText();
	ui->comboCon->clear();
	NetConMSP & hcs=DCON.connections()->hostConMP();
	for(NetConMSP::iterator it=hcs.begin(); it!=hcs.end(); ++it)
	{
		connect(it->second, SIGNAL(rawDataSig(QString, QByteArray, bool, bool)),
				this, SLOT(rawData(QString, QByteArray, bool, bool)), Qt::QueuedConnection);
		ui->comboCon->addItem(it->first.mid(0, it->first.indexOf(':')));
	}
	int ind=ui->comboCon->findText(ct);
	if(ind<0)
	{
		if(ui->comboCon->count())
			ui->comboCon->setCurrentIndex(0);
		else
			ui->comboCon->setCurrentIndex(-1);
	}
	else
		ui->comboCon->setCurrentIndex(ind);
	fillList();
	for(Texts::iterator it=texts.begin(); it!=texts.end(); )
	{
		if(ui->comboCon->findText(it->first)<0)
		{
			Texts::iterator delIt=it;
			++it;
			texts.erase(delIt);
		}
		else
			++it;
	}
}

void WidConnectionInspector::connectionNameChanged(QString oldName, QString newName)
{
	qDebug()<<"rename"<<oldName<<newName;
	Texts::iterator oldIt=texts.find(oldName);
	if(oldIt!=texts.end())
	{
		texts.insert(Texts::value_type(newName, oldIt->second));
		texts.erase(oldIt);
	}
	int index=ui->comboCon->findText(oldName);
	if(index<0)
		return;
	bool moveCurrent=(ui->comboCon->currentIndex()==index);
	ui->comboCon->removeItem(index);
	ui->comboCon->addItem(newName);
	if(moveCurrent)
	{
		ui->comboCon->setCurrentIndex(ui->comboCon->count()-1);
	}
}

void WidConnectionInspector::rawData(QString host, QByteArray data, bool sending, bool error)
{ // use only witj queued connection, otherwise threaded data file transfer scrambles output!
	//	if(QThread::currentThread()!=qApp->thread())
	//		qCritical()<<"AUTSCH!!!";
	//qDebug()<<host<<data<<sending<<error;
	Texts::iterator it=texts.find(host);
	Line l;
	if(data.size()>ui->spinLineLength->value())
		l.txt=QString::fromUtf8(data.left(ui->spinLineLength->value()));
	else
		l.txt=QString::fromUtf8(data);
	if(error)
	{
		if(sending)
			l.br=QBrush(Qt::darkRed);
		else
			l.br=QBrush(Qt::red);
	}
	else
	{
		if(sending)
			l.br=QBrush(Qt::darkBlue);
		else
			l.br=QBrush(Qt::blue);
	}
	if(it==texts.end())
	{
		LineL ll;
		ll.push_back(l);
		texts.insert(Texts::value_type(host, ll));
	}
	else
	{
		it->second.push_back(l);
		if((int)it->second.size()>maxLines)
		{
			it->second.erase(it->second.begin());
			it->second.front()=Line("...", QBrush(Qt::black));
		}
	}
	if(ui->comboCon->currentIndex()==ui->comboCon->findText(host) && !ui->checkFreeze->isChecked())
	{
		QListWidgetItem *lwi=new QListWidgetItem(l.txt);
		lwi->setForeground(l.br);
		ui->listWidget->addItem(lwi);
		if(ui->listWidget->count()>maxLines)
		{
			delete ui->listWidget->takeItem(0);
			ui->listWidget->item(0)->setText("...");
		}
		ui->listWidget->scrollToBottom();
	}
}

void WidConnectionInspector::fillList()
{
	ui->listWidget->clear();
	if(ui->comboCon->currentIndex()<0)
		return;
	Texts::iterator it=texts.find(ui->comboCon->currentText());
	if(it==texts.end())
		return;
	for(LineL::iterator lit=it->second.begin(); lit!=it->second.end(); ++lit)
	{
		QListWidgetItem *lwi=new QListWidgetItem(lit->txt);
		lwi->setForeground(lit->br);
		ui->listWidget->addItem(lwi);
	}
	ui->listWidget->scrollToBottom();
}

void WidConnectionInspector::clearCurrent()
{
	if(ui->comboCon->currentIndex()<0)
		return;
	Texts::iterator it=texts.find(ui->comboCon->currentText());
	if(it==texts.end())
		return;
	it->second.clear();
	it->second.push_back(Line("...", QBrush(Qt::black)));
	fillList();
}

void WidConnectionInspector::clearAll()
{
	LineL ll;
	ll.push_back(Line("...", QBrush(Qt::black)));
	for(Texts::iterator it=texts.begin(); it!=texts.end(); ++it)
	{
		it->second.clear();
		it->second.push_back(Line("...", QBrush(Qt::black)));
	}
	fillList();
}

void WidConnectionInspector::unfreezeSl(bool checked)
{
	if(!checked)
		fillList();
}
