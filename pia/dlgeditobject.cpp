#include "dlgeditobject.h"
#include <QSpinBox>
#include <QCheckBox>
#include <QComboBox>
#include <QGraphicsScene>
#include "piadi.h"
#include "ui_dlgeditobject.h"

DlgEditObject::DlgEditObject(QWidget *parent, pi_at_home::EditorObject &obj) :
	QDialog(parent), ui(new Ui::DlgEditObject), grid(new QGridLayout()), obj(obj), currentKeyVals(obj.getKeyVals())
{
	ui->setupUi(this);
	ui->pushEditExt->setEnabled(obj.hasExtendedEditor());
	ui->lineName->setText(obj.getName());
	QFrame *frame=new QFrame();
	ui->scrollArea->setWidget(frame);
	frame->setLayout(grid);
	int r=0;
	for(auto mit=obj.md->editorParamMetadatas.begin(); mit!=obj.md->editorParamMetadatas.end(); ++mit, ++r)
	{
		auto kit=currentKeyVals.find(mit->second->name);
		bool markit=false;
		if(kit==currentKeyVals.end())
		{
			if(mit->second->type==EditorParamMetadata::Type::REQUIRED)
				qWarning()<<"DlgEditObject missing required"<<mit->second->name;
			else
				markit=true;
			//qDebug()<<"no kv for"<<mit->second->name<<markit;
		}
		if(mit->first=="id")
			idRow=r;
		grid->addWidget(new QLabel(mit->first, this), r, 0);
		QWidget *w=nullptr;
		if(mit->second->dataFilter==EditorParamMetadata::DataFilter::LIST)
		{
			QComboBox *wid=new QComboBox(this);
			w=wid;
			for(auto vit=mit->second->filterVals.begin(); vit!=mit->second->filterVals.end(); ++vit)
				wid->addItem(vit->toString());
			if(kit!=currentKeyVals.end())
			{
				wid->setCurrentText(kit->second);
				//qDebug()<<"setting text"<<kit->second;
			}
			grid->addWidget(wid, r, 1);
		}
		else
		{
			switch(mit->second->dataType)
			{
			case EditorParamMetadata::DataType::BOOL:
			{
				QCheckBox *wid=new QCheckBox(this);
				w=wid;
				if(kit!=currentKeyVals.end())
					wid->setChecked(kit->second=="true");
				grid->addWidget(w, r, 1);
				break;
			}
			case EditorParamMetadata::DataType::INT:
			{
				QSpinBox *wid=new QSpinBox(this);
				w=wid;
				if(mit->second->dataFilter==EditorParamMetadata::DataFilter::MINMAX)
				{
					wid->setMinimum(mit->second->filterVals.at(0).toInt());
					wid->setMaximum(mit->second->filterVals.at(1).toInt());
				}
				else
				{
					wid->setMinimum(std::numeric_limits<int>::min());
					wid->setMaximum(std::numeric_limits<int>::max());
				}
				if(kit!=currentKeyVals.end())
				{
					wid->setValue(kit->second.toInt());
					//qDebug()<<"set int value"<<mit->second->name<<kit->second<<kit->second.toInt();
				}
				grid->addWidget(wid, r, 1);
				break;
			}
			case EditorParamMetadata::DataType::DOUBLE:
			case EditorParamMetadata::DataType::STRING:
			case EditorParamMetadata::DataType::UNDEF:
			{
				QLineEdit *wid=new QLineEdit(this);
				w=wid;
				if(kit!=currentKeyVals.end())
					wid->setText(kit->second);
				grid->addWidget(w, r, 1);
				break;
			}
			}
		}
		if(mit->second->type==EditorParamMetadata::Type::DEFAULTVALUE || mit->second->type==EditorParamMetadata::Type::OMITABLE)
		{
			QCheckBox * wid=new QCheckBox(mit->second->type==EditorParamMetadata::Type::DEFAULTVALUE?tr("Default"):tr("Omit"), this);
			if(mit->second->type==EditorParamMetadata::Type::DEFAULTVALUE)
				wid->setToolTip(mit->second->def.toString());
			wid->setChecked(markit);
			connect(wid, &QCheckBox::stateChanged, this, [this, r](int checkState){defOmChangedSl(checkState, r);});
			grid->addWidget(wid, r, 2);
			defOmChangedSl(wid->isChecked()?Qt::Checked:Qt::Unchecked, r);
		}
		if(w)
			w->setToolTip(mit->second->description);
	}
	grid->setRowStretch(r, 1);
}

DlgEditObject::~DlgEditObject() {delete ui;}

void DlgEditObject::accept()
{
	int newId=dynamic_cast<QSpinBox *>(grid->itemAtPosition(idRow, 1)->widget())->value();
	if(!obj.setId(newId))
	{
		QMessageBox::warning(this, tr("Invalid ID"), tr("ID %1 is already in use. IDs must be unique.").arg(newId));
		return;
	}
	extractValues(currentKeyVals);
	obj.setKeyVals(currentKeyVals);
	QDialog::accept();
}

void DlgEditObject::defOmChangedSl(int checkState, int row)
{
	QWidget *w=grid->itemAtPosition(row, 1)->widget();
	if(!w)
		return;
	w->setEnabled(checkState!=Qt::Checked);
	if(checkState!=Qt::Checked)
		return;
	QWidget *w2=grid->itemAtPosition(row, 2)->widget();
	if(!w2)
		return;
	QString d=w2->toolTip();
	//qDebug()<<"tooltip as default"<<d;
	QLineEdit * led=dynamic_cast<QLineEdit *>(w);
	if(led)
		led->setText(d);
	else
	{
		QComboBox * cbd=dynamic_cast<QComboBox *>(w);
		if(cbd)
			cbd->setCurrentText(d);
		else
		{
			QCheckBox * chd=dynamic_cast<QCheckBox *>(w);
			if(chd)
				chd->setChecked(d=="true");
			else
			{
				QSpinBox * spd=dynamic_cast<QSpinBox *>(w);
				if(spd)
					spd->setValue(d.toInt());
			}
		}
	}
}

void DlgEditObject::on_pushEditExt_clicked()
{
	extractValues(currentKeyVals);
	obj.updateExtendedEditor(this, currentKeyVals);
}

void DlgEditObject::extractValues(KeyValMV &targetKeyVals)
{
	int rcnt=grid->rowCount();
	for(int r=0; r<rcnt; ++r)
	{
		auto li=grid->itemAtPosition(r, 0);
		if(!li)
			continue; // the last line ususally which has only a spacer
		QLabel * l=dynamic_cast<QLabel *>(li->widget());
		if(!l)
		{
			qWarning()<<"DlgEditNode::accept no label"<<r;
			continue;
		}
		QCheckBox * ch=dynamic_cast<QCheckBox *>(grid->itemAtPosition(r, 2)?grid->itemAtPosition(r, 2)->widget():nullptr);
		bool dowrite=true;
		if(ch && ch->isChecked())
			dowrite=false; // doesn't matter if omitable or default_value
		auto it=targetKeyVals.find(l->text());
		QString data;
		bool dok=false;
		QLineEdit * led=dynamic_cast<QLineEdit *>(grid->itemAtPosition(r, 1)->widget());
		if(led)
		{
			dok=true;
			data=led->text();
			if(l->text()=="prefix")
				obj.prefix=data;
		}
		if(!dok)
		{
			QComboBox * cbd=dynamic_cast<QComboBox *>(grid->itemAtPosition(r, 1)->widget());
			if(cbd)
			{
				dok=true;
				data=cbd->currentText();
			}
			if(!dok)
			{
				QCheckBox * chd=dynamic_cast<QCheckBox *>(grid->itemAtPosition(r, 1)->widget());
				if(chd)
				{
					dok=true;
					data=(chd->isChecked()?"true":"false");
				}
				if(!dok)
				{
					QSpinBox * spd=dynamic_cast<QSpinBox *>(grid->itemAtPosition(r, 1)->widget());
					if(spd)
					{
						dok=true;
						data=QString::number(spd->value());
					}
				}
			}
		}
		if(!dok)
		{
			qWarning()<<"DlgEditNode::accept no data"<<r<<l->text();
			continue;
		}
		if(dowrite)
		{
			if(it==targetKeyVals.end())
			{
				targetKeyVals.insert(KeyValMV::value_type(l->text(), data));
				//qDebug()<<"inserting"<<l->text()<<data;
			}
			else
			{
				it->second=data;
				//qDebug()<<"updating"<<it->first<<data;
			}
		}
		else
		{
			if(it==targetKeyVals.end())
				continue; // it's fine
			else
				targetKeyVals.erase(it);
		}
	}
}
