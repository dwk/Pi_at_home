#ifndef DLGHOSTDICT_H
#define DLGHOSTDICT_H

#include "piadh.h"
#include <QDialog>
#include <QTableWidgetItem>

namespace Ui
{
	class DlgHostDict;
}

namespace pi_at_home
{

class DlgHostDict : public QDialog
{
	Q_OBJECT
public:
	explicit DlgHostDict(QWidget *parent);
	~DlgHostDict();
public slots:
	virtual void accept();
private slots:
	void on_toolAdd_clicked();
	void on_toolDelete_clicked();
	void on_toolSave_clicked();
	void on_pushDeployAll_clicked();
	void on_tableWidget_currentCellChanged(int currentRow, int, int, int);
	void on_tableWidget_itemChanged(QTableWidgetItem *item);
private:
	void setToolbuttons(int currentState);
	void checkAllClean();
	Ui::DlgHostDict *ui;
	bool allClean=true, ignoreCellChange=true;
};
}
#endif // DLGHOSTDICT_H
