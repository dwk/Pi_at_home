#-------------------------------------------------
#
# Project created by QtCreator 2016-10-30T11:46:16
#
#-------------------------------------------------

QT       += core gui network xml

# this is awkward - for some reason qcustomplot -> QtPrintSupport does not follow the sysroot includes
# but needs an absolute path (otherwise it tries to look in the host system include folder!
#INCLUDEPATH += /opt/qtrpi/raspbian/sysroot-full/usr/include

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

tsttarget.target = tsttarget
tsttarget.depends = FORCE
tsttarget.commands = ../piah_common/createtst $$OUT_PWD

QMAKE_EXTRA_TARGETS += tsttarget
PRE_TARGETDEPS += tsttarget

CONFIG(release, debug|release):QMAKE_POST_LINK = cp $$OUT_PWD/$$TARGET ~/bin

TARGET = pia
TEMPLATE = app

SOURCES += main.cpp piaapp.cpp mainwindow.cpp \
	dlgabout.cpp dlghostcon.cpp \
	connections.cpp \
	dlghostdict.cpp \
	widconfigworld.cpp \
    widconnectioninspector.cpp \
    ../piah_common/command.cpp \
    ../piah_common/nodeaddress.cpp \
	../piah_common/node.cpp \
    ../piah_common/netcon.cpp \
    ../qcustomplot/qcustomplot.cpp \
    widgraph.cpp \
    ../piah_common/datafilereader.cpp \
    ../piah_common/datahistory.cpp \
    dockwidfactory.cpp \
    widavatars.cpp \
    widdatacollector.cpp \
    widdebug.cpp \
    dlgdata.cpp \
    dlggraphlist.cpp \
    datahistroyextgui.cpp \
    avnode.cpp \
    avnodebitport.cpp \
    avnodeds1820.cpp \
    avnodeswitchport.cpp \
    avnodetadlbus.cpp \
    avnodetlv1548.cpp \
    avnodealg.cpp \
    datacontainer.cpp \
    ../piah_common/buildtst.cpp \
    avnodelcd84x48.cpp \
    dlgdeploy.cpp \
    avnodeardubridge.cpp \
    ../piah_common/hostdict.cpp \
    widconfigeditor.cpp \
    editorobject.cpp \
    editorview.cpp \
    editorobjectderived.cpp \
    dlgeditnode.cpp \
    editorobjectmetadata.cpp \
    dlgeditobject.cpp \
    dlgeotimer.cpp \
    dlgeditbaseconfig.cpp \
    editorscene.cpp \
    editornode.cpp \
    editorconnection.cpp \
	worldobject.cpp

HEADERS  += piaapp.h mainwindow.h \
	dlgabout.h dlghostcon.h \
	connections.h \
	dlghostdict.h \
	widconfigworld.h \
	widconfigworld_fac.h \
    widconnectioninspector.h \
    piadh.h \
    piadi.h \
    ../piah_common/ex.h \
    ../piah_common/command.h \
    ../piah_common/nodeaddress.h \
    ../piah_common/singleton.h \
    ../piah_common/commandtarget.h \
	../piah_common/node.h \
    ../piah_common/netcon.h \
    ../qcustomplot/qcustomplot.h \
    widgraph.h \
    ../piah_common/datafilereader.h \
    ../piah_common/datahistory.h \
    dockwidfactory.h \
    widgraph_fac.h \
    widconnectioninspector_fac.h \
    widavatars_fac.h \
    widavatars.h \
    widdatacollector_fac.h \
    widdatacollector.h \
    tinylistwidget.h \
    widactor.h \
    widdebug.h \
    dlgdata.h \
    dlggraphlist.h \
    datahistoryextgui.h \
    avnode.h \
    avnodealg.h \
    avnodebitport_fac.h \
    avnodebitport.h \
    avnodeds1820_fac.h \
    avnodeds1820.h \
    avnodeswitchport_fac.h \
    avnodeswitchport.h \
    avnodetadlbus_fac.h \
    avnodetadlbus.h \
    avnodetlv1548_fac.h \
    avnodetlv1548.h \
    avnodealg_fac.h \
    ../piah_common/nodealg_nm.h \
    ../piah_common/nodebitport_nm.h \
    ../piah_common/nodeds1820_nm.h \
    ../piah_common/nodeswitchport_nm.h \
    ../piah_common/nodetadlbus_nm.h \
    ../piah_common/nodetlv1548_nm.h \
    datacontainer.h \
    ../piah_common/buildtst.h \
    avnodelcd84x48_fac.h \
    avnodelcd84x48.h \
    dlgdeploy.h \
    avnodeardubridge_fac.h \
    avnodeardubridge.h \
    ../piah_common/hostdict.h \
    widconfigeditor_fac.h \
    widconfigeditor.h \
    editorobject.h \
    editorview.h \
    editorobjectderived.h \
    editorobjectmetadata.h \
    dlgeditnode.h \
    dlgeditobject.h \
    dlgeotimer.h \
    dlgeditbaseconfig.h \
    editorscene.h \
    editornode.h \
    editorconnection.h \
    editordict.h \
	worldobject.h

FORMS    += mainwindow.ui \
    dlgabout.ui \
	dlghostcon.ui \
	dlghostdict.ui \
	widconfigworld.ui \
    widconnectioninspector.ui \
    widnode.ui \
    widgraph.ui \
    widdatacollector.ui \
    dlgdata.ui \
    dlggraphlist.ui \
    dlgdeploy.ui \
    widconfigeditor.ui \
    dlgeditnode.ui \
    dlgeditobject.ui \
    dlgeotimer.ui \
    dlgeditbaseconfig.ui

RESOURCES += res.qrc
