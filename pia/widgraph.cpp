#include "widgraph.h"
#include "widgraph_fac.h"
#include <QFileDialog>
#include <QDir>
#include "datahistoryextgui.h"
#include "dlgdata.h"
#include "dlggraphlist.h"
#include "../piah_common/datafilereader.h"
#include "ui_widgraph.h"
#include "piadi.h"

WidGraph::WidGraph(QWidget *parent) :
	QWidget(parent),
	ui(new ::Ui::WidGraph)
{
	DCON.setDataHistoryExtFactory(ExtGuiFactory);
	ui->setupUi(this);
	//ui->listRight->setVisible(false);
	//ui->listLeft->setMinimumHeight(30);
	ui->graph->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom|QCP::iSelectPlottables);
	ui->graph->axisRect()->setRangeZoom(Qt::Horizontal);
	connect(ui->graph, SIGNAL(selectionChangedByUser()), this, SLOT(selectedSomethingSl()));
	connect(ui->graph->xAxis, SIGNAL(rangeChanged(const QCPRange &)), this, SLOT(xRangeChangedSl(const QCPRange &)));
	connect(ui->pushUpdateGraph, SIGNAL(clicked(bool)), this, SLOT(pushUpdateGraph()));
	//connect(ui->toolLeftAll, SIGNAL(clicked(bool));
	connect(ui->toolLeftAll, static_cast<void (QToolButton::*)(bool)>(&QToolButton::clicked), [this]() -> void {selectAllSl(leftMap);});
	connect(ui->toolRightAll, static_cast<void (QToolButton::*)(bool)>(&QToolButton::clicked), [this]() -> void {selectAllSl(rightMap);});
	connect(ui->toolLeftFav, static_cast<void (QToolButton::*)(bool)>(&QToolButton::clicked), [this]() -> void {selectFavSl(leftMap);});
	connect(ui->toolRightFav, static_cast<void (QToolButton::*)(bool)>(&QToolButton::clicked), [this]() -> void {selectFavSl(rightMap);});
	connect(ui->toolLeftSelect, static_cast<void (QToolButton::*)(bool)>(&QToolButton::clicked), [this]() -> void {selectDlgSl(leftMap);});
	connect(ui->toolRightSelect, static_cast<void (QToolButton::*)(bool)>(&QToolButton::clicked), [this]() -> void {selectDlgSl(rightMap);});
	connect(ui->comboLeftUnit, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this]() -> void {comboChanged(ui->comboLeftUnit->currentData(), leftMap);});
	connect(ui->comboRightUnit, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this]() -> void {comboChanged(ui->comboRightUnit->currentData(), rightMap);});
	connect(ui->pushConfigure, SIGNAL(clicked(bool)), this, SLOT(pushConfigureSl()));
	connect(ui->comboXStyle, SIGNAL(currentIndexChanged(int)), this, SLOT(xStyleChangeSl(int)));
	fillCombo();
	selectedSomethingSl();
}

WidGraph::~WidGraph()
{
	delete ui;
}

void WidGraph::connectAll(QObjectList &otherActors)
{
	foreach(QObject * obj, otherActors)
	{
		//qDebug()<<"testing"<<obj->metaObject()->className();
		if(!strcmp(obj->metaObject()->className(), "pi_at_home::WidDataCollector"))
		{
			//qDebug()<<"connect!";
			connect(obj, SIGNAL(loadCompleted()), this, SLOT(fillCombo()));
		}
	}
}

void WidGraph::fillCombo()
{
	leftMap.clear();
	rightMap.clear();
	activateGraph();
	ui->comboLeftUnit->blockSignals(true);
	ui->comboRightUnit->blockSignals(true);
	ui->comboLeftUnit->clear();
	ui->comboRightUnit->clear();
	ui->comboLeftUnit->addItem(tr("no axis"));
	const DataHistoryMP & dhs=DCON.getAllDataHistory();
	for(DataHistoryMP::const_iterator it=dhs.begin(); it!=dhs.end(); ++it)
	{
		QString unitTxt=Node::unit(it->second->getUnit());
		int index=ui->comboLeftUnit->findText(unitTxt);
		if(index<0)
		{
			QVariant vd=QVariant(QStringList()<<it->first.toString());
			ui->comboLeftUnit->addItem(unitTxt, vd);
		}
		else
		{
			QStringList vd=ui->comboLeftUnit->itemData(index).toStringList();
			vd<<it->first.toString();
			ui->comboLeftUnit->setItemData(index, QVariant(vd));
		}
	}
	for(int i=0; i<ui->comboLeftUnit->count(); ++i)
	{
		ui->comboRightUnit->addItem(ui->comboLeftUnit->itemText(i), ui->comboLeftUnit->itemData(i));
	}
	ui->comboLeftUnit->setCurrentIndex(0);
	ui->comboRightUnit->setCurrentIndex(0);
	ui->comboLeftUnit->blockSignals(false);
	ui->comboRightUnit->blockSignals(false);
	//ui->listLeft->clear();
	//ui->listRight->clear();
}

void WidGraph::pushUpdateGraph()
{
	ui->graph->clearGraphs();
	selectedSomethingSl();
	ui->graph->setAutoAddPlottableToLegend(true);
	bool leftYUsed=false, rightYUsed=false;
	Scaler scaler;
	/* QList<QListWidgetItem *> plots=ui->listLeft->selectedItems();
	foreach(QListWidgetItem * lwi, plots)
	{
		addPlot(lwi->text(), true, scaler);
		leftYUsed=true;
	}*/
	for(DispMap::iterator it=leftMap.begin(); it!=leftMap.end(); ++it)
	{
		if (!it->second)
			continue;
		addPlot(it->first, true, scaler);
		leftYUsed=true;
	}
	for(DispMap::iterator it=rightMap.begin(); it!=rightMap.end(); ++it)
	{
		if (!it->second)
			continue;
		addPlot(it->first, false, scaler);
		rightYUsed=true;
	}
	ui->graph->legend->setVisible(true);
	ui->graph->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignLeft);
	// configure bottom axis to show date instead of number:
	QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
	dateTicker->setTickStepStrategy(QCPAxisTicker::tssMeetTickCount);
	dateTicker-> setTickOrigin(QDateTime(QDate(2017,1,1), QTime(0, 0)));
	dateTicker->setTickStepStrategy(QCPAxisTicker::TickStepStrategy::tssReadability);
	dateTicker->setTickCount(10);
	ui->graph->xAxis->setTicker(dateTicker);
	ui->comboXStyle->setCurrentIndex(1);
	//xStyleChangeSl(ui->comboXStyle->currentIndex());
	ui->graph->xAxis->setTickLabelFont(QFont(QFont().family(), 8));
	// set axis labels:
	ui->graph->xAxis->setLabel("Date & Time");
	ui->graph->xAxis->setRange(scaler.xmin, scaler.xmax);
	if(leftYUsed)
	{
		ui->graph->yAxis->setVisible(true);
		ui->graph->yAxis->setLabel(ui->comboLeftUnit->currentText());
		ui->graph->yAxis->setRange(scaler.y1min, scaler.y1max);
		//qDebug()<<"scaler left"<<scaler.y1min<<scaler.y1max;
	}
	else
		ui->graph->yAxis->setVisible(false);
	if(rightYUsed)
	{
		ui->graph->yAxis2->setVisible(true);
		ui->graph->yAxis2->setLabel(ui->comboRightUnit->currentText());
		ui->graph->yAxis2->setRange(scaler.y2min, scaler.y2max);
		//qDebug()<<"scaler right"<<scaler.y2min<<scaler.y2max;
	}
	else
		ui->graph->yAxis2->setVisible(false);
	ui->graph->replot();
}

void WidGraph::pushConfigureSl()
{
	DataHistory * dh=DCON.getDataHistory(ui->labInfo->text());
	if(dh)
	{
		DlgData dlg(this, *dh);
		dlg.exec();
	}
	else
	{
		qWarning()<<"no DataHistory for"<<ui->labInfo->text();
		ui->pushConfigure->setEnabled(false);
	}
	return;
}

void WidGraph::addPlot(const NodeAddress & adr, bool leftAxis, Scaler & scaler)
{
	DataHistory *dh=DCON.getDataHistory(adr, false);
	if(!dh)
	{
		qCritical()<<"inconsistent data row names"<<ui->comboLeftUnit->currentText();
		return;
	}
	//
	DataHistoryExtGui * dhe=qobject_cast< DataHistoryExtGui * > (dh->guiData().data());
	QPen pen(dhe->graphColor());
	pen.setWidth(dhe->graphLineWidth());
	QCPGraph *cg=ui->graph->addGraph(0, leftAxis?0:ui->graph->yAxis2);
	cg->setName(dh->getAdr().toStringUidAlias());
	cg->setLineStyle(QCPGraph::lsStepLeft);
	cg->setPen(pen);
	cg->setBrush(Qt::NoBrush);
	//
	QVector<QCPGraphData> timeData(dh->dr.size());
	int i=0;
	for(DataHistory::DataRow::const_iterator it=dh->dr.begin(); it!=dh->dr.end(); ++it, ++i)
	{
		timeData[i].key = (double)it->first.toTime_t();
		timeData[i].value = it->second;
		scaler.updateY(it->second, leftAxis);
	}
	cg->data()->set(timeData);
	scaler.updateX(timeData[0].key);
	scaler.updateX(timeData[timeData.size()-1].key);
}

void WidGraph::selectedSomethingSl()
{
	QList< QCPGraph * > grs=ui->graph->selectedGraphs();
	if(grs.size()<=0)
	{
		ui->labInfo->setText(tr("click on graph for more information"));
		ui->pushConfigure->setEnabled(false);
	}
	else if(grs.size()==1)
	{
		ui->labInfo->setText((*grs.begin())->name());
		ui->pushConfigure->setEnabled(true);
	}
	else
	{
		ui->labInfo->setText(tr("multiselect"));
		ui->pushConfigure->setEnabled(false);
		//foreach(QCPGraph * gr, grs)
	}
}

void WidGraph::selectAllSl(WidGraph::DispMap &dispMap)
{
	for(DispMap::iterator it=dispMap.begin(); it!=dispMap.end(); ++it)
		it->second=true;
	activateGraph();
}

void WidGraph::selectFavSl(WidGraph::DispMap &dispMap)
{
	for(DispMap::iterator it=dispMap.begin(); it!=dispMap.end(); ++it)
	{
		DataHistory * dh = DCON.getDataHistory(it->first, false);
		if(dh && qobject_cast< DataHistoryExtGui * >(dh->guiData().data())->isFav())
			it->second=true;
		else
			it->second=false;
	}
	activateGraph();
}

void WidGraph::selectDlgSl(WidGraph::DispMap &dispMap)
{
	DlgGraphList dlg(this, dispMap);
	dlg.exec();
	activateGraph();
}

void WidGraph::xStyleChangeSl(int index)
{
	QSharedPointer<QCPAxisTickerDateTime> ticker=ui->graph->xAxis->ticker().dynamicCast< QCPAxisTickerDateTime >();
	switch(index)
	{
		case 0:
			ticker->setDateTimeFormat("yy-M-d");
			break;
		case 1:
			ticker->setDateTimeFormat("yy-M-d H:mm");
			break;
		case 2:
			ticker->setDateTimeFormat("H:mm:ss");
			break;
		case 3:
			ticker->setDateTimeFormat("mm:ss.zzz");
			break;
		case 4:
			break;
	}
	ui->graph->replot();
}

void WidGraph::xRangeChangedSl(const QCPRange &newRange)
{
	ui->labInfo->setText(QCPAxisTickerDateTime::keyToDateTime(newRange.lower).toString("yyyy-MM-dd hh:mm:ss"));
	double tdiff=newRange.upper-newRange.lower;
	int ci=0;
	if(tdiff<120)
		ci=3;
	else if(tdiff<86400)
		ci=2;
	else if(tdiff<86400*3)
		ci=1;
	else
		ci=0;
	if(ui->comboXStyle->currentIndex()!=ci)
		ui->comboXStyle->setCurrentIndex(ci);
	//qDebug()<<startDt;
}

void WidGraph::comboChanged(QVariant vnamelist, pi_at_home::WidGraph::DispMap &dispMap)
{
	dispMap.clear();
	if(!vnamelist.isValid())
	{
		activateGraph();
		return;
	}
	QStringList adrs=vnamelist.toStringList();
	const DataHistoryMP & dhs=DCON.getAllDataHistory();
	for(DataHistoryMP::const_iterator it=dhs.begin(); it!=dhs.end(); ++it)
	{
		if(adrs.contains(it->second->getAdr().toString()))
		{
			//QListWidgetItem *lwi=new QListWidgetItem(it->second->getName());
			//dispMap->addItem(lwi);
			dispMap.insert(DispMap::value_type(it->second->getAdr(), true)); // displayed by default
		}
	}
	activateGraph();
	//pushUpdateGraph();
}

void WidGraph::activateGraph()
{
	bool leftAxis=leftMap.size(), rightAxis=rightMap.size();
	ui->pushUpdateGraph->setEnabled(leftAxis || rightAxis);
	ui->toolLeftAll->setEnabled(leftAxis);
	ui->toolLeftFav->setEnabled(leftAxis);
	ui->toolLeftSelect->setEnabled(leftAxis);
	ui->toolRightAll->setEnabled(rightAxis);
	ui->toolRightFav->setEnabled(rightAxis);
	ui->toolRightSelect->setEnabled(rightAxis);
}

