#ifndef DLGHOSTCON_H
#define DLGHOSTCON_H

#include <QDialog>
#include <QModelIndex>
#include <QThread>

namespace Ui
{
	class DlgHostCon;
}
class QTableWidgetItem;

namespace pi_at_home
{
class NetCon;

struct WorkerCmd
{
	QString cmd;
	QStringList args;
	int timeoutMs=1000;
};
typedef std::list<WorkerCmd> WorkerCmdLV;

class WorkerThread : public QThread
{
	Q_OBJECT
public:
	WorkerThread(QString cmd, QStringList args, int timeoutMs, QObject *parent) :
		QThread(parent), cmd(cmd), args(args), timeoutMs(timeoutMs) {}
	bool hasError() const {return errStr.size();}
	const QString & getResult() const {return resultStr;}
	const QString & getError() const {return errStr;}
protected:
	virtual void run() Q_DECL_OVERRIDE;
	QString cmd, resultStr, errStr;
	QStringList args;
	int timeoutMs;
};


class DlgHostCon : public QDialog
{
	Q_OBJECT
public:
	explicit DlgHostCon(QWidget *parent = 0);
	~DlgHostCon();
	QString hostAddress() const;
	int port() const;
public slots:
	virtual void accept() override;
	void sockErrorSig(int errNo);
	void sockReady(bool ready);
protected:
	void timerEvent(QTimerEvent *event) override;
private slots:
	void test();
	//void removeSl(const QString netAdr);
	void on_tableKnownHosts_doubleClicked(const QModelIndex &index);
	void hostSl(QModelIndex idx);
	void remoteConSl();
	void viewLogSl();
	void workerDoneSl();
	void workerNext();
private:
	void buttonState();
	Ui::DlgHostCon *ui;
	NetCon *hc=nullptr;
	int tid=0, progress=0;
	WorkerThread *worker=nullptr;
	WorkerCmdLV workerCmds;
};
}
#endif // DLGHOSTCON_H
