#ifndef DATAHISTORYEXTGUI_H
#define DATAHISTORYEXTGUI_H

#include "piadh.h"
#include <QObject>
#include "../piah_common/datahistory.h"
#include <QColor>

namespace pi_at_home
{

class DataHistoryExtGui : public QObject
{
	Q_OBJECT
public:
	DataHistoryExtGui(const DataHistory & dh);
	~DataHistoryExtGui() {}
	const QColor & graphColor() const {return graphColor_;}
	void setGraphColor(const QColor & color);
	int graphLineWidth() const {return graphLineWidth_;}
	void setGraphLineWidth(int width);
	bool isFav() const {return fav_;}
	void setFav(bool fav);
protected:
	QString baskey;
	QColor graphColor_;
	int graphLineWidth_;
	bool fav_;
};

QObject * ExtGuiFactory(const DataHistory & dh);

}
#endif // DATAHISTORYEXTGUI_H
