#ifndef DLGEOTIMER_H
#define DLGEOTIMER_H

#include "piadh.h"
#include <QDialog>
#include <QListWidgetItem>
#include "editorobject.h"
#include "editorobjectderived.h"

namespace Ui
{
	class DlgEOTimer;
}

namespace pi_at_home
{

class DlgEOTimer : public QDialog
{
	Q_OBJECT
public:
	explicit DlgEOTimer(EditorObject *obj, KeyValMV &currentKeyVals, QWidget *parent);
	~DlgEOTimer();
public slots:
	virtual void accept();
private slots:
	void on_toolAdd_clicked();
	void on_toolDelete_clicked();
	void on_toolUp_clicked();
	void on_toolDown_clicked();
	void on_listWidget_currentRowChanged(int currentRow);
	void updateTimerVal();
	void spinChangedSl(int v);
private:
	QString tm2Str(long long tm);
	bool checkConsistency();
	Ui::DlgEOTimer *ui;
	EOTimer *obj;
	KeyValMV &currentKeyVals;
	bool hms=false;
	QString delayStyle;
};
}
#endif // DLGEOTIMER_H
