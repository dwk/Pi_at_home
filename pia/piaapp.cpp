#include "piaapp.h"
#include <QSharedMemory>
#include "piadi.h"

PiaApp::PiaApp(int &argc, char **argv) : QApplication(argc, argv), shm(new QSharedMemory("PiAtHomeIPC", this))
{
	/*
	qDebug()<<"PiaApp ctor: argc "<<argc;
	for(int i=0; i<argc; ++i)
	{
		qDebug()<<"argv["<<i<<"]"<<argv[i];
		qDebug()<<"strlen"<<strlen(argv[i]);
	}*/
	//qsrand(424242); no qrand used in this app
}

PiaApp::~PiaApp()
{
	if(shm && shm->isAttached())
		shm->detach();
}

bool PiaApp::notify ( QObject * receiver, QEvent * event )
{
	bool res=true;
	try
	{
		return QApplication::notify(receiver, event);
    }
	catch(Ex & ex)
	{
		qDebug()<<"Exception catched: "<<ex.getCode()<<" "<<ex.getMessage();
	}
	catch(...)
	{
		qDebug()<<"unknwon exception catched";
	}
    return res;
}

bool PiaApp::attachIpc()
{
	if(shm->attach(QSharedMemory::ReadOnly))
	{
		shm->detach();
		return false;
	}
	if(shm->create(1))
		return true;
	return false;
}

