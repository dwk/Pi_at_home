#include "avnodealg.h"
#include "avnodealg_fac.h"
#include "ui_widnode.h"
#include "piadi.h"


// bool --------------------------------------------------------
AvNodeAlgBool::AvNodeAlgBool(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QCheckBox(this))
{
	defval_=NodeValue(false);
	ui->labelState->setToolTip(typeNameBool);
	val=(fragment.attribute("val")=="true");
	widVal->setChecked(val);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(clicked(bool)), this, SLOT(valChanged(bool)));
}

void AvNodeAlgBool::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	val=value_.toBool();
	widVal->blockSignals(true);
	widVal->setChecked(val);
	widVal->blockSignals(false);
}

void AvNodeAlgBool::valChanged(bool val)
{
	value_=NodeValue(val);
	nodeValueChanged();
}

// int -----------------------------------------------------
AvNodeAlgInt::AvNodeAlgInt(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QSpinBox(this))
{
	defval_=NodeValue((int)0);
	ui->labelState->setToolTip(typeNameInt);
	widVal->setRange(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
	widVal->setValue(fragment.attribute("val").toInt());
	widVal->installEventFilter(this);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(int)), this, SLOT(valChangedSl(int)));
}

void AvNodeAlgInt::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toInt());
	widVal->blockSignals(false);
}

bool AvNodeAlgInt::eventFilter(QObject *watched, QEvent *event)
{
	if(event->type() == QEvent::Wheel && watched == widVal)
	{ // no wheel input on spin boxes (risk of unintentional change!)
		return true;
	}
	return false;
}

void AvNodeAlgInt::valChangedSl(int val)
{
	Command *cmd=new Command("nodeset");
	cmd->setDestination(adr_);
	cmd->setAttribute("val", QString::number(val));
	DCON.dispatchCommand(*cmd);
}


// double -----------------------------------------------------
AvNodeAlgDbl::AvNodeAlgDbl(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QDoubleSpinBox(this))
{
	defval_=NodeValue(0.);
	ui->labelState->setToolTip(typeNameDbl);
	widVal->setRange(-1.e10, 1.e10);
	widVal->setValue(fragment.attribute("val").toDouble());
	widVal->installEventFilter(this);
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(valueChanged(double)), this, SLOT(valChangedSl(double)));
}

void AvNodeAlgDbl::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setValue(value_.toDouble());
	widVal->blockSignals(false);
}

bool AvNodeAlgDbl::eventFilter(QObject *watched, QEvent *event)
{
	if(event->type() == QEvent::Wheel && watched == widVal)
	{ // no wheel input on spin boxes (risk of unintentional change!)
		return true;
	}
	return false;
}

void AvNodeAlgDbl::valChangedSl(double val)
{
	ui->toolOnline->setChecked(false);
	value_=NodeValue(val);
	//qDebug()<<"AvNodeAlgDbl"<<QString::number(val);
}


// QString -----------------------------------------------------
AvNodeAlgStr::AvNodeAlgStr(const pi_at_home::Fragment &fragment, QWidget *parent) :
	AvNode(fragment, parent), widVal(new QLineEdit(this))
{
	defval_=NodeValue(QString(""));
	ui->labelState->setToolTip(typeNameStr);
	widVal->setText(fragment.attribute("val")); // Fragment does unmarshaling
	ui->mainLayout->addWidget(widVal, 0, 3, 1, 1);
	connect(widVal, SIGNAL(editingFinished()), this, SLOT(editingFinishedSl()));
}

void AvNodeAlgStr::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	AvNode::processCommand(cmd, relevant);
	widVal->blockSignals(true);
	widVal->setText(value_.toString());
	widVal->blockSignals(false);
}

void AvNodeAlgStr::editingFinishedSl()
{
	value_=NodeValue(widVal->text()); // Fragment does marshaling
	nodeValueChanged();
}


