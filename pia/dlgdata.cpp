#include "dlgdata.h"
#include <QColorDialog>
#include "widgraph.h"
#include "piadi.h"
#include "ui_dlgdata.h"

DlgData::DlgData(QWidget *parent, DataHistory &data) :
	QDialog(parent), ui(new Ui::DlgData), data(data)
{
	dhExt=qobject_cast< DataHistoryExtGui * > (data.guiData().data());
	newColor=dhExt->graphColor();
	ui->setupUi(this);
	ui->labDataName->setText(data.getAdr().alias());
	ui->labDataSource->setText(data.getAdr().toString());
	ui->labDataStart->setText(data.getFirstTimestamp().toString(Qt::ISODate));
	ui->labDataEnd->setText(data.getLastTimestamp().toString(Qt::ISODate));
	ui->labDataCount->setText(QString::number(data.getCount()));
	ui->spinLineWidth->setValue(dhExt->graphLineWidth());
	ui->checkFav->setChecked(dhExt->isFav());
	connect(ui->pushColorSelect, SIGNAL(clicked(bool)), this, SLOT(selectColorSl()));
	connect(ui->spinLineWidth, SIGNAL(valueChanged(int)), this, SLOT(updatePreviewSl()));
	updatePreviewSl();
}

DlgData::~DlgData() {delete ui;}

void DlgData::accept()
{
	dhExt->setGraphColor(newColor);
	dhExt->setGraphLineWidth(ui->spinLineWidth->value());
	dhExt->setFav(ui->checkFav->isChecked());
	QDialog::accept();
}

void DlgData::selectColorSl()
{
	QColor c=QColorDialog::getColor(newColor, this, tr("Graph Color"));
	if(c.isValid())
	{
		newColor=c;
		updatePreviewSl();
	}
}

void DlgData::updatePreviewSl()
{
	QPalette pal=ui->linePreview->palette();
	pal.setColor(QPalette::WindowText, newColor);
	ui->linePreview->setPalette(pal);
	ui->linePreview->setLineWidth(ui->spinLineWidth->value());
}
