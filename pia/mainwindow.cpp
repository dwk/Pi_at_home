#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPointer>
#include <QDockWidget>
#include "dlgabout.h"
#include "dlghostcon.h"
#include "dlghostdict.h"
#include "dlgdeploy.h"
#include "../piah_common/node.h"
#include "widdebug.h"
#include "dockwidfactory.h"
#include "widactor.h"
#include "piadi.h"


QPointer<WidDebug> debugList=nullptr;
MainWindow * relay=nullptr;
QString templMessage="%1|%2 %7 [%3:%5,%4 %6]";
QString msgTypeStrs[6]={"Dbg", "Inf", "Wrn", "Crt", "Ftl", "???"};

void debugHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QString tst=QTime::currentTime().toString("HH:mm:ss.zzz");
	if(debugList.isNull() || !relay)
	{
		QByteArray localMsg = (tst+" "+msg).toLocal8Bit();
		switch (type)
		{
		case QtDebugMsg:
			fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
			break;
		case QtInfoMsg:
			fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
			break;
		case QtWarningMsg:
			fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
			break;
		case QtCriticalMsg:
			fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
			break;
		case QtFatalMsg:
			fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
			abort();
		}
	}
	else
	{
		int stridx=5;
		switch (type)
		{
			case QtDebugMsg:    stridx=0; break;
			case QtInfoMsg:     stridx=1; break;
			case QtWarningMsg:  stridx=2; break;
			case QtCriticalMsg: stridx=3; break;
			case QtFatalMsg:    stridx=4; break;
		}
		long long threadId=(long long)QThread::currentThreadId();
		relay->relayDebug(templMessage.arg(tst, msgTypeStrs[stridx], context.file, context.function).arg(context.line).arg(threadId).arg(msg),
			type, threadId);
	}
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
	ui(new ::Ui::MainWindow)
{
	ui->setupUi(this);
	DCON.setup("", *this);
	QSettings settings;

	QDockWidget *dockDbg = new QDockWidget(tr("Debug Output"), this);
	dockDbg->setObjectName("DebugOutput");
	dockDbg->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);
	debugList=new WidDebug(dockDbg);
	actors.push_back(debugList);
	dockDbg->setWidget(debugList);
	ui->menuWindows->addAction(dockDbg->toggleViewAction());
	//restOkW1=restoreDockWidget(dockDbg);
	//if(!restOkW1)
	//	addDockWidget(Qt::BottomDockWidgetArea, dockDbg);
	if(QCoreApplication::arguments().contains(QStringLiteral("--debug2console")))
	{
		relay=nullptr;
		debugList.data()->addLine(QStringLiteral("Debug and trace output routed to console because --debug2console specified on commandline"),
								  QtInfoMsg, (long long)QThread::currentThreadId());
	}
	else
	{
		qInfo()<<QStringLiteral("Use commandline argument --debug2console if you want all debug & trace on console");
		relay=this;
	}
	//qDebug()<<"dockDbg activate?"<<QCoreApplication::arguments();
	qInstallMessageHandler(debugHandler);
    connect(this, &MainWindow::relayDebugSig, debugList.data(), &WidDebug::addLine, Qt::AutoConnection);

	QDockWidget *dockCL = new QDockWidget(tr("Connection List"), this);
	dockCL->setObjectName("ConnectionList");
	dockCL->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);
	QWidget *conLiWi=DCON.connections()->widget(dockCL);
	dockCL->setWidget(conLiWi);
	actors.push_back(conLiWi);
	ui->menuWindows->addAction(dockCL->toggleViewAction());
	//restOkW2=restoreDockWidget(dockCL);
	//if(!restOkW2)
	//	addDockWidget(Qt::LeftDockWidgetArea, dockCL);
	//qDebug()<<"restoring fixed dock wids"<<restOkRead<<restOkW1<<restOkW2;

	QString centrName=settings.value("layout/central").toString();
	QWidget *central=nullptr;
	QStringList dws=DockWidFactory::get().allDockWids();
	foreach(QString dwname, dws)
	{
		DockWidFactory::DWInfo *dwi=DockWidFactory::get().produce(dwname);
		if(!dwi)
			continue;
		if(dwname==centrName)
		{
			qInfo()<<"central widget"<<dwname;
			central=dwi->factory(nullptr);
			actors.push_back(central);
			setCentralWidget(central);
			continue;
		}
		qInfo()<<"dock widget"<<dwname;
		QDockWidget *dock = new QDockWidget(dwi->guiName, this);
		dock->setObjectName(dwi->techName);
		dock->setStyleSheet("QDockWidget{border: 5px solid #800080}");
		dock->setAllowedAreas(dwi->allowedAreas);
		QWidget *dw = dwi->factory(dock);
		dock->setWidget(dw);
		actors.push_back(dw);
		ui->menuWindows->addAction(dock->toggleViewAction());
		bool restore=restoreDockWidget(dock);
		if(!restore)
		{
			qWarning()<<"unable to restore layout of dock widget"<<dwname;
			addDockWidget(dwi->defaultArea, dock);
		}
	}
	if(!central)
		qWarning()<<"no central widget"<<centrName;
	//bool restOkRead, restOkW1, restOkW2;
	//restOkRead=restoreGeometry(settings.value("layout/geometry").toByteArray());
	//restOkRead=restoreState(settings.value("layout/windowState").toByteArray()) && restOkRead;
	bool restOkRead;
	restOkRead=restoreGeometry(settings.value("layout/geometry").toByteArray());
	restOkRead=restoreState(settings.value("layout/windowState").toByteArray()) && restOkRead;
	if(!restOkRead)
		qWarning()<<"unable to restore layout of dock widgets";

	connect(ui->action_About, SIGNAL(triggered(bool)), this, SLOT(about()));
	connect(ui->action_Connect_To, SIGNAL(triggered(bool)), this, SLOT(newHostCon()));
	connect(ui->action_EditHosts, &QAction::triggered, this, &MainWindow::editHostsSl);
	connect(ui->action_Deployment, &QAction::triggered, this, &MainWindow::deploymentSl);
	foreach(QObject * obj, actors)
	{
		WidActor * act=dynamic_cast<WidActor *>(obj);
		if(act)
			act->connectAll(actors);
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

QAction *MainWindow::getAction(const QString &name)
{
	return findChild<QAction *>(name);
}

void MainWindow::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	Q_UNUSED(& cmd);
	Q_UNUSED(& relevant);
//	if(cmd.cmd()=="connect")
//	{
//		const FragmentLP & frags=cmd.fragments();
//		for(FragmentLP::const_iterator it=frags.begin(); it!=frags.end(); ++it)
//		{
//			QWidget * w=nullptr;
//			try
//			{
//				w=AvatarFactory::produce(**it, central);
//			}
//			catch(Ex &ex)
//			{
//				qCritical()<<"creation of WidNode failed"<<ex.getMessage();
//			}
//			if(!w)
//				continue;
//			centralLay->addWidget(w);
//		}
//	}
	return;
}

void MainWindow::relayDebug(QString text, int type, long long threadId)
{
	//fprintf(stderr, "%s", text.toLocal8Bit().constData());
	emit relayDebugSig(text, type, threadId);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QSettings settings;
	settings.setValue("layout/geometry", saveGeometry());
	settings.setValue("layout/windowState", saveState());
	DCON.shutdown();
	qInfo()<<"DataContainer closed";
	QMainWindow::closeEvent(event);
}

void MainWindow::about()
{
	DlgAbout dlg(this);
	dlg.exec();
}

void MainWindow::newHostCon()
{
	DlgHostCon dlg(this);
	dlg.exec();
}

void MainWindow::editHostsSl()
{
	DlgHostDict dlg(this);
	dlg.exec();
}

void MainWindow::deploymentSl()
{
	DlgDeploy dlg(this);
	dlg.exec();
}

