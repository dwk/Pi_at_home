#ifndef AVNODETLV1548_FAC_H
#define AVNODETLV1548_FAC_H
#include "../piah_common/nodetlv1548_nm.h"

namespace
{
pi_at_home::AvNode * AvCreatorRw(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeTLV1548Rw(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorSc(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeTLV1548Sc(frag, guiParent);
}
pi_at_home::AvNode * AvCreatorPt(const pi_at_home::Fragment &frag, QWidget * guiParent)
{
	return new pi_at_home::AvNodeTLV1548Pt(frag, guiParent);
}
bool avatarRwRegistered=pi_at_home::AvatarFactory::registerAvatar(typeNameRaw, AvCreatorRw);
bool avatarScRegistered=pi_at_home::AvatarFactory::registerAvatar(typeNameScaled, AvCreatorSc);
bool avatarPtRegistered=pi_at_home::AvatarFactory::registerAvatar(typeNamePt1000, AvCreatorPt);
}

#endif // AVNODETLV1548_FAC_H
