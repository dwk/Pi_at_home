#!/bin/bash
# aus https://blog.h7d.de/2018/01/06/raspberry-pi-touchscreen-um-90-drehen/
# Ungebungsvariable muss gesetzt sein da sonst keine Verbindung zum X-Server aufgebaut wird:
# in /etc/xdg/lxsession/LXDE-pi/autostart einfuegen
# @lxterminal -e /home/pi/touch_rotate.sh
export DISPLAY=:0.0
# Toucheingabe um 90° drehen:
xinput set-prop 'Quanta OpticalTouchScreen' 'Coordinate Transformation Matrix' 0 1 0 -1 0 1 0 0 1
#xinput --set-prop 'FT5406 memory based driver' 'Coordinate Transformation Matrix' 0 1 0 -1 0 1 0 0 1
