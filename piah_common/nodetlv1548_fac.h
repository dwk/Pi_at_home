#ifndef NODETLV1548_FAC_H
#define NODETLV1548_FAC_H

//#include "piadh.h"
//#include "node.h"
//#include "device.h"
#include "nodetlv1548_nm.h"

namespace
{
pi_at_home::Device * Creator(const QDomElement &elem)
{
	return new pi_at_home::DeviceTLV1548(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, Creator);
}

#endif // NODETLV1548_FAC_H
