#ifndef NODE__H
#define NODE__H

#include "piadh.h"
#include "enum_result.h"
#include <QVariant>
#include <QPointer>
#include <QDomElement>
#include <QDebug>
#include <QDateTime>
#include "nodeaddress.h"
#include "commandtarget.h"

namespace Ui
{
	class WidNode;
}

namespace pi_at_home
{
class Fragment;
typedef QVariant NodeValue;
typedef std::map< QString, NodeValue > NodeValueMV;

// DOC MOVED TO ARCHITECTURE.ODT
class Node : public QObject, public CommandTarget
{
	Q_OBJECT
public:
	enum class State{ UNDEF=0, READY=1, BUSY=2, LOCKED=3, ERROR=4 };
	enum class Unit{ UNDEF=0, NONE, BIT, USER, PERCENT, FRACTION, COUNT, RADIANT, STERADIANT,
					 METER, SEC, HZ, TIME, KG, NEWTON, PASCAL, BAR,
					 WATT, JOULE, WATT_ISQM, WATTHOUR,
					 KELVIN, DEGREE_CELSIUS,
					 VOLT, AMPERE, OHM, VOLTAMPERE, VAR,
					 QM, LITRE, LITRE_ISEC,
					 KILOMETER, DECIMETER, CENTIMETER, MILLIMIETER, MICROMETER, NANOMETER, PICOMETER};
	typedef unsigned long long int TokenType;
	static Node::Unit unit(const QString & unitStr);
	static QString unit(const Node::Unit & unit);
	static Node::State state(const QString & stateStr);
	static QString state(const Node::State & state);
	Node(const QDomElement &elem, int device, QObject * parent, int channel=-1); // default is "read from DomElement"
	Node(const QString & name, int device, int channel, QObject *parent);
	virtual ~Node();
	Node::State state() const {return state_;}
	int logHoldoffMs() const {return logHoldoffUs_<0?-1:static_cast<int>(logHoldoffUs_/1000);} // -1 = no logging, 0 = instant logging, >0 = x ms dead time after log
	bool doLog(qint64 currentTst);
	const NodeAddress & address() const {return adr_;}
	#ifndef GUI_ONLY
	//QString fullAlias() const; // prefix qualified alias
	#endif
	bool isHidden() const {return isHidden_;}
	void setHidden(bool hidden) {isHidden_=hidden;}
	/* interface Node */
	virtual Fragment * toFragment() const;
	virtual Fragment * toNodechFrag(const pi_at_home::NodeValue &newValue, qint64 timestamp) const;
	// type
	// string representation of the node type not neccessarily equal to class name
	virtual const QString & type() const =0;
	// serializeValue
	// overwrite if QVariant::toString is not what you want
	virtual QString serializeValue(const NodeValue & value) const {return value.toString();}
	// unit
	// overwrite: provide your unit
	virtual Node::Unit unit() const =0;
	virtual NodeValue defaultValue() const =0;
	virtual bool canSetValue(const NodeValue & value) const =0;
	virtual Result init();
	virtual const NodeValue & value() const {return value_;}
	virtual void setValue(const NodeValue & value, TokenType token=0) =0;
	// ping
	// trigger a newVal signal; some node type may initiate a hardware update before emitting the signal
	// call in non-blocking but the signal may be called asynchronously
	// default impl at Node only emittes the signal synchronously
	virtual void ping();
	virtual TokenType lock(const QString uid, int device, const QString alias); // returns access token
	virtual bool unlock(TokenType token); // true if successful
	virtual const NodeAddress * getLocker() const {return algLocker;}
	virtual void simulateValue(const NodeValue & value);
	virtual bool isSimulating() const {return simulating;}
	/* interface CommandTarget */
	virtual void processCommand(Command & cmd, Fragment & relevant) =0;
public slots:
	virtual void newValSl(const NodeValue & newValue) = 0;
protected:
	bool tokenValid(TokenType token);
	NodeAddress adr_, *algLocker=nullptr;
	State state_=State::UNDEF;
	NodeValue value_;
	bool simulating=false, isHidden_=false;
	qint64 tst, logHoldoffUs_=-1, logLastLog=-1;
	unsigned long long int lockHash=0;
signals:
	void newValSig(const NodeValue & newValue, qint64 timestamp); // microseconds since Unix epoche
};
typedef std::vector<QPointer<Node > > NodesVSP;
typedef std::map<QString, QPointer<Node> > NodesMSP;  // key is name / alias
typedef std::map<NodeAddress, Node*> NAdrNodesMP;
typedef std::map<int, Node*> IntNodesMP;

}
//Q_DECLARE_METATYPE(pi_at_home::Node)
//Q_DECLARE_METATYPE(pi_at_home::Node*)
#endif // NODE__H
