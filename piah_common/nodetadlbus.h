#ifndef NODETADLBUS_H
#define NODETADLBUS_H

#include "piadh.h"
#include <cstdint>
#include <QDateTime>
#include <QThread>
#include <QTimerEvent>
#include <QMutex>
#include "node.h"
#include "device.h"
#include <limits>
#include <time.h>

/* *** Device TaDlBus
 * see standard device attributes
 * required attributes
 *	type="TA_DL_bus"	required
 *	port="WPx"			x = any valid port number in Wiring Pi notation
 *	read_periode="x"	x = approximate time between two read attempts in ms , default 10000ms
 * created nodes
 *	<prefix>timestamp	string	ch 0	UVR61-time of the last valid transaction
 *
 * Reads and decodes the data stream from TA UVR61-3. Each transmission takes about 1.3s and is sensitive to
 * non-realtime behavior. If you have every second a high prio task which keeps the cpu busy for millisendis,
 * you will never read a valid frame.
 * The device automatically creates a timestamp node in channel 0 called <prefix>timestamp that contains the
 * UVR61-time of the last valid transaction. The timestamp of the values is based on the raspi-clock and may
 * (and will) deviate from the UVR61-timestamp.
 *
 * *** Node TaDlBusSensorT
 * see standard node attributes
 * required attributes
 *  type="T"				required
 *	channel="x"			x = valid channel address 1<=x<=15
 *
 * *** Node TaDlBusRpm
 * required attributes
 *  type="rpm"			required
 *
 * TaDlBusRpm needs no channel id and will report as channel 17.
 *
 * Exapmple
	<device id="60" type="TA_DL_bus" port="WP21" prefix="UVR61-3" read_periode="10000">
	  <node channel="1" name="roof_collector_temp" type="T" log_holdoff_ms="0"/>
	  <node channel="2" name="sctrl_ref_temp" type="T" log_holdoff_ms="10000"/>
	  <node channel="3" name="reservoir_top_temp" type="T" />
	  <node channel="4" name="solar_pump_temp" type="T" />
	  <node channel="5" name="solar_return_temp" type="T" log_holdoff_ms="10000"/>
	  <node type="rpm" name="solar_pump_revs" />
	 </device>
 */
namespace pi_at_home
{
class DeviceTaDlBus;

class NodeTaDlBus
{
	friend class DeviceTaDlBus;
protected:
	virtual void updateValue()=0;
};

class NodeTaDlBusSensorT : public Node, public NodeTaDlBus
{
	Q_OBJECT
	friend class DeviceTaDlBus;
public:
	NodeTaDlBusSensorT(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	//virtual QString serializeValue(const NodeValue & value) const; QVariant::toString is fine
	virtual Node::Unit unit() const {return Unit::DEGREE_CELSIUS;}
	virtual NodeValue defaultValue() const {return NodeValue(25.);}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return false;}
	virtual Result init() {state_=State::BUSY; return Result::COMPLETED;}
	virtual const NodeValue & value() const {return value_;}
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	virtual void updateValue();
	double val_;
	qint64 lastTst=0;
};

class NodeTaDlBusRpm : public Node, public NodeTaDlBus
{
	Q_OBJECT
	friend class DeviceTaDlBus;
public:
	NodeTaDlBusRpm(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	//virtual QString serializeValue(const NodeValue & value) const; QVariant::toString is fine
	virtual Node::Unit unit() const {return Unit::PERCENT;}
	virtual NodeValue defaultValue() const {return NodeValue(0);}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return false;}
	virtual Result init() {state_=State::BUSY; return Result::COMPLETED;}
	virtual const NodeValue & value() const {return value_;}
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	virtual void updateValue();
	unsigned int val_=0;
	qint64 lastTst=0;
};

class NodeTaDlBusTst : public Node, public NodeTaDlBus
{
	Q_OBJECT
	friend class DeviceTaDlBus;
public:
	NodeTaDlBusTst(const QString & name, int device, QObject *parent);
	virtual const QString & type() const;
	virtual QString serializeValue(const NodeValue & value) const {return value.toDateTime().toString(Qt::ISODate);}
	virtual Node::Unit unit() const {return Unit::TIME;}
	virtual NodeValue defaultValue() const {return NodeValue(QDateTime());}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return false;}
	virtual Result init() {state_=State::BUSY; return Result::COMPLETED;}
	virtual const NodeValue & value() const {return value_;}
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	virtual void updateValue();
	QDateTime val_;
	qint64 lastTst=0;
};

// energy meter node could be implemented

class DeviceTaDlBusDecoder : public QObject
{
	Q_OBJECT
public:
	struct DoubleBuffer
	{
		DoubleBuffer();
		~DoubleBuffer();
		unsigned char * switchBuffers(); // returns new write buffer
		int currentId() const {return idCount;}
		void debugDumpWirteBuffer(int count);
		unsigned char * writeBuffer=nullptr, * readBuffer=nullptr;
		QMutex mtx;
	private:
		int idCount;
		unsigned char *bufferA, *bufferB;
	};
	explicit DeviceTaDlBusDecoder(int wiringPiPin, DoubleBuffer * buffer, int readDelay);
	~DeviceTaDlBusDecoder() {}
	virtual void timerEvent(QTimerEvent *event);
	void decoderControl(bool start);
protected slots:
	void startTimerSl();
	void stopTimerSl();
signals:
	void bufferReadySig(unsigned char * buffer);
	void normalStart();
	void normalStop();
	void emergencyStop();
private:
	int waitForEdge(int timeout);
	bool verifyBuffer();
	void estop();
	int wiringPiPin=-1, timerId=0, fd=-1, frameSize=62, readDelay_=10000;
	long sampleOffset=512295; // 512295ns for perfect timing, reduce to compensate system delays
	long tickUnit=1024590; // 488Hz half period nano seconds
	// 500000ns for perfect timing, reduce to compensate system delays
	bool shutdown=false;
	DoubleBuffer *buffer;
};

class DeviceTaDlBus : public Device
{
	Q_OBJECT
public:
	explicit DeviceTaDlBus(const QDomElement &elem);
	~DeviceTaDlBus();
	virtual Result init();
	virtual Result shutdown();
	qint64 currentTst() const {return decoderTst;}
	double readSensor(int channel, int & unitId); // channel=sensor number, starts at 1! return value is true result (unit bit padded) but not scaled
	unsigned int readRpm() const;
	QDateTime readTst();
protected slots:
	void bufferReadySl();
private:
	int basePortWp=-1;
	QThread worker;
	DeviceTaDlBusDecoder *decoder=nullptr;
	DeviceTaDlBusDecoder::DoubleBuffer *buffer;
	qint64 decoderTst=0;
};

}

#endif // NODETADLBUS_H
