#include "netcon.h"
#include <QFile>
#include <QDomDocument>
#include <QTimerEvent>
#include <QRandomGenerator>
#include "command.h"
#include "buildtst.h"
#include "piadi.h"

NetCon::NetCon(const QHostAddress & address, int port, bool testOnly, bool listNodes) :
	QObject(DCON.self()), testOnly(testOnly), listNodes(listNodes), isServer_(false), address_(address), port_(port)
{
	//qDebug()<<"NetCon::NetCon (client)";
}

NetCon::NetCon(QTcpSocket *socket) :
	QObject(DCON.self()), socket(socket), isServer_(true), port_(0)
{
	//qDebug()<<"NetCon::NetCon (server)";
	connect(socket, SIGNAL(connected()), this, SLOT(sockConnected()));
	connect(socket, SIGNAL(disconnected()), this, SLOT(sockDisconnected()));
	connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(sockDataOut(qint64)));
	connect(socket, SIGNAL(readyRead()), this, SLOT(sockDataIn()));
	connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(sockError(QAbstractSocket::SocketError)));
}

NetCon::~NetCon()
{
	qDebug()<<"NetCon: closing connection to"<<uid()<<address_.toString();
	setTimer(TimerChecks::NONE);
	if(socket)
	{
		disconnect(socket, nullptr, nullptr, nullptr);
		socket->close();
		if(isServer_)
		{
			delete socket; // not created by me, so probably not my child
			//socket->deleteLater(); maybe there is no event queue anymore
		}
	}
	delete hbCmd;
}

void NetCon::setHeartbeat(int intervallSec)
{
	if(hbTimerId)
		killTimer(hbTimerId);
	hbTimerId=0;
	if(isServer_)
	{
		qWarning()<<"NetCon::setHeartbeat call on server side of"<<address_.toString()<<"(ignored)";
		toHeartbeat=0;
		return;
	}
	toHeartbeat=intervallSec*1000; // timer values are always ms
	if(toHeartbeat<0)
		toHeartbeat=0;
	if(!toHeartbeat)
		return;
	int toffset=QRandomGenerator::system()->bounded(toCon_+1, toHeartbeat);
	hbTimerId=startTimer(toHeartbeat+toffset);
	qDebug()<<"NetCon::setHeartbeat started"<<toHeartbeat<<toffset<<address_.toString();
}

bool NetCon::send(const pi_at_home::Command &cmd)
{
	if(!socket || socket->state()!=QAbstractSocket::ConnectedState)
	{
		qWarning()<<"NetCon::send attempt without connected socket";
		return false;
	}
	if(timerCheck!=TimerChecks::NONE)
		qWarning()<<"NetCon::send while waiting, try to queue"<<(int)timerCheck;
	QByteArray msg=cmd.toSocketXml();
	socket->write(msg);
	if(cmd.hasPayload())
	{
		socket->write(*cmd.payload());
		socket->write(cmd.toSocketXmlEpilog());
		msg.append("--payload data removed--");
		msg.append(cmd.toSocketXmlEpilog());
	}
	emit rawDataSig(peerUid, msg, true, false);
	setTimer(TimerChecks::WRITTEN);
	return true;
}

void NetCon::start()
{
	//qDebug()<<"NetCon::start1";
	if(isServer_)
	{
		qWarning()<<"NetCon: explicit start on server side (ignored)";
		return;
	}
	if(socket)
	{
		disconnect(socket, nullptr, nullptr, nullptr);
		socket->deleteLater();
	}
	startup=true;
	socket = new QTcpSocket(this);
	connect(socket, SIGNAL(connected()), this, SLOT(sockConnected()));
	connect(socket, SIGNAL(disconnected()), this, SLOT(sockDisconnected()));
	connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(sockDataOut(qint64)));
	connect(socket, SIGNAL(readyRead()), this, SLOT(sockDataIn()));
	connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(sockError(QAbstractSocket::SocketError)));
	socket->connectToHost(address_, static_cast<quint16>(port_));
	setTimer(TimerChecks::CONNECTED);
}

void NetCon::stop()
{
	setTimer(TimerChecks::NONE);
	if(!socket)
		return;
	socket->close();
	socket=nullptr;
}

void NetCon::timerEvent(QTimerEvent *event)
{
	//qDebug()<<"NetCon timer"<<event->timerId()<<(qint64)this;
	if(event->timerId()==hbTimerId)
	{
		if(!socket)
			return; // strange
		if(socket->state()!=QAbstractSocket::ConnectedState)
		{
			qWarning()<<"NetCon::timerEvent heartbeat while not connected ignored"<<address_.toString();
			return;
		}
		if(hbCmd)
		{
			qCritical()<<"NetCon::timerEvent stale heartbeat command"<<hbCmd->getSluid()<<address_.toString();
			// ToDo try to reconnect
			hbCmd->deleteLater();
		}
		hbCmd=new Command("heartb");
		send(*hbCmd);
		qDebug()<<"NetCon::timerEvent heartbeat sent to"<<address_.toString();
		return;
	}
	killTimer(event->timerId());
	timerId=0;
	if(!socket)
		return;
	switch(timerCheck)
	{
	case TimerChecks::CONNECTED:
	{
		if(socket->state()!=QAbstractSocket::ConnectedState)
		{
			disconnect(socket, nullptr, nullptr, nullptr);
			socket->close();
			socket->deleteLater();
			socket=nullptr;
			lastErr_=1002;
			lastErrStr_=QString("NetCon: connection attempt timed out after %1 ms").arg(toCon_);
			qWarning()<<lastErrStr_;
			emit errorSig(lastErr_);
			emit readySig(false);
			if(!isServer_)
			{
				if(testOnly)
					qInfo()<<"test only - no reconnect attempt";
				else
				{
					if(toCon_<86400000)
						toCon_*=2; // will level off at 2^27 ~= 37h retry inteval
					qWarning()<<"NetCon: retry to connect to"<<address_.toString();
					start();
				}
			}
		}
		break;
	}
	case TimerChecks::WRITTEN:
	{
		lastErr_=1003;
		lastErrStr_=QString("write attempt timed out after %1 ms").arg(toWrite_);
		qWarning()<<lastErrStr_;
		emit errorSig(lastErr_);
		break;
	}
	case TimerChecks::RECONNECT:
	{
		qDebug()<<"NetCon: try to reconnect to"<<address_.toString();
		start();
		break;
	}
	case TimerChecks::NONE:
		break;
	}
}

void NetCon::sockConnected()
{
	if(isServer_)
	{
		qWarning()<<"LATE server side TCP socket connected";
		return;
	}
	//qDebug()<<"client side TCP socket connected";
	setTimer(TimerChecks::NONE);
	toCon_=5000; // reset
	if(!socket)
		return; // why should this happen?
	QByteArray d;
	if(testOnly)
	{
		Command cmd("contest");
		cmd.setAttribute("cv", VERSION_STR);
		if(listNodes)
			cmd.setAttribute("en", "true");
		d=cmd.toSocketXml();
	}
	else
	{
		Command cmd("connect");
		cmd.setAttribute("cv", VERSION_STR);
		d=cmd.toSocketXml();
	}
	//qDebug()<<"socket connected, establishing handshake";
	socket->write(d);
	emit rawDataSig(socket->peerName(), d, true, false);
	setTimer(TimerChecks::WRITTEN);
}

void NetCon::sockDisconnected()
{
	setTimer(TimerChecks::NONE);
	emit readySig(false);
	lastErr_=1001;
	lastErrStr_=QString("socket disconnecteed");
	// silent "error", no signal
	if(isServer_)
	{
		qInfo()<<"NetCon: server socket disconnected"<<address_.toString();
		// ConPool will delete NetCon
	}
	else if(testOnly)
	{
		qInfo()<<"NetCon: connection test socket closed"<<address_.toString();
	}
	else
	{
		qWarning()<<"NetCon: client socket disconnected - start timer for reconnect"<<address_.toString();
		setTimer(TimerChecks::RECONNECT);
	}
}

void NetCon::sockDataOut(qint64 cnt)
{
	Q_UNUSED(cnt);
	setTimer(TimerChecks::NONE);
	//qDebug()<<"data written";
	if(terminateAfterWrite)
	{
		socket->close();
	}
}

void NetCon::sockDataIn()
{
	if(!socket)
		return;
	pendingData.append(socket->readAll());
	//qDebug()<<"sockDataIn from"<<socket->peerAddress().toString()<<socket->peerPort()<<"length"<<pendingData.size()<<pendingData.left(25)<<(pendingData.size()>25?"...":"");
	if(startup && isServer_)
	{// GET /cmdtext HTTP/1.1\n...
		QString curtime=QDateTime::currentDateTime().toString(Qt::ISODate);
		if(pendingData.size()>4096)
		{
			qCritical()<<"socket data for startup > 4096: invalid";
			socket->close();
			return;
		}
		QStringList parts=QString::fromUtf8(pendingData).split(' ');
		if(parts.size()>2 && parts.at(0)=="GET" && parts.at(2).startsWith("HTTP/1."))
		{
			QString cmd=parts.at(1).mid(1);
			qInfo()<<"HTTP request"<<cmd;
			QString respHeader, content;
			if(cmd=="favicon.ico")
			{
				QFile fi("logo.ico");
				if(fi.open(QIODevice::ReadOnly))
				{
					respHeader="HTTP/1.1 200 OK\r\nDate: %1\r\nServer: Pi_at_Home(RaspberryPi)\r\nContent-Length: 4286\r\nContent-Type: image/ico\r\nConnection: Closed\r\n\r\n";
					content=QString("<html><body>not found</body></html>");
					QByteArray rc=fi.readAll();
					QByteArray rh=respHeader.arg(curtime).toUtf8();
					rh.append(rc);
					startup=false;
					terminateAfterWrite=true;
					socket->write(rh);
					return;
				}
				respHeader="HTTP/1.1 404 Not Found\r\nDate: %1\r\nServer: Pi_at_Home(RaspberryPi)\r\nContent-Length: %2\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n";
				content=QString("<html><body>not found</body></html>");
			}
			else if(!cmd.startsWith("cmad"))
			{
				qCritical()<<"HTTP command != cmad"<<cmd;
				socket->close();
				return;
			}
			else
			{ // expecting something like http://10.0.1.22/cmad?cmd=noderq&device=10&channel=1
				QString query=cmd.mid(cmd.indexOf("?")+1);
				QStringList cfrags=query.split("&");
				QString fcmd;
				int idev=-1, ichan=-1;
				bool devOk=false, chanOk=false;
				QStringList pname, pval;
				foreach(QString frag, cfrags)
				{
					if(frag.startsWith("cmd"))
						fcmd=frag.mid(frag.indexOf("=")+1);
					else if(frag.startsWith("device"))
						idev=frag.mid(frag.indexOf("=")+1).toInt(&devOk);
					else if(frag.startsWith("channel"))
						ichan=frag.mid(frag.indexOf("=")+1).toInt(&chanOk);
					else
					{
						int trenn=frag.indexOf("=");
						pname<<frag.left(trenn);
						pval<<frag.mid(trenn+1);
					}
				}
				if(fcmd.isEmpty() || !devOk || idev<0 || !chanOk || ichan<0)
				{
					qCritical()<<"HTTP no command"<<cmd;
					socket->close();
					return;
				}
				QPointer<Command> command=new Command(fcmd);
				command->setDestination(NodeAddress(DCON.uid(), idev, ichan));
				command->setAttribute("HTTPsrc", "true");
				qDebug()<<"HTTP dispatch"<<fcmd<<"to"<<idev<<ichan;
				qDebug()<<"  params"<<pname<<pval;
				for(int i=0; i<pname.size(); ++i)
					command->setAttribute(pname.at(i), pval.at(i));
				QString xmlResp="none";
				try
				{
					DCON.dispatchCommand(*command);
					if(!command.isNull())
						xmlResp=command->paramsToXml();
				}
				catch(Ex & ex)
				{
					qCritical()<<"exception during HTTP cmd"<<ex.getMessage()<<"with cmd"<<cmd;
					xmlResp="<p>exception during HTTP cmd</p>"+ex.getMessage();
				}
				catch(...)
				{
					qCritical()<<"exception during HTTP cmd with cmd"<<cmd;
					xmlResp="<p>exception during HTTP cmd</p>";
				}
				qDebug()<<"  result"<<xmlResp;
				if(!command.isNull())
					command->deleteLater();
				respHeader="HTTP/1.1 200 OK\r\nDate: %1\r\nServer: Pi_at_Home(RaspberryPi)\r\nLast-Modified: %1\r\n\
Content-Length: %2\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n";
				content=QString("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\"><html><head><title>Pi@Home</title>\
</head><body><h1>Pi@Home</h1><p>%1 (%5) is up and running!</p><p>Local Time: %2</p><p>Command: %3</p><p>Response: %4</p></body></html>").arg(DCON.uid(), curtime, fcmd, xmlResp, VERSION_STR);
			}
			QByteArray rc=content.toUtf8();
			QByteArray rh=respHeader.arg(curtime).arg(rc.size()).toUtf8();
			rh.append(rc);
			startup=false;
			terminateAfterWrite=true;
			socket->write(rh);
			return;
		}
		// not an HTTP GET, check for rich client
	}
	int index=pendingData.indexOf(QByteArray("</piahmsg>"));
	while(index>0)
	{
		QByteArray body=pendingData.mid(0, index+10);
		pendingData=pendingData.right(pendingData.size()-index-10);
		parseData(body);
		index=pendingData.indexOf(QByteArray("</piahmsg>"));
	}
}

void NetCon::sockError(QAbstractSocket::SocketError err)
{
	lastErr_=(int)err+100;
	lastErrStr_=socket->errorString();
	if(err==QAbstractSocket::RemoteHostClosedError)
		qInfo()<<"NetCon: "<<lastErrStr_; // this is standard bahavior
	else
	{
		qCritical()<<"NetCon socket error"<<lastErrStr_<<address_;
		emit errorSig(lastErr_);
		if(testOnly)
		{
			qInfo()<<"test only - shutting down due to error";
			stop();
		}
	}
}

void NetCon::parseData(QByteArray &body)
{
	//qDebug()<<"parseData"<<body;
	QDomDocument doc;
	QString errStr;
	int errLine=0;
	if (!doc.setContent(body, &errStr, &errLine))
	{
		qWarning()<<"socket data invalid in line "<<errLine<<" <"<<errStr<<">";
		qDebug()<<"with base data "<<body;
		//socket->close();
		emit rawDataSig(peerUid, body, false, true);
		return;
	}
	Command * cmd=nullptr;
	try
	{
		cmd=new Command(doc.documentElement());
	}
	catch(Ex & ex)
	{
		qCritical()<<"invalid XML command: "<<ex.getMessage();
		qCritical()<<"with base data "<<body;
		return;
	}
	if(startup)
	{
		startup=false;
		if(isServer_)
		{ // running at server side
			peerVersion=cmd->attribute("cv");
			peerUid=cmd->source().uid();
			address_=socket->peerAddress();
			if(cmd->cmd()=="connect")
			{
				qInfo()<<"socket connecting to client"<<address_<<peerUid<<peerVersion;
				cmd->setAttribute("sv", VERSION_STR);
				cmd->setAttribute("sb", buildTimestamp());
				cmd->setAttribute("sc", DCON.getConfigFlavorStr());
				cmd->setAttribute("ct", DCON.getConfigTst().toString(Qt::ISODate));
				cmd->setAttribute("dst", NodeAddress().toString());
				cmd->createDluid();
				DCON.addNodeFragments(*cmd);
				//qDebug()<<"NetCon::STARTUP connect dluid"<<cmd->getDluid();
				//qDebug()<<"NetCon::STARTUP connect message"<<cmd->toSocketXml();
				socket->write(cmd->toSocketXml());
				emit readySig(true);
			}
			else if(cmd->cmd()=="contest")
			{
				qInfo()<<"confirming connection test"<<address_<<peerUid<<peerVersion;
				cmd->setAttribute("sv", VERSION_STR);
				cmd->setAttribute("sb", buildTimestamp());
				cmd->setAttribute("sc", DCON.getConfigFlavorStr());
				cmd->setAttribute("ct", DCON.getConfigTst().toString(Qt::ISODate));
				cmd->setAttribute("dst", NodeAddress().toString());
				cmd->createDluid();
				if(cmd->attribute(QStringLiteral("en"))==QStringLiteral("true"))
					DCON.addNodeFragments(*cmd);
				//qDebug()<<"NetCon::STARTUP contest dluid"<<cmd->getDluid();
				//qDebug()<<"NetCon::STARTUP contest message"<<cmd->toSocketXml();
				socket->write(cmd->toSocketXml());
				terminateAfterWrite=true;
			}
			else
			{
				qCritical()<<"socket cmd invalid for startup"<<cmd->cmd();
				socket->close();
			}
			cmd->deleteLater();
			return;
		}
		else
		{ // running at client side
			peerVersion=cmd->attribute("sv");
			peerBuild=cmd->attribute("sb");
			peerConfigFlavor=cmd->attribute("sc");
			peerConfigTst=cmd->attribute("ct");
			peerUid=cmd->destination().uid();
			if((cmd->cmd()=="connect" || cmd->cmd()=="contest") && !peerVersion.isEmpty() && !peerUid.isEmpty())
			{
				QString adrStr=address_.toString(), portStr=QString::number(port_);
				qInfo()<<"NetCon: socket connected to host "<<adrStr<<portStr<<peerUid<<" running "<<peerVersion<<peerBuild;
				emit readySig(true);
				// allow command dispatch - maybe someone has registered
			}
			else
			{
				emit rawDataSig(socket->peerName(), body, false, true); // it is not renamed!
				qCritical()<<"NetCon: response invalid for startup";
				socket->close();
				cmd->deleteLater();
				return;
			}
		}
	}
	emit rawDataSig(peerUid, body, false, false);
	if(cmd->cmd()=="heartb")
	{
		if(isServer_)
		{ // running at server side
			if(cmd->isReply())
				qCritical()<<"NetCon::parseData heartbeat reply at server -> invaliud"<<address_.toString();
			else
			{ // do reply
				cmd->setAttribute("dst", NodeAddress().toString());
				cmd->createDluid();
				socket->write(cmd->toSocketXml());
			}
		}
		else
		{ // client - we expect a reply
			if(!hbCmd || !cmd->isReply())
			{
				qCritical()<<"NetCon::parseData"<<(hbCmd?"":"no hbCmd")<<(cmd->isReply()?"reply":"NOT a reply")<<cmd->toString()<<address_.toString();
			}
			else
			{
				if(hbCmd->getSluid()==cmd->getSluid())
				{
					qDebug()<<"NetCon::parseData heartbeat OK for"<<address_.toString();
					hbCmd->deleteLater();
					hbCmd=nullptr;
				}
				else
					qCritical()<<"NetCon::parseData SLUID mismatch"<<hbCmd->getSluid()<<cmd->getSluid()<<address_.toString();
			}
		}
		cmd->deleteLater();
		return; // no further processing
	}
	DCON.dispatchCommand(*cmd);
}

void NetCon::setTimer(NetCon::TimerChecks tc)
{
	if(timerId)
		killTimer(timerId);
	timerCheck=tc;
	switch(tc)
	{
		case TimerChecks::CONNECTED:
		{
			timerId=startTimer(toCon_);
			break;
		}
		case TimerChecks::RECONNECT:
		{
			timerId=startTimer(toRecon_);
			break;
		}
		case TimerChecks::WRITTEN:
		{
			timerId=startTimer(toWrite_);
			break;
		}
		case TimerChecks::NONE:
			timerId=0;
			break;
	}
}

