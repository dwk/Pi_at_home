#include "algcompare.h"
#include "algcompare_fac.h"
#include "node.h"
#include "command.h"
#include "nodealg.h"
#include "piadi.h"

AlgCompare::AlgCompare(const QDomElement &elem) :
	Algorithm(elem)
{
	ndIRes=new NodeInt(namePrefix+QStringLiteral("ires"), id_, 1, this);
	ndIRes->setValue(NodeValue(-1));
	ndError=new NodeBool(namePrefix+QStringLiteral("error"), id_, 1000, this);
	ndError->setValue(NodeValue(false));
	QString opnm=elem.attribute("op");
	if(opnm=="lt")
	{
		op=BaseOp::LT;
	}
	else if(opnm=="le")
	{
		op=BaseOp::LE;
	}
	else if(opnm=="eq")
	{
		op=BaseOp::EQ;
	}
	else if(opnm=="ge")
	{
		op=BaseOp::GE;
	}
	else if(opnm=="gt")
	{
		op=BaseOp::GT;
	}
	else if(opnm=="ioo")
	{
		op=BaseOp::IOO;
	}
	else if(opnm=="icc")
	{
		op=BaseOp::ICC;
	}
	else if(opnm=="ioc")
	{
		op=BaseOp::IOC;
	}
	else if(opnm=="ico")
	{
		op=BaseOp::ICO;
	}
	if(op==BaseOp::UNDEF)
		qCritical()<<objectName()<<": undefined operation"<<opnm;
	QDomNodeList nodes=elem.elementsByTagName("node");
	bool dtSet=false;
	IntNodesMP lnds;
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		QString tn=e.attribute(QStringLiteral("type_name"));
		if(tn==QStringLiteral("input(dbl)"))
		{
			if( (dtSet && useInt==true) || !ndInNm.isEmpty() )
			{
				qCritical()<<objectName()<<": mixed or multiple input(dbl)"<<tn<<dtSet<<useInt;
				return;
			}
			ndInNm=e.attribute(QStringLiteral("name"));
		}
		else if(tn==QStringLiteral("input(int)"))
		{
			if( (dtSet && useInt==false) || !ndInNm.isEmpty() )
			{
				qCritical()<<objectName()<<": mixed or multiple input(int)"<<tn<<dtSet<<useInt;
				return;
			}
			ndInNm=e.attribute(QStringLiteral("name"));
			useInt=true;
		}
		else if(tn==QStringLiteral("level(dbl)"))
		{
			if(dtSet && useInt==true)
			{
				qCritical()<<objectName()<<": mixed level(dbl)"<<tn<<dtSet<<useInt;
				return;
			}
			Node * nd=new NodeDbl(e, id_, this);
			int ch=nd->address().channel();
			lnds.insert(IntNodesMP::value_type(ch, nd));
		}
		else if(tn==QStringLiteral("level(int)"))
		{
			if(dtSet && useInt==false)
			{
				qCritical()<<objectName()<<": mixed level(int)"<<tn<<dtSet<<useInt;
				return;
			}
			Node * nd=new NodeInt(e, id_, this);
			int ch=nd->address().channel();
			lnds.insert(IntNodesMP::value_type(ch, nd));
			useInt=true;
		}
	}
	// make sure vector is ordered in channel
	for(auto nit=lnds.begin(); nit!=lnds.end(); ++nit)
		ndLevels.push_back(nit->second);
	if(!ndLevels.size() || ndInNm.isEmpty())
	{
		qCritical()<<objectName()<<": no compare levels or no input"<<ndLevels.size()<<ndInNm;
		return;
	}
	lcnt=(int)ndLevels.size();
	if(useInt)
		levelI=new int[lcnt];
	else
		levelD=new double[lcnt];
	QString otempl=namePrefix+"out%1";
	for(int i=0; i<lcnt; ++i)
	{
		Node * nd=new NodeBool(otempl.arg(i), id_, 200+i, this);
		ndOuts.push_back(nd);
		tokens.push_back(nd->lock(DCON.uid(), id_, objectName()));
	}
	if(op==BaseOp::IOO || op==BaseOp::ICC || op==BaseOp::IOC || op==BaseOp::ICO)
	{
		ress=new bool[lcnt+1];
		Node * nd=new NodeBool(otempl.arg(lcnt), id_, 200+lcnt, this);
		ndOuts.push_back(nd);
		tokens.push_back(nd->lock(DCON.uid(), id_, objectName()));
	}
	else
		ress=new bool[lcnt];
	qInfo()<<objectName()<<lcnt<<(useInt?"integer":"floating point")<<"levels with operation"<<opnm;
}

AlgCompare::~AlgCompare()
{
	if(levelI)
		delete [] levelI;
	if(levelD)
		delete [] levelD;
	if(ress)
		delete [] ress;
}

void AlgCompare::execute()
{
	if(algState!=AlgState::RUNNING && algState!=AlgState::STARTUP)
		return;
	//qDebug()<<objectName()<<"exe"<<(useInt?inValI:0)<<(useInt?0.:inValD);
	bool targetVal=true;
	if(op==BaseOp::GE || op==BaseOp::GT)
		targetVal=false;
	int resI=-1;
	for(int i=0; i<lcnt; ++i)
	{
		switch(op)
		{ // LT, LE, EQ, GE, GT, IOO, ICC, IOC, ICO
			case BaseOp::LT:
				if(useInt)
					ress[i]=(inValI<levelI[i]);
				else
					ress[i]=(inValD<levelD[i]);
				break;
			case BaseOp::LE:
				if(useInt)
					ress[i]=(inValI<=levelI[i]);
				else
					ress[i]=(inValD<=levelD[i]);
				break;
			case BaseOp::EQ:
				if(useInt)
					ress[i]=(inValI==levelI[i]);
				else
					ress[i]=(inValD==levelD[i]);
				break;
			case BaseOp::GE:
				if(useInt)
					ress[i]=(inValI>=levelI[i]);
				else
					ress[i]=(inValD>=levelD[i]);
				break;
			case BaseOp::GT:
				if(useInt)
					ress[i]=(inValI>levelI[i]);
				else
					ress[i]=(inValD>levelD[i]);
				break;
			case BaseOp::IOO:
				if(useInt)
					ress[i]=((i?(inValI>levelI[i-1]):true) && inValI<levelI[i]);
				else
					ress[i]=((i?(inValD>levelD[i-1]):true) && inValD<levelD[i]);
				break;
			case BaseOp::ICC:
				if(useInt)
					ress[i]=((i?(inValI>=levelI[i-1]):true) && inValI<=levelI[i]);
				else
					ress[i]=((i?(inValD>=levelD[i-1]):true) && inValD<=levelD[i]);
				break;
			case BaseOp::IOC:
				if(useInt)
					ress[i]=((i?(inValI>levelI[i-1]):true) && inValI<=levelI[i]);
				else
					ress[i]=((i?(inValD>levelD[i-1]):true) && inValD<=levelD[i]);
				break;
			case BaseOp::ICO:
				if(useInt)
					ress[i]=((i?(inValI>=levelI[i-1]):true) && inValI<levelI[i]);
				else
					ress[i]=((i?(inValD>=levelD[i-1]):true) && inValD<levelD[i]);
				break;
			case BaseOp::UNDEF:
				return;
		}
		if(resI<0 && ress[i]==targetVal)
			resI=i;
		//qDebug()<<objectName()<<"exe"<<i<<ress[i]<<(useInt?levelI[i]:0)<<(useInt?0.:levelD[i]);
	}
	for(int i=0; i<lcnt; ++i)
	{
		ndOuts.at(i)->setValue(NodeValue(ress[i]), tokens.at(i));
	}
	if(op==BaseOp::IOO || op==BaseOp::ICC || op==BaseOp::IOC || op==BaseOp::ICO)
	{
		switch(op)
		{ // IOO, ICC, IOC, ICO
			case BaseOp::IOO:
			case BaseOp::IOC:
				if(useInt)
					ress[lcnt]=(inValI>levelI[lcnt-1]);
				else
					ress[lcnt]=(inValD>levelD[lcnt-1]);
				break;
			case BaseOp::ICC:
			case BaseOp::ICO:
				if(useInt)
					ress[lcnt]=(inValI>=levelI[lcnt-1]);
				else
					ress[lcnt]=(inValD>=levelD[lcnt-1]);
				break;
			default:
				break;
		}
		if(resI<0 && ress[lcnt]==targetVal)
			resI=lcnt;
		ndOuts.at(lcnt)->setValue(NodeValue(ress[lcnt]), tokens.at(lcnt));
		//qDebug()<<lcnt<<"-"<<ress[lcnt];
	}
	ndIRes->setValue(NodeValue(resI));
}

pi_at_home::Result AlgCompare::preExec()
{
	if(algState!=AlgState::STARTUP || op==BaseOp::UNDEF || !lcnt)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	Node *ndIn=DCON.findNode(ndInNm, false);
	if(!ndIn)
	{
		qCritical()<<objectName()<<"input not found"<<ndInNm;
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	if(useInt)
		inValI=ndIn->value().toInt();
	else
		inValD=ndIn->value().toDouble();
	for(int i=0; i<lcnt; ++i)
	{
		if(useInt)
			levelI[i]=ndLevels.at(i)->value().toInt();
		else
			levelD[i]=ndLevels.at(i)->value().toDouble();
		connect(ndLevels.at(i).data(), &Node::newValSig, [this, i](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, i);});
	}
	levelcheck();
	execute();
	connect(ndIn, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, -1);});
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

pi_at_home::Result AlgCompare::postExec()
{
	for(size_t i=0; i<ndOuts.size(); ++i)
	{
		ndOuts.at(i)->setValue(ndOuts.at(i)->defaultValue(), tokens.at(i));
		ndOuts.at(i)->unlock(tokens.at(i));
	}
	return Result::COMPLETED;
}

void AlgCompare::valCaptureSl(const NodeValue &newValue, qint64 timestamp, int inChannel)
{
	Q_UNUSED(timestamp)
	//qDebug()<<"valCaptureSl"<<inChannel<<newValue;
	if(inChannel<0)
	{
		if(useInt)
			inValI=newValue.toInt();
		else
			inValD=newValue.toDouble();
		execute();
		return;
	}
	// inChannel-=100; no, lambda already based on vector index instead of channel
	if(inChannel>=lcnt)
		return;
	if(useInt)
		levelI[inChannel]=newValue.toInt();
	else
		levelD[inChannel]=newValue.toDouble();
	levelcheck();
	execute();
}

void AlgCompare::levelcheck()
{
	if(!lcnt)
		return;
	bool anyFail=false;
	if(useInt)
	{
		int v=levelI[0];
		for(int i=1; i<lcnt; ++i)
		{
			if(levelI[i]<=v)
			{
				qWarning()<<objectName()<<"level not in strictly monotonous ascending order"<<i<<v<<levelI[i];
				anyFail=true;
			}
			v=levelI[i];
		}
	}
	else
	{
		double v=levelD[0];
		for(int i=1; i<lcnt; ++i)
		{
			if(levelD[i]<=v)
			{
				qWarning()<<objectName()<<"level not in strictly monotonous ascending order"<<i<<v<<levelD[i];
				anyFail=true;
			}
			v=levelD[i];
		}
	}
	ndError->setValue(NodeValue(anyFail));
}
