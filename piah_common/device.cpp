#include "device.h"
#include "piadi.h"


Device::Device(const QDomElement &elem)
{
	QString idStr=elem.attribute("id"), typeStr=elem.attribute("type");
	bool ok=false;
	id_=idStr.toInt(&ok);
	setObjectName(idStr+"-"+typeStr);
	if(elem.hasAttribute("prefix"))
		namePrefix=elem.attribute("prefix");
	if(!ok || typeStr.isEmpty())
		qCritical()<<"no Device id/type";
	else
		qInfo()<<"Creating (XML) device"<<typeStr<<id_;
	if(elem.hasAttribute("poll_mult"))
	{
		int i=elem.attribute("poll_mult").toInt(&ok);
		if(ok && i>=-1)
			pollMult=i;
		else
			qCritical()<<"poll_mult not a valid integer"<<elem.attribute("poll_mult")<<objectName();
	}
}

Device::Device(int id, QObject *parent) :
	QObject(parent), id_(id)
{
	qInfo()<<"Creating (expl) device"<<id_;
}

Result Device::init()
{
	Result resfin=Result::COMPLETED;
	QList<Node * > chldr=findChildren<Node * >();
	foreach(Node * n, chldr)
	{
		Result res=n->init();
		if(res==Result::COMPLETED || res==Result::PENDING)
		{
			DCON.registerNode(n);
		}
		else
			resfin=Result::FAILED;
	}
	return resfin;
}

void Device::pollLoop()
{
	//qDebug()<<"pollLoop on"<<objectName();
	QList<Node * > chldr=findChildren<Node * >();
	foreach(Node * n, chldr)
	{
		//qDebug()<<"pollLoop ping"<<n->objectName();
		n->ping(); // trigger newValSig (broadcasts...)
	}
}

DeviceFactory::DeviceCreatorMP DeviceFactory::deviceCreatorMP;

bool DeviceFactory::registerDevice(const QString &typeId, DeviceFactory::DeviceCreator creator)
{
	auto res=deviceCreatorMP.insert(DeviceCreatorMP::value_type(typeId, creator));
	qInfo()<<"registring"<<typeId<<res.second;
	return res.second;
}

Device *DeviceFactory::produce(const QDomElement &elem)
{
	QString type=elem.attribute("type");
	DeviceCreatorMP::iterator it=deviceCreatorMP.find(type);
	if(it==deviceCreatorMP.end())
	{
		qCritical()<<"unknown device type"<<type;
		return nullptr;
	}
	return (it->second)(elem);
}
