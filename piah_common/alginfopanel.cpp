#include "alginfopanel.h"
#include "alginfopanel_fac.h"
#include "node.h"
#include "command.h"
#include "piadi.h"


LineProcessor::LineProcessor(const QDomElement &elem, QObject *parent) :
	QObject(parent)
{
	//<node name=":Pi3Kueche:15:0" channel="0" type_name="lineValue"/>
	bool ok=false;
	channel=elem.attribute("channel").toInt(&ok);
	if(!ok || channel<0)
	{
		qCritical()<<"invalid channel"<<elem.attribute("channel");
		return;
	}
	inputNm=elem.attribute("name");
}

LineProcessor::LineProcessor(LineProcessor::DataSource ds, QObject *parent) :
	QObject(parent), src(ds), channel(200-64)
{
}

bool LineProcessor::addOutNode(const QDomElement &elem, Node *outNode)
{
	if(!outNode)
		return false;
	outputNode=outNode;
	if(src==DataSource::DATETIME)
		return true;
	if(getOutChannel()!=outNode->address().channel())
		return false;
	QString label=elem.attribute("name");
	QString unit=elem.attribute("unit");
	QString stylenm=elem.attribute("in_format");
	if(stylenm=="dbl0dec")
		style=FormatStyle::DOUBLE0;
	else if(stylenm=="dbl1dec")
		style=FormatStyle::DOUBLE1;
	else if (stylenm=="dbl1decP")
		style=FormatStyle::DOUBLE1P;
	else if (stylenm=="boolList")
	{
		style=FormatStyle::BOOLLIST;
		translations=unit.split("|");
		if(translations.size()!=2)
		{
			qWarning()<<"AlgInfoPanel in_format boolList invaild 'unit', need two options"<<unit;
			translations.clear();
			translations<<"0"<<"1";
		}
	}
	else if (stylenm=="intList")
	{
		style=FormatStyle::INTLIST;
		translations=unit.split("|");
		if(translations.size()<2)
		{
			qWarning()<<"AlgInfoPanel in_format intList 'unit' seems to make no sense"<<unit;
			translations.clear();
		}
	}
	if(inputNm.isEmpty())
		src=DataSource::NONE;
	else
		src=DataSource::NODEVALUE;
	templ=elem.attribute("line_format");
	int idx=templ.indexOf(QStringLiteral("$NAME$"));
	if(idx>=0)
		templ.replace(idx, 6, label);
	idx=templ.indexOf(QStringLiteral("$UNIT$"));
	if(idx>=0)
		templ.replace(idx, 6, unit);
	idx=templ.indexOf(QStringLiteral("$VAL$"));
	if(idx>=0)
		templ.replace(idx, 5, QStringLiteral("%2"));
	else
		style=FormatStyle::NOVAL;
	idx=templ.indexOf(QStringLiteral("$LINE"));
	if(idx>=0)
	{
		int i2=idx+5;
		for(int i2m=templ.size(); i2<i2m && templ.at(i2).category()==QChar::Number_DecimalDigit; ++i2)
			;
		bool ok=false;
		int numlen=i2-(idx+5);
		lineLength=templ.mid(idx+5, numlen).toInt(&ok);
		if(!ok)
		{
			qWarning()<<"AlgInfoPanel invalid line length in $LINEnc$ default to 14"<<templ;
			lineLength=14;
		}
		if(i2+1>=templ.size()) // fill char and second $
			qWarning()<<"AlgInfoPanel terminated line length in $LINEnc$ "<<templ;
		else
			fillChar=templ.at(i2);
		templ.replace(idx, 7+numlen, QStringLiteral("%1"));
	}
	else
		lineLength=-1;
	return true;
}

Result LineProcessor::init()
{
	NodeValue v;
	switch(src)
	{
	case DataSource::NODEVALUE:
	{
		Node * inNd=QPointer<Node>(DCON.findNode(inputNm, false));
		if(inNd)
		{
			connect(inNd, &Node::newValSig, this, &LineProcessor::updateSl);
			if(inNd->state()==Node::State::READY)
				v=inNd->value();
			else
				v=NodeValue("?");
		}
		else
			return Result::FAILED;
		break;
	}
	case DataSource::DATETIME:
		startTimer(1000);
		break;
	default:
		break;
	}
	updateSl(v, 0);
	qDebug()<<"LineProcessor::init"<<channel<<inputNm<<templ<<v.toString();
	return Result::COMPLETED;
}

void LineProcessor::updateSl(const NodeValue & newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	if(!outputNode)
		return;
	switch(src)
	{
		case DataSource::NONE:
			outputNode->setValue(NodeValue(templ));
			break;
		case DataSource::NODEVALUE:
		{
			QString formated;
			switch(style)
			{
			case FormatStyle::NOVAL:
				break;
			case FormatStyle::STRING:
				if(newValue.isValid())
					formated=newValue.toString();
				else
					formated=QStringLiteral("?");
				break;
			case FormatStyle::DOUBLE0:
			case FormatStyle::DOUBLE1:
			{
				bool ok=false;
				double d=newValue.toDouble(&ok);
				if(!ok)
					formated="NaN";
				formated=QString::number(d, 'f', style==FormatStyle::DOUBLE0?0:1);
				break;
			}
			case FormatStyle::DOUBLE1P:
			{
				bool ok=false;
				double d=newValue.toDouble(&ok);
				if(!ok)
					formated="NaN";
				formated=(d>0.?QString("+"):QString())+QString::number(d, 'f', 1);
				break;
			}
			case FormatStyle::BOOLLIST:
			{
				if(newValue.isValid())
				{
					bool d=newValue.toBool();
					formated=(d?translations.at(1):translations.at(0));
				}
				else
					formated=QStringLiteral("?");
				break;
			}
			case FormatStyle::INTLIST:
			{
				bool ok=false;
				int d=newValue.toInt(&ok);
				if(ok)
				{
					if(d>=0 && d<translations.size())
						formated=translations.at(d);
					else
						formated=QString::number(d);
				}
				else
					formated="?";
				break;
			}
			}
			if(lineLength>0)
			{
				int padlen=lineLength-(templ.size()-2); // corrected for %1
				if(style==FormatStyle::NOVAL)
					outputNode->setValue(NodeValue(templ.arg(QStringLiteral(""), padlen, fillChar)));
				else
				{
					padlen-=formated.size()-2; // corrected for %2
					outputNode->setValue(NodeValue(templ.arg(QStringLiteral(""), padlen, fillChar).arg(formated)));
				}
			}
			else
			{
				if(style==FormatStyle::NOVAL)
					outputNode->setValue(NodeValue(templ)); // strange - should be DataSource::NONE
				else
					outputNode->setValue(NodeValue(templ.arg(formated)));
			}
			break;
		}
		case DataSource::DATETIME:
			outputNode->setValue(NodeValue(QDateTime::currentDateTime().toString("dd-MM HH:mm:ss")));
			break;
	}
}

void LineProcessor::forceUpdate()
{
	NodeValue nv;
	if(src==DataSource::NODEVALUE)
	{
		Node * inNd=DCON.findNode(inputNm, false);
		if(!inNd)
			return;
		if(inNd->state()==Node::State::READY)
			nv=inNd->value();
	}
	updateSl(nv, 0);
}

void LineProcessor::timerEvent(QTimerEvent *event)
{
	Q_UNUSED(event);
	NodeValue nv;
	updateSl(nv, 0);
}


AlgInfoPanel::AlgInfoPanel(const QDomElement &elem) :
	Algorithm(elem)
{
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		if(e.attribute(QStringLiteral("type_name"))==QStringLiteral("lineValue"))
		{
			LineProcessor *lp=new LineProcessor(e, this);
			lines.insert(LineProcessorMISP::value_type(lp->getOutChannel(), QPointer<LineProcessor>(lp)));
		}
	}
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		if(e.attribute(QStringLiteral("type_name"))==QStringLiteral("lineText"))
		{
			NodeStr *nd=new NodeStr(e, id_, this);
			auto lit=lines.find(nd->address().channel());
			if(lit==lines.end())
				qCritical()<<objectName()<<"lineText has no matching lineValue"<<nd->address().toString();
			else
				lit->second->addOutNode(e, nd);
		}
		else if(e.attribute(QStringLiteral("type_name"))==QStringLiteral("datetime"))
		{
			NodeStr *nd=new NodeStr(e, id_, this);
			LineProcessor *lp=new LineProcessor(LineProcessor::DataSource::DATETIME, this);
			lp->addOutNode(e, nd);
			lines.insert(LineProcessorMISP::value_type(lp->getOutChannel(), QPointer<LineProcessor>(lp)));
		}
	}
}

void AlgInfoPanel::execute()
{
	if(algState!=AlgState::RUNNING && algState!=AlgState::STARTUP)
		return;
	for(auto lit=lines.begin(); lit!=lines.end(); ++lit)
		lit->second->forceUpdate();
}

pi_at_home::Result AlgInfoPanel::preExec()
{
	if(algState!=AlgState::STARTUP)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	for(auto lit=lines.begin(); lit!=lines.end(); ++lit)
	{
		Result res=lit->second->init();
		if(res!=Result::COMPLETED && res!=Result::PENDING)
			return Result::FAILED;
	}
	execute();
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

pi_at_home::Result AlgInfoPanel::postExec()
{
	return Result::COMPLETED;
}

