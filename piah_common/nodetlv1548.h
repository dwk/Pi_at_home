#ifndef NODETLV1548_H
#define NODETLV1548_H

#include "piadh.h"
#include <limits>
#include "node.h"
#include "device.h"
#include <QThread>
#include <QMutex>
#include <QDoubleSpinBox>

/* *** Device TLV1548
 * see standard device attributes
 * required attributes
 *	type="TLV1548"
 *	SPI="x"		x = 0|1
 * optional attributes
 *	interval="x"	x = delay between conversions in ms; default is 1200
 *	u_ref="x"		x = external referece voltage [V] Used for scaled output; default=5V
 *	ratio_min="x"	x = 0-result voltage = u_ref*ratio_min
 *	ratio_max="x"	x = 1023-result voltage = u_ref*ratio_max
 *	avg="n"			n = positive int, rolling average over n ADC-samples (averaging is always applied to raw 10bit values before processing); default=1
 *
 * Uses the TI TLV1548 8 channel 10bit AD converter.
 * Although technical optional ratio_min and ratio_max must be set to reasonable values for any type of node.
 * Every <interval>ms one node will be read, nodes in [<min_config_channel> , <max_config_channel>] will be
 * converted, even if some channels are not named in the config.
 * Update interval for one channel: <interval>*(<max_config_channel>-<min_config_channel>+1) milliseconds
 *
 * *** Node TLV1548
 * see standard node attributes
 * required attributes
 *	channel="x"			x = valid channel address (0<=x<=7), unique for its device; note that
 *						channels 3-7 may not be connected to anything
 * optional attributes
 *	type="x"			x = {PT1000|scaled} if omitted you get raw, unscaled 10 bit and Unit::COUNT
 *	gain="x"			x = gain factor of pre-amp, default is 2.5
 *	r_ref="x"			x = reference resistor [Ohm] default is 4300
 *	precision="x"		x = double; unit of measure to round to; default = 0.01, 0. turns off rounding
 *
 * Exapmple
	<device id="50" type="TLV1548" port="SPI0" interval="1100" u_ref="4.74" ratio_min="0.38671" ratio_max="0.64768">
	  <node channel="0" name="T0" type="PT1000" gain="2.509071157" r_ref="4314"/>
	  <node channel="1" name="T1" type="PT1000" gain="2.509806034" r_ref="4285"/>
	  <node channel="2" name="T2" type="PT1000" gain="2.499568407" r_ref="4310"/>
	  <node channel="3" name="T3" type="PT1000" gain="2.507821771" r_ref="4300"/>
	  <node channel="4" name="raw0"/>
	  <node channel="5" name="raw1"/>
	  <node channel="6" name="raw2"/>
	  <node channel="7" name="raw3"/>
	</device>
 */
namespace pi_at_home
{
class DeviceTLV1548;

class NodeTLV1548 : public Node
{
	Q_OBJECT
public:
	NodeTLV1548(const QDomElement &elem, int device, QObject * parent);
	virtual ~NodeTLV1548();
	//virtual const QString & type() const;
	//virtual QString serializeValue(const NodeValue & value) const; QVariant::toString is fine
	virtual Node::Unit unit() const {return unit_;}
	virtual NodeValue defaultValue() const {return NodeValue(0);}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return false;}
	virtual Result init();
	//virtual value();
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
	//
	virtual void processValue(double avgVal, qint64 timestamp, DeviceTLV1548 *converter) =0;
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	Unit unit_=Unit::COUNT;
	double prec=0.01, val_=std::numeric_limits<double>::quiet_NaN();
};

class NodeTLV1548Raw : public NodeTLV1548
{
	Q_OBJECT
public:
	NodeTLV1548Raw(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	virtual NodeValue defaultValue() const {return NodeValue(0.);}
	virtual void processValue(double avgVal, qint64 timestamp, DeviceTLV1548 *converter);
protected:
};

class NodeTLV1548Scaled : public NodeTLV1548
{
	Q_OBJECT
public:
	NodeTLV1548Scaled(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	virtual NodeValue defaultValue() const {return NodeValue(0.);}
	virtual void processValue(double avgVal, qint64 timestamp, DeviceTLV1548 *converter);
protected:
	double gain=2.5;
};

class NodeTLV1548PT1000 : public NodeTLV1548
{
	Q_OBJECT
public:
	NodeTLV1548PT1000(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	virtual NodeValue defaultValue() const {return NodeValue(25.);}
	virtual void processValue(double avgVal, qint64 timestamp, DeviceTLV1548 *converter);
protected:
	double gain=2.5, rRef=4300;
};

class TLV1548Decoder : public QObject
{
	Q_OBJECT
public:
	explicit TLV1548Decoder(int spiPort, int firstAdr, int lastAdr, int tickMs, int average);
	~TLV1548Decoder();
	virtual void timerEvent(QTimerEvent *event);
	double rawData(int adr, qint64 *timestamp);
public slots:
	void startDecoderSl();
	void stopDecoderSl();
signals:
	//void emergencyStop();
	void resultReadySig(int channel, double avgVal, qint64 timestamp);
private:
	int spiPort=0, firstAdr=0, lastAdr=7, curAdr=0, tickMs=1000, timerId=0, avg=1;
	unsigned char spibits[2];
	QMutex mtx;
	int *dcnt=nullptr, *dpnt=nullptr;
	double *davg=nullptr;
	unsigned int **data=nullptr;
	qint64 *timestamps=nullptr;
};

class DeviceTLV1548 : public Device
{
	Q_OBJECT
public:
	explicit DeviceTLV1548(const QDomElement &elem);
	virtual Result init();
	virtual Result shutdown();
	//qint64 currentTimestamp(int channel) {return decoder?decoder->currentTimestamp(channel):0;}
	double raw2RelativeData(double raw);
	double raw2AbsoluteData(double raw);
public slots:
	void conversionCompletedSl(int channel, double avgVal, qint64 timestamp);
private:
	int spiPort=-1, firstAdr=-1, lastAdr=-1, interval=1200, avg=1;
	double uRef=5, ratMin=0., ratMax=1.;
	QThread worker;
	TLV1548Decoder *decoder=nullptr;
	NodeTLV1548 *channels[8];
signals:
	void startDecoderSig();
	void stopDecoderSig();
};
}

#endif // NODETLV1548_H
