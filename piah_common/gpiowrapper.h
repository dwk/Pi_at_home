#ifndef GPIOWRAPPER_H
#define GPIOWRAPPER_H

#include "piadh.h"
#include "enum_result.h"
#include <time.h>
#include <QObject>
#include <QMutex>
#include <QThread>

namespace pi_at_home
{
class HwInPort;
class HwOutPort;
class SwitchMonitor;

/* similar to wiringPi gpio utility but obejct oriented
 * see https://github.com/WiringPi/WiringPi/blob/master/gpio/gpio.c
 */
class GpioWrapper : public QObject
{
	Q_OBJECT
public:
	//enum class PortMode {UNDEF=0, IN, OUT, EDGE};
	enum class PullupMode {UNDEF=0, PULLUP, PULLDOWN, NONE};
	//enum class EdgeMode {UNDEF=0, RISING, FALLING, BOTH};
	GpioWrapper(QObject * parent);
	~GpioWrapper();
	static int wp2Gpio(int wiringPiPort);
	// OUT is faster in wiringPi - direct register access there; IN is done through /sys/class for edge capability
	HwInPort* createInPort(int wpPort, bool inverted, GpioWrapper::PullupMode pullupMode, int holdOnMs, int holdOffMs);
	HwOutPort* createOutPort(int wpPort);
private:
	static int wp2gpioTab[32];
};

class HwPort : public QObject
{
	Q_OBJECT
public:
	virtual ~HwPort();
	bool isValid() const {return valid_;}
	int wpPort() const {return wpPort_;}
	virtual bool readBit() const {return false;}
	virtual void setBit(bool value) {Q_UNUSED(value);}
protected:
	HwPort(int wpPort, QObject * parent);
	bool valid_=false;
	int wpPort_;
};

class HwInPort : public HwPort
{
	Q_OBJECT
public:
	HwInPort(int wpPort, bool inverted, GpioWrapper::PullupMode pullupMode, int holdOnMs, int holdOffMs, QObject * parent);
	virtual ~HwInPort();
	virtual bool readBit() const {return val_;}
	//virtual void setBit(bool value) {Q_UNUSED(value);}
	GpioWrapper::PullupMode pullMode() const {return pullupMode_;}
	int holdOn() const {return holdOnMs_;}
	int holdOff() const {return holdOffMs_;}
	void prepareShutdown();
protected:
	int gpPort=-1, fd=-1, holdOnMs_, holdOffMs_;
	qint64 lastflip=Q_INT64_C(0);
	bool inverted_, val_=false, terminationPrepared=false;
	GpioWrapper::PullupMode pullupMode_;
	QThread *worker=nullptr;
	SwitchMonitor *monitor=nullptr;
protected slots:
	void updateValSl(bool newVal, qint64 timestamp);
signals:
	void updateValSig(bool newVal, qint64 timestamp);
	void startStopSig(bool start);
};

class HwOutPort : public HwPort
{
	Q_OBJECT
public:
	HwOutPort(int wpPort, QObject * parent);
	virtual ~HwOutPort() {}
	//virtual bool readBit() {return false;}
	virtual void setBit(bool value);
protected:
};

class SwitchMonitor : public QObject
{
	Q_OBJECT
public:
	//explicit SwitchMonitor() {}
	explicit SwitchMonitor(int fd, bool inverted, int holdOnMs, int holdOffMs);
	~SwitchMonitor() {}
	bool readBit(qint64 &timestamp);
public slots:
	void startStopSl(bool start);
protected:
	virtual void timerEvent(QTimerEvent *event);
private:
	inline void readRawBit();
	inline void readRawAllBits();
	int fd_, holdOnMs_, holdOffMs_;
	bool inverted_, bit_=false, terminate=false;
	struct timespec tst_;
	QMutex mtx_;
signals:
	void eventSig(bool newVal, qint64 timestamp);
};


}
#endif // GPIOWRAPPER_H
