#include "command.h"
#include <limits>
#include <QTimer>
#include <QPointer>
#include <QTimerEvent>
#include "netcon.h"
#include "nodealg.h"
#include "nodealg_nm.h"
#include "piadi.h"

NodeAlg::NodeAlg(const QDomElement &elem, int device, QObject *parent) : Node(elem, device, parent)
{
	if(elem.hasAttribute("source"))
	{
		mirror=new NodeAddress(elem.attribute("source"));
		//qDebug()<<"adding mirror for"<<mirror->toString();
	}
}

NodeAlg::~NodeAlg()
{
	if(mirror)
	{
		// unregisterCmdTarget
		// close HostCon
		delete(mirror);
	}
}

Result NodeAlg::init()
{
	value_=defval_;
	Result res=Result::COMPLETED;
	if(mirror)
	{
		CommandFilter filter("nodech");
		filter.sourceNodesS.insert(*mirror);
		DCON.registerCmdTarget(filter, this);
		establishHostConSl();
		if(state_==State::ERROR)
			res=Result::FAILED;
		else
			res=Result::PENDING;
	}
	else
	{
		state_=State::READY;
	}
	//qDebug()<<"NodeAlg::init()"<<address().alias()<<value_<<value_.typeName();
	return res;
}

void NodeAlg::setValue(const pi_at_home::NodeValue & value, pi_at_home::Node::TokenType token)
{
	if(!tokenValid(token))
	{
		qWarning()<<adr_.toString()<<"got invalid token"<<token;
		return;
	}
	newValSl(value);
}

void NodeAlg::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	if(relevant.cmd()=="nodeset" ||
			(mirror && (relevant.cmd()=="nodech" ||
					   (cmd.cmd()=="noderq" && cmd.isReply())) ) )
	{
		NodeValue newval;
		switch((QMetaType::Type)defval_.type())
		{
			case QMetaType::Bool:
				newval=NodeValue(relevant.attribute("val")=="true");
				break;
			case QMetaType::Int:
				newval=NodeValue(relevant.attribute("val").toInt());
				break;
			case QMetaType::Double:
				newval=NodeValue(relevant.attribute("val").toDouble());
				break;
			case QMetaType::QString:
				newval=NodeValue(relevant.attribute("val"));
				break;
			default:
				qCritical()<<"NodeAlg processCommand unknown type"<<defval_.type()<<objectName();
				break;
		}
		if(mirror && cmd.cmd()=="noderq" && cmd.isReply())
			qDebug()<<"NodeAlg: got initial reply"<<objectName();
		newValSl(newval);
	}
	else if(relevant.cmd()=="noderq")
	{ // just return the current value
		relevant.setAttribute("val", value_.toString());
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeAlg"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeAlg::newValSl(const NodeValue &newValue)
{
	if(state_==State::BUSY)
		state_=State::READY;
	if(newValue==value_)
		return; // nothing to do!
	value_=cast2Type(newValue);
	emit newValSig(value_, DCON.getUsecTst());
}

void NodeAlg::netConSl(QString uid, bool connected)
{
	if(uid!=mirror->uid())
	{
		//qDebug()<<"NodeAlg IGNORED netConSl (not for me)"<<mirror->uid()<<uid<<connected;
		return; // not our connection
	}
	if(connected)
	{
		//qDebug()<<"NodeAlg connection established, will read remote value";
		readRemoteValue(); // query value when established
	}
	else
	{ // disconnection
		state_=State::BUSY;
	}
}

void NodeAlg::establishHostConSl()
{
	QPointer<NetCon> hc=DCON.getConPool()->addHost(mirror->uid());
	if(hc)
	{
		connect(DCON.getConPool(), &ConPool::hostConStateSig, this, &NodeAlg::netConSl); // get updates!
		//connect(DCON.getConPool(), &ConPool::conErrorSig, this, &NodeAlg::netErrSl); we are just interesetd in connection state
		if(hc->uid().isEmpty())
		{ // wait for connection to establish
			state_=State::BUSY;
			//qDebug()<<"pending HostCon for"<<mirror->uid()<<objectName();
		}
		else if(hc->uid()==mirror->uid())
		{ // already established
			state_=State::BUSY;
			//qDebug()<<"established HostCon for, querying"<<mirror->uid()<<objectName();
			readRemoteValue();
		}
		else
		{ // permanent error, unclear why
			qCritical()<<"Host UIDs do not match"<<mirror->uid()<<hc->uid()<<objectName();
			state_=State::ERROR;
		}
		if(state_==State::BUSY)
		{
			if(!hc->getHeartbeatSec())
				hc->setHeartbeat(24*3600); // once a day
		}
	}
	else
	{ // permanent error, probably no route to uid
		qCritical()<<"cannot create NetCon for"<<mirror->uid()<<objectName();
		state_=State::BUSY; // avoid ERROR so that piahd can start and can receive host address updates
	}
}

void NodeAlg::readRemoteValue()
{
	Command *cmd=new Command("noderq");
	cmd->setDestination(*mirror);
	cmd->setSource(adr_);
	DCON.dispatchCommand(*cmd);
	//qDebug()<<"sending noderq"<<objectName();
	// state remains BUSY until response received, newValSl will move to READY
}


// bool --------------------------------------------------------
NodeBool::NodeBool(const QDomElement &elem, int device, QObject *parent) :
	NodeAlg(elem, device, parent)
{
	if(elem.attribute("default_value")=="true")
		defval_=NodeValue(true);
	else
		defval_=NodeValue(false);
}

const QString &NodeBool::type() const
{
	return typeNameBool;
}

NodeValue NodeBool::cast2Type(const NodeValue &value)
{
	if(value.type()==(QVariant::Type)QMetaType::Bool)
		return value;
	if(!value.canConvert<bool>())
	{
		qCritical()<<"NodeBool::cast2Type not convertible to bool"<<adr_.toStringVerbose()<<value;
		throw Ex(QString("NodeBool::cast2Type not convertible to bool on %1").arg(adr_.toStringVerbose()));
	}
	return NodeValue(value.toBool());
}


// int -----------------------------------------------------
NodeInt::NodeInt(const QDomElement &elem, int device, QObject *parent) :
	NodeAlg(elem, device, parent)
{
	if(elem.hasAttribute("default_value"))
		defval_=NodeValue(elem.attribute("default_value").toInt());
	else
		defval_=NodeValue((int)0);
}

const QString &NodeInt::type() const
{
	return typeNameInt;
}

NodeValue NodeInt::cast2Type(const NodeValue &value)
{
	if(value.type()==(QVariant::Type)QMetaType::Int)
		return value;
	if(!value.canConvert<int>())
		throw Ex(QString("NodeValue not convertible to int on %1").arg(adr_.toStringVerbose()));
	return NodeValue(value.toInt());
}


// double -----------------------------------------------------
NodeDbl::NodeDbl(const QDomElement &elem, int device, QObject *parent) :
	NodeAlg(elem, device, parent)
{
	if(elem.hasAttribute("default_value"))
		defval_=NodeValue(elem.attribute("default_value").toDouble());
	else
		defval_=NodeValue((double)0.);
}

const QString &NodeDbl::type() const
{
	return typeNameDbl;
}

NodeValue NodeDbl::cast2Type(const NodeValue &value)
{
	if(value.type()==(QVariant::Type)QMetaType::Double)
		return value;
	if(!value.canConvert<double>())
		throw Ex(QString("NodeValue not convertible to double on %1").arg(adr_.toStringVerbose()));
	return NodeValue(value.toDouble());
}


// QString -----------------------------------------------------
NodeStr::NodeStr(const QDomElement &elem, int device, QObject *parent) :
	NodeAlg(elem, device, parent)
{
	if(elem.hasAttribute("default_value"))
		defval_=NodeValue(elem.attribute("default_value"));
	else
		defval_=NodeValue(QString(""));
}

const QString &NodeStr::type() const
{
	return typeNameStr;
}

NodeValue NodeStr::cast2Type(const NodeValue &value)
{
	if(value.type()==(QVariant::Type)QMetaType::QString)
		return value;
	if(!value.canConvert<QString>())
		throw Ex(QString("NodeValue not convertible to QString on %1").arg(adr_.toStringVerbose()));
	return NodeValue(value.toString());
}
