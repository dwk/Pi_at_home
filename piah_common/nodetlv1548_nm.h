#ifndef NODETLV1548_NM_H
#define NODETLV1548_NM_H

#include "piadh.h"
#include "node.h"
#include "device.h"


namespace
{
const QString typeName="TLV1548";
const QString typeNameRaw="TLV1548-raw";
const QString typeNameScaled="TLV1548-scaled";
const QString typeNamePt1000="TLV1548-PT1000";
}

#endif // NODETLV1548_NM_H
