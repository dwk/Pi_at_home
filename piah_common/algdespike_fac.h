#ifndef ALGDESPIKE_FAC_H
#define ALGDESPIKE_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgDespike(elem);
}
const QString typeName("Despike");
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGDESPIKE_FAC_H
