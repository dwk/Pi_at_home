#ifndef NODEBITPORT_FAC_H
#define NODEBITPORT_FAC_H

#include "nodebitport_nm.h"

namespace
{
pi_at_home::Device * NodeCreator(const QDomElement &elem)
{
	return new pi_at_home::DeviceBitPort(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, NodeCreator);
}

#endif // NODEBITPORT_FAC_H
