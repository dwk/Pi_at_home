#ifndef NODETADLBUS_FAC_H
#define NODETADLBUS_FAC_H

#include "nodetadlbus_nm.h"

namespace
{
pi_at_home::Device * Creator(const QDomElement &elem)
{
	return new pi_at_home::DeviceTaDlBus(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, Creator);
}

#endif // NODETADLBUS_FAC_H
