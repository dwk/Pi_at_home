#include "gpiowrapper.h"
#include <wiringPi.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <poll.h>
#include <sys/resource.h>
#include <signal.h>
#include <QTimerEvent>
#include "piadi.h"

int GpioWrapper::wp2gpioTab [] =
{
  17, 18, 27, 22, 23, 24, 25, 4,// From the Original Wiki - GPIO 0 through 7:	wpi  0 -  7
   2,  3,						// I2C  - SDA0, SCL0				wpi  8 -  9
   8,  7,						// SPI  - CE1, CE0				wpi 10 - 11
  10,  9, 11,					// SPI  - MOSI, MISO, SCLK			wpi 12 - 14
  14, 15,						// UART - Tx, Rx				wpi 15 - 16
  28, 29, 30, 31,				// Rev 2: New GPIOs 8 though 11			wpi 17 - 20
   5,  6, 13, 19, 26,			// B+						wpi 21, 22, 23, 24, 25
  12, 16, 20, 21,				// B+						wpi 26, 27, 28, 29
   0,  1						// B+						wpi 30, 31
} ;

GpioWrapper::GpioWrapper(QObject *parent) : QObject(parent)
{
	wiringPiSetup();
}

GpioWrapper::~GpioWrapper()
{
	int i=0;
	const QObjectList &chdn=children();
	foreach(QObject *chld, chdn)
	{
		HwInPort *n=qobject_cast<HwInPort *>(chld);
		if(!n)
			continue;
		n->prepareShutdown();
	}
	foreach(QObject *chld, chdn)
	{
		HwPort *n=qobject_cast<HwPort *>(chld);
		if(!n)
			continue;
		delete n;
		++i;
	}
	qInfo()<<"closed"<<i<<"ports";
}

int GpioWrapper::wp2Gpio(int wiringPiPort)
{
	if(wiringPiPort<0 || wiringPiPort>31)
		return -1;
	return wp2gpioTab[wiringPiPort];
}

HwInPort * GpioWrapper::createInPort(int wpPort, bool inverted, pi_at_home::GpioWrapper::PullupMode pullupMode, int holdOnMs, int holdOffMs)
{
	if(wpPort<0 || wpPort>31)
	{
		qCritical()<<"invalid port"<<wpPort;
		return nullptr;
	}
	HwInPort *port=nullptr;
	foreach(HwInPort* prt, this->findChildren<HwInPort * >())
	{
		if(prt->wpPort()==wpPort)
		{
			port=prt;
			break;
		}
	}
	if(port)
	{
		if(port->isValid() && port->pullMode()==pullupMode && port->holdOn()==holdOnMs && port->holdOff()==holdOffMs)
		{
			qInfo()<<"reused port WP"<<wpPort;
			return port;
		}
		if(!port->isValid())
		{
			delete port;
			port=nullptr; // try again
		}
		else
		{
			qCritical()<<"port WP"<<wpPort<<"already created with different properties:"<<static_cast<int>(port->pullMode())<<port->holdOn()<<port->holdOff();
			return nullptr;
		}
	}
	port=new HwInPort(wpPort, inverted, pullupMode, holdOnMs, holdOffMs, this);
	return port;
}

HwOutPort* GpioWrapper::createOutPort(int wpPort)
{
	HwOutPort *port=nullptr;
	if(wpPort<0 || wpPort>31)
	{
		qCritical()<<"invalid port"<<wpPort;
		return port;
	}
	foreach(HwOutPort* prt, this->findChildren<HwOutPort * >())
	{
		if(prt->wpPort()==wpPort)
		{
			port=prt;
			break;
		}
	}
	if(port)
	{
		qInfo()<<"reused port WP"<<wpPort;
		return port;
	}
	port=new HwOutPort(wpPort, this);
	return port;
}

HwPort::HwPort(int wpPort, QObject *parent) :
	QObject(parent), wpPort_(wpPort)
{
}

HwPort::~HwPort()
{ // called late in shutdown sequence - do not expect anything else to be working
	pinMode(wpPort_, INPUT);
	pullUpDnControl(wpPort_, PUD_OFF);
	//qDebug()<<"~HwPort wpPort"<<wpPort_;
}

HwInPort::HwInPort(int wpPort, bool inverted, GpioWrapper::PullupMode pullupMode, int holdOnMs, int holdOffMs, QObject *parent) :
	HwPort(wpPort, parent), holdOnMs_(holdOnMs), holdOffMs_(holdOffMs), inverted_(inverted), pullupMode_(pullupMode)
{
	gpPort=GpioWrapper::wp2Gpio(wpPort);
	pinMode(wpPort, INPUT);
	switch(pullupMode)
	{
		case GpioWrapper::PullupMode::UNDEF:
		case GpioWrapper::PullupMode::NONE:
			pullUpDnControl(wpPort, PUD_OFF);
			break;
		case GpioWrapper::PullupMode::PULLDOWN:
			pullUpDnControl(wpPort, PUD_DOWN);
			break;
		case GpioWrapper::PullupMode::PULLUP:
			pullUpDnControl(wpPort, PUD_UP);
			//qDebug()<<"setting pullup"<<wpPort;
			break;
	}
	FILE *fid ;
	if(!(fid=fopen ("/sys/class/gpio/export", "w")))
	{
		qCritical()<<"/sys/class/gpio error on port"<<gpPort<<"(WP"<<wpPort<<"): export, "<<strerror(errno);
		return;
	}
	fprintf(fid, "%d\n", gpPort);
	fclose(fid);
	QString baseFName=QString("/sys/class/gpio/gpio%1/").arg(gpPort);
	if(!(fid=fopen((baseFName+"direction").toUtf8().data(), "w")))
	{
		qCritical()<<"/sys/class/gpio error on port"<<gpPort<<"(WP"<<wpPort<<"): direction, "<<strerror(errno);
		return;
	}
	fprintf(fid, "in\n");
	fclose(fid) ;
	if(!(fid=fopen((baseFName+"edge").toUtf8().data(), "w")))
	{
		qCritical()<<"/sys/class/gpio error on port"<<gpPort<<"(WP"<<wpPort<<"): edge, "<<strerror(errno);
		return;
	}
	fprintf(fid, "both\n");
	fclose(fid);
	if((fd=open((baseFName+"value").toUtf8().data(), O_RDWR))<0)
	{
		qCritical()<<"/sys/class/gpio error on port"<<gpPort<<"(WP"<<wpPort<<"): value, "<<strerror(errno);
		return;
	}
	// setup monitor
	worker=new QThread();
	//qDebug()<<"HwInPort ctor GPIO WP"<<wpPort_<<"-> fd"<<fd;
	monitor=new SwitchMonitor(fd, inverted_, holdOnMs_, holdOffMs_);
	monitor->moveToThread(worker);
	connect(this, &HwInPort::startStopSig, monitor, &SwitchMonitor::startStopSl, Qt::QueuedConnection);
	connect(monitor, &SwitchMonitor::eventSig, this, &HwInPort::updateValSl, Qt::QueuedConnection);
	worker->start();
	emit startStopSig(true); // val_ will be updated when this signal gets (asynchronously) executed
	//qDebug()<<"HwInPort ctor GPIO value"<<val_;
	valid_=true;
}

HwInPort::~HwInPort()
{ // called late in shutdown sequence - do not expect anything else to be working
	if(!terminationPrepared)
		emit startStopSig(false);
	worker->quit();
	if(!worker->wait(1500)) // wait longer than the poll timeout in SwitchMonitor!
	{
		qCritical()<<"forcing termination of SwitchMonitor";
		worker->terminate();
	}
	if(fd>=0)
		close(fd);
	qDebug()<<"~HwInPort WP"<<wpPort_;
}

void HwInPort::prepareShutdown()
{
	emit startStopSig(false);
	terminationPrepared=true;
}

void HwInPort::updateValSl(bool newVal, qint64 timestamp)
{
	val_=newVal;
	lastflip=timestamp;
	//qDebug()<<"HwInPort::updateValSl GPIO fd"<<fd<<newVal<<timestamp;
	emit updateValSig(val_, lastflip);
}


HwOutPort::HwOutPort(int wpPort, QObject *parent) :
	HwPort(wpPort, parent)
{
	pinMode(wpPort, OUTPUT);
	valid_=true;
}

void HwOutPort::setBit(bool value)
{
	digitalWrite(wpPort_, value?HIGH:LOW);
}


SwitchMonitor::SwitchMonitor(int fd, bool inverted, int holdOnMs, int holdOffMs) :
	fd_(fd), holdOnMs_(holdOnMs), holdOffMs_(holdOffMs), inverted_(inverted)
{
	readRawAllBits();
	if(clock_gettime(CLOCK_MONOTONIC_RAW, &tst_))
		qCritical()<<"SwitchMonitor(ctor) timer error"<<strerror(errno);
}

bool SwitchMonitor::readBit(qint64 & timestamp)
{
	QMutexLocker lock(&mtx_);
	timestamp=static_cast<qint64>(tst_.tv_sec)*Q_INT64_C(1000000)+static_cast<qint64>(tst_.tv_nsec)/Q_INT64_C(1000);
	return bit_;
}

void SwitchMonitor::startStopSl(bool start)
{
	if(start)
	{
		struct timespec rawtst;
		if(clock_gettime(CLOCK_MONOTONIC_RAW, &rawtst))
		{
			qCritical()<<"cannot read timer"<<strerror(errno);
			terminate=true;
		}
		mtx_.lock();
		readRawBit();
		tst_=rawtst;
		mtx_.unlock();
		// always emit a signal at start so that upstream data gets initiaized
		//qDebug()<<"SwitchMonitor::startStopSl GPIO start fd"<<fd_<<bit_;
		emit eventSig(bit_, static_cast<qint64>(tst_.tv_sec)*Q_INT64_C(1000000)+static_cast<qint64>(tst_.tv_nsec)/Q_INT64_C(1000));
		if(bit_)
			startTimer(holdOnMs_);
		else
			startTimer(holdOffMs_);
	}
	else
		terminate=true;
}

void SwitchMonitor::timerEvent(QTimerEvent *event)
{
	killTimer(event->timerId());
	if(terminate)
		return;
	struct pollfd polls;
	polls.fd     = fd_;
	polls.events = POLLPRI;
	polls.revents= 0;
	// use ppoll with SIGINT (for proper termination) unclear
	// SIGINT will unblock ppoll probably before terminate flag is set - quite useless
	// we time out periodically - downside: main thread has to wait for termination worst case one full timeout
	//sigset_t sigs;
	//sigfillset(&sigs);
	//res=ppoll(&polls, 1, nullptr, &sigs);
	int res=poll(&polls, 1, 1000);
	struct timespec rawtst;
	if(clock_gettime(CLOCK_MONOTONIC_RAW, &rawtst))
	{
		qCritical()<<"cannot read timer"<<strerror(errno);
		terminate=true;
	}
	if(terminate)
		return;
	if(res==-1 && !polls.revents)
	{
		qWarning()<<"poll (SwitchMonitor) returned -1 with no bits in revents set - retry";
		startTimer(0);
		return;
	}
	if(!res)
	{ // timeout; everything is fine, nothing happened, start the next poll
		startTimer(0);
		return;
	}
	if(res!=1)
	{
		qCritical()<<"SwitchMonitor poll failed with"<<res<<strerror(errno);
		terminate=true;
		return;
	}
	//bool oldbit=bit_;
	mtx_.lock();
	readRawBit();
	tst_=rawtst;
	mtx_.unlock();
	//if(oldbit!=bit_)
	// for fast signals we may read may miss half a periode and see the same
	// value as before. Signal anyway so that the app knows we missed something
	emit eventSig(bit_, static_cast<qint64>(tst_.tv_sec)*Q_INT64_C(1000000)+static_cast<qint64>(tst_.tv_nsec)/Q_INT64_C(1000));
	if(bit_)
		startTimer(holdOnMs_);
	else
		startTimer(holdOffMs_);
}

void SwitchMonitor::readRawBit()
{
	uint8_t c=0;
	lseek (fd_, 0, SEEK_SET) ;
	read(fd_, &c, 1);
	bit_=(c==(inverted_?'0':'1'));
	//qDebug()<<"SwitchMonitor::readRawBit GPIO fd"<<fd_<<bit_;
}

void SwitchMonitor::readRawAllBits()
{
	// read and seek, see https://www.kernel.org/doc/Documentation/gpio/sysfs.txt
	// flush pin
	int count = 0;
	uint8_t c=0;
	lseek (fd_, 0, SEEK_SET) ;
	ioctl(fd_, FIONREAD, &count);
	if(count>1 && count != 4096) // kernel driver seems to announce always 4096 --> no practical information
		qDebug()<<"multi-read on IO-fd"<<fd_<<count;
	for(int i = 0 ; i < count ; ++i)
		read(fd_, &c, 1);
	lseek (fd_, 0, SEEK_SET) ;
	read(fd_, &c, 1);
	bit_=(c==(inverted_?'0':'1'));
	//qDebug()<<"readRawAllBits value"<<bit_;
}


