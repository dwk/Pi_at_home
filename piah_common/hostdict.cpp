#include "hostdict.h"
#include <QSettings>
#include <QCoreApplication>
#include "piadi.h"

HostDict::HostDict(bool isServer, const QDomElement *elem, int maxMemoryDays, QObject *parent) :
	QObject(parent), isServer_(isServer), memoryDays(maxMemoryDays)
{
	//qDebug()<<"ctor HostInstance"<<isServer;
	if(elem)
	{
		QDomNodeList hosts=elem->elementsByTagName("host");
		QDateTime deftime;
		for(int i=0; i<hosts.size(); ++i)
		{
			QDomElement he=hosts.at(i).toElement();
			QString uid = he.attribute("uid");
			QHostAddress ip(he.attribute("ip"));
			int port = he.attribute("port").toInt();
			if(!uid.isEmpty() && !ip.isNull() && port)
			{
				hostInstanceMV_.insert(HostInstanceMV::value_type(uid, HostInstance(ip.toIPv4Address(), port, deftime)));
				qDebug()<<"static: HostInstance"<<uid<<ip<<port;
			}
		}
	}
	if(isServer_)
		settings=new QSettings(QCoreApplication::applicationDirPath()+"/HostDict", QSettings::IniFormat, this);
	else
		settings=new QSettings();
	// >> hosts
	settings->beginGroup("hosts");
	QStringList uids = settings->childKeys();
	// << hosts
	settings->endGroup();
	foreach (QString uid, uids)
	{
		// >> "uid"
		settings->beginGroup(uid);
		//qDebug()<<uid<<":"<<settings->value("ip")<<settings->value("port")<<settings->value("lastcon");
		QHostAddress hostip(settings->value("ip").toString());
		quint32 ip=hostip.toIPv4Address();
		int port=settings->value("port").toInt();
		QDateTime lastcon=settings->value("lastcon").toDateTime();
		if(ip && port)
		{
			if(!lastcon.isValid())
			{
				hostInstanceMV_.insert(HostInstanceMV::value_type(uid, HostInstance(ip, port, lastcon)));
				qInfo()<<"virgin HostInstance"<<uid<<hostip.toString()<<port;
				settings->endGroup();
			}
			else if(lastcon.daysTo(QDateTime::currentDateTime())>memoryDays)
			{
				qInfo()<<"removing host address older than "<<memoryDays<<"days"<<settings->value("ip").toString();
				settings->endGroup();
				settings->remove(uid);
			}
			else
			{
				//HostInstanceMV::iterator hit=
				hostInstanceMV_.insert(HostInstanceMV::value_type(uid, HostInstance(ip, port, lastcon)));
				qInfo()<<"dynamic HostInstance"<<uid<<hostip.toString()<<port<<lastcon;
				/* >> >> nodes
				settings->beginGroup("nodes");
				QStringList aliases = settings->childKeys();
				int nodeCnt=0;
				foreach (QString alias, aliases)
				{
					NodeAddress na(settings->value(alias).toString());
					hit->second.nodeDict.insert(NodeInstanceMV::value_type(alias, na));
					++nodeCnt;
				}
				// << << nodes
				settings->endGroup(); */
				// << "uid"
				settings->endGroup();
			}
		}
		else
		{
			qWarning()<<"HostDict: invalid host"<<uid<<settings->value("ip")<<settings->value("port")<<settings->value("lastcon");
			settings->endGroup(); // << "uid"
			settings->remove(uid);
			settings->beginGroup("hosts");
			settings->remove(uid);
			settings->endGroup(); // << hosts
		}
	}
}

HostDict::~HostDict()
{
	flush();
	settings=nullptr;
}

const HostInstance *HostDict::find(const QString &uid)
{
	auto it=hostInstanceMV_.find(uid);
	if(it==hostInstanceMV_.end())
		return nullptr;
	return &(it->second);
}

QString HostDict::find(const QHostAddress &addr, int port)
{
	quint32 adr32=addr.toIPv4Address();
	for(auto it=hostInstanceMV_.begin(); it!=hostInstanceMV_.end(); ++it)
	{
		if(it->second.hadr==adr32 && it->second.port==port)
		{
			return it->first;
		}
	}
	return QString();
}

void HostDict::confirm(const QString &uid, const QHostAddress &addr, int port)
{
	QDateTime dt=QDateTime::currentDateTime();
	confirm(uid, addr, port, dt);
}

void HostDict::confirm(const QString &uid, const QHostAddress &addr, int port, const QDateTime &verified)
{
	auto it=hostInstanceMV_.find(uid);
	if(it==hostInstanceMV_.end())
	{
		//QDateTime dt=verified;
		hostInstanceMV_.insert(HostInstanceMV::value_type(uid, HostInstance(addr.toIPv4Address(), port, verified)));
	}
	else
	{
		HostInstance &hi=(it->second);
		if(hi.lastConfirm>verified)
		{
			qInfo()<<"received outdated host info"<<uid<<verified.toString(Qt::ISODate)<<"current is"<<hi.lastConfirm.toString(Qt::ISODate);
			return;
		}
		hi.lastConfirm=verified;
		if(hi.hadr!=addr.toIPv4Address())
		{
			qWarning()<<"host changed IP"<<uid<<hi.hadr<<addr.toString();
			hi.hadr=addr.toIPv4Address();
		}
		if(hi.port!=port)
		{
			qWarning()<<"host changed port"<<uid<<hi.port<<port;
			hi.port=port;
		}
	}
}

bool HostDict::update(HostInstance &hi)
{
	auto it=hostInstanceMV_.find(hi.uid);
	if(it==hostInstanceMV_.end())
		return false;
	it->second=hi;
	return true;
}

bool HostDict::create(HostInstance &hi)
{
	auto it=hostInstanceMV_.find(hi.uid);
	if(it!=hostInstanceMV_.end())
		return false;
	hostInstanceMV_.insert(HostInstanceMV::value_type(hi.uid, hi));
	return true;
}

bool HostDict::remove(const QString &uid)
{
	auto it=hostInstanceMV_.find(uid);
	if(it==hostInstanceMV_.end())
		return false;
	if(settings)
	{
		settings->remove(uid);
		settings->beginGroup("hosts");
		settings->remove(uid);
		settings->endGroup();
	}
	hostInstanceMV_.erase(it);
	return true;
}

void HostDict::flush()
{
	if(settings)
	{
		qDebug()<<"HostDict saving at"<<settings->group();
		// >> hosts
		settings->beginGroup("hosts");
		for(auto it=hostInstanceMV_.begin(); it!=hostInstanceMV_.end(); ++it)
		{
			settings->setValue(it->first, " ");
		}
		// << hosts
		settings->endGroup();
		for(auto it=hostInstanceMV_.begin(); it!=hostInstanceMV_.end(); ++it)
		{
			// >> "uid"
			settings->beginGroup(it->first);
			qDebug()<<"HostDict subkey at"<<settings->group();
			settings->setValue("ip", QHostAddress(it->second.hadr).toString());
			settings->setValue("port", it->second.port);
			settings->setValue("lastcon", it->second.lastConfirm);
			//qDebug()<<"saving setting"<<it->first<<settings->value("ip")<<settings->value("port")<<settings->value("lastcon");
			// << "uid"
			settings->endGroup();
		}
	}
}

/*void HostDict::remove(const QHostAddress &addr, int port)
{
	quint32 adr32=addr.toIPv4Address();
	for(auto it=hostInstanceMV_.begin(); it!=hostInstanceMV_.end(); ++it)
	{
		if(it->second.hadr==adr32 && it->second.port==port)
		{
			if(settings)
			{
				settings->beginGroup(it->first);
				if(settings->value("ip").toString()==addr.toString() && settings->value("port").toInt()==port)
				{
					settings->endGroup();
					settings->remove(it->first);
					settings->beginGroup("hosts");
					settings->remove(it->first);
					settings->endGroup();
					hostInstanceMV_.erase(it);
					return;
				}
				settings->endGroup();
			}
			else
			{
				hostInstanceMV_.erase(it);
				return;
			}
		}
	}
}*/



