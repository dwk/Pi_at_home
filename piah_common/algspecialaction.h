#ifndef ALGSPECIALACTION_H
#define ALGSPECIALACTION_H

#include "piadh.h"
#include "algorithm.h"
#include "node.h"
#include "dwkapp.h"

namespace pi_at_home
{
/* Algorithm for execution of system-related tasks.
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="SpecialAction"	fixed
 *	input="x"				x = node alias; node value is converted to bool, action triggered if true
 *	action="x"				x = action; list of valid action see below
 * optional attributes
 *	<none>
 * connectors
 *  input				node value is converted to bool, action triggered if true
 * Valid action names are:
 * quit			tells systemd to stop piahd; well fail if piahd is run without --systemd option
 * <DwkApp::ServerCmd>		if the action parameter converts to a positive integer it is cast to DwkApp::ServerCmd and forwarded to DwkApp::executeServerCmd
*/
class AlgSpecialAction : public Algorithm
{
	Q_OBJECT
public:
	//enum class Action{ UNDEF=0, QUIT=1, SERVER_CMD=2 };
	AlgSpecialAction(const QDomElement &elem);
	virtual ~AlgSpecialAction() {}
	virtual void execute();
	virtual bool safeState() {return true;}
public slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp);
protected:
	virtual Result preExec();
	virtual Result postExec();
protected:
	QString inNodeName_;
	NodeValue v_;
	DwkApp::ServerCmd scmd=DwkApp::ServerCmd::NOP;
};

}
#endif // ALGSPECIALACTION_H
