#ifndef ALGINFOPANEL_H
#define ALGINFOPANEL_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"
#include "nodealg.h"

namespace pi_at_home
{

/* Fills a display connected as piahd-device with nice informaation
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	 type="InfoPanel"			fixed
 *
 * sub-elements <line>
 * required attributes
 *	channel="x"			x = integer; output will be written to this channel of the display_id
 *	name="x"			x = alias, used as label except for some magic values
 *						$DATETIME Display Date and Time
 * optional attributes
 *	unit="x"			x = label printed at the end of the line
 *	input="x"			x = alias of input node
 *	style="x"			x = formating information
 *						Double0 read NodeVale as dounle but display no decimal places
 *						Double1 read NodeVale as dounle and display with one decimal place
 *						pmDouble1 read NodeVale as dounle, display one decimal and plus sign for positive values
 *						onoff read NodeVale as bool, display "on" or "off"; unit may conatin "|" separated phrases used instead of on and off
 *						all other styles will try a toString conversion
 *
 * The config file looks similar to AlgDummy, just with <line> sub-elements instead of <node>. These
 * sub elements are not created and registered as nodes, so they will not have a NodeAddress,
 * they will not show up in the GUI and so on. Their only purpose is to structure the config file.
*/

class LineProcessor : public QObject
{
	Q_OBJECT
public:
	enum class FormatStyle{NOVAL, STRING, DOUBLE0, DOUBLE1, DOUBLE1P, BOOLLIST, INTLIST};
	enum class DataSource{NONE, NODEVALUE, DATETIME};
	LineProcessor(const QDomElement &elem, QObject * parent);
	LineProcessor(DataSource ds, QObject * parent);
	bool addOutNode(const QDomElement &elem, Node * outNode);
	Result init();
	void updateSl(const NodeValue & newValue, qint64 timestamp);
	void forceUpdate();
	int getOutChannel() const {return channel<0?-1:channel+64;}
	Node * getOutNode() const {return outputNode;}
protected:
	virtual void timerEvent(QTimerEvent *event);
	FormatStyle style=FormatStyle::STRING;
	DataSource src=DataSource::NONE;
	QString templ, inputNm;
	QChar fillChar;
	QStringList translations;
	int channel=-1, lineLength=14;
	Node* outputNode=nullptr;
};
typedef std::map<int, QPointer<LineProcessor> > LineProcessorMISP;

class AlgInfoPanel : public Algorithm
{
	Q_OBJECT
public:
	typedef std::map< int, NodeAlg * > NodeAlgMP;
	explicit AlgInfoPanel(const QDomElement &elem);
	virtual ~AlgInfoPanel() {}
	virtual void execute();
	virtual bool safeState() {return true;}
protected:
	virtual Result preExec();
	virtual Result postExec();
	LineProcessorMISP lines;
};

}
#endif // ALGINFOPANEL_H
