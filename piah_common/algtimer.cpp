#include "algtimer.h"
#include "algtimer_fac.h"
#include <limits>
#include <QTimerEvent>
#include "node.h"
#include "command.h"
#include "piadi.h"

QString AlgTimer::statusTempl="%1 %2-%3";
QString AlgTimer::valueTempl="%1:%2:%3";

AlgTimer::AlgTimer(const QDomElement &elem) :
	Algorithm(elem)
{
	bool timerAbs=false;
	QString templ="timer%1";
	for(int i=0; true; ++i)
	{
		QString nm=elem.attribute(templ.arg(i));
		if(nm.isEmpty())
			break;
		bool iok=false;
		qint64 ival=nm.toLongLong(&iok);
		if(!iok)
		{
			QTime testt=QTime::fromString(nm, "hh:mm:ss");
			if(testt.isValid())
			{
				iok=true;
				ival=testt.msecsSinceStartOfDay();
			}
		}
		if(iok)
		{
			if(!i && !ival)
				startval=true;
			else
				times.push_back(ival);
		}
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<": undefined timer value"<<nm;
		}
	}
	if(!times.size())
	{
		algState=AlgState::ERROR;
		qCritical()<<"no valid timer values found";
	}
	QString opnm=elem.attribute("op");
	if(!opnm.isEmpty())
	{
		if(opnm=="pulse")
			pulse=true;
		else if(opnm=="toggle")
			pulse=false;
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<": undefined operation"<<opnm;
		}
	}
	opnm=elem.attribute("delay_style");
	if(opnm=="absolute")
		timerAbs=true;
	else if(opnm=="time_of_day")
		timeOfDay=true; // delays stay referenced to 0:00 !
	else if(opnm!="incremental")
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<": undefined delay_style"<<opnm;
	}
	opnm=elem.attribute("runonce");
	if(!opnm.isEmpty())
	{
		if(opnm=="true")
			runonce=true;
		else if(opnm=="false")
			runonce=false;
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<": undefined runonce"<<opnm;
		}
	}
	opnm=elem.attribute("start_on_init");
	if(!opnm.isEmpty())
	{
		if(opnm=="true")
			startOnInit=true;
		else if(opnm=="false")
			startOnInit=false;
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<": undefined startOnInit"<<opnm;
		}
	}
	opnm=elem.attribute("pulse_duration");
	if(!opnm.isEmpty())
	{
		bool iok=false;
		int ival=opnm.toInt(&iok);
		if(iok && ival>0)
		{
			pulsDur=ival;
		}
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<": undefined pulse value"<<opnm;
		}
	}
	if(timerAbs)
	{
		for(int i=times.size()-1; i>0; --i)
		{
			times[i]=times[i]-times[i-1];
			if(times[i]<=0)
			{
				algState=AlgState::ERROR;
				qCritical()<<"timer values not strictly ascending in absolute mode"<<i;
			}
		}
	}
	if(timeOfDay)
	{
		for(int i=0; i<(int)times.size(); ++i)
		{
			if(!i || times[i]>times[i-1])
			{
				NodeStr *n=new NodeStr(namePrefix+"time"+QString::number(i), id_, 10+i, this);
				//n->setUnit(Node::Unit::TIME);
				delayNodes.push_back(QPointer<Node>(n));
			}
			else
			{
				algState=AlgState::ERROR;
				qCritical()<<objectName()<<": inconsistent timer values"<<times[i-1]<<">="<<times[i];
				break;
			}
		}
	}
	else
	{
		for(int i=0; i<(int)times.size(); ++i)
		{
			NodeDbl *n=new NodeDbl(namePrefix+"delay"+QString::number(i), id_, 10+i, this);
			n->setUnit(Node::Unit::SEC);
			delayNodes.push_back(QPointer<Node>(n));
		}
	}
	if(!pulse && (times.size()%2))
		qWarning()<<objectName()<<": odd number of timer events for toggle operation";
	if(pulse && startval)
		qWarning()<<objectName()<<": leading zero delay has no effect in pulse mode";
	ndOut=new NodeBool(namePrefix+"out", id_, 0, this);
	ndState=new NodeStr(namePrefix+"state", id_, 1, this);
	if(!timeOfDay)
		ndReset=new NodeBool(namePrefix+"reset", id_, 2, this);
}

void AlgTimer::execute()
{
	if(algState!=AlgState::RUNNING)
		return;
	// pure timer control
}

void AlgTimer::valCaptureSl(const NodeValue &newValue, qint64 timestamp, int ch)
{
	Q_UNUSED(timestamp)
	if(algState!=AlgState::RUNNING)
		return;
	if(ch<0)
	{
		if(newValue.toBool())
		{
			if(timerMain)
				killTimer(timerMain);
			timerMain=0;
			if(ndReset)
				ndReset->setValue(NodeValue(false));
			initTimes();
			// reset always starts
			ndOut->setValue(NodeValue(pulse?false:toggleVal)); // toggleVal set during initTimes
			toggleVal=!toggleVal; // first update included
			t0=QDateTime::currentMSecsSinceEpoch();
			startMainTimer(Q_INT64_C(0));
			updateStatus(true);
		}
	}
	else
	{
		bool iok=false;
		qint64 ival=0;
		double dval=newValue.toDouble(&iok);
		if(iok)
		{
			ival=(qint64)(dval*1000.);
		}
		else
		{
			QTime testt=QTime::fromString(newValue.toString(), "hh:mm:ss");
			//qDebug()<<"valCaptureSl"<<testt;
			if(testt.isValid())
			{
				iok=true;
				ival=testt.msecsSinceStartOfDay();
			}
		}
		if(!iok)
		{
			qWarning()<<objectName()<<"invalid update of timer delay"<<newValue.toString()<<ch;
			return;
		}
		qInfo()<<objectName()<<"timer delay update"<<newValue.toString()<<ch<<ival;
		times.at(ch)=ival;
		if(timerMain)
			killTimer(timerMain);
		timerMain=0;
		initTimes();
		if(startOnInit)
		{
			ndOut->setValue(NodeValue(pulse?false:toggleVal)); // toggleVal set during initTimes
			toggleVal=!toggleVal; // first update included
			t0=QDateTime::currentMSecsSinceEpoch();
			startMainTimer(Q_INT64_C(0));
		}
		updateStatus(true);
	}
}

pi_at_home::Result AlgTimer::preExec()
{
	if(algState!=AlgState::STARTUP)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	if(!ndOut)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"no output";
        ndState->setValue(NodeValue("ERROR no output"));
		return Result::FAILED;
	}
	if(ndReset)
	{
		connect(ndReset, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, -1);});
	}
	int ti=0;
	for(NodesVSP::iterator it=delayNodes.begin(); it!=delayNodes.end(); ++it, ++ti)
	{
		if(*it)
		{
			if(timeOfDay)
				(*it)->setValue(NodeValue(ms2Timeofday(times[ti])));
			else
				(*it)->setValue(NodeValue((double)times[ti]/1000.));
			connect((*it).data(), &Node::newValSig, [this, ti](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, ti);});
		}
	}
	initTimes();
	if(startOnInit)
	{
		ndOut->setValue(NodeValue(pulse?false:toggleVal)); // toggleVal set during initTimes
		toggleVal=!toggleVal; // first update included
		t0=QDateTime::currentMSecsSinceEpoch();
		startMainTimer(Q_INT64_C(0));
	}
	timerStatus=startTimer(statusDur);
	//qDebug()<<"AlgTimer starting status timer"<<timerStatus<<statusDur;
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

pi_at_home::Result AlgTimer::postExec()
{
	if(timerMain)
		killTimer(timerMain);
	timerMain=0;
	if(timerStatus)
		killTimer(timerStatus);
	timerStatus=0;
	if(ndOut)
		ndOut->setValue(ndOut->defaultValue());
	return Result::COMPLETED;
}

void AlgTimer::timerEvent(QTimerEvent *event)
{
	//qDebug()<<"AlgTimer::timerEvent"<<event->timerId()<<(int)algState;
	if(algState!=AlgState::RUNNING)
	{
		killTimer(event->timerId());
		return;
	}
	if(event->timerId()==timerMain)
	{
		killTimer(event->timerId());
		++loopI;
		qint64 tlaps=Q_INT64_C(0);
		if(loopI>=(int)times.size())
		{
			if(runonce)
			{
				if(!timerPulse)
					ndOut->setValue(NodeValue(ndOut->defaultValue()));
				lastStatusUpdate=std::numeric_limits<long long int>::max();
				ndState->setValue(NodeValue("run-once done"));
				return;
			}
			toggleVal=startval; // doesn't matter for pulse
			loopI=0;
			++loopCnt;
			if(!timeOfDay) // time_of_day has 24h periodicity by definition
			{
				tlaps=QDateTime::currentMSecsSinceEpoch()-t0-cycleDur*loopCnt;
				if(tlaps<Q_INT64_C(-1000))
				{
					qWarning()<<objectName()<<"large NEGATIVE time laps (ignored, t0 reset)"<<tlaps<<t0<<cycleDur<<loopCnt;
					tlaps=Q_INT64_C(0);
					t0=QDateTime::currentMSecsSinceEpoch();
					loopCnt=0;
				}
				else if(tlaps>=times[0])
				{
					qWarning()<<objectName()<<"UNCORRECTED time laps (larger than delay, t0 reset)"<<tlaps<<t0<<cycleDur<<loopCnt;
					tlaps=Q_INT64_C(0);
					t0=QDateTime::currentMSecsSinceEpoch();
					loopCnt=0;
				}
				//else if(tlaps)
				//	qInfo()<<objectName()<<"correcting"<<tlaps;
			}
		}
		startMainTimer(tlaps);
		if(pulse)
		{
			ndOut->setValue(NodeValue(true));
			if(timerPulse)
				killTimer(timerPulse);
			timerPulse=startTimer(pulsDur);
		}
		else
		{
			ndOut->setValue(NodeValue(toggleVal));
			toggleVal=!toggleVal;
		}
	}
	else if(event->timerId()==timerPulse)
	{
		killTimer(event->timerId());
		timerPulse=0;
		ndOut->setValue(NodeValue(false));
	}
	else if(event->timerId()==timerStatus)
	{
		updateStatus();
	}
	else
		qCritical()<<"invalid timer ID"<<event->timerId();
}

void AlgTimer::startMainTimer(qint64 tlaps)
{
	qint64 delay=Q_INT64_C(0);
	if(timeOfDay)
	{
		delay=times[loopI]-QTime::currentTime().msecsSinceStartOfDay();
		if(delay<Q_INT64_C(0))
		{
			delay+=Q_INT64_C(86400000);
			if(delay<Q_INT64_C(0))
			{
				qCritical()<<objectName()<<"negative delay in timeOfDay, using 0 instead"<<times[loopI]<<delay;
				delay=Q_INT64_C(0);
			}
		}
	}
	else
		delay=times[loopI]-tlaps;
	timerMain=startTimer(delay);
	//qDebug()<<"AlgTimer starting main timer"<<timerMain<<delay<<stateInfo<<loopI;
	timerMainStart=QDateTime::currentMSecsSinceEpoch();
	if(stateInfo==2 || (stateInfo==1 && !loopI))
		updateStatus(true);
}

void AlgTimer::updateStatus(bool forceUpdate)
{
	qint64 tms=QDateTime::currentMSecsSinceEpoch();
	//qDebug()<<"AlgTimer status"<<tms<<loopI;
	if(forceUpdate || tms-lastStatusUpdate>statusDur/2)
	{
		if(!startOnInit && !timerMain)
		{
			ndState->setValue(NodeValue(QStringLiteral("waitig for reset")));
		}
		else
		{
			if(stateInfo==2)
			{
				int t=0;
				if(timeOfDay)
				{
					qint64 tdms=QTime::currentTime().msecsSinceStartOfDay();
					if(tdms>times[loopI])
						t=times[loopI]+(Q_INT64_C(86400000)-tdms);
					else
						t=times[loopI]-tdms;
				}
				else
					t=times[loopI]-(tms-timerMainStart);
				ndState->setValue(NodeValue(statusTempl.arg(ms2Timeofday(t)).arg(loopI).arg(loopCnt)));
			}
			else
				ndState->setValue(NodeValue(statusTempl.arg("", "hidden").arg(loopCnt)));
		}
		lastStatusUpdate=tms;
	}
}

QString AlgTimer::ms2Timeofday(qint64 ms)
{
	int h=ms/3600000;
	ms-=h*3600000;
	int m=ms/60000;
	ms-=m*60000;
	int s=(ms+500)/1000;
	return valueTempl.arg(h,2,10,QChar('0')).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0'));
}

void AlgTimer::initTimes()
{
	int ti=0;
	qint64 mindur=(times.size()?times[0]:1);
	cycleDur=Q_INT64_C(0);
	for(auto it=times.begin(); it!=times.end(); ++it, ++ti)
	{
		cycleDur+=times[ti];
		if(ti && times[ti]<mindur)
			mindur=times[ti];
		//qDebug()<<"mindur"<<ti<<times[ti]<<mindur;
	}
	if(pulse && pulsDur<mindur)
		mindur=pulsDur;
	if(timeOfDay)
		cycleDur=Q_INT64_C(86400000);
    if(cycleDur/2<=DCON.getBroadcastDelayMs())
    {
		qInfo()<<objectName()<<"setting out-node hidden, no status"<<mindur<<DCON.getBroadcastDelayMs();
        ndOut->setHidden(true);
        stateInfo=0;
    }
    else if(!timeOfDay && mindur/2<=DCON.getBroadcastDelayMs())
    {
		qInfo()<<objectName()<<"setting out-node hidden, cycle only status"<<mindur<<DCON.getBroadcastDelayMs();
        ndOut->setHidden(true);
        stateInfo=1;
    }
    else
	{
		ndOut->setHidden(false);
		stateInfo=2;
	}
	loopCnt=0;
	loopI=0;
	toggleVal=startval; // doesn't matter for pulse
	if(timeOfDay)
	{
		qint64 cms=QTime::currentTime().msecsSinceStartOfDay();
		while(cms>=times[loopI] && loopI<(int)times.size())
		{
			++loopI;
			toggleVal=!toggleVal;
		}
		if(loopI>=(int)times.size())
		{
			loopI=0; // wait for next day
			qInfo()<<objectName()<<"init time-of-day for next day"<<loopI<<times[loopI];
		}
		else
			qInfo()<<objectName()<<"init time-of-day loop at step"<<loopI<<times[loopI];
	}
}
