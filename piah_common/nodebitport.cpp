#include "command.h"
#include <QTimer>
#include "nodebitport.h"
#include "nodebitport_fac.h"
#include "gpiowrapper.h"
#include "command.h"
#include "piadi.h"

NodeBitPort::NodeBitPort(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent)
{
	if(elem.attribute("default_value")=="true")
		defval_=true;
	if(elem.attribute("inverted")=="true")
		inverted=true;
	bool intOk=false;
	int intVal=elem.attribute("delay").toInt(&intOk);
	if(intOk && intVal>0)
		delay_=intVal;
	intVal=elem.attribute("push_pull_offset").toInt(&intOk);
	if(intOk)
		chOff=intVal;
	intVal=elem.attribute("pulse").toInt(&intOk);
	if(intOk && intVal>0)
		pulse=intVal;
	//qDebug()<<objectName()<<delay_<<pulse<<chOff;
}

const QString &NodeBitPort::type() const
{
	return typeName;
}

Result NodeBitPort::init()
{
	DeviceBitPort *d=qobject_cast<DeviceBitPort * >(parent());
	if(!d)
		throw Ex(QString("init failed on %1").arg(adr_.toStringVerbose()));
	if(delay_>DCON.getBroadcastDelayMs() || pulse>DCON.getBroadcastDelayMs())
	{
		reportStatus=true;
		qInfo()<<adr_.toString()<<"status reporting activated";
	}
	val_=defval_;
	value_=val_;
	state_=State::READY;
	basePortWp_=d->basePortWp();
	hwPort=DCON.io()->createOutPort(basePortWp_+adr_.channel());
	if(!hwPort)
		throw Ex(QString("init hwPort failed on %1").arg(adr_.toStringVerbose()));
	//hwPort->setBit(val_?(!inverted):inverted); let setRaw do it
	if(chOff)
	{ // push/pull set both off first
		hwPortPp=DCON.io()->createOutPort(basePortWp_+adr_.channel()+chOff);
		if(!hwPortPp)
			throw Ex(QString("init hwPortPp failed on %1").arg(adr_.toStringVerbose()));
		/* let setRaw do it
		if(pulse)
			hwPort->setBit(val_?(!inverted):inverted); // set it to defval_, that is passive
		else
			hwPortPp->setBit(val_?inverted:(!inverted)); // true push-pull in "passive"*/
	}
	setRaw();
	return (state_==State::READY)?Result::COMPLETED:Result::PENDING; // may be pending for pulsed or delyed nodes
}

void NodeBitPort::setValue(const pi_at_home::NodeValue & value, pi_at_home::Node::TokenType token)
{
	if(!tokenValid(token))
	{
		qWarning()<<adr_.toString()<<"got invalid token"<<token;
		return;
	}
	if(!value.canConvert<bool>())
	{
		qCritical()<<"NodeBitPort::setValue: not convertible to bool"<<adr_.toStringVerbose()<<value;
		throw Ex(QString("NodeValue not convertible to bool on %1").arg(adr_.toStringVerbose()));
	}
	bool newVal=value.toBool();
	setValue(newVal);
}

void NodeBitPort::setValue(bool value)
{
	if(value==val_ && delayedUpdate==DelayedUpdate::NONE) // if a delayed update is pending we must not miss a reset to the original value
		return; // nothing to do!
	if(state()!=Node::State::READY)
	{
		qWarning()<<"set value postponed while not ready"<<adr_.toStringVerbose()<<Node::state(state());
		delayedUpdate=(value?DelayedUpdate::TO_ACTIVE:DelayedUpdate::TO_INACTIVE);
		return;
	}
	//qDebug()<<objectName()<<value;
	val_=value;
	value_=val_;
	tst=DCON.getUsecTst();
	setRaw();
	emit newValSig(value_, tst);
	if(reportStatus)
		DCON.add2Broadcast(toNodechFrag(value_, DCON.getUsecTst()));
}

void NodeBitPort::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	if(relevant.cmd()=="nodeset")
	{
		setValue(relevant.attribute("val")=="true");
	}
	else if(relevant.cmd()=="noderq")
	{ // just return the current value
		relevant.setAttribute("val", value_.toString());
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeBitPort"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeBitPort::newValSl(const NodeValue &newValue)
{
	if(!newValue.canConvert<bool>())
	{
		qCritical()<<"NodeBitPort::newValSl not convertible to bool"<<adr_.toStringVerbose()<<newValue;
		throw Ex(QString("NodeValue not convertible to bool on %1 (s)").arg(adr_.toStringVerbose()));
	}
	bool newVal2=newValue.toBool();
	setValue(newVal2);
}

void NodeBitPort::stateReadySl()
{
	state_=Node::State::READY;
	if(delayedUpdate!=DelayedUpdate::NONE)
	{
		bool sv=(delayedUpdate==DelayedUpdate::TO_ACTIVE);
		delayedUpdate=DelayedUpdate::NONE;
		if(sv==val_)
			qWarning()<<adr_.toStringVerbose()<<"Delayed update has no effect because already there"<<sv;
		else
		{
			qWarning()<<adr_.toStringVerbose()<<"Applying delayed update"<<sv;
			setValue(sv);
			return;
		}
	}
	//qDebug()<<objectName()<<"status READY";
	emit newValSig(value_, DCON.getUsecTst());
	if(reportStatus)
		DCON.add2Broadcast(toNodechFrag(value_, DCON.getUsecTst()));
}

void NodeBitPort::statePulseSl()
{
	if(chOff && val_)
		hwPortPp->setBit(inverted); // turn off
	else
		hwPort->setBit(inverted); // turn off
	if(delay_>0)
	{
		//qDebug()<<objectName()<<"status still BUSY"<<(state_==Node::State::BUSY);
		QTimer::singleShot(delay_, this, SLOT(stateReadySl()));
	}
	else
	{
		state_=State::READY;
		//qDebug()<<objectName()<<"status READY";
		emit newValSig(value_, DCON.getUsecTst());
		if(reportStatus)
			DCON.add2Broadcast(toNodechFrag(value_, DCON.getUsecTst()));
	}
}

void NodeBitPort::setRaw()
{
	state_=State::BUSY;
	//qDebug()<<objectName()<<"status BUSY";
	//qDebug()<<"setting BitPort value"<<val_<<"on"<<adr_.toStringVerbose()<<chOff;
	if(chOff)
	{ // push/pull node
		if(!pulse)
		{ // turn off other channel
			if(val_)
				hwPort->setBit(inverted);
			else
				hwPortPp->setBit(inverted);
		}
		if(val_)
			hwPortPp->setBit(!inverted);
		else
			hwPort->setBit(!inverted);
		if(pulse>0)
			QTimer::singleShot(pulse, this, SLOT(statePulseSl()));
		else if(delay_>0)
			QTimer::singleShot(delay_, this, SLOT(stateReadySl()));
		else
		{
			state_=State::READY;
			//qDebug()<<objectName()<<"status READY";
		}
	}
	else
	{ // simple one-ended node
		hwPort->setBit(inverted?(!val_):val_);
		if(pulse>0)
			QTimer::singleShot(pulse, this, SLOT(statePulseSl()));
		else if(delay_>0)
			QTimer::singleShot(delay_, this, SLOT(stateReadySl()));
		else
		{
			state_=State::READY;
			//qDebug()<<objectName()<<"status READY";
		}
	}
}

DeviceBitPort::DeviceBitPort(const QDomElement &elem) : Device(elem)
{
	QString p=elem.attribute("port");
	bool ok=false;
	if(p.startsWith("WP"))
		basePortWp_=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid port specification <%1> at device %2").arg(p).arg(id_));
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		new NodeBitPort(nodes.item(i).toElement(), id_, this);
	}
}

pi_at_home::Result DeviceBitPort::init()
{
	if(basePortWp_<0)
		throw Ex(QString("init failed on device %1, port not set %2").arg(id_).arg(basePortWp_));
	return Device::init();
}

pi_at_home::Result DeviceBitPort::shutdown()
{
	if(basePortWp_<0)
		throw Ex(QString("shutdown failed on device %1, port not set %2").arg(id_).arg(basePortWp_));
	/* nothing to do - ~HwPort will perform shutdown
	const QObjectList &chdn=children();
	foreach(QObject *chld, chdn)
	{
		NodeBitPort *n=qobject_cast<NodeBitPort *>(chld);
		if(!n)
			continue;
		int port=basePortWp_+n->address().channel();
		pinMode(port, INPUT);
		pullUpDnControl(port, PUD_DOWN);
	}
	*/
	return Result::COMPLETED;
}
