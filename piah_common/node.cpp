#include "node.h"
#include "ui_widnode.h"
#include "command.h"
#include <QCryptographicHash>
#include "piadi.h"

Node::Unit Node::unit(const QString &unitStr)
{
	if(unitStr=="NONE")
		return Node::Unit::NONE;
	else if(unitStr=="BIT")
		return Node::Unit::BIT;
	else if(unitStr=="USER")
		return Node::Unit::USER;
	else if(unitStr=="PERCENT")
		return Node::Unit::PERCENT;
	else if(unitStr=="FRACTION")
		return Node::Unit::FRACTION;
	else if(unitStr=="COUNT")
		return Node::Unit::COUNT;
	else if(unitStr=="RADIANT")
		return Node::Unit::RADIANT;
	else if(unitStr=="STERADIANT")
		return Node::Unit::STERADIANT;
	else if(unitStr=="METER")
		return Node::Unit::METER;
	else if(unitStr=="SEC")
		return Node::Unit::SEC;
	else if(unitStr=="HZ")
		return Node::Unit::HZ;
	else if(unitStr=="TIME")
		return Node::Unit::TIME;
	else if(unitStr=="KG")
		return Node::Unit::KG;
	else if(unitStr=="NEWTON")
		return Node::Unit::NEWTON;
	else if(unitStr=="PASCAL")
		return Node::Unit::PASCAL;
	else if(unitStr=="BAR")
		return Node::Unit::BAR;
	else if(unitStr=="WATT")
		return Node::Unit::WATT;
	else if(unitStr=="JOULE")
		return Node::Unit::JOULE;
	else if(unitStr=="WATT_ISQM")
		return Node::Unit::WATT_ISQM;
	else if(unitStr=="WATTHOUR")
		return Node::Unit::WATTHOUR;
	else if(unitStr=="KELVIN")
		return Node::Unit::KELVIN;
	else if(unitStr=="DEGREE_CELSIUS")
		return Node::Unit::DEGREE_CELSIUS;
	else if(unitStr=="VOLT")
		return Node::Unit::VOLT;
	else if(unitStr=="AMPERE")
		return Node::Unit::AMPERE;
	else if(unitStr=="OHM")
		return Node::Unit::OHM;
	else if(unitStr=="VOLTAMPERE")
		return Node::Unit::VOLTAMPERE;
	else if(unitStr=="VAR")
		return Node::Unit::VAR;
	else if(unitStr=="QM")
		return Node::Unit::QM;
	else if(unitStr=="LITRE")
		return Node::Unit::LITRE;
	else if(unitStr=="LITRE_ISEC")
		return Node::Unit::LITRE_ISEC;
	else if(unitStr=="KILOMETER")
		return Node::Unit::KILOMETER;
	else if(unitStr=="DECIMETER")
		return Node::Unit::DECIMETER;
	else if(unitStr=="CENTIMETER")
		return Node::Unit::CENTIMETER;
	else if(unitStr=="MILLIMIETER")
		return Node::Unit::MILLIMIETER;
	else if(unitStr=="MICROMETER")
		return Node::Unit::MICROMETER;
	else if(unitStr=="NANOMETER")
		return Node::Unit::NANOMETER;
	else if(unitStr=="PICOMETER")
		return Node::Unit::PICOMETER;
	return Node::Unit::UNDEF;
}

QString Node::unit(const Node::Unit &unit)
{
	switch(unit)
	{
	case Unit::NONE: return QStringLiteral("NONE");
	case Unit::BIT: return QStringLiteral("BIT");
	case Unit::USER: return QStringLiteral("USER");
	case Unit::PERCENT: return QStringLiteral("PERCENT");
	case Unit::FRACTION: return QStringLiteral("FRACTION");
	case Unit::COUNT: return QStringLiteral("COUNT");
	case Unit::RADIANT: return QStringLiteral("RADIANT");
	case Unit::STERADIANT: return QStringLiteral("STERADIANT");
	case Unit::METER: return QStringLiteral("METER");
	case Unit::SEC: return QStringLiteral("SEC");
	case Unit::HZ: return QStringLiteral("HZ");
	case Unit::TIME: return QStringLiteral("TIME");
	case Unit::KG: return QStringLiteral("KG");
	case Unit::NEWTON: return QStringLiteral("NEWTON");
	case Unit::PASCAL: return QStringLiteral("PASCAL");
	case Unit::BAR: return QStringLiteral("BAR");
	case Unit::WATT: return QStringLiteral("WATT");
	case Unit::JOULE: return QStringLiteral("JOULE");
	case Unit::WATT_ISQM: return QStringLiteral("WATT_ISQM");
	case Unit::WATTHOUR: return QStringLiteral("WATTHOUR");
	case Unit::KELVIN: return QStringLiteral("KELVIN");
	case Unit::DEGREE_CELSIUS: return QStringLiteral("DEGREE_CELSIUS");
	case Unit::VOLT: return QStringLiteral("VOLT");
	case Unit::AMPERE: return QStringLiteral("AMPERE");
	case Unit::OHM: return QStringLiteral("OHM");
	case Unit::VOLTAMPERE: return QStringLiteral("VOLTAMPERE");
	case Unit::VAR: return QStringLiteral("VAR");
	case Unit::QM: return QStringLiteral("QM");
	case Unit::LITRE: return QStringLiteral("LITRE");
	case Unit::LITRE_ISEC: return QStringLiteral("LITRE_ISEC");
	case Unit::KILOMETER: return QStringLiteral("KILOMETER");
	case Unit::DECIMETER: return QStringLiteral("DECIMETER");
	case Unit::CENTIMETER: return QStringLiteral("CENTIMETER");
	case Unit::MILLIMIETER: return QStringLiteral("MILLIMIETER");
	case Unit::MICROMETER: return QStringLiteral("MICROMETER");
	case Unit::NANOMETER: return QStringLiteral("NANOMETER");
	case Unit::PICOMETER: return QStringLiteral("PICOMETER");
	default: return "UNDEF";
	}
}

Node::State Node::state(const QString &stateStr)
{
	if(stateStr=="READY")
		return Node::State::READY;
	else if(stateStr=="BUSY")
		return Node::State::BUSY;
	else if(stateStr=="LOCKED")
		return Node::State::LOCKED;
	else if(stateStr=="ERROR")
		return Node::State::ERROR;
	return Node::State::UNDEF;
}

QString Node::state(const Node::State &state)
{
	switch(state)
	{
		case State::READY:
			return "READY";
		case State::BUSY:
			return "BUSY";
		case State::LOCKED:
			return "LOCKED";
		case State::ERROR:
			return "ERROR";
		default:
			return "UNDEF";
	}
}

Node::Node(const QDomElement &elem, int device, QObject *parent, int channel) :
	QObject(parent), adr_(DCON.uid(), device, 0), tst(DCON.getUsecTst())
{
	QString name=elem.attribute("name");
#ifndef GUI_ONLY
	Device * d=qobject_cast<Device * >(parent);
	if(d)
		name=d->getPrefix()+name;
#endif
	bool ok=true;
	if(channel<0)
		channel=elem.attribute("channel").toInt(&ok);
	if(name.isEmpty() || !ok)
		throw Ex(QString("node misses name or channel <%1> %2").arg(name).arg(channel));
	setObjectName(name);
	adr_.setChannel(channel);
	adr_.setAlias(name);
	if(elem.hasAttribute("log_holdoff_ms"))
	{
		int i=elem.attribute("log_holdoff_ms").toInt(&ok);
		if(ok)
			logHoldoffUs_=i*1000;
		else
			qWarning()<<"log_holdoff_ms is not an integer"<<elem.attribute("log_holdoff_ms");
	}
	if(elem.attribute("hidden")=="true")
		isHidden_=true;
	qInfo()<<"channel"<<adr_.toStringVerbose()<<"added to"<<parent->objectName();
}

Node::Node(const QString & name, int device, int channel, QObject *parent) :
	QObject(parent), adr_(DCON.uid(), device, channel, name)
{
	setObjectName(name);
	qInfo()<<"channel"<<adr_.toStringVerbose()<<"added to"<<parent->objectName();
}

Node::~Node()
{
	if(algLocker)
		delete algLocker;
	algLocker=nullptr;
}

bool Node::doLog(qint64 currentTst)
{
	if(logHoldoffUs_<0)
		return false;
	if(!logHoldoffUs_)
		return true;
	if(logLastLog<0 || logLastLog+logHoldoffUs_<currentTst)
	{
		//qDebug()<<"dolog"<<logLastLog<<currentTst<<logHoldoffUs_;
		logLastLog=currentTst;
		return true;
	}
	//qDebug()<<"holdoff"<<logLastLog<<currentTst<<logHoldoffUs_;
	return false;
}

#ifndef GUI_ONLY
/*QString Node::fullAlias() const
{
	Device * d=qobject_cast<Device * >(parent());
	if(!d)
		return adr_.alias();
	return d->getPrefix()+adr_.alias();
}*/
#endif

Fragment *Node::toFragment() const
{
	Fragment * fr=new Fragment("node");
	fr->setAttribute("type", type());
	fr->setAttribute("addr", adr_.toString());
	fr->setAttribute("val", serializeValue(value_));
	fr->setAttribute("sim", simulating?"true":"false");
	fr->setAttribute("hidden", isHidden_?"true":"false");
	fr->setAttribute("name", objectName());
	fr->setAttribute("state", Node::state(state()));
	if(algLocker)
		fr->setAttribute("lock", algLocker->toString());
	return fr;
}

Fragment *Node::toNodechFrag(const pi_at_home::NodeValue &newValue, qint64 timestamp) const
{
	Fragment *fr=new Fragment("nodech");
	fr->setAttribute("addr", address().toString());
	fr->setAttribute("val", serializeValue(newValue));
	fr->setAttribute("tst", QString::number(timestamp));
	fr->setAttribute("state", Node::state(state()));
	return fr;
}

Result Node::init()
{
	return Result::COMPLETED;
}

void Node::ping()
{
	//qDebug()<<"pinging"<<value_<<tst;
	emit newValSig(value_, tst);
}

pi_at_home::Node::TokenType Node::lock(const QString uid, int device, const QString alias)
{
	if(algLocker)
	{ // theoreticaly 0 could be a true hash, but we take that risk...
		return 0;
	}
	TokenType res=0;
	int shifter=0;
	algLocker=new NodeAddress(uid, device, 0, alias);
	QString input=algLocker->toString()+QDateTime::currentDateTime().toString("yyyyMMddHHmmsszzz");
	QByteArray rawHash=QCryptographicHash::hash(input.toUtf8(), QCryptographicHash::Sha256);
	foreach(char c, rawHash)
	{
		res+=((TokenType)c<<shifter);
		//qDebug()<<(TokenType)c<<shifter<<res;
		shifter+=8;
		if(shifter==64)
			shifter=0;
	}
	lockHash=res;
	if(lockHash)
		qInfo()<<objectName()<<"locked to"<<algLocker->toString(); // QString("%1").arg(lockHash , 0, 16)
	else
		qCritical()<<"ZERO HASH when locking"<<objectName()<<"to"<<algLocker->toString();
	return lockHash;
}

bool Node::unlock(pi_at_home::Node::TokenType token)
{
	if(algLocker)
	{
		if(token!=lockHash)
			return false;
		lockHash=0;
		delete algLocker;
		algLocker=nullptr;
	}
	lockHash=0; // just be sure...
	return true;
}

void Node::simulateValue(const pi_at_home::NodeValue &value)
{
	value_=value;
	simulating=true;
}

bool Node::tokenValid(Node::TokenType token)
{
	return algLocker?(token==lockHash):true;
}

