#include "nodeswitchport.h"
#include "nodeswitchport_fac.h"
#include <time.h>
#include <QTimerEvent>
#include "gpiowrapper.h"
#include "command.h"
#include "piadi.h"

NodeSwitchPort::NodeSwitchPort(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent)
{
	if(elem.attribute("inverted")=="true")
		inverted=true;
	bool intOk=false;
	if(elem.hasAttribute("hold_on"))
	{
		int intVal=elem.attribute("hold_on").toInt(&intOk);
		if(intOk && intVal>=0)
			holdOn=intVal;
		else
			qWarning()<<"invalid hold_on"<<elem.attribute("hold_on");
	}
	if(elem.hasAttribute("hold_off"))
	{
		int intVal=elem.attribute("hold_off").toInt(&intOk);
		if(intOk && intVal>=0)
			holdOff=intVal;
		else
			qWarning()<<"invalid hold_off"<<elem.attribute("hold_off");
	}
	if(elem.hasAttribute("ignore_on"))
	{
		int intVal=elem.attribute("ignore_on").toInt(&intOk);
		if(intOk && intVal>=0)
			ignoreOn=intVal;
		else
			qWarning()<<"invalid ignore_on"<<elem.attribute("ignore_on");
	}
	if(elem.hasAttribute("ignore_off"))
	{
		int intVal=elem.attribute("ignore_off").toInt(&intOk);
		if(intOk && intVal>=0)
			ignoreOff=intVal;
		else
			qWarning()<<"invalid ignore_off"<<elem.attribute("ignore_off");
	}
	if(elem.hasAttribute("pull"))
		pull=elem.attribute("pull");
}

const QString &NodeSwitchPort::type() const
{
	return typeName;
}

Result NodeSwitchPort::init()
{
	DeviceSwitchPort *d=qobject_cast<DeviceSwitchPort*>(parent());
	if(!d)
		throw Ex(QString("init failed on %1").arg(adr_.toStringVerbose()));
	int bp=d->basePortWp();
	GpioWrapper::PullupMode pullm=GpioWrapper::PullupMode::NONE;
	if(pull=="up")
		pullm=GpioWrapper::PullupMode::PULLUP;
	else if(pull=="down")
		pullm=GpioWrapper::PullupMode::PULLDOWN;
	else if(!pull.isNull() && pull!="none")
		qWarning()<<"pull mode"<<pull<<"unknown, using none"<<adr_.toStringVerbose();
	hwPort=DCON.io()->createInPort(bp+adr_.channel(), inverted, pullm, holdOn, holdOff);
	if(!hwPort)
		throw Ex(QString("init hwPort failed on %1").arg(adr_.toStringVerbose()));
	connect(hwPort, &HwInPort::updateValSig, this, &NodeSwitchPort::updateValSl);
	// val_=hwPort->readBit(); doesn't make sense since the worker thread may not be started yet
	// value_=val_;
	state_=State::BUSY; // will be set to READY in updateValSl
	tst=DCON.getUsecTst();
	//qDebug()<<"NodeSwitchPort::init"<<value_<<val_<<tst;
	// no-one connected yet... emit newValSig(value_, tst);
	return Result::COMPLETED;
}

void NodeSwitchPort::readValue(bool &valueOut)
{
	valueOut=val_;
}

void NodeSwitchPort::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	Q_UNUSED(token);
	newValSl(value);
}

void NodeSwitchPort::ping()
{
	//qDebug()<<"pinging"<<value_<<tst;
	if(state_!=State::READY)
		return; // usually BUSY - will signal when READY
	emit newValSig(value_, tst);
}

void NodeSwitchPort::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		relevant.setAttribute("val", val_?"true":"false");
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeSwitchPort"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeSwitchPort::newValSl(const NodeValue &newValue)
{
	Q_UNUSED(newValue);
	qCritical()<<"setValue/newValSl on NodeSwitchPort";
}

void NodeSwitchPort::timerEvent(QTimerEvent *event)
{
	killTimer(event->timerId());
	if(event->timerId()!=ignoreTimer)
	{
		qWarning()<<"Spurious timer event in NodeSwitchPort"<<event->timerId();
		return; // may be an outdated event
	}
	ignoreTimer=0;
	// now we may report the new value with the original timestamp
	val_=valStore_;
	value_=val_;
	state_=State::READY;
	tst=ignoreTst;
	//qDebug()<<"NodeSwitchPort::timerEvent GPIO signal after ignore"<<objectName()<<val_<<tst;
	emit newValSig(value_, tst);
}

void NodeSwitchPort::updateValSl(bool newVal, qint64 timestamp)
{
	//if(adr_.channel()==6)
	//qDebug()<<"NodeSwitchPort::updateValSl GPIO"<<objectName()<<newVal<<timestamp<<normOp;
	if(ignoreTimer)
	{
		//qDebug()<<"cancel timer"<<ignoreTimer<<newVal;
		killTimer(ignoreTimer);
		ignoreTimer=0;
		ignoreTst=0;
	}
	if(newVal && normOp && ignoreOn)
	{
		ignoreTst=timestamp;
		ignoreTimer=startTimer(ignoreOn);
		state_=State::BUSY;
		valStore_=newVal;
		//qDebug()<<"going true, start timer"<<ignoreTimer;
		return; // do not change the value and signal yet
	}
	else if(!newVal && normOp && ignoreOff)
	{
		ignoreTst=timestamp;
		ignoreTimer=startTimer(ignoreOff);
		valStore_=newVal;
		state_=State::BUSY;
		//qDebug()<<"going false, start timer"<<ignoreTimer;
		return; // do not change the value and signal yet
	}
	// no ignore-interval applies - signal now
	state_=State::READY;
	normOp=true;
	tst=timestamp;
	if(newVal!=val_)
	{
		val_=newVal;
		value_=val_;
		//qDebug()<<"NodeSwitchPort::updateValSl GPIO changed"<<objectName()<<newVal<<timestamp;
		emit newValSig(value_, timestamp);
	}
}


DeviceSwitchPort::DeviceSwitchPort(const QDomElement &elem) : Device(elem)
{
	QString p=elem.attribute("port");
	bool ok=false;
	if(p.startsWith("WP"))
		basePortWp_=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid port specification <%1> at device %2").arg(p).arg(id_));
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		new NodeSwitchPort(nodes.item(i).toElement(), id_, this);
	}
}

pi_at_home::Result DeviceSwitchPort::init()
{
	if(basePortWp_<0)
		throw Ex(QString("init failed on device %1, port not set %2").arg(id_).arg(basePortWp_));
	return Device::init();
}

pi_at_home::Result DeviceSwitchPort::shutdown()
{
	if(basePortWp_<0)
		throw Ex(QString("shutdown failed on device %1, port not set %2").arg(id_).arg(basePortWp_));
/* nothing to do - ~HwPort will perform shutdown
	QList<NodeSwitchPort * > chldr=findChildren<NodeSwitchPort * >();
	foreach(NodeSwitchPort * n, chldr)
	{
		int port=basePortWp+n->address().channel();
		pinMode(port, INPUT);
		pullUpDnControl(port, PUD_UP);
	}
*/
	return Result::COMPLETED;
}
