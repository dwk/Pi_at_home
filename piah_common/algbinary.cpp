#include "algbinary.h"
#include "algbinary_fac.h"
#include "node.h"
#include "command.h"
#include "nodealg.h"
#include "piadi.h"

AlgBinary::AlgBinary(const QDomElement &elem) :
	Algorithm(elem)
{
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		QString tn=e.attribute(QStringLiteral("type_name"));
		if(tn==QStringLiteral("input"))
			ndNmInput=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("bit"))
		{
			Node *nd=new NodeBool(e, id_, this);
			int ch=nd->address().channel();
			if(ch<0 || ch>31)
			{
				qCritical()<<objectName()<<"AlgBinary bit value out of range"<<ch;
				delete nd;
			}
			else
				ndBits.insert(NodeMP::value_type(ch, nd));
		}
		else if(tn==QStringLiteral("sign"))
			ndSign=new NodeBool(e, id_, this);
	}
}

void AlgBinary::execute()
{
	if(algState!=AlgState::RUNNING && algState!=AlgState::STARTUP)
		return;
	if(ndSign)
		ndSign->setValue(NodeValue(val<0));
	int b=0;
	unsigned int uc=abs(val);
	for(auto it=ndBits.begin(); it!=ndBits.end(); ++it)
	{
		if(b<it->first)
			uc>>=(it->first-b);
		b=it->first;
		it->second->setValue(uc & 1);
	}
}

pi_at_home::Result AlgBinary::preExec()
{
	if(algState!=AlgState::STARTUP )
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	if(!ndNmInput.isEmpty())
	{
		ndInput=DCON.findNode(ndNmInput, false);
		if(ndInput)
		{
			connect(ndInput, &Node::newValSig, this, &AlgBinary::valCaptureSl);
			val=ndInput->value().toInt();
		}
		else
			qCritical()<<objectName()<<"AlgBinary no input"<<ndNmInput;
	}
	execute();
	return Result::COMPLETED;
}

pi_at_home::Result AlgBinary::postExec()
{
	return Result::COMPLETED;
}

void AlgBinary::valCaptureSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	//qDebug()<<"valCaptureSl"<<inChannel<<newValue;
	val=newValue.toInt();
	execute();
}
