#ifndef NODELCD84x48_FAC_H
#define NODELCD84x48_FAC_H

#include "nodelcd84x48_nm.h"

namespace
{
pi_at_home::Device * Creator(const QDomElement &elem)
{
	return new pi_at_home::DeviceLcd84x48(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, Creator);
}

#endif // NODELCD84x48_FAC_H
