#include "algsolarprotector.h"
#include "algsolarprotector_fac.h"
#include <QTimer>
#include <QMutexLocker>
#include <QTimerEvent>
#include "node.h"
#include "nodebitport.h"
#include "piadi.h"

AlgSolarProtector::AlgSolarProtector(const QDomElement &elem) :
	Algorithm(elem)
{
	ndSWTvalveStateNm=elem.attribute("Tvalvestate");
	ndSWPumpEarthcStateNm=elem.attribute("pumpearthstate");
	bool ok;
	double dblval=0.;
	dblval=elem.attribute("def_max_Troofc").toDouble(&ok);
	if(ok)
		switchTRoofc=dblval;
	dblval=elem.attribute("def_min_Tfresh").toDouble(&ok);
	if(ok)
		switchTFreshTop=dblval;
	int intval=0;
	intval=elem.attribute("def_earthSleepHoldoffS").toInt(&ok);
	if(ok)
		earthSleepHoldoffMs=intval*1000;

	ndSWSctrlRef=new NodeBool(namePrefix+"sw_ctrl_ref", id_, 0, this);
	ndSWTvalve=new NodeBool(namePrefix+"sw_Tvalve", id_, 1, this);
	ndSWPumpEarthc=new NodeBool(namePrefix+"sw_pumpearthc", id_, 2, this);
	ndState=new NodeStr(namePrefix+"state", id_, 10, this);
	ndSwitchTRoofc=new NodeDbl(namePrefix+"max_Troofc", id_, 11, this);
	ndSwitchTRoofc->setDefaultValue(NodeValue(switchTRoofc));
	connect(ndSwitchTRoofc, &Node::newValSig, this, &AlgSolarProtector::nvSwitchTempRoofSl);
	ndSwitchTFreshTop=new NodeDbl(namePrefix+"min_Tfresh", id_, 12, this);
	ndSwitchTFreshTop->setDefaultValue(NodeValue(switchTFreshTop));
	connect(ndSwitchTFreshTop, &Node::newValSig, this, &AlgSolarProtector::nvSwitchTempResSl);
	ndForceWater=new NodeBool(namePrefix+"force_WATER", id_, 13, this);
	ndForceWater->setDefaultValue(NodeValue(forceWater));
	connect(ndForceWater, &Node::newValSig, this, &AlgSolarProtector::nvForceWaterSl);
	ndForceEarth=new NodeBool(namePrefix+"force_EARTH", id_, 14, this);
	ndForceEarth->setDefaultValue(NodeValue(forceEarth));
	connect(ndForceEarth, &Node::newValSig, this, &AlgSolarProtector::nvForceEarthSl);

	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		QString tn=e.attribute(QStringLiteral("type_name"));
		//qDebug()<<"AlgSolarProtector conn"<<tn<<e.attribute(QStringLiteral("name"));
		if(tn==QStringLiteral("Troofc"))
			ndTRoofcNm=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("Tfreshtop"))
			ndTFreshTopNm=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("pumpsolar"))
			ndPumpSolarNm=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("pumpearth"))
			ndPumpEarthNm=e.attribute(QStringLiteral("name"));
	}
	if(ndTRoofcNm.isEmpty() || ndTFreshTopNm.isEmpty() || ndPumpSolarNm.isEmpty() || ndPumpEarthNm.isEmpty())
		qWarning()<<"AlgSolarProtector:: Missing node name?"<<ndTRoofcNm << ndTFreshTopNm << ndPumpSolarNm << ndPumpEarthNm;

	lastSolarOn=QDateTime::currentDateTime();
}

AlgSolarProtector::~AlgSolarProtector()
{
}

void AlgSolarProtector::execute()
{
	if(algState!=AlgState::RUNNING && algState!=AlgState::STARTUP)
		return;
	//qDebug()<<"AlgSolar stateAction"<<(int)opMode;
	QMutexLocker lock(&exeLock);
	bool busyDelay=false;
	if(opMode==OpMode::EARTH_STANDBY && (forceEarth || forceWater))
	{ // we need normal mode processing in addition to this change
		qInfo()<<"forced wakeup from EARTH_STANDBY"<<forceEarth<<forceWater;
		toMode(OpMode::EARTH);
	}
	switch(opMode)
	{
		case OpMode::WATER:
		{
			if(forceWater)
				break;
			if(tRoofc<-300. && !forceEarth)
			{ // not yet initilized
				qWarning()<<objectName()<<"waiting for tRoofc being updated";
				return;
			}
			if(tRoofc>switchTRoofc || forceEarth)
				toMode(OpMode::TO_EARTH_PUMP);
			break;
		}
		case OpMode::TO_EARTH_PUMP:
		{
			switch(ndSWPumpEarthc->state())
			{
				case Node::State::READY:
				{
					ndSWSctrlRef->setValue(true);
					ndSWTvalve->setValue(true);
					toMode(OpMode::TO_EARTH_TVALVE);
					break;
				}
				case Node::State::BUSY:
					busyDelay=true;
					break;
				default:
					toMode(OpMode::ERROR);
					break;
			}
			break;
		}
		case OpMode::TO_EARTH_TVALVE:
		{
			switch(ndSWTvalve->state())
			{
				case Node::State::READY:
				{
					toMode(OpMode::EARTH);
					break;
				}
				case Node::State::BUSY:
					busyDelay=true;
					break;
				default:
					toMode(OpMode::ERROR);
					break;
			}
			break;
		}
		case OpMode::EARTH:
		case OpMode::EARTH_STANDBY: // need to check tFreshTop - it could go under switchTFreshTop while in EARTH_STANDBY
		{
			if(forceEarth)
				break;
			if(tFreshTop<-300. && !forceWater)
			{ // not yet initilized
				qWarning()<<objectName()<<"waiting for tFreshTop being updated";
				return;
			}
			if(tFreshTop<switchTFreshTop || forceWater)
			{
				ndSWSctrlRef->setValue(false);
				ndSWTvalve->setValue(false);
				toMode(OpMode::TO_WATER_TVALVE);
				busyDelay=true; // trigger next exec - state change of node will not trigger signal
				break;
			}
			//if(!pumpSolar) no, this creates a lot of unneccessary timer events starting a timer at pump-off is sufficient
			//{
			//	qDebug()<<"starting timer for EARTH_STANDBY because of state change";
			//	QTimer::singleShot(earthSleepHoldoffMs, this, &AlgSolarProtector::earthPumpSleepCheck);
			//}
			break;
		}
		case OpMode::TO_WATER_TVALVE:
		{
			switch(ndSWTvalveState->state())
			{
				case Node::State::READY:
				{
					toMode(OpMode::TO_WATER_PUMP);
					busyDelay=true; // trigger next exec - state change of node will not trigger signal
					break;
				}
				case Node::State::BUSY:
					busyDelay=true;
					break;
				default:
					toMode(OpMode::ERROR);
					break;
			}
			break;
		}
		case OpMode::TO_WATER_PUMP:
		{
			switch(ndSWPumpEarthcState->state())
			{
				case Node::State::READY:
				{
					toMode(OpMode::WATER);
					break;
				}
				case Node::State::BUSY:
					busyDelay=true;
					break;
				default:
					toMode(OpMode::ERROR);
					break;
			}
			break;
		}
		case OpMode::UNDEF:
		{
			// should not happen
			qCritical()<<objectName()<<"execute while in UNDEF mode - try to recover to WATER";
			ndSWTvalve->setValue(false);
			ndSWSctrlRef->setValue(false);
			ndSWPumpEarthc->setValue(pumpEarth);
			toMode(OpMode::WATER);
			return;
		}
		case OpMode::ERROR:
		{ // try enter safe state
			qCritical()<<objectName()<<"execute while in ERROR mode - try safe state";
			ndSWSctrlRef->setValue(false);
			ndSWTvalve->setValue(false);
			ndSWPumpEarthc->setValue(pumpEarth);
			algState=AlgState::ERROR;
			return;
		}
		case OpMode::SHUTDOWN: // nothing to do in this case
			break;
	}
	if(busyDelay)
	{
		qWarning()<<objectName()<<"mode busy timer"<<(int)opMode;
		QTimer::singleShot(1111, this, &AlgSolarProtector::execute);
	}
	earthPumpSet();
}

bool AlgSolarProtector::safeState()
{
	if(opMode==OpMode::WATER || opMode==OpMode::UNDEF || opMode==OpMode::ERROR)
		return true;
	if(opMode==OpMode::SHUTDOWN)
	{
		if(ndSWTvalve->state()==Node::State::BUSY || ndSWTvalve->state()==Node::State::BUSY || ndSWTvalve->state()==Node::State::BUSY)
			return false; // wait...
		return true; // no reason for waiting in error state
	}
	return false; // we are still in some operating mode
}

void AlgSolarProtector::nvTRoofcSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool ok=false;
	double dval=newValue.toDouble(&ok);
	if(ok)
	{
		tRoofc=dval;
		execute();
	}
	else
		qWarning()<<"input_Troofc has no valid value, ignored"<<newValue.toString();
}

void AlgSolarProtector::nvTFreshTopSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool ok=false;
	double dval=newValue.toDouble(&ok);
	if(ok)
	{
		tFreshTop=dval;
		execute();
	}
	else
		qWarning()<<"input_Tfreshtop has no valid value, ignored"<<newValue.toString();
}

void AlgSolarProtector::nvPumpSolarSl(const NodeValue &newValue, qint64 timestamp)
{ // solar pump started or stopped
	Q_UNUSED(timestamp)
	bool newval=newValue.toBool(); // intentionally converting from int to bool
	if(pumpSolar && !newval)
	{
		lastSolarOn=QDateTime::currentDateTime();
		qDebug()<<"starting timer for EARTH_STANDBY because of solar pump off";
		QTimer::singleShot(earthSleepHoldoffMs, this, &AlgSolarProtector::earthPumpSleepLocking);
	}
	pumpSolar=newval;
	if(pumpSolar) // maybe we should turn on the earth pump
		earthPumpSleepLocking();
}

void AlgSolarProtector::nvPumpEarthSl(const NodeValue &newValue, qint64 timestamp)
{ // external request for earth pump changed
	Q_UNUSED(timestamp)
	pumpEarth=newValue.toBool();
	earthPumpSetLocking();
}

void AlgSolarProtector::nvSwitchTempRoofSl(const NodeValue & newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool ok=false;
	double dval=newValue.toDouble(&ok);
	if(ok)
	{
		switchTRoofc=dval;
		qInfo()<<objectName()<<"max_Troofc updated to"<<switchTRoofc;
		if(switchTRoofc<switchTFreshTop+10.)
		{
			switchTFreshTop=switchTRoofc-10.;
			ndSwitchTFreshTop->setValue(NodeValue(switchTFreshTop));
			qWarning()<<objectName()<<"min_Tfresh adjusted to"<<switchTFreshTop<<"(minimum hysteresis requiered is 10K)";
		}
		execute();
	}
	else
		qWarning()<<objectName()<<"invalid value for max_Troofc"<<newValue.toString();
}

void AlgSolarProtector::nvSwitchTempResSl(const NodeValue & newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool ok=false;
	double dval=newValue.toDouble(&ok);
	if(ok)
	{
		switchTFreshTop=dval;
		qInfo()<<objectName()<<"min_Tfresh updated to"<<switchTFreshTop;
		if(switchTRoofc<switchTFreshTop+10.)
		{
			switchTRoofc=switchTFreshTop+10.;
			ndSwitchTRoofc->setValue(NodeValue(switchTRoofc));
			qWarning()<<objectName()<<"max_Troofc adjusted to"<<switchTRoofc<<"(minimum hysteresis requiered is 10K)";
		}
		execute();
	}
	else
		qWarning()<<objectName()<<"invalid value for min_Tfresh"<<newValue.toString();
}

void AlgSolarProtector::nvForceEarthSl(const pi_at_home::NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	if(newValue.canConvert<bool>())
	{
		forceEarth=newValue.toBool();
		qInfo()<<objectName()<<"received force_EARTH"<<forceEarth;
		execute();
	}
	else
		qWarning()<<objectName()<<"invalid value for force_EARTH"<<newValue.toString();
}

void AlgSolarProtector::nvForceWaterSl(const pi_at_home::NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	if(newValue.canConvert<bool>())
	{
		forceWater=newValue.toBool();
		qInfo()<<objectName()<<"received force_WATER"<<forceWater;
		execute();
	}
	else
		qWarning()<<objectName()<<"invalid value for force_WATER"<<newValue.toString();
}

void AlgSolarProtector::earthPumpSetLocking()
{
	QMutexLocker lock(&exeLock);
	earthPumpSet();
}

void AlgSolarProtector::earthPumpSet()
{ // enter only with exeLock locked
	if(ndSWPumpEarthc->state()==Node::State::BUSY)
	{
		QTimer::singleShot(1111, this, &AlgSolarProtector::earthPumpSetLocking);
		return;
	}
	if(pumpEarth ||
			(opMode==OpMode::TO_EARTH_PUMP || opMode==OpMode::TO_EARTH_TVALVE || opMode==OpMode::EARTH || opMode==OpMode::TO_WATER_TVALVE) )
		ndSWPumpEarthc->setValue(NodeValue(true));
	else
		ndSWPumpEarthc->setValue(NodeValue(false));
}

void AlgSolarProtector::earthPumpSleepLocking()
{ // entered only with exeLock unlocked! (e.g. timer)
	QMutexLocker lock(&exeLock);
	if(opMode==OpMode::EARTH)
	{
		if(!pumpSolar)
		{
			if(lastSolarOn.msecsTo(QDateTime::currentDateTime())>=earthSleepHoldoffMs-2000) // timer is sometimes slightly early -> transition to standby missed
			{
				qInfo()<<"entering EARTH_STANDBY";
				toMode(OpMode::EARTH_STANDBY);
			}
			else
				qDebug()<<"EARTH_STANDBY not activated because pump was active at "<<lastSolarOn.toString();
		} // else everything's fine
		else
			qDebug()<<"EARTH_STANDBY not available when solar pump active";
		earthPumpSet();
	}
	else if(opMode==OpMode::EARTH_STANDBY)
	{
		if(pumpSolar)
		{
			qInfo()<<"waking up from EARTH_STANDBY";
			toMode(OpMode::EARTH);
		} // else everything's fine
		earthPumpSet();
	}
	else
	{ // earth pump sleep check but the requirements are not met anymore
		qDebug()<<"EARTH_STANDBY not available in"<<(int)opMode<<pumpSolar;
	}
}

Result AlgSolarProtector::preExec()
{
	if(algState!=AlgState::STARTUP)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	opMode=OpMode::UNDEF;
	ndState->setValue(QString("startup"));
	bool ndTRoofcOk=false, ndTFreshTopOk=false;
	// roof collector temp
	Node * ndTRoofc=DCON.findNode(ndTRoofcNm, false);
	if(ndTRoofc)
	{
		connect(ndTRoofc, &Node::newValSig, this, &AlgSolarProtector::nvTRoofcSl);
		if(ndTRoofc->state()==Node::State::READY)
		{
			tRoofc=ndTRoofc->value().toDouble(&ndTRoofcOk);
			if(!ndTRoofcOk)
			{
				algState=AlgState::ERROR;
				qCritical()<<"input_Troofc has no valid value"<<ndTRoofc->value();
				return Result::FAILED;
			}
		}
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<"cannot find input_Troofc"<<ndTRoofcNm;
		return Result::FAILED;
	}
	// reservoir temp
	Node * ndTFreshTop=DCON.findNode(ndTFreshTopNm, false);
	if(ndTFreshTop)
	{
		connect(ndTFreshTop, &Node::newValSig, this, &AlgSolarProtector::nvTFreshTopSl);
		if(ndTFreshTop->state()==Node::State::READY)
		{
			tFreshTop=ndTFreshTop->value().toDouble(&ndTFreshTopOk);
			if(!ndTFreshTopOk)
			{
				algState=AlgState::ERROR;
				qCritical()<<"input_Tfreshtop has no valid value"<<ndTFreshTop->value();
				return Result::FAILED;
			}
		}
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<"cannot find input_Tfreshtop"<<ndTFreshTopNm;
		return Result::FAILED;
	}
	// solar pump on / off
	Node * ndPumpSolar=DCON.findNode(ndPumpSolarNm, false);
	if(ndPumpSolar)
	{
		connect(ndPumpSolar, &Node::newValSig, this, &AlgSolarProtector::nvPumpSolarSl);
		pumpSolar=ndPumpSolar->value().toBool();
		/* if(!ok)
		{
			algState=AlgState::ERROR;
			qCritical()<<"input_pumpsolar has no valid value"<<ndPumpSolar->value();
			return Result::FAILED;
		}*/
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<"cannot find input_pumpsolar"<<ndPumpSolarNm;
		return Result::FAILED;
	}
	// earth pump on / off request
	Node * ndPumpEarth=DCON.findNode(ndPumpEarthNm, false);
	if(ndPumpEarth)
	{
		connect(ndPumpEarth, &Node::newValSig, this, &AlgSolarProtector::nvPumpEarthSl);
		pumpEarth=ndPumpEarth->value().toBool();
		/* if(!ok)
		{
			algState=AlgState::ERROR;
			qCritical()<<"input_pumpearth has no valid value"<<ndPumpEarth->value();
			return Result::FAILED;
		}*/
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<"cannot find input_pumpearth"<<ndPumpEarthNm;
		return Result::FAILED;
	}
	if(ndSWTvalve->state()!=Node::State::READY || ndSWSctrlRef->state()!=Node::State::READY || ndSWPumpEarthc->state()!=Node::State::READY)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<" output nodes not ready";
		return Result::FAILED;
	}
	if( (ndTRoofc->state()!=Node::State::BUSY && ndTRoofc->state()!=Node::State::READY) ||
		(ndTFreshTop->state()!=Node::State::BUSY && ndTFreshTop->state()!=Node::State::READY) ||
		(ndPumpEarth->state()!=Node::State::BUSY && ndPumpEarth->state()!=Node::State::READY) )
	{
		algState=AlgState::ERROR;
		toMode(OpMode::ERROR);
		qCritical()<<objectName()<<"invalid stateon input node during init";
		return Result::FAILED;
	}
	ndSWTvalveState=DCON.findNode(ndSWTvalveStateNm);
	ndSWPumpEarthcState=DCON.findNode(ndSWPumpEarthcStateNm);
	if(!ndSWTvalveState || !ndSWPumpEarthcState)
	{
		algState=AlgState::ERROR;
		toMode(OpMode::ERROR);
		qCritical()<<objectName()<<"state info-nodes not found"<<ndSWTvalveStateNm<<ndSWPumpEarthcStateNm;
		return Result::FAILED;
	}
	// startup default is like WATER
	ndSWTvalve->setValue(false);
	ndSWSctrlRef->setValue(false);
	ndSWPumpEarthc->setValue(pumpEarth);
	toMode(OpMode::WATER);
	if(ndTRoofcOk && ndTFreshTopOk)
		execute(); // would not get newValSig - will not happen for TADLBus nodes anyway...
	algState=AlgState::RUNNING;
	qInfo()<<objectName()<<"initializing with max roofc"<<switchTRoofc<<"min freshtop"<<switchTFreshTop;
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

Result AlgSolarProtector::postExec()
{
	if(opMode==OpMode::WATER)
		return Result::COMPLETED; // do not enter op-mode SHUTDOWN, we are already there!
	qInfo()<<objectName()<<"shutdown: fast to WATER";
	ndSWPumpEarthc->setValue(false);
	ndSWSctrlRef->setValue(false);
	ndSWTvalve->setValue(false);
	opMode=OpMode::SHUTDOWN;
	return Result::PENDING;
}

void AlgSolarProtector::toMode(AlgSolarProtector::OpMode newMode)
{
	if(opMode==newMode)
		return;
	opMode=newMode;
	qInfo()<<objectName()<<"switch to"<<(int)opMode<<tRoofc<<tFreshTop<<switchTRoofc<<switchTFreshTop<<pumpSolar<<pumpEarth<<forceWater<<forceEarth;
	switch(opMode)
	{
		case OpMode::UNDEF:
			ndState->setValue(QString("UNDEF"));
			break;
		case OpMode::WATER:
			ndState->setValue(QString("WATER"));
			break;
		case OpMode::TO_EARTH_PUMP:
			ndState->setValue(QString("TO_EARTH_PUMP"));
			break;
		case OpMode::TO_EARTH_TVALVE:
			ndState->setValue(QString("TO_EARTH_TVALVE"));
			break;
		case OpMode::EARTH:
			ndState->setValue(QString("EARTH"));
			break;
		case OpMode::EARTH_STANDBY:
			ndState->setValue(QString("EARTH_STANDBY"));
			break;
		case OpMode::TO_WATER_TVALVE:
			ndState->setValue(QString("TO_WATER_TVALVE"));
			break;
		case OpMode::TO_WATER_PUMP:
			ndState->setValue(QString("TO_WATER_PUMP"));
			break;
		case OpMode::ERROR:
			ndState->setValue(QString("ERROR"));
			break;
		case OpMode::SHUTDOWN:
			ndState->setValue(QString("SHUTDOWN"));
			break;
	}
}

