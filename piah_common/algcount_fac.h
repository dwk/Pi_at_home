#ifndef ALGCOUNT_FAC_H
#define ALGCOUNT_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgCount(elem);
}
const QString typeName("Count");
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGCOUNT_FAC_H
