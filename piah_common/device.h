#ifndef DEVICE_H
#define DEVICE_H

#include "piadh.h"
#include "enum_result.h"
#include <QObject>
#include <QDomElement>

namespace pi_at_home
{
// DOC MOVED TO ARCHITECTURE.ODT
class Device : public QObject
{
	Q_OBJECT
public:
	explicit Device(const QDomElement &elem);
	explicit Device(int id, QObject * parent =nullptr);
	int id() const {return id_;}
	virtual Result init();
	virtual Result shutdown() =0; //{return Result::COMPLETED;}
	int pollMultiplier() {if (pollMult>=0) return pollMult; pollMult=0; return -1;} // during startup it should be used only by DataConD
	void pollLoop();
	const QString & getPrefix() const {return namePrefix;}
protected:
	int id_=0, pollMult=0;
	QString namePrefix;
};
typedef std::map<int, Device* > DeviceMP;

class DeviceFactory
{
public:
	typedef Device * (*DeviceCreator)(const QDomElement &);
	static bool registerDevice(const QString & typeId, DeviceCreator creator);
	static Device * produce(const QDomElement &elem);
private:
	typedef std::map< QString, DeviceCreator> DeviceCreatorMP;
	static DeviceCreatorMP deviceCreatorMP;
};

}
#endif // DEVICE_H
