#ifndef ALGTIMER_H
#define ALGTIMER_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"
#include "nodealg.h"

namespace pi_at_home
{
/* Activate an output node with a given time pattern.
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="Timer"			fixed
 *	timerN="x"				N = 0 | positive int, x = delay in ms if integer or hh:mm:ss, which is translated with msecsSinceStartOfDay()
 *	delay_style="x"			x = absolute | incremental | time_of_day; how to interpret timerN values
 *								absolute: ms since alg start (or reset), loop duration = largest (=last) timer value
 *								incremental: ms since last timer-event, loop duration = sum of all timer values
 *								time_of_day: ms since 0:00 local time, loop duration = 24h
 * optional attributes
 *	runonce="b"				b = true | false; if true stop after the first complete loop; default false
 *	update_on_init="b"		b = true | false; if true the ouput node is set during init, if false
 *								output is not touched until the first timer event; default true
 *	op="x"					x = pulse | toggle; default pulse
 *	pulse_duration="x"		x = pulse duration in milliseconds; default 500
 * created nodes
 *  <prefix>out			bool	channel 0, timer pattern
 *  <prefix>state		string	channel 1, some info for humans
 *  <prefix>reset		bool	channel 2, reset the timer, only avaliable for delay_style absolute and incremental
 *
 * <prefix>out will be hidden if pulse_duration<=2*broadcast_ms or if in toggle mode and at least one delay is
 * <=2*broadcast_ms. Will never be hidden for time_of_day.
 * <prefix>state will be never be hidden but will receive reduced (one delay is <=2*broadcast_ms) or no (cycle
 * is <=2*broadcast_ms) updates from normal operation.
 *
 * Generates timer events at timerN ms (absolute style) or timerN ms after timer(N-1) (incremental style).
 * Timer loops automatically to timer0 after the highest timer event triggered, except runonce is set true.
 * By default the alg starts with output inactive.
 * If delay_style="absolute" make sure that values are strictly ascending.
 * Initialisation is not considered a timer-event.
 *
 * op="pulse" creates an active pulse for every timer event. If pulses overlap output stays active uniterrupted.
 *
 * op="toggle" every timer event toggles the output. If you want to start the sequence with active output use
 * timer0="0". This will not create an actual timer event but invert the sequence, thus starting
 * active. If you have an odd number of timer events in the loops the last transition is supressed because the
 * loop always start with default polarity. (Warning during startup)
 *
 * Within one loop timer events may fire later than expected (non-realtime) and these lapses add up.
 * The first delay of the following loop is adjusted so that the global timing error is minimized. If the lapse
 * is more than the first timer delay, a warning is logged and no adjustment is made.
*/
class AlgTimer : public Algorithm
{
	Q_OBJECT
public:
	enum class BaseOp{ UNDEF=0, UNITY, AND, OR, PARITY, CONSTANT};
	explicit AlgTimer(const QDomElement &elem);
	virtual ~AlgTimer() {}
	virtual void execute();
	virtual bool safeState() {return true;}
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp, int ch);
protected:
	typedef std::vector<qint64> Times;
	virtual Result preExec();
	virtual Result postExec();
	void timerEvent(QTimerEvent *event);
	void startMainTimer(qint64 tlaps); // negative tlaps = timer is early, not used for timeofday
	void updateStatus(bool forceUpdate=false);
	QString ms2Timeofday(qint64 ms);
	void initTimes();
	static QString statusTempl, valueTempl;
	Times times;
	qint64 t0=0 /* epoch ticks at reset*/, cycleDur=0;
	qint64 timerMainStart=0, lastStatusUpdate=0; // both used by updateStatus
	int pulsDur=500, statusDur=10000, timerMain=0, timerPulse=0, timerStatus=0;
	int loopI=0 /* index in loop */;
	int loopCnt=0 /* number of loops completed*/;
    int stateInfo=2; // 0=no state info updates, 1=update cycle count onlz, 2=full update
	bool startval=false, toggleVal=false, timeOfDay=false, pulse=true, startOnInit=true, runonce=false;
	NodesVSP delayNodes;
	Node * ndOut=nullptr;
	NodeStr *ndState=nullptr;
	NodeBool *ndReset=nullptr;
};


}
#endif // ALGTIMER_H
