#include "nodeardubridge.h"
#include "nodeardubridge_fac.h"
#include <math.h>
//#include <wiringPiSPI.h>
#include <QFileInfo>
#include <QList>
#include <QTimer>
#include "command.h"
#include "piadi.h"

NodeArduBridge::NodeArduBridge(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent), hwType_(HwType::UNDEF)
{
	QString hwtypenm=elem.attribute("type");
	if(hwtypenm==QStringLiteral("HYSRF05"))
		hwType_=HwType::HYSRF05;
	else if(hwtypenm==QStringLiteral("HX711"))
		hwType_=HwType::HX711;
	else if(hwtypenm==QStringLiteral("DigiOut"))
		hwType_=HwType::DIGIOUT;
	if(hwType_==HwType::HYSRF05 || hwType_==HwType::HX711)
	{
		bool ok=false;
		if(elem.hasAttribute("calib"))
		{
			double v=elem.attribute("calib").toDouble(&ok);
			if(!ok || fabs(v)<1.e-10)
				qWarning()<<"HYSRF05/HX711 invalid calib, using 58.5"<<elem.attribute("calib");
			else
				calib_=v;
		}
		if(elem.hasAttribute("zero"))
		{
			double v=elem.attribute("zero").toDouble(&ok);
			if(!ok)
				qWarning()<<"HYSRF05/HX711 invalid zero, using 0."<<elem.attribute("zero");
			else
				zero=v;
		}
		if(elem.hasAttribute("format"))
		{
			QString v=elem.attribute("format");
			if(v.size()<8 || !v.startsWith(QStringLiteral("format")))
			{
				qWarning()<<"NodeArduBridge invalid (truncated) format - ignored; use format[efg][0-16]"<<v;
				return;
			}
			bool ok=false;
			format=v.mid(6,1).toUtf8().at(0);
			precision=v.mid(7).toInt(&ok);
			if( (format!='e' && format!='f' && format!='g') || !ok || precision<0 || precision>16 )
			{
				qWarning()<<"NodeArduBridge invalid format - reset; use format[efg][0-16]"<<v;
				format=' ';
				precision=2;
			}
		}
	}
	else
		qCritical()<<"invalid ArduBridge node type"<<hwtypenm<<adr_.toString();
	pinout=elem.attribute("pinout");
	if(elem.hasAttribute("init_commands"))
	{
		initCmds=elem.attribute("init_commands").split('|', Qt::SkipEmptyParts);
	}
}

const QString &NodeArduBridge::type() const
{
	return typeName;
}

Node::Unit NodeArduBridge::unit() const
{
	switch(hwType_)
	{
	case HwType::HYSRF05:
		return Unit::CENTIMETER;
	case HwType::HX711:
		return Unit::NEWTON;
	case HwType::DIGIOUT:
		return Unit::NONE;
	default:
		return Unit::UNDEF;
	}
}

Result NodeArduBridge::init()
{
	switch(hwType_)
	{
	case HwType::HYSRF05:
			lastcmd=QString("IU%1").arg(pinout).toUtf8();
			break;
		case HwType::HX711:
			lastcmd=QString("IF%1").arg(pinout).toUtf8();
			break;
		case HwType::DIGIOUT:
			lastcmd=QString("ID%1").arg(pinout).toUtf8();
			break;
		default:
			lastcmd=QString("IX%1").arg(pinout).toUtf8();
			break;
	}
	DeviceArduBridge *d=qobject_cast<DeviceArduBridge*>(parent());
	if(!d)
		throw Ex(QString("newValSl failed on %1").arg(adr_.toStringVerbose()));
	//qDebug()<<"NodeArduBridge::init"<<address().toString()<<lastcmd;
	d->write(lastcmd, adr_.channel());
	state_=State::BUSY;
	return Result::COMPLETED;
}

void NodeArduBridge::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	if(!tokenValid(token))
	{
		qWarning()<<adr_.toString()<<"got invalid token"<<token;
		return;
	}
	if(!value.canConvert<QString>())
		throw Ex(QString("NodeValue not convertible to string on %1").arg(adr_.toStringVerbose()));
	//if(value==lastcmd_) it makes sense to send the same command twice!
	//	return;
	newValSl(value);
}

void NodeArduBridge::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	//qDebug()<<"NodeArduBridge::processCommand";
	if(relevant.cmd()=="noderq")
	{
		relevant.setAttribute("val", value_.toString());
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else if(relevant.cmd()=="nodeset")
	{
		setValue(relevant.attribute("val"));
		return;
	}
	else
		qCritical()<<"unknown command for NodeArduBridge"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeArduBridge::setReturnValue(const QString &retval)
{
	if(!retval.size())
	{
		qWarning()<<objectName()<<QStringLiteral("no payload, no value");
		return; // no value, no signal :-)
	}
	bool doSignal=false;
	switch(hwType_)
	{
	case HwType::HYSRF05:
	case HwType::HX711:
		if(retval[0]=='V' || retval[0]=='Z') // handle zero return as normal value
		{
			bool ok=false;
			int dist=retval.mid(1).toInt(&ok);
			if(ok)
			{
				NodeValue nv;
				if(format=='e' || format=='f' || format=='g')
					nv=QString::number(((double)dist-zero)/calib_, format, precision);
				else
					nv=((double)dist-zero)/calib_;
				if(nv!=value_)
				{
					doSignal=true;
					value_=nv;
				}
			}
			else
				qWarning()<<objectName()<<"invalid V command / number"<<retval;
		}
		else if(retval.startsWith("OK"))
		{
			value_=retval;
			state_=State::READY;
			playInit();
			doSignal=true;
		}
		else if(retval==QStringLiteral("w")) // HX711 may send w(ait) if not ready yet - ignore completely
		{
			qDebug()<<objectName()<<"wait received";
			// don't signal; ardubrige shall never log value w
		}
		else
		{
			value_=retval;
			qWarning()<<objectName()<<"stray return data"<<retval;
			doSignal=true;
		}
		break;
	case HwType::DIGIOUT:
		if(retval.startsWith("OK"))
		{
			value_=retval;
			state_=State::READY;
			playInit();
		}
		else
		{
			value_=retval; // currently no return values except error codes
		}
		doSignal=true;
		break;
	default:
		value_=retval;
		doSignal=true;
		break;
	}
	if(doSignal)
		emit newValSig(value_, DCON.getUsecTst()); // signal always, could be ping-like data
}

void NodeArduBridge::newValSl(const NodeValue &newValue)
{
	if(state()!=Node::State::READY)
	{
		qWarning()<<"set value ignored while not ready"<<adr_.toStringVerbose()<<Node::state(state());
		return;
	}
	QString cmdStr=newValue.toString();
	lastcmd=cmdStr.toUtf8();
	DeviceArduBridge *d=qobject_cast<DeviceArduBridge*>(parent());
	if(!d)
		throw Ex(QString("newValSl failed on %1").arg(adr_.toStringVerbose()));
	d->write(lastcmd, adr_.channel());
	//emit newValSig(value_, tst); nope, signal only new data arriving from Arduino
}

void NodeArduBridge::playInit()
{
	if(initCmds.size())
	{
		DeviceArduBridge *d=qobject_cast<DeviceArduBridge*>(parent());
		if(!d)
			return;
		for(int i=0, im=initCmds.size(); i<im; ++i)
		{
			lastcmd=initCmds.at(i).toUtf8();
			d->write(lastcmd, adr_.channel());
		}
	}
}


DeviceArduBridge::DeviceArduBridge(const QDomElement &elem) : Device(elem)
{
	devName=elem.attribute("serial");
	if(devName.isEmpty())
		throw Ex(QString("invalid serial port device specification <%1> at device %2").arg(devName).arg(id_));
	if(devName.startsWith('/'))
	{
		QFileInfo fi(devName);
		if(fi.exists())
		{
			QStringList cn=fi.canonicalFilePath().split('/');
			devName=cn.last();
			qInfo()<<objectName()<<"resolving device name"<<elem.attribute("serial")<<devName;
		}
		else
			qCritical()<<objectName()<<"serial seems to be a full path but it does not exist"<<devName;
	}
	serpi=new QSerialPortInfo(devName);
	if(!serpi || serpi->isNull())
	{
		qCritical()<<"Cannot retreive serial port info for"<<devName;
		QList<QSerialPortInfo> ports=QSerialPortInfo::availablePorts();
		qInfo()<<"serial ports are:"<<ports.size();
		foreach(QSerialPortInfo port, ports)
		{
			qInfo()<<"name:"<<port.portName()<<"description:"<<port.description();
		}
		delete serpi;
		serpi=nullptr;
	}
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		new NodeArduBridge(nodes.item(i).toElement(), id_, this);
	}
	ndOut=new NodeStr(namePrefix+"out", id_, 0, this);
}

DeviceArduBridge::~DeviceArduBridge()
{
	if(serpi)
		delete serpi;
}

pi_at_home::Result DeviceArduBridge::init()
{
	if(createPort()==Result::FAILED)
	{
		ndOut->setValue(NodeValue("port FAILED"));
		return Result::FAILED;
	}
	ndOut->setValue(NodeValue("waiting for startup..."));
	return Device::init();
}

pi_at_home::Result DeviceArduBridge::shutdown()
{
	if(!serp)
		return Result::COMPLETED;
	serp->close();
	delete serp;
	serp=nullptr;
	return Result::COMPLETED;
}

void DeviceArduBridge::write(QByteArray &data, int channel)
{
	if(!serp || !arduOnline)
		return;
	char d[]=" :|\0";
	d[0]=(char)channel+'A';
	serp->write(d, 2);
	qint64 n=serp->write(data);
	if(n!=data.size())
		qWarning()<<"mismatch between written bytes and bytes to write"<<n<<data.size();
	serp->write(d+2, 1);
	serp->flush();
	qDebug()<<objectName()<<"DeviceArduBridge write"<<channel<<data;
}

pi_at_home::Result DeviceArduBridge::createPort()
{
	if(!serpi)
		return Result::FAILED;
	serp=new QSerialPort(*serpi, this);
	serp->setParity(QSerialPort::NoParity);
	serp->setStopBits(QSerialPort::OneStop);
	serp->setDataBits(QSerialPort::Data8);
	serp->setBaudRate(QSerialPort::Baud9600);
	serp->setFlowControl(QSerialPort::NoFlowControl);
	serp->clearError();
	if(!serp->open(QIODevice::ReadWrite))
	{
		qCritical()<<"serial open failed"<<serp->error()<<serp->errorString();
		delete serp; serp=nullptr; // serp==nullptr indicates failed state
		return Result::FAILED;
	}
	qDebug()<<"DTR RTS"<<serp->isDataTerminalReady()<<serp->isRequestToSend();
	connect(serp, &QSerialPort::errorOccurred, this, &DeviceArduBridge::serpErrSl);
	connect(serp, SIGNAL(readyRead()), this, SLOT(readyReadSl()));
	return Result::COMPLETED;
}

void DeviceArduBridge::createPortSl()
{
	if(createPort()==Result::FAILED)
	{
		if(reconnectTimeout<65536000)
			reconnectTimeout*=2;
		qWarning()<<objectName()<<"reconnect failed, now waiting"<<reconnectTimeout/1000;
		QTimer::singleShot(reconnectTimeout, this, &DeviceArduBridge::createPortSl);
	}
	else
		qInfo()<<objectName()<<"reconnect succeeded, now waiting for startup";
}

void DeviceArduBridge::readyReadSl()
{
	if(!serp)
		return;
	readBuffer+=serp->readAll();
	while(readBuffer.size())
	{
		int endi=readBuffer.indexOf('|');
		if(endi<0)
		{
			if(readBuffer.size()>1000)
			{
				qCritical()<<"DeviceArduBridge read biffer >1000 bytes - discarded";
				readBuffer.clear();
			}
			return; // no end-marker found (or reset)
		}
		if(endi<2 || readBuffer.at(1)!=':')
		{
			int truestart=readBuffer.indexOf(".:pi2Ar1");
			if(truestart>0)
			{
				qWarning()<<"DeviceArduBridge: leading garbage removed"<<readBuffer.left(truestart);
				readBuffer.remove(0, truestart);
			}
			else
			{
				qWarning()<<"DeviceArduBridge: invalid message (purged)"<<readBuffer.left(endi+1);
				readBuffer.remove(0, endi+1);
				continue;
			}
		}
		int channel=readBuffer.at(0)-'A';
		QString payload(readBuffer.mid(2, endi-2));
		readBuffer.remove(0, endi+1);
		if(channel>25)
		{
			qDebug()<<"DeviceArduBridge echo"<<channel-32<<payload;
			continue;
		}
		else if(channel==-19) // '.'-'A'
		{
			qInfo()<<"DeviceArduBridge device command"<<channel<<payload;
			if(payload.startsWith(QStringLiteral("pi2Ar1")))
			{ // now the bootloader gave up and we can init the nodes
				arduOnline=true;
				ndOut->setValue(NodeValue(payload));
				// don't use Device::init, would re-register nodes
				QList<NodeArduBridge * > chldr=findChildren<NodeArduBridge * >();
				foreach(Node * n, chldr)
					n->init();
			}
			continue;
		}
		// forward to appropriate channel / node
		// qDebug()<<"DeviceArduBridge data"<<channel<<payload;
		QList<NodeArduBridge * > chldr=findChildren<NodeArduBridge * >();
		foreach(NodeArduBridge * n, chldr)
		{
			if(n->address().channel()==channel)
			{
				n->setReturnValue(payload);
				return;
			}
		}
		qWarning()<<"DeviceArduBridge reply channel out of range"<<channel<<payload;
	}
}

void DeviceArduBridge::serpErrSl(QSerialPort::SerialPortError error)
{
	//if(error!=QSerialPort::NoError)
	qCritical()<<objectName()<<"SerialPortError"<<error;
	arduOnline=false;
	if(serp)
	{
		serp->close();
		//serp->clearError();
		delete serp;
		serp=nullptr;
		QTimer::singleShot(reconnectTimeout, this, &DeviceArduBridge::createPortSl);
	}
}

