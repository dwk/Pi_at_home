#ifndef NODETADLBUS_NM_H
#define NODETADLBUS_NM_H

#include "piadh.h"
#include "node.h"
#include "device.h"


namespace
{
const QString typeName="TA_DL_bus";
const QString typeNameSensorT="TA_DL_bus_SensorT";
const QString typeNameRpm="TA_DL_bus_Rpm";
const QString typeNameTst="TA_DL_bus_Tst";
}

#endif // NODETADLBUS_NM_H
