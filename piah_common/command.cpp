#include "command.h"
#include "piadi.h"

QString Fragment::templ="<%1 %2></%1>";
QString Fragment::ptempl="%1=\"%2\" ";

Fragment::Fragment(QDomElement elem)
{
	cmd_=elem.tagName();
	QDomNamedNodeMap nnm=elem.attributes();
	for(int i=0; i<nnm.length(); ++i)
	{
		QDomAttr a=nnm.item(i).toAttr();
		QString nm=a.name();
		QString val=a.value();
		val.replace("&amp;", "&", Qt::CaseSensitive);
		val.replace("&lt;", "<", Qt::CaseSensitive);
		val.replace("&quot;", "\"", Qt::CaseSensitive);
		val.replace("&perc;", "%", Qt::CaseSensitive);
		setAttribute(nm, val);
	}
}

QString Fragment::attribute(const QString &name) const
{
	Params::const_iterator it=params_.find(name);
	if(it==params_.end())
		return QString();
	return it->second;
}

bool Fragment::setAttribute(const QString &name, const QString &value)
{
	return params_.insert(Params::value_type(name, value)).second;
}

QString Fragment::toXml() const
{
	//qDebug()<<cmd_;
	return templ.arg(cmd_, paramsToXml());
}

QString Fragment::paramsToXml() const
{
	QString res;
	for(Params::const_iterator it=params_.begin(); it!=params_.end(); ++it)
	{
		QString payload=it->second;
		payload.replace("\"", "&quot;", Qt::CaseSensitive); // will interfere with xml syntax
		payload.replace("%", "&perc;", Qt::CaseSensitive); // will interfere with QString-arg subsitution downstream
		res+=ptempl.arg(it->first, payload);
	}
	res.replace("&", "&amp;", Qt::CaseSensitive);
	res.replace("<", "&lt;", Qt::CaseSensitive);
	return res;
}

QString Command::msgTempl="<piahmsg cmd=\"%1\" src=\"%2\" ctst=\"%3\" sluid=\"%5\" %4>%6</piahmsg>"; // %4 for dst, dluid, etst and params, %5 for frags
QString Command::msgTemplBulk="<piahmsg cmd=\"%1\" src=\"%2\" ctst=\"%3\" sluid=\"%5\" %4>%6";
QByteArray Command::msgTemplEpilog="</piahmsg>";

Command::Command(const QDomElement &root) : QObject(DCON.self())
{
	//qDebug()<<"Command::Command() template"<<msgTempl;
	if(root.tagName()!="piahmsg")
		throw Ex("XML command: invalid root name");
	QDomNamedNodeMap nnm=root.attributes();
	bool sluidOk=true, dluidOk=true;
	for(int i=0; i<nnm.length(); ++i)
	{
		QDomAttr a=nnm.item(i).toAttr();
		QString nm=a.name();
		if(nm=="cmd")
			cmd_=a.value();
		else if(nm=="src")
			src=NodeAddress(a.value());
		else if(nm=="dst")
			dst=NodeAddress(a.value());
		else if(nm=="sluid")
		{
			//qDebug()<<"sluid_"<<a.value();
			sluid_=a.value().toULongLong(&sluidOk);
		}
		else if(nm=="dluid")
		{
			//qDebug()<<"dluid_"<<a.value();
			dluid_=a.value().toULongLong(&dluidOk);
		}
		else if(nm=="ctst")
			tstCreation=QDateTime::fromString(a.value(), Qt::ISODate);
		else if(nm=="etst")
			tstExecution=QDateTime::fromString(a.value(), Qt::ISODate);
		else
			setAttribute(nm, a.value());
	}
	for(QDomNode n = root.firstChild(); !n.isNull(); n = n.nextSibling())
	{
		if(n.isElement())
		{
			QDomElement f = n.toElement();
			if(f.isNull())
				continue;
			addFragment(new Fragment(f));
		}
		else if(n.isText())
		{
			QDomText f = n.toText();
			if(f.isNull())
				continue;
			QByteArray compr=QByteArray::fromBase64(f.data().toUtf8());
			payload_=new QByteArray(qUncompress(compr));
		}
	}
	if(cmd_.isEmpty())
		throw Ex("XML command: no cmd");
	if(!sluidOk || !dluidOk)
		throw Ex("XML command: sluid or dluid conversion failed");
}

//                                             DCON.self() as long as you parse the command it will be deleted later
Command::Command(const QString &cmd) : QObject(nullptr), Fragment(cmd), tstCreation(QDateTime::currentDateTime()), sluid_(DCON.nextId())
{
	//qDebug()<<"Command::Command() template"<<cmd<<msgTempl;
}

Command::~Command()
{
	for(FragmentLP::iterator it=frags.begin(); it!=frags.end(); ++it)
	{
		delete *it;
	}
	if(payload_)
		delete payload_;
	//qDebug()<<"dtor"<<sluid_;
}

QString Command::toString() const
{
	QString varpar;
	if(cmd_!="contest" && cmd_!="connect" && cmd_!="nodech")
		varpar+=ptempl.arg("dst", dst.toString());
	if(tstExecution.isValid())
		varpar+=ptempl.arg("etst", tstExecution.toString(Qt::ISODate));
	if(dluid_)
		varpar+=ptempl.arg("dluid").arg(dluid_);
	varpar+=paramsToXml();
	QString frStr;
	for(FragmentLP::const_iterator it=frags.begin(); it!=frags.end(); ++it)
	{
		frStr+=(*it)->toXml();
	}
	if(payload_)
		// <piahmsg cmd=\"%1\" src=\"%2\" ctst=\"%3\" sluid=\"%5\" %4>%6
		return msgTemplBulk.arg(cmd_, src.toString(), tstCreation.toString(Qt::ISODate), varpar).arg(sluid_).arg(frStr);
	else
	{
		// <piahmsg cmd=\"%1\" src=\"%2\" ctst=\"%3\" sluid=\"%5\" %4>%6</piahmsg> // %4 for dst, dluid, etst and params, %6 for frags
		return msgTempl.arg(cmd_, src.toString(), tstCreation.toString(Qt::ISODate), varpar).arg(sluid_).arg(frStr);
	}
}

QByteArray Command::toSocketXml() const
{
	return toString().toUtf8();
}

void Command::createDluid()
{
	dluid_=DCON.nextId();
}

void Command::setPayloadEncode(const QByteArray &source)
{
	QByteArray cd=qCompress(source);
	payload_=new QByteArray(cd.toBase64());
}

const pi_at_home::NodeAddress &Command::procTarget() const
{
	if(dluid_)
		return src;
	return dst;
}

void Command::convertToReply()
{
	if(dst.uid()==DCON.uid())
		dluid_=DCON.nextId();
	else
	{
		qCritical()<<"only the target may flag a command as reply"<<cmd()<<procTarget().toStringVerbose();
	}
}

QString CommandFilter::toString() const
{
	QStringList cmds, srcs;
	for(auto it=cmdsS.begin(); it!=cmdsS.end(); ++it)
	{
		cmds<<(*it);
	}
	for(auto it=sourceNodesS.begin(); it!=sourceNodesS.end(); ++it)
	{
		srcs<<(*it).toString();
	}
	QString res="CommandFilter [%1] [%2]";
	return res.arg(cmds.join(','), srcs.join('|'));
}

bool CommandFilter::isMatch(const Command &cmd, pi_at_home::Fragment &relevant) const
{
	//qDebug()<<"matching"<<cmd.cmd()<<cmdsS.size()<<sourceNodesS.size();
	const FragmentLP & frs=cmd.fragments();
	if(cmdsS.size())
	{
		bool found=false;
		if(cmdsS.find(cmd.cmd())!=cmdsS.end())
		{
			//qDebug()<<"found"<<cmd.cmd();
			found=true;
		}
		if(!found && checkFragCmds)
		{
			for(auto fit=frs.begin(); !found && fit!=frs.end(); ++fit)
			{
				//qDebug()<<"matching frag"<<(*fit)->cmd();
				if(cmdsS.find((*fit)->cmd())!=cmdsS.end())
				{
					//qDebug()<<"found frag"<<(*fit)->cmd();
					found=true;
					relevant=(**fit);
				}
			}
		}
		if(!found)
		{
			//qDebug()<<"rejected by cmd filter";
			return false;
		}
	}
	if(sourceNodesS.size())
	{
		bool cfa=false;
		const NodeAddress & src=cmd.source();
		for(auto sit=sourceNodesS.begin(); sit!=sourceNodesS.end(); ++sit)
		{
			//qDebug()<<"testing source"<<(*sit).toString()<<src.toString();
			if((*sit)==src)
			{
				//qDebug()<<"found!";
				return true;
			}
			if((*sit).uid()==src.uid())
				cfa=true; // fragments and command always share the uid
		}
		if(!cfa)
		{
			//qDebug()<<"rejected by source filter (no frags)";
			return false;
		}
		for(auto sit=sourceNodesS.begin(); sit!=sourceNodesS.end(); ++sit)
		{
			if((*sit).uid()!=src.uid())
				continue;
			for(auto fit=frs.begin(); fit!=frs.end(); ++fit)
			{
				//qDebug()<<"testing frag"<<(*fit)->attribute("addr");
				if((*sit)==NodeAddress((*fit)->attribute("addr")))
				{
					//qDebug()<<"found!";
					relevant=(**fit);
					return true;
				}
			}
		}
		//qDebug()<<"rejected by source filter";
		return false;
	}
	//qDebug()<<"accepted by full wild";
	return true;
}

void CommandFilter::merge(const CommandFilter &other)
{
	cmdsS.insert(other.cmdsS.begin(), other.cmdsS.end());
	sourceNodesS.insert(other.sourceNodesS.begin(), other.sourceNodesS.end());
}
