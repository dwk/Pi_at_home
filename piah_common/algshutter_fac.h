#ifndef ALGSHUTTER_FAC_H
#define ALGSHUTTER_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgShutter(elem);
}
const QString typeName="Shutter";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGSHUTTER_FAC_H
