#ifndef NODEALG_H
#define NODEALG_H

#include "piadh.h"
#include "node.h"
#include "device.h"
#include <QDebug>
#include <QDoubleSpinBox>
#include <QLineEdit>

namespace pi_at_home
{
class Algorithm;

/* These Nodes are are used as sub-elements in AlgDummy definition. E.g.
 * <node channel="42" name="uid-unique_name" type="bool"/>
 * Parameters from Node do apply!
 * required
 *	data_type="x"			x = bool | int | double | string
 * optional
 *  default_value="x"		x = value convertible to type; will be set during startup
 *  source="adr"			adr = full node address, :<host>:<device>:<channel>; if set this node will mirror the values of the remote node
 *
 * AlgNode's are always biderectional, you can read and write to them. If you write to a mirror-node (source parameter set) it WILL NOT
 * change the remote node but only the local mirror. The next remote update will overwrite the local change. Mirrors intentionaly work
 * unidirectional over the network from remote to local.
 */
class NodeAlg : public Node
{
	Q_OBJECT
	friend class Algorithm;
public:
	NodeAlg(const QDomElement &elem, int device, QObject * parent);
	NodeAlg(const QString & name, int device, int channel, QObject *parent) : Node(name, device, channel, parent) {}
	virtual ~NodeAlg();
	virtual NodeValue defaultValue() const {return defval_;}
	void setDefaultValue(NodeValue defValue) {defval_=defValue;}
	virtual bool canSetValue(const NodeValue &) const {return true;}
	virtual Result init();
	// Node::value is fine
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment &relevant);
	//
	virtual NodeValue cast2Type(const NodeValue & value) =0;
public slots:
	virtual void newValSl(const NodeValue & newValue);
	void netConSl(QString uid, bool connected);
protected slots:
	void establishHostConSl();
protected:
	//virtual void timerEvent(QTimerEvent *event);
	void readRemoteValue();
	NodeValue defval_;
	NodeAddress *mirror=nullptr;
};

// bool -----------------------------------------------------

class NodeBool : public NodeAlg
{
	Q_OBJECT
	friend class Algorithm;
public:
	NodeBool(const QDomElement &elem, int device, QObject * parent);
	NodeBool(const QString & name, int device, int channel, QObject *parent) : NodeAlg(name, device, channel, parent) {defval_=NodeValue(false);}
	virtual const QString & type() const;
	virtual QString serializeValue(const NodeValue & value) const {return value.toBool()?"true":"false";}
	virtual Node::Unit unit() const {return Unit::BIT;}
	//
	virtual NodeValue cast2Type(const NodeValue & value);
protected:
};

// int -----------------------------------------------------

class NodeInt : public NodeAlg
{
	Q_OBJECT
	friend class Algorithm;
public:
	NodeInt(const QDomElement &elem, int device, QObject * parent);
	NodeInt(const QString & name, int device, int channel, QObject *parent) : NodeAlg(name, device, channel, parent) {defval_=NodeValue((int)0);}
	virtual const QString & type() const;
	virtual QString serializeValue(const NodeValue & value) const {return QString::number(value.toInt());}
	virtual Node::Unit unit() const {return unit_;}
	//
	virtual NodeValue cast2Type(const NodeValue & value);
	void setUnit(Node::Unit newUnit) {unit_=newUnit;}
protected:
	Node::Unit unit_=Node::Unit::NONE;
};

// double -----------------------------------------------------

class NodeDbl : public NodeAlg
{
	Q_OBJECT
	friend class Algorithm;
public:
	NodeDbl(const QDomElement &elem, int device, QObject * parent);
	NodeDbl(const QString & name, int device, int channel, QObject *parent) : NodeAlg(name, device, channel, parent) {defval_=NodeValue((double)0.);}
	virtual const QString & type() const;
	virtual QString serializeValue(const NodeValue & value) const {return QString::number(value.toDouble());}
	virtual Node::Unit unit() const {return unit_;}
	//
	virtual NodeValue cast2Type(const NodeValue & value);
	void setUnit(Node::Unit newUnit) {unit_=newUnit;}
protected:
	Node::Unit unit_=Node::Unit::NONE;
};

// QString -----------------------------------------------------

class NodeStr : public NodeAlg
{
	Q_OBJECT
	friend class Algorithm;
public:
	NodeStr(const QDomElement &elem, int device, QObject * parent);
	NodeStr(const QString & name, int device, int channel, QObject *parent) : NodeAlg(name, device, channel, parent) {defval_=NodeValue(QString(""));}
	virtual const QString & type() const;
	//virtual QString serializeValue(const NodeValue & value) base works fine
	virtual Node::Unit unit() const {return Unit::NONE;}
	//
	virtual NodeValue cast2Type(const NodeValue & value);
protected:
};


}
#endif // NODEALG_H
