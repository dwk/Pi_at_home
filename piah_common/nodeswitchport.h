#ifndef NODESWITCHPORT_H
#define NODESWITCHPORT_H

#include "piadh.h"
#include "node.h"
#include "device.h"

/* *** Device SwitchPort
 * required attributes
 *   id="x"			x = vaild device address
 *   type="SwitchPort"
 *   port="WPx"		x = any valid port number in Wiring Pi notation
 * optional attributes
 *   <none>
 *
 * SwitchPort provides digital input(s) with some debouncing logic. The device is just a
 * concept, not actual hardware. The channel number is the offset to the base
 * address. Channels need not to be contiguous and the GPIOs of the missing
 * channels are not touched in any way.
 *
 * *** Node SwitchPort
 * required attributes
 *   see Node attributes!
 * optional attributes
 *   see Node attributes!
 *   hold_on="x"		x = delay in ms; hold_on milliseconds after turning on transitions are ignored; default 100ms
 *   hold_off="x"		x = delay in ms; hold_off milliseconds after turning off transitions are ignored; default 100ms
 *   ignore_on="x"		x = duration in ms; on-pulses of less than ignore_on milliseconds are ignored; default 0ms
 *   ignore_off="x"		x = duration in ms; off-pulses of less than ignore_off milliseconds are ignored; default 0ms
 *   inverted="b"		b = true|false If true a hardware-low is reported as true/active and a hardware-high as false/inactive; default is false
 *						Do not use for application logic but only for hardware abstraction. true should always correspond to the active, "on"-state.
 *   pull="x"			x= up | down | none;  enable pull-up or pull-down resistor or none; default is none
 *
 * During the hold_on and hold_off intervall the hardware state is ignored. At the end of the intervall the node value is
 * synchronized with the hardware and signaled. Be carefull with low hold_on and hold_off values; fast changing inputs
 * (e.g. noisy signals) may create excessive CPU load and even flood the network with messages. Note that the node will only signal
 * a message if the state at the end of the hold time is different from what you read before. If there were changes at the hardware
 * within the hold period but ending up in the original state, GpioWrapper will signal, but the node will filter this ON->ON (or
 * OFF->OFF) transition.
 * ---> Currently you CANNOT configure the node to forward this "state changes missed"-information. <---
 * Hold_on / hold_off is perfect for debouncing noisy transitions, that is the typical on or off times of the signal are much longer than
 * the hold_on / hold_off time. If applied to a fast changing signal you will basically see one or more hold_on intervalls in state "on"
 * followed by one or more hold_off intervalls "off", depending on the hardware seeing a positive or negative edge after a hold
 * periode. You cannot rely on a specific pattern.
 * Hold times do not include processing time; the true hold time is thus usually slightly longer than specified.
 *
 * For a low-pass-like function use the ignore_on / ignore_off intervalls. Be aware that the ignore intervalls do not reduce the
 * CPU-load resulting from fast signals. hold intervalls (for lowering the CPU load) and ignore intervalls for low-pass operation
 * can be combined. Use hold_on < ignore_on and hold_off < ignore_off to make sense (for ignore!=0). E.g. 50 Hz (positive) bursts from an opto coupler
 * (about 10ms on, 10 off while "active" and continous off while inactive) can use hold_on 100ms, hold_off 0ms, ignore_on 0ms,
 * ignore_off 100ms. This would create fast response (no intentional delay) when turning on, a minimal reported on-time of 200ms
 * (100ms hold and then 100ms ingored off) and an off delay jittering between 100ms and 200ms. (the hold periode may miss between 0 and
 * 100ms off state, the ingnore interval does not contribute to jitter.) The CPU load is roughly 10 to 20% of treating every puls. (50Hz
 * makes 100 transitions / second reduced to about 10 to 20 activities / second depenting where you wake up)
 * When using ignore-durations the signal will be generated ignore_on or ignore_off ms after the actual transition happened -
 * we cannot look in the future. Still the timestamp in the newValSig and all downstream processing will reflect the true time of
 * the transition. During the ignore periode the state of the node will read BUSY instead of READY but for performance reasons this
 * state change is silent - no updates sent.
 *
 * Exapmple
 * <device id="20" type="SwitchPort" port="WP3">
 *  <node channel="0" name="ManKillSwitch" inverted="true"/>
 *  <node channel="5" name="230Vsense" pull="up" inverted="true" hold_on="100" hold_off="0" ignore_on="0" ignore_off="100" />
 * </device>
 */
namespace pi_at_home
{
class HwInPort;

class NodeSwitchPort : public Node
{
	Q_OBJECT
public:
	NodeSwitchPort(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	// virtual QString serializeValue(const NodeValue & value) const; QVariant::toString on bool yields "true" and "false" --> OK
	virtual Node::Unit unit() const {return Unit::BIT;}
	virtual NodeValue defaultValue() const {return NodeValue(false);}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return false;}
	virtual Result init();
	//virtual value();
	void readValue(bool & valueOut);
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void ping(); // don't use base class!
	virtual void processCommand(Command & cmd, Fragment & relevant);
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	virtual void timerEvent(QTimerEvent *event);
protected slots:
	void updateValSl(bool newVal, qint64 timestamp);
protected:
	QString pull;
	bool val_=false, valStore_=false, inverted=false, normOp=false;
	int holdOn=100, holdOff=100, ignoreOn=0, ignoreOff=0, ignoreTimer=0;
	qint64 ignoreTst=0;
	HwInPort * hwPort=nullptr;
};

class DeviceSwitchPort : public Device
{
	Q_OBJECT
public:
	explicit DeviceSwitchPort(const QDomElement &elem);
	virtual Result init();
	virtual Result shutdown();
	int basePortWp() const {return basePortWp_;}
private:
	int basePortWp_=-1;
};

}

#endif // NODESWITCHPORT_H
