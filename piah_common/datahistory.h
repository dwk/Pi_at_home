#ifndef DATAHISTORY_H
#define DATAHISTORY_H

#include "piadh.h"
#include <QObject>
#include <QDateTime>
#include "nodeaddress.h"
#include "node.h"

namespace pi_at_home
{
class WidGraph;
class DataContainer;

class DataHistory : public QObject
{
	Q_OBJECT
friend class WidGraph;
friend class DataContainer;
public:
	typedef QObject * (*ExtFactory)(const DataHistory &);
	DataHistory(const NodeAddress & adr);
	~DataHistory() {}
	const QString & getName() const;
	const NodeAddress & getAdr() const {return adr;}
	Node::Unit getUnit() const {return unit_;}
	void setUnit(Node::Unit unit);
	QDateTime getFirstTimestamp() const;
	QDateTime getLastTimestamp() const;
	int getCount() const;
	bool isEmpty() const;
	bool addData(const QDateTime & timestamp, double value); // true if inserted, false if already there (and ignored)
	void addData(const DataHistory & other);
	QSharedPointer < QObject > guiData();
protected:
	typedef std::map<QDateTime, double> DataRow;
	NodeAddress adr;
	DataRow dr;
	Node::Unit unit_=Node::Unit::UNDEF;
	QSharedPointer < QObject > guiData_;
};
typedef std::map<NodeAddress, DataHistory * > DataHistoryMP;

}
#endif // DATAHISTORY_H
