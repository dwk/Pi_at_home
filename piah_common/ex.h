#ifndef EX_H
#define EX_H

#include "piadh.h"
#include <exception>

namespace pi_at_home
{
class Ex : public std::exception
{
public:
	Ex(const QString & message, int code = 0) : code(code), message(message) {}
	int getCode() const {return code;}
	const QString & getMessage() const {return message;}
	virtual const char* what() const noexcept {return message.toUtf8().data();}
private:
	int code;
	QString message;
};
}
#endif // EX_H
