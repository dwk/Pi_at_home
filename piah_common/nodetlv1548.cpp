#include "nodetlv1548.h"
#include "nodetlv1548_fac.h"
#include <math.h>
#ifndef GUI_ONLY
#include <unistd.h> // usleep
#include <wiringPi.h>
#include <wiringPiSPI.h>
#endif
#include "command.h"
#include "piadi.h"

NodeTLV1548::NodeTLV1548(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent)
{
	bool ok=false;
	double d=elem.attribute("precision").toDouble(&ok);
	if(ok)
		prec=d;
	if(prec!=0.01)
		qInfo()<<"NodeTLV1548: precision"<<prec;
}

NodeTLV1548::~NodeTLV1548()
{
}

Result NodeTLV1548::init()
{
	//qDebug()<<"dummy read during node-init";
	//NodeValue dummy=value(); doesn't have any side effect -> useless
	return Result::COMPLETED;
}

void NodeTLV1548::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	Q_UNUSED(value);
	Q_UNUSED(token);
	qCritical()<<"setValue on NodeTLV1548";
}

void NodeTLV1548::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		relevant.setAttribute("val", QString::number(val_));
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeTLV1548"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeTLV1548::newValSl(const NodeValue &newValue)
{
	Q_UNUSED(newValue);
	qCritical()<<"newValSl on NodeTLV1548";
}


NodeTLV1548Raw::NodeTLV1548Raw(const QDomElement &elem, int device, QObject *parent) :
	NodeTLV1548(elem, device, parent)
{
}

const QString &NodeTLV1548Raw::type() const
{
	return typeNameRaw;
}

void NodeTLV1548Raw::processValue(double avgVal, qint64 timestamp, pi_at_home::DeviceTLV1548 *converter)
{
	Q_UNUSED(converter)
	double tval=prec?floor(avgVal/prec+.5)*prec:avgVal;
	if(tval!=val_)
	{
		val_=tval;
		value_=val_;
		state_=State::READY;
		emit newValSig(value_, timestamp);
	}
}


NodeTLV1548Scaled::NodeTLV1548Scaled(const QDomElement &elem, int device, QObject *parent) :
	NodeTLV1548(elem, device, parent)
{
	unit_=Unit::VOLT;
	bool ok=false;
	double vx=elem.attribute("gain").toDouble(&ok);
	if(ok)
		gain=vx;
	qInfo()<<"channel"<<address().toStringVerbose()<<"added as NodeTLV1548Scaled calib"<<gain;
}

const QString &NodeTLV1548Scaled::type() const
{
	return typeNameScaled;
}

void NodeTLV1548Scaled::processValue(double avgVal, qint64 timestamp, pi_at_home::DeviceTLV1548 *converter)
{
	//qDebug()<<"ch"<<adr_.channel()<<"relative"<<newval<<"denom"<<gain-newval;
	double rval=converter->raw2AbsoluteData(avgVal);
	double tval=prec?floor(rval/(gain*prec)+.5)*prec:(rval/gain);
	if(tval!=val_)
	{
		val_=tval;
		value_=tval;
		state_=State::READY;
		emit newValSig(value_, timestamp);
	}
}


NodeTLV1548PT1000::NodeTLV1548PT1000(const QDomElement &elem, int device, QObject *parent) :
	NodeTLV1548(elem, device, parent)
{
	unit_=Unit::DEGREE_CELSIUS;
	bool ok=false;
	double vx=elem.attribute("gain").toDouble(&ok);
	if(ok)
		gain=vx;
	vx=elem.attribute("r_ref").toDouble(&ok);
	if(ok)
		rRef=vx;
	qInfo()<<"channel"<<address().toStringVerbose()<<"added as NodeTLV1548PT1000 calib"<<gain<<rRef;
}

const QString &NodeTLV1548PT1000::type() const
{
	return typeNamePt1000;
}

void NodeTLV1548PT1000::processValue(double avgVal, qint64 timestamp, pi_at_home::DeviceTLV1548 *converter)
{
	//qDebug()<<"ch"<<adr_.channel()<<"relative"<<newval<<"denom"<<gain-newval;
	double tval=std::numeric_limits<double>::quiet_NaN(); // default, like open loop
	double rval=converter->raw2RelativeData(avgVal);
	double denom=gain - rval;
	if(fabs(denom)>0.00001)
	{
		double rt=rRef*rval/denom;
		const double a=3.9083e-3;
		const double b=-5.7750e-7;
		tval=(-1000.*a + sqrt(1e6*a*a-4000.*b*(1000.-rt))) / (2000.*b);
		if(prec)
			tval=floor(tval/prec+.5)*prec;
		//qDebug()<<"ch"<<adr_.channel()<<"Rt"<<rt<<"T"<<val_;
	}
	if(tval!=val_)
	{
		val_=tval;
		value_=tval;
		state_=State::READY;
		emit newValSig(value_, timestamp);
	}
}


TLV1548Decoder::TLV1548Decoder(int spiPort, int firstAdr, int lastAdr, int tickMs, int average) :
	spiPort(spiPort), firstAdr(firstAdr), lastAdr(lastAdr), curAdr(firstAdr), tickMs(tickMs), avg(average)
{
	int sz=lastAdr-firstAdr+1;
	dcnt=new int [sz];
	dpnt=new int [sz];
	davg=new double [sz];
	data=new unsigned int * [sz];
	timestamps=new qint64[sz];
	for(int i=0; i<sz; ++i)
	{
		dcnt[i]=0;
		dpnt[i]=0;
		davg[i]=0.;
		data[i]=new unsigned int [avg]; // no need to initialize
		timestamps[i]=0;
	}
	// slow cycle time is about 46µs - no worry, even for 1ms ticks
	spibits[0]=0xA0;
	spibits[1]=0;
	wiringPiSPIDataRW(spiPort, spibits, 2);
	usleep(50);
	qint64 tst=DCON.getUsecTst();
	//qDebug()<<"TLV1548Decoder starting read loop";
	for(int i=firstAdr; i<=lastAdr+1; ++i)
	{
		spibits[0]=(i>lastAdr?firstAdr:i)*0x10;
		spibits[1]=0;
		wiringPiSPIDataRW(spiPort, spibits, 2);
		//qDebug()<<"index"<<i<<i-firstAdr-1;
		if(i>firstAdr)
		{
			int tindex=i-firstAdr-1;
			//qDebug()<<"c p"<<dcnt[tindex]<<dpnt[tindex];
			++(dcnt[tindex]);
			data[tindex][(dpnt[tindex])++]=(((unsigned int)spibits[0])<<2) + (((unsigned int)spibits[1])>>6);
			timestamps[tindex]=tst;
			//qDebug()<<"data"<<data[tindex][dpnt[tindex]-1];
		}
		usleep(50);
	}
}

TLV1548Decoder::~TLV1548Decoder()
{
	for(int i=0; i<lastAdr-firstAdr+1; ++i)
	{
		delete[] data[i];
	}
	delete[] dcnt;
	delete[] dpnt;
	delete[] davg;
	delete[] data;
	delete[] timestamps;
}

void TLV1548Decoder::timerEvent(QTimerEvent *event)
{
	Q_UNUSED(event);
	/* example:
	TX | D0 00 - address / command left aligned in first byte
	yields
	RX | FF C0 - 10 bit result left aligned in 16 bit */
	int resAdr=curAdr;
	++curAdr;
	if(curAdr>lastAdr)
		curAdr=firstAdr;
	spibits[0]=curAdr*0x10;
	spibits[1]=0;
	wiringPiSPIDataRW(spiPort, spibits, 2);
	qint64 cspitsts=DCON.getUsecTst();
	int index=resAdr-firstAdr;
	QMutexLocker lock(&mtx);
	if(dpnt[index]>=avg)
		dpnt[index]=0;
	if(dcnt[index]<avg)
		++(dcnt[index]);
	data[index][(dpnt[index])++]=(((unsigned int)spibits[0])<<2) + (((unsigned int)spibits[1])>>6);
	timestamps[index]=cspitsts;
	//qDebug()<<"decoder writing to channel"<<index<<"buffer"<<dpnt[index]-1<<"value"<<data[index][(dpnt[index])-1]<<"count"<<dcnt[index];
	int cnt=dcnt[index];
	int res=0;
	for(int i=0; i<cnt; ++i) // array is always filled starting at 0
	{
		res+=data[index][i];
		//qDebug()<<"raw data"<<i<<data[index][i];
	}
	davg[index]=static_cast<double>(res)/static_cast<double>(cnt);
	//qDebug()<<"providing result for index"<<index<<res/(double)cnt;
	emit resultReadySig(resAdr, davg[index], cspitsts);
}

double TLV1548Decoder::rawData(int adr, qint64 * timestamp)
{
	if(adr<firstAdr || adr>lastAdr)
		return 0;
	int res=0;
	int index=adr-firstAdr;
	QMutexLocker lock(&mtx);
	if(timestamp)
		*timestamp=timestamps[index];
	int cnt=dcnt[index];

	if(!cnt)
	{
		qWarning()<<"TLV1548Decoder read data on uninitialized channel"<<adr;
		return std::numeric_limits<double>::quiet_NaN();
	}

	for(int i=0; i<cnt; ++i)
	{
		res+=data[index][i];
		//qDebug()<<"raw data"<<i<<data[index][i];
	}
	//qDebug()<<"providing result for index"<<index<<res/(double)cnt;
	return static_cast<double>(res)/static_cast<double>(cnt);
}

void TLV1548Decoder::startDecoderSl()
{
	if(timerId)
		qCritical()<<"TLV1548Decoder double start";
	else
		timerId=startTimer(tickMs);
}

void TLV1548Decoder::stopDecoderSl()
{
	if(timerId)
	{
		killTimer(timerId);
		timerId=0;
		// software power down
		spibits[0]=0x80;
		spibits[1]=0;
		wiringPiSPIDataRW(spiPort, spibits, 2);
	}
	else
		qCritical()<<"TLV1548Decoder double stop";
}

DeviceTLV1548::DeviceTLV1548(const QDomElement &elem) : Device(elem)
{
	QString p=elem.attribute("SPI");
	bool ok=false;
	spiPort=p.toInt(&ok);
	if(!ok || spiPort<0 || spiPort>1)
		throw Ex(QString("invalid port specification <%1> at device %2").arg(p).arg(id_));
	double vx=elem.attribute("ratio_min").toDouble(&ok);
	if(ok)
		ratMin=vx;
	vx=elem.attribute("ratio_max").toDouble(&ok);
	if(ok)
		ratMax=vx;
	vx=elem.attribute("u_ref").toDouble(&ok);
	if(ok)
		uRef=vx;
	int i=elem.attribute("avg").toInt(&ok);
	if(ok && i>0 && i<65536)
		avg=i;
	QDomNodeList nodes=elem.elementsByTagName("node");
	firstAdr=9;
	lastAdr=-1;
	for(int i=0; i<8; ++i)
		channels[i]=nullptr;
	for(int i=0; i<nodes.size(); ++i)
	{
		const QDomElement &elem=nodes.item(i).toElement();
		QString ntype=elem.attribute("type");
		NodeTLV1548 *n=nullptr;
		if(ntype=="PT1000")
			n = new NodeTLV1548PT1000(elem, id_, this);
		else if(ntype=="scaled")
			n = new NodeTLV1548Scaled(elem, id_, this);
		else
			n = new NodeTLV1548Raw(elem, id_, this);
		int cha=n->address().channel();
		if(cha<0 || cha>7)
			qCritical()<<"invalid channel for NodeTLV1548"<<cha;
		else
		{
			if(cha<firstAdr) firstAdr=cha;
			if(cha>lastAdr) lastAdr=cha;
			channels[cha]=n;
		}
	}
	if(lastAdr-firstAdr<0)
		throw Ex(QString("TLV1548 has no nodes"));
	interval=elem.attribute("interval").toInt();
	if(interval<=0)
		throw Ex(QString("invalid interval specification <%1> at device %2").arg(elem.attribute("interval")).arg(id_));
	qInfo()<<"DeviceTLV1548"<<id_<<firstAdr<<"->"<<lastAdr<<"periode"<<interval<<"average"<<avg<<"calib"<<uRef<<ratMin<<ratMax;
}

pi_at_home::Result DeviceTLV1548::init()
{
	if(spiPort<0)
	{
		qCritical()<<"init failed on device"<<id_<<"port not set"<<spiPort;
		return Result::FAILED;
	}
	if(interval<=0)
	{
		qCritical()<<"invalid interval specification or no nodes at device"<<id_;
		spiPort=-3;
		return Result::FAILED;
	}
	if(wiringPiSPISetup(spiPort, 2000000)<0)
	{
		qCritical()<<"cannot setup SPI";
		spiPort=-2;
		return Result::FAILED;
	}
	qDebug()<<"DeviceTLV1548 creating deconder";
	decoder=new TLV1548Decoder(spiPort, firstAdr, lastAdr, interval, avg);
	decoder->moveToThread(&worker);
	connect(this, SIGNAL(startDecoderSig()), decoder, SLOT(startDecoderSl()), Qt::QueuedConnection); // cross thread comm!
	connect(this, SIGNAL(stopDecoderSig()), decoder, SLOT(stopDecoderSl()), Qt::BlockingQueuedConnection); // cross thread comm!
	connect(decoder, &TLV1548Decoder::resultReadySig, this, &DeviceTLV1548::conversionCompletedSl);
	// initial values were read durcing construction of decoder, whitout timer running
	Result res=Device::init();
	if(res==Result::COMPLETED)
	{
		qDebug()<<"DeviceTLV1548 starting worker thread";
		worker.start();
		emit startDecoderSig();
	}
	return res;
}

pi_at_home::Result DeviceTLV1548::shutdown()
{
	if(!decoder)
		return Result::FAILED;
	qInfo()<<"stoping TLV1548 decoder...";
	emit stopDecoderSig(); // blocking
	worker.exit(); // kill event queue
	bool res=worker.wait(1000);
	if(res)
		qInfo()<<"...successful";
	else
		qCritical()<<"...waiting for worker-thread timed out";
	return Result::COMPLETED;
}

double DeviceTLV1548::raw2RelativeData(double raw)
{
	double res=ratMin+raw*(ratMax-ratMin)/1024.;
	//qDebug()<<"readRelativeData"<<rd<<res<<ratMin<<ratMax;
	return res;
}

double DeviceTLV1548::raw2AbsoluteData(double raw)
{
	return (ratMin+raw*(ratMax-ratMin)/1024.)*uRef;
}

void DeviceTLV1548::conversionCompletedSl(int channel, double avgVal, qint64 timestamp)
{
	channels[channel]->processValue(avgVal, timestamp, this);
}
