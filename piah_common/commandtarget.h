#ifndef COMMANDTARGET_H
#define COMMANDTARGET_H

#include "piadh.h"

namespace pi_at_home
{

class CommandTarget
{
public:
	virtual void processCommand(Command & cmd, Fragment & relevant) =0;
};
}
#endif // COMMANDTARGET_H
