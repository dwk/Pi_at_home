#ifndef ALGDUMMY_H
#define ALGDUMMY_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"
#include "nodealg.h"

namespace pi_at_home
{

/* Container for NodeAlg's. These nodes can be used as "variables" for intermediate results or to
 * connect algorithms where external input and output nodes are expected.
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="Dummy"			fixed
 * Nodes are defined by sub-elements as in device elements with the addition of a data_type attribute;
 * see NodeAlg.
 *
 * This algorithm does nothing except generating the nodes.
*/
class AlgDummy : public Algorithm
{
	Q_OBJECT
public:
	typedef std::map< int, NodeAlg * > NodeAlgMP;
	explicit AlgDummy(const QDomElement &elem);
	virtual ~AlgDummy() {}
	virtual void execute() {} // yes, quite dummy
	virtual bool safeState() {return true;}
protected:
	virtual Result preExec();
	virtual Result postExec();
	NodeAlgMP nodeAlgMP;
};

}
#endif // ALGDUMMY_H
