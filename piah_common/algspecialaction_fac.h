#ifndef ALGSPECIALACTION_FAC_H
#define ALGSPECIALACTION_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgSpecialAction(elem);
}
const QString typeName="SpecialAction";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGSPECIALACTION_FAC_H
