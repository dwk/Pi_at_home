#include "nodelcd84x48.h"
#include "nodelcd84x48_fac.h"
#include <unistd.h> // usleep
//#include <wiringPi.h>
#include <wiringPiSPI.h>
#include "command.h"
#include "gpiowrapper.h"
#include "piadi.h"

static unsigned char bitPattern[][5] = {
{0x00, 0x00, 0x00, 0x00, 0x00}, // 20	0
{0x00, 0x00, 0x5f, 0x00, 0x00}, // 21 !	1
{0x00, 0x07, 0x00, 0x07, 0x00}, // 22 "	2
{0x14, 0x7f, 0x14, 0x7f, 0x14}, // 23 #	3
{0x24, 0x2a, 0x7f, 0x2a, 0x12}, // 24 $	4
{0x23, 0x13, 0x08, 0x64, 0x62}, // 25 %	5
{0x36, 0x49, 0x55, 0x22, 0x50}, // 26 &	6
{0x00, 0x05, 0x03, 0x00, 0x00}, // 27 '	7
{0x00, 0x1c, 0x22, 0x41, 0x00}, // 28 (	8
{0x00, 0x41, 0x22, 0x1c, 0x00}, // 29 )	9
{0x14, 0x08, 0x3e, 0x08, 0x14}, // 2a *	10
{0x08, 0x08, 0x3e, 0x08, 0x08}, // 2b +	11
{0x00, 0x50, 0x30, 0x00, 0x00}, // 2c ,	12
{0x08, 0x08, 0x08, 0x08, 0x08}, // 2d -	13
{0x00, 0x60, 0x60, 0x00, 0x00}, // 2e .	14
{0x20, 0x10, 0x08, 0x04, 0x02}, // 2f /	15
{0x3e, 0x51, 0x49, 0x45, 0x3e}, // 30 0	16
{0x00, 0x42, 0x7f, 0x40, 0x00}, // 31 1	17
{0x42, 0x61, 0x51, 0x49, 0x46}, // 32 2	18
{0x21, 0x41, 0x45, 0x4b, 0x31}, // 33 3	19
{0x18, 0x14, 0x12, 0x7f, 0x10}, // 34 4	20
{0x27, 0x45, 0x45, 0x45, 0x39}, // 35 5	21
{0x3c,	0x4a,	0x49,	0x49,	0x30}	, //	36	6	22
{0x01,	0x71,	0x09,	0x05,	0x03}	, //	37	7	23
{0x36,	0x49,	0x49,	0x49,	0x36}	, //	38	8	24
{0x06,	0x49,	0x49,	0x29,	0x1e}	, //	39	9	25
{0x00,	0x36,	0x36,	0x00,	0x00}	, //	3a	:	26
{0x00,	0x56,	0x36,	0x00,	0x00}	, //	3b	;	27
{0x08,	0x14,	0x22,	0x41,	0x00}	, //	3c	<	28
{0x14,	0x14,	0x14,	0x14,	0x14}	, //	3d	=	29
{0x00,	0x41,	0x22,	0x14,	0x08}	, //	3e	>	30
{0x02,	0x01,	0x51,	0x09,	0x06}	, //	3f	?	31
{0x32,	0x49,	0x79,	0x41,	0x3e}	, //	40	@	32
{0x7e,	0x11,	0x11,	0x11,	0x7e}	, //	41	A	33
{0x7f,	0x49,	0x49,	0x49,	0x36}	, //	42	B	34
{0x3e,	0x41,	0x41,	0x41,	0x22}	, //	43	C	35
{0x7f,	0x41,	0x41,	0x22,	0x1c}	, //	44	D	36
{0x7f,	0x49,	0x49,	0x49,	0x41}	, //	45	E	37
{0x7f,	0x09,	0x09,	0x09,	0x01}	, //	46	F	38
{0x3e,	0x41,	0x49,	0x49,	0x7a}	, //	47	G	39
{0x7f,	0x08,	0x08,	0x08,	0x7f}	, //	48	H	40
{0x00,	0x41,	0x7f,	0x41,	0x00}	, //	49	I	41
{0x20,	0x40,	0x41,	0x3f,	0x01}	, //	4a	J	42
{0x7f,	0x08,	0x14,	0x22,	0x41}	, //	4b	K	43
{0x7f,	0x40,	0x40,	0x40,	0x40}	, //	4c	L	44
{0x7f,	0x02,	0x0c,	0x02,	0x7f}	, //	4d	M	45
{0x7f,	0x04,	0x08,	0x10,	0x7f}	, //	4e	N	46
{0x3e,	0x41,	0x41,	0x41,	0x3e}	, //	4f	O	47
{0x7f,	0x09,	0x09,	0x09,	0x06}	, //	50	P	48
{0x3e,	0x41,	0x51,	0x21,	0x5e}	, //	51	Q	49
{0x7f,	0x09,	0x19,	0x29,	0x46}	, //	52	R	50
{0x46,	0x49,	0x49,	0x49,	0x31}	, //	53	S	51
{0x01,	0x01,	0x7f,	0x01,	0x01}	, //	54	T	52
{0x3f,	0x40,	0x40,	0x40,	0x3f}	, //	55	U	53
{0x1f,	0x20,	0x40,	0x20,	0x1f}	, //	56	V	54
{0x3f,	0x40,	0x38,	0x40,	0x3f}	, //	57	W	55
{0x63,	0x14,	0x08,	0x14,	0x63}	, //	58	X	56
{0x07,	0x08,	0x70,	0x08,	0x07}	, //	59	Y	57
{0x61,	0x51,	0x49,	0x45,	0x43}	, //	5a	Z	58
{0x00,	0x7f,	0x41,	0x41,	0x00}	, //	5b	[	59
{0x02,	0x04,	0x08,	0x10,	0x20}	, //	5c	\	60
{0x00,	0x41,	0x41,	0x7f,	0x00}	, //	5d	]	61
{0x04,	0x02,	0x01,	0x02,	0x04}	, //	5e	^	62
{0x40,	0x40,	0x40,	0x40,	0x40}	, //	5f	_	63
{0x00,	0x01,	0x02,	0x04,	0x00}	, //	60	`	64
{0x20,	0x54,	0x54,	0x54,	0x78}	, //	61	a	65
{0x7f, 0x48, 0x44, 0x44, 0x38}			, //	62	b ,	66
{0x38, 0x44, 0x44, 0x44, 0x20}			, //	63	c ,	67
{0x38, 0x44, 0x44, 0x48, 0x7f}			, //	64	d ,	68
{0x38, 0x54, 0x54, 0x54, 0x18}			, //	65	e ,	69
{0x08, 0x7e, 0x09, 0x01, 0x02}			, //	66	f ,	70
{0x0c, 0x52, 0x52, 0x52, 0x3e}			, //	67	g ,	71
{0x7f, 0x08, 0x04, 0x04, 0x78}			, //	68	h ,	72
{0x00, 0x44, 0x7d, 0x40, 0x00}			, //	69	i ,	73
{0x20, 0x40, 0x44, 0x3d, 0x00}			, //	6a	j ,	74
{0x7f, 0x10, 0x28, 0x44, 0x00}			, //	6b	k ,	75
{0x00, 0x41, 0x7f, 0x40, 0x00}			, //	6c	l ,	76
{0x7c, 0x04, 0x18, 0x04, 0x78}			, //	6d	m ,	77
{0x7c, 0x08, 0x04, 0x04, 0x78}			, //	6e	n ,	78
{0x38, 0x44, 0x44, 0x44, 0x38}			, //	6f	o ,	79
{0x7c, 0x14, 0x14, 0x14, 0x08}			, //	70	p ,	80
{0x08, 0x14, 0x14, 0x18, 0x7c}			, //	71	q ,	81
{0x7c, 0x08, 0x04, 0x04, 0x08}			, //	72	r ,	82
{0x48, 0x54, 0x54, 0x54, 0x20}			, //	73	s ,	83
{0x04, 0x3f, 0x44, 0x40, 0x20}			, //	74	t ,	84
{0x3c, 0x40, 0x40, 0x20, 0x7c}			, //	75	u ,	85
{0x1c, 0x20, 0x40, 0x20, 0x1c}			, //	76	v ,	86
{0x3c, 0x40, 0x30, 0x40, 0x3c}			, //	77	w ,	87
{0x44, 0x28, 0x10, 0x28, 0x44}			, //	78	x ,	88
{0x0c, 0x50, 0x50, 0x50, 0x3c}			, //	79	y ,	89
{0x44, 0x64, 0x54, 0x4c, 0x44}			, //	7a	z ,	90
{0x00, 0x08, 0x36, 0x41, 0x00}			, //	7b	{ ,	91
{0x00, 0x00, 0x7f, 0x00, 0x00}			, //	7c	| ,	92
{0x00, 0x41, 0x36, 0x08, 0x00}			, //	7d	} ,	93
{0x10, 0x08, 0x08, 0x10, 0x08}			, //	7e	~ ,	94
{0x78, 0x46, 0x41, 0x46, 0x78}			, //	7f	DEL	95
{0xff, 0xff, 0xff, 0xff, 0xff}			}; //	80	<block>	96

static unsigned char bitPatternEx[][6] = {
{0xB0,    0,    7,    5,    7,    0},		//	B0 176 °
{0xC4,  125,   34,   34,   34,  125},		//	C4 196 Ä
{0xD6,   61,   66,   66,   66,   61},		//	D6 214 Ö
{0xDC, 0x3d, 0x40, 0x40, 0x40, 0x3d},		//	DC 220 Ü
{0xDF,  126,    1,   21,   10,    0},		//	DF 223 ß
{0xE4, 0x20, 0x55, 0x54, 0x55, 0x78},		//	E4 228 ä
{0xF6, 0x38, 0x45, 0x44, 0x45, 0x38},		//	F6 246 ö
{0xFC, 0x3c, 0x41, 0x40, 0x21, 0x7c},		//	FC 252 ü
};
static int bitPatternExSize=8;

NodeLcd84x48Line::NodeLcd84x48Line(const QString &name, int device, int channel, QObject *parent) :
	Node(name, device, channel, parent)
{
	if(adr_.channel()<0 || adr_.channel()>5)
		throw Ex(QString("invalide channel / line in NodeLcd84x48Line %1").arg(adr_.toStringVerbose()));
}

const QString &NodeLcd84x48Line::type() const
{
	return typeName;
}

Result NodeLcd84x48Line::init()
{
	val_="01234501234501";
	value_=val_;
	DeviceLcd84x48 *d=qobject_cast<DeviceLcd84x48*>(parent());
	if(!d)
		throw Ex(QString("init failed on %1").arg(adr_.toStringVerbose()));
	d->writeLine(val_, adr_.channel());
	state_=State::READY;
	return Result::COMPLETED;
}

void NodeLcd84x48Line::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	if(!value.canConvert<QString>())
		throw Ex(QString("NodeValue not convertible to string on %1").arg(adr_.toStringVerbose()));
	if(!tokenValid(token))
	{
		qWarning()<<adr_.toString()<<"got invalid token"<<token;
		return;
	}
	setValue(value.toString());
}

void NodeLcd84x48Line::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		relevant.setAttribute("val", val_);
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else if(relevant.cmd()=="nodeset")
	{
		setValue(relevant.attribute("val"));
		return;
	}
	else
		qCritical()<<"unknown command for NodeSwitchPort"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeLcd84x48Line::newValSl(const NodeValue &newValue)
{
	if(!newValue.canConvert<QString>())
		throw Ex(QString("NodeValue not convertible to string on %1").arg(adr_.toStringVerbose()));
	setValue(newValue.toString());
}

void NodeLcd84x48Line::setValue(const QString &value)
{
	if(value==val_)
		return; // nothing to do!
	if(state()!=Node::State::READY)
	{
		qWarning()<<"set value ignored while not ready"<<adr_.toStringVerbose()<<Node::state(state());
		return;
	}
	//qDebug()<<objectName()<<value;
	val_=value;
	value_=val_;
	DeviceLcd84x48 *d=qobject_cast<DeviceLcd84x48*>(parent());
	if(!d)
		throw Ex(QString("write failed on %1").arg(adr_.toStringVerbose()));
	d->writeLine(val_, adr_.channel());
	emit newValSig(value_, DCON.getUsecTst());
}


DeviceLcd84x48::DeviceLcd84x48(const QDomElement &elem) : Device(elem)
{
	QString p=elem.attribute("SPI");
	bool ok=false;
	portSpi_=p.toInt(&ok);
	if(!ok || portSpi_<0 || portSpi_>1)
		throw Ex(QString("invalid SPI-channel specification <%1> at device %2").arg(p).arg(id_));
	ok=false;
	p=elem.attribute("d_c");
	if(p.startsWith("WP"))
		portDC=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid data/command port <%1> at device %2").arg(p).arg(id_));
	ok=false;
	p=elem.attribute("reset");
	if(p.startsWith("WP"))
		portReset=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid reset port <%1> at device %2").arg(p).arg(id_));
	if(elem.attribute("hidden")=="true")
		hiddenNodes=true;
	QString nmtempl=QString("%1%2").arg(namePrefix.isEmpty()?"line-":namePrefix);
	for(int i=0; i<6; ++i)
	{
		Node * nd=new NodeLcd84x48Line(nmtempl.arg(i), id_, i, this);
		nd->setHidden(hiddenNodes);
	}
}

pi_at_home::Result DeviceLcd84x48::init()
{
	wiringPiSPISetup(portSpi_, 1000000); // channel 1, 1MHz
	hwPortDC=DCON.io()->createOutPort(portDC);
	if(!hwPortDC)
		throw Ex(QString("init hwPortDC failed"));
	hwPortReset=DCON.io()->createOutPort(portReset);
	if(!hwPortReset)
		throw Ex(QString("init hwPortReset failed"));
	// apply reset pulse
	hwPortReset->setBit(false);
	usleep(50);
	hwPortReset->setBit(true);
	hwPortDC->setBit(false); // command mode
	unsigned char cmd[]={
		0x21, // power up, horizontal addressing, extended commands
		0xB0, // 0xB0 Vop=b0110000=d48 U=3-06+0.06*48=5.95V
		0x04, // TC = 00, lowest 1mV/K
		0x13, // BS = b011 for 1:48MUX (orig. b100, 0x14)
		0x20, // power up, horizontal addressing, std commands
		0x0C}; // 0x0C display configuration DE=b10 = normal display
	wiringPiSPIDataRW(portSpi_, cmd, 6); // MSB first is fine
	return Device::init();
}

pi_at_home::Result DeviceLcd84x48::shutdown()
{
	//qDebug()<<"DeviceLcd84x48::shutdown"<<portSpi_;
	unsigned char cmd[]={
		0x24}; // power down, horizontal addressing, std commands
	hwPortDC->setBit(false); // command mode
	wiringPiSPIDataRW(portSpi_, cmd, 1); // MSB first is fine
	//QThread::msleep(1);
	/* nothing to do with ports - ~HwPort will perform shutdown */
	//qDebug()<<"DeviceLcd84x48::shutdown"<<portSpi_;
	return Result::COMPLETED;
}

void DeviceLcd84x48::moveToLine(int line)
{
	hwPortDC->setBit(false); // command mode
	unsigned char cmd[]={0x80, 0};
	cmd[1]=0x40 | line;
	wiringPiSPIDataRW(portSpi_, cmd, 2); // MSB first is fine
}

void DeviceLcd84x48::writeLine(const QString text, int line)
{
	moveToLine(line);
	unsigned char d[84];
	hwPortDC->setBit(true); // data mode
	int len=text.size();
	if(len>14)
	{
		qWarning()<<"DeviceLcd84x48 line >14 chars"<<len;
		len=14;
	}
	int dc=0;
	for(int i=0; i<len; ++i)
	{
		ushort ucode=text.at(i).unicode();
		if(ucode<32)
			ucode=96;
		else if(ucode>127)
		{
			//qDebug()<<"searching"<<ucode;
			int csi=0;
			for(; csi<bitPatternExSize; ++csi)
			{
				//qDebug()<<"testing"<<bitPatternEx[csi][0];
				if(bitPatternEx[csi][0]==ucode)
				{
					for(int ii=0; ii<5; ++ii, ++dc)
						d[dc]=bitPatternEx[csi][ii+1];
					ucode=256;
					break;
				}
			}
			if(csi==bitPatternExSize)
				ucode=96; // not found in extended list
			//qDebug()<<"found"<<ucode;
		}
		else
			ucode-=32;
		for(int ii=0; ii<5 && ucode<256; ++ii, ++dc)
			d[dc]=bitPattern[ucode][ii];
		d[dc++]=0;
	}
	wiringPiSPIDataRW(portSpi_, d, len*6); // MSB first is fine
}
