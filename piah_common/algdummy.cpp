#include "algdummy.h"
#include "algdummy_fac.h"
#include "node.h"
#include "command.h"
#include "piadi.h"

AlgDummy::AlgDummy(const QDomElement &elem) :
	Algorithm(elem)
{
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement el=nodes.item(i).toElement();
		QString type=el.attribute("data_type");
		if(type=="bool")
		{
			new NodeBool(el, id_, this);
		}
		else if(type=="int")
		{
			new NodeInt(el, id_, this);
		}
		else if(type=="double")
		{
			new NodeDbl(el, id_, this);
		}
		else if(type=="string")
		{
			new NodeStr(el, id_, this);
		}
		else
			qCritical()<<"node type unkwon"<<type<<"in"<<this->objectName();
	}
}

pi_at_home::Result AlgDummy::preExec()
{
	if(algState!=AlgState::STARTUP)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	/*
	stateNode->setValue(NodeValue("initialization failed")); // be pessimistic, will be overwritten during update
	ndOut=DCON.findNode(ndOutNm, false);
	ok=(!argsFailed);
	if(!ok)
		return Result::FAILED;
	if(!ndOut)
	{
		qCritical()<<objectName()<<"cannot acquire output"<<ndOutNm;
		ok=false;
		return Result::FAILED;
	}
	if(ndOut->getLocker())
	{
		qCritical()<<objectName()<<ndOutNm<<"already locked";
		ok=false;
		return Result::FAILED;
	}
	token=ndOut->lock(DCON.uid(), id_, objectName());
	if(updateOnInit)
		ndOut->setValue(NodeValue(pulse?false:(startval?true:false)), token);
	int ti=0;
	for(NodesVSP::iterator it=delayNodes.begin(); it!=delayNodes.end(); ++it, ++ti)
		if(*it)
			(*it)->setValue(NodeValue((double)times[ti]/1000.));
	t0=QDateTime::currentMSecsSinceEpoch();
*/
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

pi_at_home::Result AlgDummy::postExec()
{
	return Result::COMPLETED;
}

