#ifndef ALGSOLAR_H
#define ALGSOLAR_H

#include "piadh.h"
#include <QDateTime>
#include <QMutex>
#include "algorithm.h"
#include "nodealg.h"

namespace pi_at_home
{
class NodeBitPort;
/* Algorithm to switch the solar heater between freshwater heating and earth collector.
 * The aim is to avoid overheating of the solar collector while keeping the fresh water
 * as hot as possible.
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="SolarProtector"	fixed
 *  Tvalvestate="x"	x = node alias; node STATE is used to determine "ready for next state", i.e. algorithm will wait until READY
 *  pumpearthstate="x"x = node alias; node STATE is used to determine "ready for next state", i.e. algorithm will wait until READY
 * optional attributes
 *	def_max_Troofc="x"		x = initial value of max_Troofc (degree C), default 95
 *	def_min_Tfresh="x"		x = initial value of min_Tfresh (degree C), default 50
 *	def_earthSleepHoldoffS="x" x = seconds to wait after solar pump stop until earth pump is turned off, default 900 (15 minutes)
 * connectors
 *	input_Troofc="x"		x = node alias; node value is converted to double, degree C at solar roof collector
 *	input_Tfreshtop="x"		x = node alias; node value is converted to double, degree C at hot end / top of fresh water storage
 *	input_pumpsolar="x"		x = node alias; node value is converted to bool, true indicates that the solar pump is active
 *	input_pumpearth="x"		x = node alias; node value is converted to bool, true indicates that the earth collector pump shall be
 *							switched on (heat pump request)
 * created nodes
 *	<prefix>sw_ctrl_ref		bool	ch 0	passive = solar controller ref temp switched to fresh water
 *											(=cool side, bottom), active = earth collector return line
 *	<prefix>sw_Tvalve		bool	ch 1	passive = solar collector connected to freah water, active = collector connected to earth collector
 *	<prefix>sw_pumpearthc	bool	ch 2	passive = earth collector pump off, active = on
 *	<prefix>state			string	ch 10	human readable state information
 *	<prefix>max_Troofc		double	ch 11	max solar collector temperature, will switch to EARTH when reached
 * 	<prefix>min_Tfresh		double	ch 12	min fresh water temperture (top, hot side) will switch tu WATER when reached
 *	<prefix>force_WATER		bool	ch 13	force to WATER mode, even if input_Troofc>max_Troofc; default off
 *	<prefix>force_EARTH		bool	ch 14	force to EARTH mode, even if input_Tfreshtop<min_Tfresh; default off
 *
 * The solar heat is either routed to the fresh water resevoir (output_SWsctrlref and output_SWTvalve passive)
 * called WATER-mode or to the earth collector (output_SWsctrlref and output_SWTvalve active) called EARTH-mode.
 * Startup and shutdown configuration is WATER. If input_Troofc gets over max_Troofc the algorithm switches to
 * EARTH. If the hot end of the resevoir gets below min_Tfresh it switches back to WATER.
 * You can force the algorithm to switch to a certain state by force_WATER and force_EARTH. Holding the node active
 * will override the limits, so be careful with force_WATER since it may overheat both the collector and the storage
 * tank. If these inputs are only pulsed the hardware will switch to the given mode but will return if outside the limits.
 * sw_pumpearthc is on when in EARTH-mode or if requested by input_pumpearth, except when
 * input_pumpsolar is off for some time and input_pumpearth is also off. (No need to run the earth collector pump
 * if no solar heat is available, e.g. during night.)
 * Switching between WATER and EARTH follows a state machine with staggered activites; "state" reflects this.
*/
class AlgSolarProtector : public Algorithm
{
	Q_OBJECT
public:
	enum class OpMode {UNDEF=0,
				WATER,			// 1 heat water: ndTValve=false=through res; ndPT1000Rel=false=rs bottom temp; ndPumpHp=false=off
								//   if ndTRoof>switchTemp_ --> TO_EARTH_PUMP
				TO_EARTH_PUMP,  // 2 spin up earth pump: ndPumpHp->true=on (use delay in NodeBitPort)
				TO_EARTH_TVALVE,// 3 switch ref temp and T-valve and let stabilize: ndTValve->true=through heat-exchanger; ndPT1000Rel->true=earth coll temp
								//   stabilization time is set through delay on ndTValve
				EARTH,			// 4 heat earth collector: ndTValve=true=through heat-exchanger; ndPT1000Rel=true=earth coll temp; ndPumpHp=true=on
				EARTH_STANDBY,	// 5 solar pump not active AND TIME?
				TO_WATER_TVALVE,// 6
				TO_WATER_PUMP,	// 7
				ERROR,			// 8
				SHUTDOWN		// 9 fast move to WATER
			};
	AlgSolarProtector(const QDomElement &elem);
	virtual ~AlgSolarProtector();
	virtual void execute();
	virtual bool safeState();
	//bool setParam(const QString & name, const NodeValue &value);
protected slots:
	void nvTRoofcSl(const NodeValue & newValue, qint64 timestamp);
	void nvTFreshTopSl(const NodeValue & newValue, qint64 timestamp);
	void nvPumpSolarSl(const NodeValue & newValue, qint64 timestamp);
	void nvPumpEarthSl(const NodeValue & newValue, qint64 timestamp);
	void nvSwitchTempRoofSl(const NodeValue & newValue, qint64 timestamp);
	void nvSwitchTempResSl(const NodeValue & newValue, qint64 timestamp);
	void nvForceEarthSl(const NodeValue & newValue, qint64 timestamp);
	void nvForceWaterSl(const NodeValue & newValue, qint64 timestamp);
	void earthPumpSetLocking();
	void earthPumpSet();
	void earthPumpSleepLocking();
protected:
	//virtual void timerEvent(QTimerEvent *event);
	virtual Result preExec();
	virtual Result postExec();
	void toMode(OpMode newMode);
	QMutex exeLock;
	QString ndTRoofcNm, ndTFreshTopNm, ndPumpSolarNm, ndPumpEarthNm, ndSWTvalveStateNm, ndSWPumpEarthcStateNm;
	double tRoofc=-1000., tFreshTop=-1000.;
	bool pumpSolar=false, pumpEarth=false;
	// created nodes
	NodeBool *ndSWSctrlRef=nullptr, *ndSWTvalve=nullptr, *ndSWPumpEarthc=nullptr;
	Node *ndSWTvalveState=nullptr, *ndSWPumpEarthcState=nullptr;
	NodeStr * ndState=nullptr;
	NodeDbl * ndSwitchTRoofc=nullptr, *ndSwitchTFreshTop=nullptr;
	double switchTRoofc=95., switchTFreshTop=50.;
	NodeBool *ndForceWater=nullptr, * ndForceEarth=nullptr;
	bool forceWater=false, forceEarth=false;
	//
	OpMode opMode=OpMode::UNDEF;
	int earthSleepHoldoffMs=900000; // wait 15 minutes
	QDateTime lastSolarOn;
};


}
#endif // ALGSOLAR_H
