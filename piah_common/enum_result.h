#ifndef ENUM_RESULT_H
#define ENUM_RESULT_H

#include "piadh.h"

namespace pi_at_home
{
enum class Result{ UNDEF=0, COMPLETED=1, PENDING=2, FAILED=3 };
inline Result result(const QString & resultStr)
{
	if(resultStr=="COMPLETED")
		return Result::COMPLETED;
	else if(resultStr=="PENDING")
		return Result::PENDING;
	else if(resultStr=="FAILED")
		return Result::FAILED;
	return Result::UNDEF;
}

inline QString result(const Result & result)
{
	switch(result)
	{
		case Result::COMPLETED:
			return "COMPLETED";
		case Result::PENDING:
			return "PENDING";
		case Result::FAILED:
			return "FAILED";
		default:
			return "UNDEF";
	}
}
}
#endif // ENUM_RESULT_H
