#ifndef ALGSOLARPROTECTOR_FAC_H
#define ALGSOLARPROTECTOR_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgSolarProtector(elem);
}
const QString typeName="SolarProtector";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGSOLARPROTECTOR_FAC_H
