#ifndef ALGDESPIKE_H
#define ALGDESPIKE_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"

namespace pi_at_home
{

class NodeDbl;

/* AlgDespike
 * Parameters see ConfigEditorMarkup.xml
*/
class AlgDespike : public Algorithm
{
	Q_OBJECT
public:
	explicit AlgDespike(const QDomElement &elem);
	virtual ~AlgDespike() {}
	virtual void execute() override;
	virtual bool safeState() override {return true;}
protected:
	virtual Result preExec() override;
	virtual Result postExec() override;
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp);
protected:
	QString inName;
	double stepNeg=0., stepPos=0., lastVal=0.;
	Node *ndInput=nullptr;
	NodeDbl *ndOut=nullptr;
	Node::TokenType token=0;
	int forceCnt=1, forceI=1;
	bool linearInc=false;
};


}
#endif // ALGDESPIKE_H
