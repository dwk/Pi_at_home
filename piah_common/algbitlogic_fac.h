#ifndef ALGBITLOGIC_FAC_H
#define ALGBITLOGIC_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgBitLogic(elem);
}
const QString typeName="BitLogic";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGBITLOGIC_FAC_H
