#ifndef ALGCOMPARE_H
#define ALGCOMPARE_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"

namespace pi_at_home
{
/* AlgCompare compares one input value against 1 to N levels
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="Compare"			fixed
 *  op="x"					x = compare operation, see below
 * connectors and input nodes (type_name)
 *	input(<datatype>)		connector	name=source, <datatype> is int or dbl
 *	level(<datatype>)		input		name=level<N>, comparison level 0..N, <datatype> is int or dbl
 * output nodes
 *  <prefix>out<N>			bool		0..N or N+1 depending on operation, result of comparison with level n
 *  <prefix>error			bool		true if levels are not in sequence and output will not be updated
 *
 * Levels must be numbered by their channels. Input and levels myst match in their <datatype>.
 * The compare operation may be one of:
 *  lt	less than		o<N> is true if input < level<N>
 *  le	less or equal	o<N> is true if input <= level<N>
 *  eq	equal			o<N> is true if input == level<N> (not recommended for double)
 *  ge	greater or equal o<N> is true if input >= level<N>
 *  gt	greater than	o<N> is true if input > level<N>
 *  ioo open intervall	(), not includint either end
 *  icc closed intervall [], including both ends
 *  ioc (intervall]
 *  ico [intervall)
 * The operations lt, le, eq, ge, and gt will create as many output nodes as levels.
 * The intervall operations create N+1 output nodes: o0 covers minus inf to level0, o1 covers level0 to level1
 * and so on. o<N+1> corresponds to levelN to plus inf.
*/
class AlgCompare : public Algorithm
{
	Q_OBJECT
public:
	enum class BaseOp{ UNDEF=0, LT, LE, EQ, GE, GT, IOO, ICC, IOC, ICO};
	explicit AlgCompare(const QDomElement &elem);
	virtual ~AlgCompare();
	virtual void execute();
	virtual bool safeState() {return true;}
protected:
	virtual Result preExec();
	virtual Result postExec();
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp, int inChannel);
protected:
	void levelcheck();
	QString ndInNm;
	int lcnt=0, inValI=0, *levelI=nullptr;
	double inValD=0., *levelD=nullptr;
	NodesVSP ndOuts, ndLevels;
	Node *ndError=nullptr, *ndIRes=nullptr;
	bool * ress=nullptr;
	std::vector<Node::TokenType> tokens;
	BaseOp op=BaseOp::UNDEF;
	bool useInt=false;
};


}
#endif // ALGCOMPARE_H
