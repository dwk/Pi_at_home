#include "algbitlogic.h"
#include "algbitlogic_fac.h"
#include "node.h"
#include "command.h"
#include "nodealg.h"
#include "piadi.h"

AlgBitLogic::AlgBitLogic(const QDomElement &elem) :
	Algorithm(elem)
{
	ndOut=new NodeBool(namePrefix+"out", id_, 0, this);
	QString opnm=elem.attribute("op");
	if(opnm=="unity")
	{
		op=BaseOp::UNITY;
	}
	else if(opnm=="not")
	{
		op=BaseOp::UNITY;
		negate=true;
	}
	else if(opnm=="and")
	{
		op=BaseOp::AND;
	}
	else if(opnm=="or")
	{
		op=BaseOp::OR;
	}
	else if(opnm=="parity")
	{
		op=BaseOp::PARITY;
	}
	else if(opnm=="nand")
	{
		op=BaseOp::AND;
		negate=true;
	}
	else if(opnm=="nor")
	{
		op=BaseOp::OR;
		negate=true;
	}
	else if(opnm=="notparity")
	{
		op=BaseOp::PARITY;
		negate=true;
	}
	else if(opnm=="true")
	{
		op=BaseOp::CONSTANT;
	}
	else if(opnm=="false")
	{
		op=BaseOp::CONSTANT;
		negate=true;
	}
	if(op==BaseOp::UNDEF)
		qCritical()<<objectName()<<": undefined operation"<<opnm;
	// v1.3 config
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		if(e.attribute(QStringLiteral("type_name"))!=QStringLiteral("input"))
			continue;
		ndInsNm<<e.attribute(QStringLiteral("name"));
		// assume xml order is according to channel numbering
	}
	/* pre v1.3 config
	QString templ="input%1";
	for(int i=0; true; ++i)
	{
		QString nm=elem.attribute(templ.arg(i));
		if(nm.isEmpty())
			break;
		ndInsNm<<nm;
	}*/
	if(!ndInsNm.size() && op!=BaseOp::CONSTANT)
		qCritical()<<objectName()<<": no input nodes";
	if(op==BaseOp::UNITY && ndInsNm.size()>1)
		qWarning()<<objectName()<<": more than one input for unity / not operatrion";
	inVals=new bool[ndInsNm.size()];
}

AlgBitLogic::AlgBitLogic(const QString &inNodeName, int id, QObject *parent) :
	Algorithm(id, parent), ndInsNm(inNodeName)
{
}

void AlgBitLogic::execute()
{
	if(algState!=AlgState::RUNNING && algState!=AlgState::STARTUP)
		return;
	bool res;
	switch(op)
	{
		case BaseOp::AND:
		case BaseOp::CONSTANT:
			res=true;
			break;
		default:
			res=false;
	}
	for(int i=0; i<ndInsNm.size(); ++i)
	{
		switch(op)
		{
			case BaseOp::AND:
				res=res && inVals[i];
				break;
			case BaseOp::OR:
				res=res || inVals[i];
				break;
			case BaseOp::PARITY:
				if(inVals[i])
					res=!res;
				break;
			case BaseOp::UNITY:
				res=inVals[i];
				break;
			default:
				break;
		}
	}
	if(negate)
		res=!res;
	ndOut->setValue(NodeValue(res), token); // will bypass if not changed
}

pi_at_home::Result AlgBitLogic::preExec()
{
	if(algState!=AlgState::STARTUP || op==BaseOp::UNDEF)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	NodeValue newValue;
	for(int i=0; i<ndInsNm.size(); ++i)
	{
		QString ndnm=ndInsNm.at(i);
		Node * nd=DCON.findNode(ndnm, false);
		if(nd)
		{
			connect(nd, &Node::newValSig, [this, i](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, i);});
			newValue=nd->value();
			inVals[i]=newValue.toBool();
			//qDebug()<<"AlgBitLogic::preExec"<<ndnm<<i<<inVals[i];
		}
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<"cannot find input"<<ndnm;
			break;
		}
	}
	if(algState==AlgState::ERROR)
		return Result::FAILED;
	if(!ndOut)
	{
		qCritical()<<objectName()<<"no output node";
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	if(ndOut->getLocker())
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"out already locked";
		return Result::FAILED;
	}
	token=ndOut->lock(DCON.uid(), id_, objectName());
	if(!token)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot lock out";
		return Result::FAILED;
	}
	execute();
	QStringList dbugStr;
	for(int i=0; i<ndInsNm.size(); ++i)
		dbugStr<<(inVals[i]?"T":"F");
	//qDebug()<<objectName()<<"init"<<dbugStr.join('|')<<ndOut->value();
	return Result::COMPLETED;
}

pi_at_home::Result AlgBitLogic::postExec()
{
	if(ndOut)
	{
		ndOut->setValue(ndOut->defaultValue(), token);
		ndOut->unlock(token);
	}
	return Result::COMPLETED;
}

void AlgBitLogic::valCaptureSl(const NodeValue &newValue, qint64 timestamp, int inChannel)
{
	Q_UNUSED(timestamp)
	//qDebug()<<"valCaptureSl"<<inChannel<<newValue;
	if(inChannel>=ndInsNm.size())
		return;
	inVals[inChannel]=newValue.toBool();
	execute();
}
