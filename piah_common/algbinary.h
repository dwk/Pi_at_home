#ifndef ALGBINARY_H
#define ALGBINARY_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"

namespace pi_at_home
{

/* AlgBinary
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="Binary"			fixed
 * optional attributes
 *	max-count
 *  min-count
 *  reset2zero
 * created nodes
 *  <prefix>count			int	channel 32
 *  <prefix>sign			int	channel 33
 *  <prefix>cntbitN			int	channel 0-31 if/as defined in config
 *  <prefix>incr			int	channel 100
 *  <prefix>decr			int	channel 101
 *  <prefix>reset			int	channel 102
 *
 * positive transition on incr, decr and reset will increment, decrment or reset to zero the count.
*/
class AlgBinary : public Algorithm
{
	Q_OBJECT
public:
	explicit AlgBinary(const QDomElement &elem);
	virtual ~AlgBinary() {}
	virtual void execute();
	virtual bool safeState() {return true;}
protected:
	virtual Result preExec();
	virtual Result postExec();
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp);
protected:
	typedef std::map< int, Node* > NodeMP;
	QString ndNmInput;
	NodeMP ndBits;
	Node *ndInput=nullptr, *ndSign=nullptr;
	int val=0;
};


}
#endif // ALGBINARY_H
