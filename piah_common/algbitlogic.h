#ifndef ALGBITLOGIC_H
#define ALGBITLOGIC_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"

namespace pi_at_home
{

/* AlgBitLogic combines one or more inputs using a boolean logic function to one output.
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="BitLogic"			fixed
 *	op="x"					x = operation; list of valid operations see below
 * optional attributes
 *	<none>
 * output node
 *  <prefix>out			bool	channel 0, result of the operation
 *
 * Inputs must be numbered consecutive starting at zero.
 * Input nodeValues are cast to bool. Output node is bool.
 * The operation may be one of:
 *  true		creates an active signal at output
 *  false		creates an inactive signal at output
 *	unity		Mirror the highest input to output. All other inputs are read but ignored.
 *	not			Mirror the inverse of the highest input to output. All other inputs are read but ignored.
 *	and			Logical AND of all inputs
 *	or			Logical OR of all inputs
 *	parity		true if an uneven number of inputs is true
 *	nand		Logical NAND of all inputs
 *	nor			Logical NOR of all inputs
 *	notparity	true if an even number of inputs is true
 *
 * Input nodes are defined by sub-elements as in device elements with the addition of type_name="input" attribute
*/
class AlgBitLogic : public Algorithm
{
	Q_OBJECT
public:
	enum class BaseOp{ UNDEF=0, UNITY, AND, OR, PARITY, CONSTANT};
	explicit AlgBitLogic(const QDomElement &elem);
	AlgBitLogic(const QString & inNodeName, int id=0, QObject * parent =nullptr);
	virtual ~AlgBitLogic() {delete [] inVals;}
	virtual void execute();
	virtual bool safeState() {return true;}
protected:
	virtual Result preExec();
	virtual Result postExec();
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp, int inChannel);
protected:
	QStringList ndInsNm;
	//QString ndOutNm;
	//NodesVSP ndIns;
	bool * inVals=nullptr;
	Node * ndOut=nullptr;
	Node::TokenType token=0;
	BaseOp op=BaseOp::UNDEF;
	bool negate=false;
};


}
#endif // ALGBITLOGIC_H
