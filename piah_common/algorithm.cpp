#include "algorithm.h"
#include "nodealg.h"
#include "command.h"
#include "piadi.h"


Algorithm::Algorithm(const QDomElement &elem) :
	Device(elem)
{
	if(elem.hasAttribute("prio"))
	{
		bool ok=false;
		int intval=elem.attribute("prio").toInt(&ok);
		if(ok)
			prio_=intval;
		else
			qCritical()<<"prio not a valid integer"<<elem.attribute("prio")<<objectName();
	}
}

Algorithm::Algorithm(int id, QObject *parent) :
	Device(id, parent)
{
}

Algorithm::~Algorithm()
{
	if(algState==AlgState::RUNNING)
		shutdown();
}

Result Algorithm::init()
{
	if(algState!=AlgState::VIRGIN)
		return Result::FAILED;
	algState=AlgState::STARTUP;
	// init sequence changed; init of children (e.g. AlgNodes) before Alg seems more reasonable
	Result res2=Device::init();
	//Result res=preExec(); wrong logic! First ALL algs shall init, then ALL preExec, not init/preExec/init/preExec/...
	/*if(res==Result::COMPLETED && res2==Result::COMPLETED)
	{
		algState=AlgState::RUNNING;
		return  Result::COMPLETED;
	}
	else if( (res==Result::PENDING || res==Result::COMPLETED) && (res2==Result::PENDING || res2==Result::COMPLETED))
	{
		return Result::PENDING; // stay at startup
	}*/
	if(res2==Result::PENDING || res2==Result::COMPLETED)
	{
		return res2;
	}
	algState=AlgState::ERROR;
	return Result::FAILED;
}

Result Algorithm::shutdown()
{
	algState=AlgState::SHUTDOWN;
	return postExec();
}

void Algorithm::processCommand(Command &cmd, pi_at_home::Fragment & relevant)
{
	Q_UNUSED(cmd)
	qCritical()<<objectName()<<"unknown command"<<relevant.cmd();
	return;
}

