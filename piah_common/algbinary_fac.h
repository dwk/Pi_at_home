#ifndef ALGBINARY_FAC_H
#define ALGBINARY_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgBinary(elem);
}
const QString typeName("Binary");
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGBINARY_FAC_H
