#ifndef ALGPULSE_FAC_H
#define ALGPULSE_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgPulse(elem);
}
const QString typeName("Pulse");
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGPULSE_FAC_H
