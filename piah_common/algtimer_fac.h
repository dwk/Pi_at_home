#ifndef ALGTIMER_FAC_H
#define ALGTIMER_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgTimer(elem);
}
const QString typeName="Timer";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGTIMER_FAC_H
