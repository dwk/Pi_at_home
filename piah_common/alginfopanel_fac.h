#ifndef ALGINFOPANEL_FAC_H
#define ALGINFOPANEL_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgInfoPanel(elem);
}
const QString typeName="InfoPanel";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGINFOPANEL_FAC_H
