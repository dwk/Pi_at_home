#ifndef ALGCOUNT_H
#define ALGCOUNT_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"

namespace pi_at_home
{

/* AlgCount
 * Parameters from Device and Algorithm do apply!
 * NOT UP TO DATE
 * required attributes
 *	type="Count"			fixed
 * optional attributes
 *	max-count
 *  min-count
 *  reset2zero
 * created nodes
 *  <prefix>count			int	channel
 *  <prefix>sign
 *  <prefix>incr			int	channel 100
 *  <prefix>decr			int	channel 101
 *  <prefix>reset			int	channel 102
 *
 * positive transition on incr, decr and reset will increment, decrment or reset to zero the count.
*/
class AlgCount : public Algorithm
{
	Q_OBJECT
public:
	explicit AlgCount(const QDomElement &elem);
	virtual ~AlgCount() {}
	virtual void execute();
	virtual bool safeState() {return true;}
protected:
	virtual Result preExec();
	virtual Result postExec();
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp, int inChannel);
protected:
	QString ndNmIncr, ndNmDecr, ndNmReset;
	Node *ndIncr=nullptr, *ndDecr=nullptr, *ndReset=nullptr, *ndCount=nullptr, *ndLastCnt=nullptr;
	Node *ndNeg=nullptr, *ndZero=nullptr, *ndPos=nullptr, *ndOverflow=nullptr, *ndUnderflow=nullptr;
	int count=0, lastCnt=0, cntMin=-1, cntMax=1, startVal=0;
	bool ouflow2start=true, ovfl=false, unfl=false;
};


}
#endif // ALGCOUNT_H
