#include "datafilereader.h"
#include <math.h>
#include <QFile>
#include "piadi.h"

DataFileReader::DataFileReader(const QString & filePath, DataHistoryMP & dataStorage) :
	filePath(filePath), file(new QFile(filePath)), dataHistMP(dataStorage)
{
	if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
		 qCritical()<<"cannot open file"<<filePath;
	setDevice(file);
	healthy=true;
	//source = new QXmlInputSource(file);
	//handler = new DataFileHandler(dataStorage);
	//setContentHandler(handler);
	//setErrorHandler(handler);
}

DataFileReader::~DataFileReader()
{
	/*if(handler)
	{
		qInfo()<<"data file "<<filePath<<handler->getCount()<<"values";
		delete handler;
	}
	if(source)
		delete source;*/
	if(file)
		delete file;
}

bool DataFileReader::parse()
{
	if(!healthy)
		return false;
	while(!atEnd())
	{
		switch(readNext())
		{
		case QXmlStreamReader::StartElement:
			startElement();
			break;
		case QXmlStreamReader::EndElement:
			endElement();
			break;
		default:
			break;
		}
	}
	if(hasError())
	{
		qCritical()<<errorString()<<"at line"<<lineNumber()<<"column"<<columnNumber()<<"file"<<filePath;
		return false;
	}
	return true;
}

bool DataFileReader::startElement()
//const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts)
{
	auto localName=name();
	auto atts=attributes();
	if(localName=="data") // implies version <= 10
	{
		QStringRef tststr=atts.value("tst");
		QDateTime tst;
		bool ok=true;
		int secs=0;
		secs=tststr.toInt(&ok);
		if(ok)
		{
			secs+=baset;
			tst=QDateTime::fromTime_t(secs);
		}
		if(!tst.isValid())
		{
			qWarning()<<"invalid timestamp in data file"<<atts.value("tst");
			return true;
		}
		DataHistory *dh=nullptr;
		LookupMV::iterator luit=lookup.find(atts.value("label").toString());
		if(luit==lookup.end())
		{
			qWarning()<<"no DataHistory lookup for "<<atts.value("label");
			return true;
		}
		else
		{
			NodeAddress & na=luit->second;
			DataHistoryMP::const_iterator it=dataHistMP.find(na);
			if(it==dataHistMP.end())
			{
				qWarning()<<"no DataHistory for "<<na.toStringVerbose();
				return true;
			}
			else
				dh=it->second;
		}
		double val=0.;
		switch (dh->getUnit())
		{
			case Node::Unit::BIT:
				if(atts.value("value")=="true")
					val=1.;
				ok=true;
				break;
			default:
				val=atts.value("value").toDouble(&ok);
				break;
		}
		if(!ok)
		{
			qWarning()<<"invalid value in data file"<<atts.value("value");
			return true;
		}
		//namesInSession.insert(luit->second.getAlias());
		if(dh && dh->addData(tst, val))
			++count;
		//else
		// stay silent qWarning()<<"duplicate data in data file"<<name<<tst<<val;
		return true;
	} // data
	if(localName=="d") // implies version > 10
	{
		QStringRef tststr=atts.value("t");
		QDateTime tst;
		bool ok=true;
		double fsec=tststr.toDouble(&ok), fs;
		if(ok)
		{
			int ims=static_cast<int>(modf(fsec, &fs)*1000.);
			int is=static_cast<int>(fs);
			int ih=is/3600;
			is-=ih*3600;
			int im=is/60;
			is-=im*60;
			//qDebug()<<ih<<im<<is<<ims;
			if(ih<24)
				tst=QDateTime(baseDate, QTime(ih, im, is, ims));
			else // log files may run slightly over midnight
				tst=QDateTime(baseDate.addDays(1), QTime(ih-24, im, is, ims));
		}
		if(!tst.isValid())
		{
			qWarning()<<"invalid timestamp in data file"<<atts.value("t");
			return true;
		}
		DataHistory *dh=nullptr;
		LookupMV::iterator luit=lookup.find(atts.value("l").toString());
		if(luit==lookup.end())
		{
			qWarning()<<"no DataHistory lookup for "<<atts.value("l");
			return true;
		}
		else
		{
			NodeAddress & na=luit->second;
			DataHistoryMP::const_iterator it=dataHistMP.find(na);
			if(it==dataHistMP.end())
			{
				qWarning()<<"no DataHistory for "<<na.toStringVerbose();
				return true;
			}
			else
				dh=it->second;
		}
		double val=0.;
		switch (dh->getUnit())
		{
			case Node::Unit::BIT:
				if(atts.value("v")=="true")
					val=1.;
				ok=true;
				break;
			default:
				val=atts.value("v").toDouble(&ok);
				break;
		}
		if(!ok)
		{
			qWarning()<<"invalid value in data file"<<atts.value("v");
			return true;
		}
		//namesInSession.insert(luit->second.getAlias());
		if(dh && dh->addData(tst, val))
			++count;
		//else
		// stay silent qWarning()<<"duplicate data in data file"<<name<<tst<<val;
		return true;
	} // d
	if(localName=="node")
	{
		NodeAddress na(atts.value("adr").toString());
		na.setAlias(atts.value("name").toString());
		// this check for uniqueness doesn't really work in mulit-session files. Sorry...
		if(!sessionCompleted && !lookup.insert(LookupMV::value_type(na.alias(), na)).second)
		{ // element was already there --> name / alias not unique!
			qWarning()<<na.alias()<<"is not unique when talking about"<<na.toStringVerbose();
		}
		//DataHistory *dh=dataHistMP.insert(DataHistoryMP::value_type(name, DCON.getDataHistory(name, true))).first->second;
		DataHistory *dh=nullptr;
		DataHistoryMP::const_iterator it=dataHistMP.find(na);
		if(it==dataHistMP.end())
		{
			dh=new DataHistory(na);
			dh->setUnit(Node::unit(atts.value("unit").toString()));
			if( (version<=10 && atts.value("log")!="file") || (version>10 && atts.value("log")=="-1") )
				dh->addData(base, std::numeric_limits<double>::quiet_NaN()); // create a gap in the data flow
			dataHistMP.insert(DataHistoryMP::value_type(na, dh));
		}
		return true;
	} // node
	if(localName=="session")
	{
		QStringRef vstr=atts.value("version");
//		if(vstr=="piahd0.7")
//			version=7;
//		else if(vstr=="piahd0.8")
//			version=8;
		if(vstr=="piahd0.9")
			version=9;
		else if(vstr=="piahd0.10" || vstr=="piahd1.0" || vstr=="piahd1.1")
			version=10;
		else if(vstr=="piahd1.2" || vstr=="piahd1.3")
			version=11;
		else
		{
			version=-1;
			qWarning()<<"unknown version"<<vstr;
			return false;
		}
		if(version<=10)
		{
			base=QDateTime::fromString(atts.value("start").toString(), Qt::ISODate);
			baseDate=base.date();
			baset=base.toTime_t();
		}
		else
		{
			baseDate=QDate::fromString(atts.value("start").toString(), Qt::ISODate);
			base=QDateTime(baseDate, QTime(0, 0));
			baset=0; // not used in this version
		}
		return true;
	} // session
	return true;
}

bool DataFileReader::endElement()
//const QString &namespaceURI, const QString &localName, const QString &qName)
{
	if(name()=="session")
	{
		sessionCompleted=true;
		return true;
	} // session
	return true;
}

