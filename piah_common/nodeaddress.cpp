#include "nodeaddress.h"
#include "piadi.h"

QString NodeAddress::defUid;

NodeAddress::NodeAddress(const QString textForm, bool throwIfInvalid)
{
	valid=false;
	QStringList l=textForm.split(':');
	if(l.size()!=4)
	{
		if(throwIfInvalid)
			throw Ex(QString("invalid text representation of NodeAddress: %1").arg(textForm));
		else
			return;
	}
	avUid_=l.at(0);
	uid_=l.at(1);
	bool okd=false, okc=false;
	device_=l.at(2).toInt(&okd);
	channel_=l.at(3).toInt(&okc);
	if(!okd ||!okc)
	{
		if(throwIfInvalid)
			throw Ex(QString("invalid device or channel NodeAddress: %1").arg(textForm));
		else
			return;
	}
	valid=true;
}

bool NodeAddress::operator<(const NodeAddress &o) const
{
	// ignore alias and avatar intentionally
	if(uid_.isEmpty() || o.uid_.isEmpty() || uid_==o.uid_) // empty uid is wildcard
	{
		if(device_==o.device_)
			return channel_<o.channel_;
		else
			return device_<o.device_;
	}
	else
		return uid_<o.uid_;
}

bool NodeAddress::operator==(const NodeAddress &o) const
{
	bool res=true;
	if(!uid_.isEmpty() && !o.uid_.isEmpty() && uid_!=o.uid_) // empty uid is wildcard
		res=false;
	if(res && device_>=0 && device_!=o.device_)
		res=false;
	if(res && channel_>=0 && channel_!=o.channel_)
		res=false;
	return res;
}

QString NodeAddress::toStringVerbose() const
{
	if(isAvatar())
		return QString("Avatar on %5 for HW %1 dev %2 ch %3 alias %4").arg(uid_).arg(device_).arg(channel_).arg(alias_, avUid_);
	else
		return QString("HW %1 dev %2 ch %3 alias %4").arg(uid_).arg(device_).arg(channel_).arg(alias_);
}

QString NodeAddress::toStringUidAlias() const
{
	return uid_+":"+alias_;
}

QString NodeAddress::toSettingsKey() const
{
	return QString("data/")+uid_+"/"+alias_;
}

void NodeAddress::uidAlias(const QString &uidAlias, QString &uid, QString &alias)
{
	QStringList i=uidAlias.split(':');
	if(i.size()==2)
	{
		uid=i.at(0);
		alias=i.at(1);
	}
	else
	{
		qWarning()<<"invalid uidAlias format"<<uidAlias;
		uid.clear();
		alias.clear();
	}
	return;
}
