#ifndef NETCON_H
#define NETCON_H

#include "piadh.h"
#include <QObject>
#include <QPointer>
#include <QHostAddress>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <QDateTime>

namespace pi_at_home
{

/* client side NetCon's are persistent and try to re-connect or to re-establish a lost connection
 * server side NetCon's die with their sockets
 * ConPool takes care of NetCon's life cycle, communication via readySig
*/
class NetCon : public QObject
{
	Q_OBJECT
public:
	explicit NetCon(const QHostAddress & address, int port, bool testOnly = false, bool listNodes = false);
	explicit NetCon(QTcpSocket * socket);
	~NetCon();
	const QHostAddress & address() const {return address_;}
	int port() const {return port_;}
	bool isHost() const {return isServer_;} // true, if we are on the server / host side of the socket
	const QString & version() const {return peerVersion;}
	const QString & build() const {return peerBuild;}
	const QString & configFlavor() const {return peerConfigFlavor;}
	const QString & configTst() const {return peerConfigTst;}
	const QString & uid() const {return peerUid;}
	int timeout() const {return toCon_;}
	//void setTimeout(int value) {timeout_=value;}
	int getHeartbeatSec() const {return toHeartbeat/1000;} // 0 = no heartbeat
	void setHeartbeat(int intervallSec); // client: do a heartbeat all intervallSec seconds; server: no function; default=0=off; will reset existing heartbeat. Independent of QAbstractSocket::KeepAliveOption
	QAbstractSocket::SocketState state() const {if(socket) return socket->state(); return QAbstractSocket::UnconnectedState;}
	bool send(const Command & cmd);
	QString lastErrorStr() const {return lastErrStr_;}
	//const QStringList & errorStrs() const {return lastErrStr;}
	int lastError() const {return lastErr_;}
public slots:
	void start();
	void stop();
protected:
	void timerEvent(QTimerEvent *event);
private slots:
	void sockConnected();
	void sockDisconnected();
	void sockDataOut(qint64 cnt);
	void sockDataIn();
	void sockError(QAbstractSocket::SocketError err);
private:
	enum class TimerChecks {NONE, CONNECTED, WRITTEN, RECONNECT};
	void parseData(QByteArray & body);
	void setTimer(TimerChecks tc);
	QTcpSocket * socket = nullptr;
	TimerChecks timerCheck = TimerChecks::NONE;
	bool startup = true, terminateAfterWrite=false, testOnly=false, listNodes=false;
	int toCon_ = 5000, toWrite_ = 3000, toRecon_ = 10000, timerId=0, toHeartbeat=0, hbTimerId=0, lastErr_=0;
	Command * hbCmd=nullptr;
	QString lastErrStr_;
	bool isServer_; // true if this is the server side of the connection (opposite to a remote client); set depending on ctor used
	QHostAddress address_;
	int port_; // port at host
	QString peerBuild; // build, version and uid of remote side - client or host
	QString peerVersion, peerConfigFlavor, peerConfigTst, peerUid;
	QByteArray pendingData;
signals:
	void errorSig(int erno);
	void readySig(bool ready);
	void rawDataSig(QString, QByteArray, bool, bool); // hostUid, data, sending, error
};
typedef std::map<QString, QPointer<NetCon> > NetConMSP;  // key is uid or (during startup) address:port
//typedef std::list<QPointer<NetCon> > NetConLSP;

}
#endif // NETCON_H
