#include "algshutter.h"
#include "algshutter_fac.h"
#include "node.h"
#include "command.h"
#include <QTimerEvent>
#include "piadi.h"

AlgShutter::AlgShutter(const QDomElement &elem) :
	Algorithm(elem)
{
	ndInUpNm=elem.attribute("input_up");
	ndInDwnNm=elem.attribute("input_down");
	int ival=0;
	bool okv=false;
	ival=elem.attribute("duration_up").toInt(&okv);
	if(okv && ival>=1000)
	{
		durUp=ival;
		//durDwn=durUp;
	}
	ival=elem.attribute("duration_down").toInt(&okv);
	if(okv && ival>=1000)
		durDwn=ival;
	ival=elem.attribute("duration_lockup").toInt(&okv);
	if(okv && ival>=0)
	{
		durLockUp=ival;
		//durLockDwn=durLockUp;
	}
	ival=elem.attribute("duration_lockdown").toInt(&okv);
	if(okv && ival>=0)
		durLockDwn=ival;
	ival=elem.attribute("duration_relaxup").toInt(&okv);
	if(okv && (!ival || ival>=100))
	{
		durRelaxUp=ival;
		//durRelaxDwn=durRelaxUp;
	}
	ival=elem.attribute("duration_relaxdown").toInt(&okv);
	if(okv && (!ival || ival>=100))
		durRelaxDwn=ival;
	ival=elem.attribute("duration_wait").toInt(&okv);
	if(okv && ival>=100)
		durWait=ival;
	stateNode=new NodeStr(namePrefix+"state", id_, 0, this);
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		if(e.attribute(QStringLiteral("type_name"))==QStringLiteral("input_down"))
			ndInDwnNm=e.attribute(QStringLiteral("name"));
		else if(e.attribute(QStringLiteral("type_name"))==QStringLiteral("input_up"))
			ndInUpNm=e.attribute(QStringLiteral("name"));
	}
	if(ndInUpNm.isEmpty() || ndInDwnNm.isEmpty())
		qCritical()<<objectName()<<": empty node name(s)"<<ndInUpNm<<ndInDwnNm;
	else
	{
		ndOutUp=new NodeBool(namePrefix+"up", id_, 1, this);
		ndOutDwn=new NodeBool(namePrefix+"down", id_, 2, this);
	}
}

void AlgShutter::execute()
{
	if(Algorithm::algState!=AlgState::RUNNING)
		return;
	if(!inUp && !inDown)
	{
		if(logicState==LogicState::MIDTURN)
		{ // clearing both switches while in midturn cancels the action
			if(timerId)
			{
				killTimer(timerId);
				timerId=0;
			}
			logicState=LogicState::DONE;
		}
		return; // let the timers run - or do nothing
	}
	if(inUp && inDown)
	{ // stop instantly
		if(logicState==LogicState::DONE)
		{
			// no sense to enter STOP
			// since releasing the "wrong" switch would start the motor
			// although there is no reason to move - stay at DONE
			return;
		}
		if(logicState==LogicState::STOP)
			return;
		if(timerId)
		{
			killTimer(timerId);
			timerId=0;
		}
		logicState=LogicState::STOP;
		ndOutUp->setValue(NodeValue(false)); // will bypass if not changed
		ndOutDwn->setValue(NodeValue(false)); // will bypass if not changed
		updateStateNode();
		return;
	}
	// either up or down is set
	switch(logicState)
	{
		case LogicState::STOP:
			// not stopped anymore
			// no timer running, all motors stoped but we don't know for how long
			logicState=LogicState::MIDTURN;
			goingUp=inUp;
			timerId=startTimer(durWait);
			updateStateNode();
			break;
		case LogicState::MOVE:
		//case LogicState::LOCK:
		case LogicState::WAIT:
		case LogicState::RELAX:
			// check if we are still running in the right direction
			if( (goingUp && inDown) || (!goingUp && inUp) )
			{
				if(timerId)
				{
					killTimer(timerId);
					timerId=0;
				}
				ndOutUp->setValue(NodeValue(false)); // will bypass if not changed
				ndOutDwn->setValue(NodeValue(false)); // will bypass if not changed
				logicState=LogicState::MIDTURN;
				goingUp=inUp;
				timerId=startTimer(durWait);
				updateStateNode();
			}
			// wait for timer...
			break;
		case LogicState::MIDTURN:
			// wait for timer, no matter what
			break;
		case LogicState::DONE:
			// hold in end position?
			if( (inUp && hwState==HwState::FULLUP) || (inDown && hwState==HwState::FULLDOWN) )
				break; // fine, we are where we are expected to be
			// normal start
			logicState=LogicState::MOVE;
			goingUp=inUp;
			if(goingUp)
			{
				ndOutUp->setValue(NodeValue(true));
				timerId=startTimer(durUp + durLockUp);
			}
			else
			{
				ndOutDwn->setValue(NodeValue(true));
				timerId=startTimer(durDwn + durLockDwn);
			}
			hwState=HwState::ANYWHERE;
			updateStateNode();
			break;
		default: // UNDEF
			break;
	}
}

void AlgShutter::valCaptureSl(const NodeValue &newValue, qint64 timestamp, int channel)
{
	Q_UNUSED(timestamp)
	switch(channel)
	{
		case 1:
			inUp=newValue.toBool();
			execute();
			break;
		case 2:
			inDown=newValue.toBool();
			execute();
			break;
		default:
			break;
	}
}

void AlgShutter::timerEvent(QTimerEvent *event)
{
	killTimer(event->timerId());
	switch(logicState)
	{
		case LogicState::STOP:
			break;
		case LogicState::MOVE:
			ndOutUp->setValue(NodeValue(false)); // will bypass if not changed
			ndOutDwn->setValue(NodeValue(false)); // will bypass if not changed
			if( (goingUp && durRelaxUp) || (!goingUp && durRelaxDwn) )
			{
				timerId=startTimer(durWait);
				logicState=LogicState::WAIT;
				updateStateNode();
				break;
			}
			logicState=LogicState::DONE;
			hwState=goingUp?HwState::FULLUP: HwState::FULLDOWN;
			updateStateNode();
			break;
		/*case LogicState::LOCK:
			// currently not used
			qWarning()<<objectName()<<"broken state machine"<<(int)logicState;
			logicState=LogicState::DONE;
			updateStateNode();
			break;*/
		case LogicState::WAIT:
			if(goingUp)
			{
				ndOutUp->setValue(NodeValue(false)); // just for safety
				ndOutDwn->setValue(NodeValue(true));
				timerId=startTimer(durRelaxUp);
			}
			else
			{
				ndOutUp->setValue(NodeValue(true));
				ndOutDwn->setValue(NodeValue(false)); // just for safety
				timerId=startTimer(durRelaxDwn);
			}
			logicState=LogicState::RELAX;
			updateStateNode();
			break;
		case LogicState::RELAX:
			ndOutUp->setValue(NodeValue(false)); // will bypass if not changed
			ndOutDwn->setValue(NodeValue(false)); // will bypass if not changed
			logicState=LogicState::DONE;
			hwState=goingUp?HwState::FULLUP: HwState::FULLDOWN;
			updateStateNode();
			break;
		case LogicState::MIDTURN:
			if(goingUp)
			{
				ndOutUp->setValue(NodeValue(true));
				timerId=startTimer(durUp + durLockUp);
			}
			else
			{
				ndOutDwn->setValue(NodeValue(true));
				timerId=startTimer(durDwn + durLockDwn);
			}
			logicState=LogicState::MOVE;
			updateStateNode();
			break;
		default: // UNDEF, DONE - none of them should ever happen
			qWarning()<<objectName()<<"broken state machine"<<(int)logicState;
			//stateNode->setValue(NodeValue("???"));
			break;
	}
}

pi_at_home::Result AlgShutter::preExec()
{
	if(algState!=AlgState::STARTUP)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	Node *nd=DCON.findNode(ndInUpNm, false);
	if(nd)
	{
		connect(nd, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, 1);});
		inUp=nd->value().toBool();
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot find input_up"<<ndInUpNm;
		return Result::FAILED;
	}
	nd=DCON.findNode(ndInDwnNm, false);
	if(nd)
	{
		connect(nd, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, 2);});
		inDown=nd->value().toBool();
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot find input_down"<<ndInDwnNm;
		return Result::FAILED;
	}
	if(!ndOutUp)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"no output up";
		return Result::FAILED;
	}
	if(!ndOutDwn)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"no output down";
		return Result::FAILED;
	}
	// make sure nothing moves
	ndOutUp->setValue(NodeValue(false));
	ndOutDwn->setValue(NodeValue(false));
	logicState=LogicState::DONE; // HwState remains UNDEF - we don't know where the shutter is
	updateStateNode();
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

pi_at_home::Result AlgShutter::postExec()
{
	if(timerId)
	{
		killTimer(timerId);
		timerId=0;
	}
	if(ndOutUp)
	{
		ndOutUp->setValue(ndOutUp->defaultValue());
	}
	if(ndOutDwn)
	{
		ndOutDwn->setValue(ndOutDwn->defaultValue());
	}
	logicState=LogicState::UNDEF;
	return Result::COMPLETED;
}

void AlgShutter::updateStateNode()
{
	QString res;
	switch(logicState)
	{
		case LogicState::UNDEF:
			res="UNDEF";
			break;
		case LogicState::STOP:
			res="STOP";
			break;
		case LogicState::MOVE:
			res="MOVE";
			break;
		/*case LogicState::LOCK:
			res="LOCK";
			break;*/
		case LogicState::WAIT:
			res="WAIT";
			break;
		case LogicState::RELAX:
			res="RELAX";
			break;
		case LogicState::MIDTURN:
			res="MIDTURN";
			break;
		case LogicState::DONE:
			res="DONE";
			break;
	}
	res+='/';
	switch(hwState)
	{
		case HwState::ANYWHERE:
			res+="ANYWHERE";
			break;
		case HwState::FULLUP:
			res+="FULLUP";
			break;
		case HwState::FULLDOWN:
			res+="FULLDOWN";
			break;
	}
	stateNode->setValue(res);
}
