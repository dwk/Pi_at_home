#ifndef ALGDUMMY_FAC_H
#define ALGDUMMY_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgDummy(elem);
}
const QString typeName="Dummy";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGDUMMY_FAC_H
