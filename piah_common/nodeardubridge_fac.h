#ifndef NODEARDUBRIDGE_FAC_H
#define NODEARDUBRIDGE_FAC_H

#include "nodeardubridge_nm.h"

namespace
{
pi_at_home::Device * Creator(const QDomElement &elem)
{
	return new pi_at_home::DeviceArduBridge(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, Creator);
}

#endif // NODEARDUBRIDGE_FAC_H
