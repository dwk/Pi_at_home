#include "nodeardubridge.h"
#include "nodeardubridge_fac.h"
#include <wiringPiSPI.h>
#include "command.h"
#include "piadi.h"

NodeArduBridge::NodeArduBridge(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent)
{
	//if(adr_.channel()<0 || adr_.channel()>5)
	//	throw Ex(QString("invalide channel / line in NodeArduBridge %1").arg(adr_.toStringVerbose()));
}

const QString &NodeArduBridge::type() const
{
	return typeName;
}

Result NodeArduBridge::init()
{
	val_="";
	value_=val_;
	// write init cmds to channel, if required
	//qDebug()<<"NodeArduBridge::init";
	state_=State::READY;
	return Result::COMPLETED;
}

void NodeArduBridge::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	//qDebug()<<"NodeArduBridge::setValue1";
	if(!value.canConvert<QString>())
		throw Ex(QString("NodeValue not convertible to string on %1").arg(adr_.toStringVerbose()));
	QString newVal=value.toString();
	setValue(newVal, token);
}

void NodeArduBridge::setValue(const QString &value, Node::TokenType token)
{
	//qDebug()<<"NodeArduBridge::setValue2";
	if(!tokenValid(token))
	{
		qWarning()<<adr_.toString()<<"got invalid token"<<token;
		return;
	}
	if(value==val_)
		return; // nothing to do!
	if(state()!=Node::State::READY)
	{
		qWarning()<<"set value ignored while not ready"<<adr_.toStringVerbose()<<Node::state(state());
		return;
	}
	//qDebug()<<"pre-send"<<objectName()<<value;
	val_=value;
	value_=val_;
	DeviceArduBridge *d=qobject_cast<DeviceArduBridge*>(parent());
	if(!d)
		throw Ex(QString("init failed on %1").arg(adr_.toStringVerbose()));
	QStringList vals=val_.split(' ');
	unsigned char * data=new unsigned char [vals.size()];
	int dlen=0;
	bool ok=false;
	foreach(QString valStr, vals)
	{
		int v=valStr.toInt(&ok, 16);
		if(ok)
		{
			if(v>255)
				qWarning()<<"value overflow"<<valStr<<v;
			data[dlen++]=v%256;
		}
	}
	if(dlen)
	{
		d->write(data, dlen, adr_.channel());
		vals.clear();
		for(int i=0; i<dlen; ++i)
		{
			qDebug()<<i<<data[i];
			vals.push_back(QString::number((qulonglong)(data[i]), 16));
		}
		val_=vals.join(' ');
		qDebug()<<val_;
		value_=val_;
	}
	else
		qWarning()<<"no transmittable data in"<<vals;
	delete[] data;
	//qDebug()<<"post-send"<<objectName()<<val_;
	broadcastUpdate();
}

void NodeArduBridge::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	//qDebug()<<"NodeArduBridge::processCommand";
	if(relevant.cmd()=="noderq")
	{
		relevant.setAttribute("val", val_);
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else if(relevant.cmd()=="nodeset")
	{
		setValue(relevant.attribute("val"));
		return;
	}
	else
		qCritical()<<"unknown command for NodeSwitchPort"<<relevant.cmd()<<adr_.toString();
	return;
}


DeviceArduBridge::DeviceArduBridge(const QDomElement &elem) : Device(elem)
{
	QString p=elem.attribute("SPI");
	bool ok=false;
	portSpi_=p.toInt(&ok);
	if(!ok || portSpi_<0 || portSpi_>1)
		throw Ex(QString("invalid SPI-channel specification <%1> at device %2").arg(p).arg(id_));
	/*
	ok=false;
	p=elem.attribute("d_c");
	if(p.startsWith("WP"))
		portDC=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid data/command port <%1> at device %2").arg(p).arg(id_));
	ok=false;
	p=elem.attribute("reset");
	if(p.startsWith("WP"))
		portReset=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid reset port <%1> at device %2").arg(p).arg(id_));
	*/
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		new NodeArduBridge(nodes.item(i).toElement(), id_, this);
	}
}

pi_at_home::Result DeviceArduBridge::init()
{
	// at higher clock frequencies writing the SPDR at atmega fails because the first
	// bit was already sent - you just get the last received value (i.e. the current content of SPDR) back
	wiringPiSPISetup(portSpi_, 500000); // channel x, 500kHz, mode 0 is fine...
	unsigned char cmd[]={
		0xff, //
		0x00, //
		0x33, //
		0xcc}; //
	wiringPiSPIDataRW(portSpi_, cmd, 4); // MSB first is fine
	return Device::init();
}

pi_at_home::Result DeviceArduBridge::shutdown()
{
	unsigned char cmd[]={
		0xaa}; //
	wiringPiSPIDataRW(portSpi_, cmd, 1); // MSB first is fine
	return Result::COMPLETED;
}

void DeviceArduBridge::write(unsigned char * data, int size, int channel)
{
	Q_UNUSED(channel)
	wiringPiSPIDataRW(portSpi_, data, size);
}

