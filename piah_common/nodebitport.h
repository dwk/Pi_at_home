#ifndef NODEBITPORT_H
#define NODEBITPORT_H

#include "piadh.h"
#include "node.h"
#include "device.h"

/* *** Device BitPort
 * see standard device attributes
 * required parameters
 * type="BitPort"
 * port="WPx"		x = any valid port number in Wiring Pi notation
 * optional attributes
 * <none>
 *
 * BitPorts offer output bits with some switching logic. The device is just a
 * concept, not actual hardware. The channel number is the offset to the base
 * address. Channels need not to be contiguous and the GPIOs of the missing
 * channels are not touched in any way.
 *
 * *** Node BitPort
 * see standard node attributes
 * required parameters
 *  <none>
 * optional parameters
 * default_value="x"	x = true|false The node is initialized to its default value during startup; default is false
 * inverted="x"			x = true|false If true inverts the hardware output to low for true and high for false; default is false
 *						Do not use for application logic but only for hardware abstraction. true should always correspond to the active, "on"-state.
 * push_pull_offset="x"	x = integer!=0 Use the ports <base>+<channel> and <base>+<channel>+<push_pull_offset> as push-pull pair.
 *						The offseted channel is the inverted one.
 * delay="x"			x = delay in ms The node does not allow value changes <delay>ms after the _completion_ of the last state change.
 * pulse="x"			x = pulse duration in ms Activate the port only for <pulse>ms after a state change and set
 *						it to logical false then (hardware level determined by <inverted>). If used with push-pull logic both
 *						channels will end in false. If you query the node you will always get the set-value. Check for state BUSY.
 *
 * The hardware associated with a GPIO may be slow and / or sensitive to rapid turn on / turn off cycles.
 * <delay> and <pulse> should be set accordingly. Attempts to change the value during these periods are
 * ignored and a warning logged.
 * <delay> and <pulse> only come into effect if a setValue actually changes the logical value of the node. Writing the same value
 * again does not restart the timer. Note that the timer is started even if the hardware port does not change but the logical value
 * does, e.g. setVale(false) on a node in pulse-mode start pulse and delay timers.
 *
 * Exapmple
 * <device id="80" type="BitPort" port="WP25">
 *  <node channel="0" name="simple" default_value="false"/>
 *  <node channel="1" name="pushpull" push_pull_offset="1" pulse="8000" delay="5000"/>
 *  <node channel="5" name="delayed" delay="5000" log="file"/>
 * </device>
 * WP26 & WP27 is a push-pull pair and WP28, WP29 unused. WP30 can be toggled only once in 5 seconds.
 */
namespace pi_at_home
{
class HwPort;
class DeviceBitPort;

class NodeBitPort : public Node
{
	Q_OBJECT
	friend class DeviceBitPort;
	enum class DelayedUpdate{NONE=0, TO_ACTIVE, TO_INACTIVE};
public:
	NodeBitPort(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	//virtual QString serializeValue(const NodeValue & value) const; QVariant::toString on bool yields "true" and "false" --> OK
	virtual Node::Unit unit() const {return Unit::BIT;}
	virtual bool canSetValue(const NodeValue & value) const {return state()==Node::State::READY && value.canConvert<bool>();}
	virtual Result init();
	virtual void setValue(const NodeValue & value, TokenType token=0);
	void setValue(bool value);
	virtual NodeValue defaultValue() const {return NodeValue(defval_);}
	virtual void processCommand(Command & cmd, Fragment &relevant);
	//
	bool defaultValueBool() const {return defval_;}
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected slots:
	void stateReadySl();
	void statePulseSl();
protected:
	void setRaw();
	HwPort * hwPort=nullptr, * hwPortPp=nullptr;
	bool val_=false, defval_=false, inverted=false, reportStatus=false;
	DelayedUpdate delayedUpdate=DelayedUpdate::NONE;
	int basePortWp_=0, delay_=0, chOff=0, pulse=0; // no "busy" duration by default; chOff!=0 means push/pull; pulse onky relevant for push/pull
};

class DeviceBitPort : public Device
{
	Q_OBJECT
public:
	explicit DeviceBitPort(const QDomElement &elem);
	virtual Result init();
	virtual Result shutdown();
	int basePortWp() const {return basePortWp_;}
private:
	int basePortWp_=-1;
};

}

#endif // NODEBITPORT_H
