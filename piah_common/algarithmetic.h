#ifndef ALGARITHMETIC_H
#define ALGARITHMETIC_H

#include "piadh.h"
#include "node.h"
#include "algorithm.h"
#include "../exprtk/exprtk.hpp"

namespace pi_at_home
{

/* AlgArithmetic evaluates general arithmetic expressions.
 * required attributes
 *	type="Arithmetic"		fixed
 *	pN="x"					N = 0 | positive int, x = node alias; parameter 0..max
 *	output="x"				x = node alias; output node
 *	expression="x"			x = expression to be evaluated; see below
 * optional attributes
 *	<none>
 *
 * Parameters must be numbered consecutively stating at zero.
 * Input nodevales are cast to double and all evaluations use daouble.
 * Parameters in the expression have to be named p0, p1, p2, ...
 *
 * This algorithm uses the ExprTk from http://www.partow.net/programming/exprtk/index.html
 * under MIT license https://opensource.org/licenses/MIT .
 * For full documentation on ExprTk see http://www.partow.net/programming/exprtk/code/readme.txt
 * or exprtk/readme.txt in the source tree.
 *
*/
class AlgArithmetic : public Algorithm
{
	Q_OBJECT
public:
	explicit AlgArithmetic(const QDomElement &elem);
	virtual ~AlgArithmetic() {delete px;}
	virtual void execute();
	virtual bool safeState() {return true;}
protected:
	typedef double ExprT;
	typedef exprtk::symbol_table<ExprT> symbol_table_t;
	typedef exprtk::expression<ExprT>     expression_t;
	typedef exprtk::parser<ExprT>             parser_t;

	virtual Result preExec();
	virtual Result postExec();
	QStringList ndInsNm;
	unsigned int pSize=0;
	QString ndOutNm;
	NodesVSP ndIns;
	QPointer< Node > ndOut=nullptr;
	Node::TokenType token=0;
	ExprT * px=nullptr;
	std::string expressionString;
	symbol_table_t symbolTable;
	expression_t expression;
	parser_t parser;
};


}
#endif // ALGARITHMETIC_H
