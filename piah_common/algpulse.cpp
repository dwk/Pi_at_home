#include "algpulse.h"
#include "algpulse_fac.h"
#include "node.h"
#include "command.h"
#include "nodealg.h"
#include "piadi.h"

AlgPulse::AlgPulse(const QDomElement &elem) :
	Algorithm(elem)
{
	ndReset=new NodeBool(namePrefix+"reset", id_, 1, this);
	connect(ndReset, &Node::newValSig, this, &AlgPulse::resCaptureSl);
	ndOut=new NodeBool(namePrefix+"out", id_, 2, this);
	if(elem.hasAttribute("duration"))
	{
		bool ok=false;
		int m=elem.attribute("duration").toInt(&ok);
		if(ok && m>10)
			dur=m;
		else
			qWarning()<<"AlgPulse invalid duration"<<elem.attribute("duration");
	}
	if(elem.hasAttribute("rising"))
	{
		QString v=elem.attribute("rising");
		if(v==QStringLiteral("false"))
			rising=false;
		if(v==QStringLiteral("true"))
			rising=true;
		else
			qWarning()<<"AlgPulse invalid rising"<<elem.attribute("rising");
	}
	if(elem.hasAttribute("falling"))
	{
		QString v=elem.attribute("falling");
		if(v==QStringLiteral("false"))
			falling=false;
		if(v==QStringLiteral("true"))
			falling=true;
		else
			qWarning()<<"AlgPulse invalid falling"<<elem.attribute("falling");
	}
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		if(e.attribute(QStringLiteral("type_name"))!=QStringLiteral("input"))
			continue;
		inName=e.attribute(QStringLiteral("name"));
	}
}

void AlgPulse::execute()
{
	return; // purely timer driven
}

pi_at_home::Result AlgPulse::preExec()
{
	if(algState!=AlgState::STARTUP || !ndOut)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	NodeValue newValue;
	ndInput=DCON.findNode(inName, false);
	if(ndInput)
	{
		connect(ndInput, &Node::newValSig, this, &AlgPulse::valCaptureSl);
		lastVal=ndInput->value().toBool();
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot find input"<<inName;
	}
	if(algState==AlgState::ERROR)
		return Result::FAILED;
	if(ndOut->getLocker())
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"out already locked";
		return Result::FAILED;
	}
	token=ndOut->lock(DCON.uid(), id_, objectName());
	if(!token)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot lock out";
		return Result::FAILED;
	}
	// execute(); purely timer driven
	return Result::COMPLETED;
}

pi_at_home::Result AlgPulse::postExec()
{
	if(ndOut)
	{
		ndOut->setValue(ndOut->defaultValue(), token);
		ndOut->unlock(token);
	}
	return Result::COMPLETED;
}

void AlgPulse::timerEvent(QTimerEvent *event)
{
	Q_UNUSED(event)
	killTimer(timerId);
	timerId=0;
	ndOut->setValue(NodeValue(false), token);
}

void AlgPulse::valCaptureSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool b=newValue.toBool();
	if(b==lastVal)
		return;
	lastVal=b;
	if( (rising && b) || (falling && !b) )
	{
		if(timerId)
			killTimer(timerId);
		timerId=startTimer(dur);
		ndOut->setValue(NodeValue(true), token);
	}
}

void AlgPulse::resCaptureSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool b=newValue.toBool();
	if(!b)
		return;
	if(timerId)
		killTimer(timerId);
	timerId=0;
	ndOut->setValue(NodeValue(false), token);
}
