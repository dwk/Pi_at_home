#include "nodeds1820.h"
#include "nodeds1820_fac.h"
#include "command.h"
#include "piadi.h"

QString NodeDS1820Worker::basePath="/sys/bus/w1/devices/%1/w1_slave";

void NodeDS1820Worker::readHwSl()
{
	//	pi@raspberrypi:/sys/bus/w1/devices/10-0008030cc112 $ cat w1_slave
	//	09 00 4b 46 ff ff 06 10 9f : crc=9f YES
	//	09 00 4b 46 ff ff 06 10 9f t=4375
	double res=0.;
	bool ok=false;
	FILE *fd=fopen(basePath.arg(w1Id_).toUtf8().data(), "r");
	if(fd)
	{
		char line[64];
		if(fgets(line, 63, fd))
		{
			if(strstr(line, "YES"))
			{
				if(fgets(line, 63, fd))
				{
					const char * off=strstr(line, "t=");
					if(off && off-line<(int)strlen(line)-2)
					{
						res=(double)(atoi(off+2))/1000.;
						ok=true;
					}
					else
						qWarning()<<"w1-therm t= fail"<<line;
				}
				else
					qWarning()<<"w1-therm read2 fail";
			}
			else
				qWarning()<<"w1-therm crc fail"<<line;
		}
		else
			qWarning()<<"w1-therm read1 fail";
		fclose(fd);
	}
	else
		qCritical()<<"fopen failed on device"<<w1Id_<<strerror(errno);
	emit resultReady(res, DCON.getUsecTst(), ok);
}

NodeDS1820::NodeDS1820(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent)
{
	w1Id=elem.attribute("id");
	if(!w1Id.startsWith("10-") && !w1Id.startsWith("28-"))
		qCritical()<<"DS1820 supports only familiy codes 10 and 28; this node will probably not provide useful data"<<w1Id;
	worker=new NodeDS1820Worker(w1Id);
	worker->moveToThread(&workerThread);
	connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
	connect(this, &NodeDS1820::initiateHwConversionSig, worker, &NodeDS1820Worker::readHwSl);
	connect(worker, &NodeDS1820Worker::resultReady, this, &NodeDS1820::conversionCompletedSl);
}

const QString &NodeDS1820::type() const
{
	return typeName;
}

Result NodeDS1820::init()
{
	workerThread.start();
	ping();
	return Result::PENDING;
}

void NodeDS1820::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	Q_UNUSED(value);
	Q_UNUSED(token);
	qCritical()<<"setValue on NodeDS1820";
}

void NodeDS1820::ping()
{
	//qDebug()<<"NodeDS1820::ping()";
	emit initiateHwConversionSig();
}

void NodeDS1820::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		relevant.setAttribute("val", QString::number(val_));
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeDS1820"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeDS1820::shutdown()
{
	qInfo()<<"stopping thread for"<<w1Id;
	workerThread.quit();
	workerThread.wait();
}

void NodeDS1820::newValSl(const NodeValue &newValue)
{
	Q_UNUSED(newValue);
	qCritical()<<"newValSl on NodeDS1820";
}

void NodeDS1820::conversionCompletedSl(double temperature, qint64 timestamp, bool ok)
{
	//qDebug()<<"NodeDS1820::conversionCompletedSl"<<temperature<<timestamp<<ok;
	if(!ok)
		return;
	if(temperature!=val_)
	{
		val_=temperature;
		value_=val_;
		state_=State::READY;
		emit newValSig(value_, timestamp);
	}
}

DeviceDS1820::DeviceDS1820(const QDomElement &elem) : Device(elem)
{
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		new NodeDS1820(nodes.item(i).toElement(), id_, this);
	}
}

pi_at_home::Result DeviceDS1820::shutdown()
{
	QList<NodeDS1820 * > chldr=findChildren<NodeDS1820 * >();
	foreach(NodeDS1820 * n, chldr)
	{
		n->shutdown();
	}
	return Result::COMPLETED;
}
