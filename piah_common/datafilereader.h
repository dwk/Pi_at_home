#ifndef DATAFILEREADER_H
#define DATAFILEREADER_H

#include "piadh.h"
#include <QXmlStreamReader>
//#include <QXmlSimpleReader>
#include <QXmlDefaultHandler>
#include <QPointer>
#include "datahistory.h"

class QFile;

namespace pi_at_home
{
/*class DataFileHandler : public QXmlDefaultHandler
{
public:
	DataFileHandler(DataHistoryMP & dataStorage);
	virtual bool startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts);
	virtual bool endElement(const QString &namespaceURI, const QString &localName, const QString &qName);
	int getCount() const {return count;}
private:
	typedef std::map<QString, NodeAddress > LookupMV;
	LookupMV lookup;
	DataHistoryMP &dataHistMP;
	QDateTime base;
	QDate baseDate;
	time_t baset;
	int count=0, version;
	bool sessionCompleted=false;
	//std::set<QString> namesInSession;
};*/

class DataFileReader : public QXmlStreamReader //QXmlSimpleReader
{
public:
	explicit DataFileReader(const QString & filePath, DataHistoryMP & dataStorage);
	~DataFileReader();
	bool parse();
private:
	bool startElement();
	bool endElement();
	bool healthy=false;
	QString filePath;
	QFile * file;
	//DataFileHandler *handler=nullptr;
	//QXmlInputSource *source=nullptr;
	typedef std::map<QString, NodeAddress > LookupMV;
	LookupMV lookup;
	DataHistoryMP &dataHistMP;
	QDateTime base;
	QDate baseDate;
	time_t baset;
	int count=0, version;
	bool sessionCompleted=false;
};

}
#endif // DATAFILEREADER_H
