#ifndef COMMAND_H
#define COMMAND_H

#include "piadh.h"
#include <QObject>
#include <QDomElement>
#include <QDateTime>
#include "nodeaddress.h"

namespace pi_at_home
{

class Fragment
{
public:
	typedef std::map<QString, QString> Params;
	Fragment() {}
	Fragment(QDomElement elem);
	explicit Fragment(const QString & cmd) : cmd_(cmd) {}
	virtual ~Fragment() {}
	const QString & cmd() const {return cmd_;}
	QString attribute(const QString & name) const;
	bool setAttribute(const QString & name, const QString & value); // true if ok
	QString toXml() const;
	QString paramsToXml() const ;
protected:
	static QString templ, ptempl;
	QString cmd_;
	Params params_;
};
typedef std::list<Fragment*> FragmentLP;

class Command : public QObject, public Fragment
{
	Q_OBJECT
public:
	explicit Command(const QDomElement &root);
	explicit Command(const QString & cmd); // both src & dst are :defuid:0:0
	virtual ~Command();
	QString toString() const;
	QByteArray toSocketXml() const;
	QByteArray &toSocketXmlEpilog() const {return msgTemplEpilog;}
	unsigned long long int getSluid() {return sluid_;}
	unsigned long long int getDluid() {return dluid_;}
	void createDluid();
	bool hasPayload() const {return payload_;}
	const QByteArray *payload() const {return payload_;}
	void setPayload(QByteArray *payload) {payload_=payload;} // takes ownership
	void setPayloadEncode(const QByteArray &source);
	const NodeAddress & procTarget() const;
	const NodeAddress & source() const {return src;}
	void setSource(const NodeAddress & source) {src=source;}
	const NodeAddress & destination() const {return dst;}
	void setDestination(const NodeAddress & destination) {dst=destination; dst.convertToNode();} // only true nodes shall be targets
	void convertToBroadcast() {dst=NodeAddress("", 0, 0);}
	bool isBroadcast() const {return dst.uid().isEmpty();}
	bool isReply() const {if(dluid_) return true; return false;}
	void convertToReply();
	const FragmentLP & fragments() const {return frags;}
	void addFragment(Fragment *frag) {frags.push_back(frag);} // takes ownership of frag
	static QString msgTempl;
	static QString msgTemplBulk;
	static QByteArray msgTemplEpilog;
protected:
	QDateTime tstCreation, tstExecution;
	NodeAddress src, dst;
	unsigned long long int sluid_=0, dluid_=0;
	FragmentLP frags;
	QByteArray *payload_=nullptr;
};

class CommandFilter
{
public:
	CommandFilter() {}
	CommandFilter(const QString cmd) {addCmd(cmd);}
	QString toString() const;
	bool isMatch(const Command & cmd, Fragment & relevant) const;
	void addCmd(const QString cmd) {cmdsS.insert(cmd);}
	void merge(const CommandFilter & other);
	std::set< QString > cmdsS; // empty means "do not filter by command"
	std::set< NodeAddress > sourceNodesS; // may use wildcards in NodeAddress, empty means "do not filter by source"
	bool checkFragCmds=true;
};
}
#endif // COMMAND_H
