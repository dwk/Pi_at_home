#include "conpool.h"
#include <QCoreApplication>
#include "hostdict.h"
#include "command.h"
#include "piadi.h"

ConPool::ConPool(bool isServer, const QDomElement *elem, QObject *parent) :
	QObject(parent), hostDict(isServer, elem)
{
	setObjectName("ConPool");
	DCON.registerCmdTarget("hostdict", this);
	DCON.registerCmdTarget("connect", this);
}

ConPool::~ConPool()
{
	for(NetConMSP::iterator it=clientConMSP_.begin(); it!=clientConMSP_.end(); ++it)
	{
		//qDebug()<<"~ConPool deleting"<<it->second->uid();
		disconnect(it->second, nullptr, nullptr, nullptr);
		delete it->second;
		//qDebug()<<"~ConPool deleting complete";
	}
}

void ConPool::processCommand(Command &cmd, Fragment &relevant)
{
	if(cmd.cmd()=="hostdict")
	{
		const FragmentLP & frags=cmd.fragments();
		int cnt=0;
		for(auto fit=frags.begin(); fit!=frags.end(); ++fit)
		{
			QString uid=(*fit)->attribute("uid");
			QHostAddress ha((*fit)->attribute("ip"));
			int port=(*fit)->attribute("port").toInt();
			QDateTime ver=QDateTime::fromString((*fit)->attribute("verified"), Qt::ISODate);
			if(!uid.isEmpty() && !ha.isNull() && port && ver.isValid())
			{
				hostDict.confirm(uid, ha, port, ver);
				++cnt;
			}
		}
		if(cnt>0)
			hostDict.flush();
		qInfo()<<"received host addresses"<<cnt;
	}
	else if(relevant.cmd()=="connect")
	{
		; // nothing to do - stufff was handeled in sockReady(true)
	}
	else
		qCritical()<<"unknown command for ConPool"<<cmd.cmd()<<relevant.cmd();
}

NetCon *ConPool::hostCon(const QString &uid)
{
	NetConMSP::iterator it=hostConUidMSP_.find(uid);
	if(it==hostConUidMSP_.end())
		return nullptr;
	return it->second;
}

void ConPool::doSend(Command &cmd)
{
	//qDebug()<<"ConPool::doSend (broadcast)"<<clientConMSP_.size();
	for(NetConMSP::iterator it=clientConMSP_.begin(); it!=clientConMSP_.end(); ++it)
	{
		if(it->second)
		{
			//qDebug()<<"sending to"<<it->second->address().toString();
			it->second->send(cmd);
		}
	}
}

void ConPool::doSend(Command &cmd, const QString &uid)
{
	NetConMSP::iterator rit=clientConMSP_.find(uid);
	if(rit==clientConMSP_.end() || !(rit->second) )
	{
		auto rit2=hostConUidMSP_.find(uid);
		if(rit2==hostConUidMSP_.end() || !(rit2->second) )
			qCritical()<<"no route"<<cmd.cmd()<<cmd.procTarget().toStringVerbose();
		else
			rit2->second->send(cmd);
	}
	else
		rit->second->send(cmd);
}

NetCon * ConPool::addHost(const QString &uid)
{
	auto testit=hostConUidMSP_.find(uid);
	if(testit!=hostConUidMSP_.end())
	{ // there is already a connection established
		qDebug()<<"ConPool: already connected to"<<uid;
		return testit->second;
	}
	const HostInstance *hi=hostDict.find(uid);
	if(!hi)
	{
		qCritical()<<"ConPool: no route to"<<uid;
		return nullptr;
	}
	return addHost(QHostAddress(hi->hadr), hi->port);
}

NetCon * ConPool::addHost(const QHostAddress &host, int port)
{
	QString adrStr=host.toString()+":"+QString::number(port);
	auto testit=hostConIpMSP_.find(adrStr);
	if(testit!=hostConIpMSP_.end())
	{ // there is already a connection request underway
		//qDebug()<<"ConPool: pending connect for"<<adrStr;
		return testit->second;
	}
	NetCon *hc=new NetCon(host, port);
	pair<NetConMSP::iterator,bool> res=hostConIpMSP_.insert(NetConMSP::value_type(adrStr, hc));
	//qDebug()<<"hostConIpMSP_ has"<<hostConIpMSP_.size()<<"elements";
	//for(auto it=hostConIpMSP_.begin(); it!=hostConIpMSP_.end(); ++it)
	//	qDebug()<<it->second->uid()<<it->second->address().toString();
	if(!res.second)
	{
		qCritical()<<"Duplicate Host Address"<<adrStr<<"is already used. Connection closed.";
		delete hc;
		return nullptr;
	}
	emit connectionListChanged();
	connect(hc, SIGNAL(errorSig(int)), this, SLOT(sockErrorSl(int)));
	connect(hc, SIGNAL(readySig(bool)), this, SLOT(sockReady(bool)));
	hc->start();
	return hc;
}

void ConPool::addClient(NetCon *con)
{
	connect(con, SIGNAL(readySig(bool)), this, SLOT(sockReady(bool)));
}

bool ConPool::closeHost(QString uid)
{
	NetConMSP::iterator it=hostConUidMSP_.find(uid);
	if(it==hostConUidMSP_.end())
		return false;
	it->second->stop();
	emit connectionListChanged();
	return true;
}

void ConPool::closeAllHosts()
{
	for(NetConMSP::iterator it=hostConIpMSP_.begin(); it!=hostConIpMSP_.end(); ++it)
		it->second->stop();
	emit connectionListChanged();
}

void ConPool::sockErrorSl(int errNo)
{
	NetCon * nc=dynamic_cast<NetCon * >(sender());
	if(!nc)
	{
		qCritical()<<"spurious sockErrorSl"<<errNo;
		return;
	}
	QString ip=nc->address().toString();
	QString uid=nc->uid();
	//qCritical()<<"ConPool socket error"<<errNo<<"on"<<uid<<ip; NetCon already said that
	if(uid.isEmpty())
	{
		uid=hostDict.find(nc->address(), nc->port());
	}
	emit conErrorSig(uid, ip, errNo);
}

void ConPool::sockReady(bool ready)
{
	NetCon *con=dynamic_cast<NetCon*>(sender());
	if(!con)
	{
		qCritical()<<"cast to NetCon failed of"<<(sender()?sender()->objectName():"-");
		return;
	}
	//qDebug()<<"ConPool::sockReady"<<(con->isHost()?"host":"client")<<(ready?"ready":"dead");
	if(con->isHost())
	{ // so it points to a client - different semantics between NetCon and ConPool!
		if(ready)
		{
			clientConMSP_.insert(NetConMSP::value_type(con->uid(), con));
			// no, this would insert the client address for the uid of this host
			//hostDict.confirm(con->uid(), con->address(), con->port());
		}
		else
		{ // a closing socket on server side with delete the NetCon
			clientConMSP_.erase(con->uid());
			con->deleteLater();
		}
		// cleanup working map - remove con if not ready
		bool guiConnected=false;
		for(NetConMSP::iterator it=clientConMSP_.begin(); it!=clientConMSP_.end();)
		{
			if(!(it->second))
			{ // died away
				NetConMSP::iterator delit=it;
				++it;
				clientConMSP_.erase(delit);
				continue;
			}
			if(it->second->version().startsWith("piahGUI"))
				guiConnected=true;
			++it;
		}
		emit guiConnectedSig(guiConnected);
	}
	else
	{ // so it points to a host - different semantics between NetCon and ConPool!
		if(ready)
		{
			pair<NetConMSP::iterator,bool> res=hostConUidMSP_.insert(NetConMSP::value_type(con->uid(), con));
			if(!res.second)
			{
				qCritical()<<"Duplicate Host UID "<<con->uid()<<". Connection to"<<con->address().toString()<<"closed.";
				// avoid sockReady(false) from the dtor of NetCon
				disconnect(con, nullptr, this, nullptr);
				con->deleteLater();
			}
			hostDict.confirm(con->uid(), con->address(), con->port());
			qInfo()<<"ConPool: Connection establoished"<<con->uid()<<con->address().toString();
			emit hostConStateSig(con->uid(), true);
		}
		else
		{
			if(!con->uid().isEmpty())
			{
				qInfo()<<"ConPool: Connection lost"<<con->uid()<<con->address().toString();
				emit hostConStateSig(con->uid(), false); // report this signal only for established connection, all other will be handleed through ConErrSig
			}
			hostConIpMSP_.erase(con->address().toString()+":"+QString::number(con->port()));
			hostConUidMSP_.erase(con->uid());
			//con->deleteLater(); NO - let it live, it will try to reconnect
		}
		for(NetConMSP::iterator it=hostConIpMSP_.begin(); it!=hostConIpMSP_.end();)
		{
			if(!(it->second))
			{ // died away
				NetConMSP::iterator delit=it;
				++it;
				hostConIpMSP_.erase(delit);
			}
			else
				++it;
		}
		for(NetConMSP::iterator it=hostConUidMSP_.begin(); it!=hostConUidMSP_.end();)
		{
			if(!(it->second))
			{ // died away
				NetConMSP::iterator delit=it;
				++it;
				hostConUidMSP_.erase(delit);
			}
			else
				++it;
		}
		emit connectionListChanged();
	}
}

