#ifndef NODEDS1820_H
#define NODEDS1820_H

#include "piadh.h"
#include "node.h"
#include "device.h"
#include <QThread>

/* *** Device DS1820
 * see standard device attributes: "poll_mult" is REQUIRED here, othewise you will never get an updated value!
 * required attributes
 *	type="DS1820"
 * optional attributes
 *	<none>
 *
 * DS1820 offers access to the one-wire driver w1-gpio. The port is defined by kernel module setup and cannot changed
 * through device specification; default is WP7 = GPIO4
 * Make sure that one-wire is activated in raspi-config!
 *
 * *** Node DS1820
 * see standard node attributes
 * required attributes
 *	id="x"				the device id of the one-wire device (look in /sys/bus/w1/devices/ after conneting the hardware for the 1-wire ID)
 * optional attributes
 *	<none>
 *
 * Invalid device ids will create a critical log every time accessed
 *
 * Exapmple
 * <device id="30" type="DS1820">
 *   <node channel="0" name="w1-test" id="10-0008030cc112" log="0"/>
 * </device>
 */

//Name:   w1-gpio
//Info:   Configures the w1-gpio Onewire interface module.
//        Use this overlay if you *don't* need a GPIO to drive an external pullup.
//Load:   dtoverlay=w1-gpio,<param>=<val>
//Params: gpiopin                  GPIO for I/O (default "4")
//        pullup                   Non-zero, "on", or "y" to enable the parasitic
//                                 power (2-wire, power-on-data) feature

//Name:   w1-gpio-pullup
//Info:   Configures the w1-gpio Onewire interface module.
//        Use this overlay if you *do* need a GPIO to drive an external pullup.
//Load:   dtoverlay=w1-gpio-pullup,<param>=<val>
//Params: gpiopin                  GPIO for I/O (default "4")
//        pullup                   Non-zero, "on", or "y" to enable the parasitic
//                                 power (2-wire, power-on-data) feature
//        extpullup                GPIO for external pullup (default "5")

namespace pi_at_home
{
class NodeDS1820Worker : public QObject
{
	Q_OBJECT
public:
	NodeDS1820Worker(const QString & w1Id) : w1Id_(w1Id) {}
public slots:
	void readHwSl();
private:
	static QString basePath;
	QString w1Id_;
signals:
	void resultReady(double temperature, qint64 timestamp, bool ok);
};

class NodeDS1820 : public Node
{
	Q_OBJECT
public:
	NodeDS1820(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	//virtual QString serializeValue(const NodeValue & value) const; QVariant::toString is fine
	virtual Node::Unit unit() const {return Unit::DEGREE_CELSIUS;}
	virtual NodeValue defaultValue() const {return NodeValue(25.);}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return false;}
	virtual Result init();
	//virtual value();
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void ping();
	virtual void processCommand(Command & cmd, Fragment & relevant);
	//
	double readInternal() {return val_;}
	void shutdown();
public slots:
	virtual void newValSl(const NodeValue & newValue);
	void conversionCompletedSl(double temperature, qint64 timestamp, bool ok);
protected:
	QThread workerThread;
	NodeDS1820Worker *worker=nullptr;
	QString w1Id;
	double val_=-9999.;
signals:
	void initiateHwConversionSig();
};

class DeviceDS1820 : public Device
{
	Q_OBJECT
public:
	explicit DeviceDS1820(const QDomElement &elem);
	//virtual Result init(); Device is fine
	virtual Result shutdown();
	double readValue(const QString & w1Id); // return value is result
};

}

#endif // NODEDS1820_H
