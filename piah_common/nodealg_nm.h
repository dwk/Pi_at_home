#ifndef NODEALG_NM_H
#define NODEALG_NM_H

namespace
{
const QString typeNameBool="AlgBool";
const QString typeNameInt="AlgInt";
const QString typeNameDbl="AlgDbl";
const QString typeNameStr="AlgStr";
}

#endif // NODEALG_NM_H
