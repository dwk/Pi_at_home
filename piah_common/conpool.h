#ifndef CONPOOL_H
#define CONPOOL_H
#include "piadh.h"
#include <QObject>
#include <QDateTime>
#include <QDomElement>
#include "commandtarget.h"
#include "hostdict.h"
#include "netcon.h"

namespace pi_at_home
{

class ConPool : public QObject, public CommandTarget
{
	Q_OBJECT
public:
	explicit ConPool(bool isServer, const QDomElement *elem, QObject *parent);
	~ConPool();
	// CommandTarget
	virtual void processCommand(Command & cmd, Fragment & relevant);
	//
	//NetConMSP & hostConMP() {return hostConUidMSP_;}
	NetCon * hostCon(const QString & uid);
	void doSend(pi_at_home::Command &cmd); // Broadcast
	void doSend(pi_at_home::Command &cmd, const QString & uid);
public slots:
	NetCon * addHost(const QString & uid);
	NetCon * addHost(const QHostAddress &host, int port);
	void addClient(NetCon * con); // misnomer: this connection POINTS to a client but RUNS at the server side; con expected to have isServer_==true
	bool closeHost(QString uid);
	void closeAllHosts();
private slots:
	void sockErrorSl(int errNo);
	void sockReady(bool ready);
private:
	NetConMSP hostConIpMSP_; // key is address:port
	NetConMSP hostConUidMSP_; // key is uid
	NetConMSP clientConMSP_; // key is uid
	HostDict hostDict;
signals:
	void connectionListChanged();
	void hostConStateSig(QString uid, bool connected);
	void guiConnectedSig(bool);
	void conErrorSig(QString uid, QString ip, int errn);
};
}
#endif // CONPOOL_H
