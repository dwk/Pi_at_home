#ifndef ALGARITHMETIC_FAC_H
#define ALGARITHMETIC_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgArithmetic(elem);
}
const QString typeName="Arithmetic";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGARITHMETIC_FAC_H
