#include "algdespike.h"
#include "algdespike_fac.h"
#include <cmath>
#include "command.h"
#include "nodealg.h"
#include "piadi.h"

AlgDespike::AlgDespike(const QDomElement &elem) :
	Algorithm(elem)
{
	if(!elem.hasAttribute("step-pos") || !elem.hasAttribute("step-neg"))
	{
		qCritical()<<"AlgDespike ERROR: at least one step-X parameter missing";
		algState=AlgState::ERROR;
		return;
	}
	bool ok=false;
	stepPos=elem.attribute("step-pos").toDouble(&ok);
	if(!ok || stepPos<0.)
	{
		stepPos=1000.;
		qWarning()<<"AlgDespike invalid step+, use 1000."<<elem.attribute("step+");
	}
	stepNeg=elem.attribute("step-neg").toDouble(&ok);
	if(!ok || stepNeg>0.)
	{
		stepNeg=-1000.;
		qWarning()<<"AlgDespike invalid step-, use -1000."<<elem.attribute("step-");
	}
	if(elem.hasAttribute("force-count"))
	{
		int m=elem.attribute("force-count").toInt(&ok);
		if(ok && m>=0)
			forceCnt=m;
		else
			qWarning()<<"AlgDespike invalid force-count"<<elem.attribute("force-count");
	}
	if(elem.hasAttribute("lin-increase"))
	{
		QString v=elem.attribute("lin-increase");
		if(v==QStringLiteral("false"))
			linearInc=false;
		if(v==QStringLiteral("true"))
			linearInc=true;
		else
			qWarning()<<"AlgDespike invalid lin-increase"<<elem.attribute("lin-increase");
	}
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		if(e.attribute(QStringLiteral("type_name"))==QStringLiteral("input"))
			inName=e.attribute(QStringLiteral("name"));
		else if(!ndOut && e.attribute(QStringLiteral("type_name"))==QStringLiteral("out"))
			ndOut=new NodeDbl(e, id_, this);
	}
	if(!ndOut)
	{
		qCritical()<<objectName()<<"no out node specified in config, reating defaul";
		ndOut=new NodeDbl(namePrefix+"out", id_, 2, this);
	}
}

void AlgDespike::execute()
{
	return; // purely timer driven
}

pi_at_home::Result AlgDespike::preExec()
{
	if(algState!=AlgState::STARTUP || !ndOut)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	NodeValue newValue;
	ndInput=DCON.findNode(inName, false);
	if(ndInput)
	{
		connect(ndInput, &Node::newValSig, this, &AlgDespike::valCaptureSl);
		bool ok=false;
		lastVal=ndInput->value().toDouble(&ok);
		ndOut->setUnit(ndInput->unit());
		if(!ok)
		{
			qWarning()<<objectName()<<"AlgDespike initial value does not convert to doubel, setting NaN"<<ndInput->value().toString();
			lastVal=std::numeric_limits<double>::quiet_NaN();
		}
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"AlgDespike cannot find input"<<inName;
		return Result::FAILED;
	}
	if(ndOut->getLocker())
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"out already locked";
		return Result::FAILED;
	}
	token=ndOut->lock(DCON.uid(), id_, objectName());
	if(!token)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot lock out";
		return Result::FAILED;
	}
	// execute(); purely timer driven
	return Result::COMPLETED;
}

pi_at_home::Result AlgDespike::postExec()
{
	if(ndOut)
	{
		ndOut->setValue(ndOut->defaultValue(), token);
		ndOut->unlock(token);
	}
	return Result::COMPLETED;
}

void AlgDespike::valCaptureSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	bool ok=false;
	double v=newValue.toDouble(&ok);
	if(!ok)
	{
		qWarning()<<objectName()<<"AlgDespike newValue does not convert to double"<<newValue.toString();
		return;
	}
	if(std::isnan(lastVal) ||
		(!linearInc && v>=lastVal-stepNeg && v<=lastVal+stepPos) ||
		(linearInc && v>=lastVal-stepNeg*forceI && v<=lastVal+stepPos*forceI))
	{ // never suppress the initial value OR normal operation (within limits, incremental or not) OR over force-count
		lastVal=v;
		forceI=1;
		ndOut->setValue(newValue, token);
		//qDebug()<<objectName()<<"pass through"<<newValue.toString();
		return;
	}
	// we have a valid lastVal and v is out of bounds
	++forceI; // maybe we need it for linearInc
	if(forceCnt && forceI>forceCnt+1)
	{ // limit reached, pass through and reset
		lastVal=v;
		forceI=1;
		ndOut->setValue(newValue, token);
		qInfo()<<objectName()<<"force-count pass through"<<newValue.toString();
	}
	//else: just wait for the next change of value
	//	qDebug()<<objectName()<<"filtered"<<newValue.toString();
}

