﻿#ifndef NODEARDUBRIDGE_H
#define NODEARDUBRIDGE_H

#include "piadh.h"
#include "node.h"
#include "device.h"

/* *** Device ArduBridge
 * required attributes
 * id="x"			x = vaild device address
 * type="ArduBridge"
 * SPI="b"			x = 0 | 1; SPI channel
 * optional attributes
 * <none>
 *
 * *** Node ArduBridge
 * required attributes
 * channel="x"			x = [0,5] line number
 * name="x"				x = node alias
 * optional attributes
 * <none>
 *
 */
namespace pi_at_home
{
class HwPort;

class NodeArduBridge : public Node
{
	Q_OBJECT
public:
	NodeArduBridge(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	virtual Node::Unit unit() const {return Unit::NONE;}
	virtual NodeValue defaultValue() const {return NodeValue(QString(""));}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return true;}
	virtual Result init();
	//virtual bool readValue(NodeValue & valueOut);
	virtual void setValue(const NodeValue & value, TokenType token=0);
	void setValue(const QString & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
protected:
	QString val_;
};

class DeviceArduBridge : public Device
{
	Q_OBJECT
	friend class NodeArduBridge;
public:
	explicit DeviceArduBridge(const QDomElement &elem);
	virtual Result init();
	virtual Result shutdown();
	void write(unsigned char *data, int size, int channel);
private:
	int portSpi_=-1;
};

}

#endif // NODEARDUBRIDGE_H
