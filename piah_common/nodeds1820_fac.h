#ifndef NODEDS1820_FAC_H
#define NODEDS1820_FAC_H

#include "nodeds1820_nm.h"

namespace
{
pi_at_home::Device * Creator(const QDomElement &elem)
{
	return new pi_at_home::DeviceDS1820(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, Creator);
}

#endif // NODEDS1820_FAC_H
