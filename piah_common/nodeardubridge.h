﻿#ifndef NODEARDUBRIDGE_H
#define NODEARDUBRIDGE_H

#include "piadh.h"
#include "node.h"
#include <QSerialPortInfo>
#include <QSerialPort>
#include "device.h"
#include "nodealg.h"

/* *** Device ArduBridge
 * required attributes
 *	type="ArduBridge"	fixed
 *	serial="x"			x = name of the serial device, e.g. ttyAMA0
 * optional attributes
 *	<none>
 *
 * *** Node ArduBridge
 * required attributes
 *	channel="x"				x = [0,25] arduino channel address
 *	name="x"				x = node alias
 *	type="x"				x = HYSRF05 | ...
 *						HYSRF05: ultrasonic sensor running in a 100ms loop
 *							A<int> number of basic measurements to be averaged (1..255) (Pi->Ard)
 *							C continous measurement (Pi->Ard)
 *							N no measurement, power down HYSRF05 (Pi->Ard)
 *							S single measurement, power down HYSRF05 afterwards (Pi->Ard)
 *							V<us> delay of echo pulse in microseconds, (double)<us>/calib is the read value of this node type (Ard->Pi)
 *		calib="d"			d = double; V-value / d = distance reported
 * optional attributes
 *	<none>
 *
 * write and read operation may yield different data types:
 * Writes are usually string (a command) while the read data type
 * depends on node type
 *
 */
namespace pi_at_home
{
class HwPort;

class NodeArduBridge : public Node
{
	Q_OBJECT
public:
	enum class HwType{ UNDEF=0, HYSRF05, HX711, DIGIOUT};
	NodeArduBridge(const QDomElement &elem, int device, QObject * parent);
	virtual const QString & type() const;
	virtual Node::Unit unit() const;
	virtual NodeValue defaultValue() const {return NodeValue(QString(""));}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return true;}
	virtual Result init();
	//virtual const NodeValue & value() const; // reads Ardu->Pi data
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
	void setReturnValue(const QString & retval);
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	void playInit();
	QByteArray lastcmd; // Node::value_ used for Ardu->Pi data; lastcmd holds recent Pi->Ardu data
	HwType hwType_;
	QString pinout;
	QStringList initCmds;
	char format=' ';
	int precision=2;
	double calib_=58.5, zero=0.;
};

class DeviceArduBridge : public Device
{
	Q_OBJECT
	friend class NodeArduBridge;
public:
	explicit DeviceArduBridge(const QDomElement &elem);
	~DeviceArduBridge();
	virtual Result init();
	virtual Result shutdown();
	void write(QByteArray &data, int channel);
private:
	Result createPort();
	QString devName;
	QSerialPortInfo * serpi=nullptr;
	QSerialPort * serp=nullptr;
	QByteArray readBuffer;
	bool arduOnline=false;
	int reconnectTimeout=4000;
	NodeStr *ndOut=nullptr;
private slots:
	void createPortSl();
	void readyReadSl();
	void serpErrSl(QSerialPort::SerialPortError error);
};

}

#endif // NODEARDUBRIDGE_H
