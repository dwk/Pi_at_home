#ifndef HOSTDICT_H
#define HOSTDICT_H
#include "piadh.h"
#include <QObject>
#include <QDateTime>
#include <QDomElement>
#include <QHostAddress>

class QSettings;
namespace pi_at_home
{

/*
class NodeInstance
{
public:
	NodeInstance() {}
	NodeInstance(NodeAddress &nadr_) : nadr(nadr_) {} // , lastConfirm(QDateTime::currentDateTime())
	NodeAddress nadr;
};
typedef std::map< QString , NodeInstance > NodeInstanceMV; // key can be alias
*/

class HostInstance
{
public:
	HostInstance() : hadr(0), port(0) {}
	HostInstance(quint32 adr_, int port_, const QDateTime &lastConfirm_) : hadr(adr_), port(port_), lastConfirm(lastConfirm_) {}
	quint32 hadr;
	int port;
	QString uid;
	QDateTime lastConfirm;
	//NodeInstanceMV nodeDict;
};
typedef std::map< QString , HostInstance > HostInstanceMV; // key can be uid

/** remember hosts and nodes
/   * static update from config file (invalid "last connected")
/   * own knowlege (persistent through QSettings)
/ provide lookup UID - >last known IP+port
/ provide lookup uid+alias -> last known NodeAddress **/
class HostDict : public QObject
{
	Q_OBJECT
public:
	explicit HostDict(bool isServer, const QDomElement *elem, int maxMemoryDays=365, QObject * parent=nullptr);
	~HostDict();
	const HostInstance * find(const QString & uid);
	QString find(const QHostAddress & addr, int port);
	const HostInstanceMV & hostInstances() const {return hostInstanceMV_;}
	void confirm(const QString & uid, const QHostAddress & addr, int port);
	void confirm(const QString & uid, const QHostAddress & addr, int port, const QDateTime & verified);
	bool update(HostInstance & hi);
	bool create(HostInstance & hi);
	bool remove(const QString & uid);
	void flush();
	//void remove(const QHostAddress & addr, int port);
protected:
	bool isServer_=true; // servers store their data in a file at the executables directory, GUI uses QSettings
	HostInstanceMV hostInstanceMV_;
	QSettings *settings=nullptr;
	int memoryDays;
};
}
#endif // HOSTDICT_H
