#include "algspecialaction.h"
#include "algspecialaction_fac.h"
#include "node.h"
#include "command.h"
#include "dwkapp.h"
#include "piadi.h"


AlgSpecialAction::AlgSpecialAction(const QDomElement &elem) :
	Algorithm(elem)
{
	QString act=elem.attribute("action");
	if(act=="quit")
		scmd=DwkApp::ServerCmd::STOP_SERVICE;
	else
		scmd=DwkApp::serverCmdStr2E(act);
	if(scmd==DwkApp::ServerCmd::NOP)
		qWarning()<<"AlgSpecialAction unknown action"<<act;
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		QString tn=e.attribute(QStringLiteral("type_name"));
		if(tn==QStringLiteral("input"))
			inNodeName_=e.attribute(QStringLiteral("name"));
	}
}

void AlgSpecialAction::execute()
{
	if(algState!=AlgState::RUNNING)
		return;
	if(!v_.toBool())
		return;
	/*switch(action_)
	{
		case Action::QUIT:
		{
			qInfo()<<"keyed service shutdown";
			DwkApp *app=qobject_cast<DwkApp*>(QCoreApplication::instance());
			app->executeServerCmd(DwkApp::ServerCmd::STOP_SERVICE); // call returns!
			break;
		}
		case Action::SERVER_CMD:
		{*/
			qInfo()<<"remoted server command invoked"<<(int)scmd;
			DwkApp *app=qobject_cast<DwkApp*>(QCoreApplication::instance());
			app->executeServerCmd(scmd);
			/*break;
		}
		case Action::UNDEF:
			break;
	}*/
}

void AlgSpecialAction::valCaptureSl(const NodeValue &newValue, qint64 timestamp)
{
	Q_UNUSED(timestamp)
	v_=newValue;
	execute();
}

pi_at_home::Result AlgSpecialAction::preExec()
{
	if(algState!=AlgState::STARTUP)
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	Node * inNode=DCON.findNode(inNodeName_, false);
	if(inNode)
	{
		connect(inNode, &Node::newValSig, this, &AlgSpecialAction::valCaptureSl);
	}
	else
	{
		algState=AlgState::ERROR;
		qCritical()<<"cannot find node"<<inNodeName_;
		return Result::FAILED;
	}
	algState=AlgState::RUNNING;
	return Result::COMPLETED;
}

pi_at_home::Result AlgSpecialAction::postExec()
{
	return Result::COMPLETED;
}

