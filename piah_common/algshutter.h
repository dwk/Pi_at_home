#ifndef ALGSHUTTER_H
#define ALGSHUTTER_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"
#include "nodealg.h"
//#include <QMutex>

namespace pi_at_home
{
/* Algorithm controls one shutter.
 * required attributes
 *	type="Shutter"			fixed
 * optional attributes
 *	duration_up="x"			x = time in ms >1000; net time to fully open the shutter; default 20000
 *	duration_down="x"		x = time in ms >1000; net time to fully close the shutter; default 20000
 *	duration_lockup="x"		x = time in ms or 0; time to lock the shutter in open position; default 4000
 *	duration_lockdown="x"	x = time in ms or 0; time to lock the shutter in closed position; default 4000
 *	duration_relaxup="x"	x = time in ms 0 or >100; relax in open position; default 800
 *	duration_relaxdown="x"	x = time in ms 0 or >100; relax in closed position; default 800
 *	duration_wait="x"		x = time in ms >100; inactive time when changing direction; default 1000
 * connectors
 *	input_up
 *	input_down
 * output nodes
 *  <prefix>status			string	channel 0, someinfo
 *	<prefix>up				bool	channel 1, should be connected to motor phase "up"
 *	<prefix>down			bool	channel 2, should be connected to motor phase "down"
 *
 * A pulse on input_up / input_down will move the shutter in the fully open / closed position and enable automatic
 * movements.
 * A continous active signal on up / down will move the shutter in the desired position and keep it
 * there. No movements until set to inactive.
 * If both up and down go active, the shutter stops immideatly or stays where it is. If one signal is reset,
 * it will move as requested by the still active signal.
 * If both signals become inactive within duration_wait no movement will happen until another input change.
 * Movement:
 * For a full up movement the motor is turned on for (duration_up + duration_lockup) ms. If
 * duration_relaxup > 0 it will wait duration_wait ms and run duration_relaxup in the opposite direction.
 * Same procedure for down movement.
 */
class AlgShutter : public Algorithm
{
	Q_OBJECT
public:
	enum class LogicState{ UNDEF=0, STOP, MOVE, WAIT, RELAX, MIDTURN, DONE};
	enum class HwState{ ANYWHERE=0, FULLUP, FULLDOWN};
	explicit AlgShutter(const QDomElement &elem);
	virtual ~AlgShutter() {}
	virtual void execute();
	virtual bool safeState() {return true;}
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp, int channel);
protected:
	virtual void timerEvent(QTimerEvent *event);
	virtual Result preExec();
	virtual Result postExec();
	void updateStateNode();
	QString ndInUpNm, ndInDwnNm;
	bool goingUp=true;
	LogicState logicState=LogicState::UNDEF;
	HwState hwState=HwState::ANYWHERE;
	bool inUp, inDown;
	Node * ndOutUp=nullptr, * ndOutDwn=nullptr; // * ndInUp=nullptr, * ndInDwn=nullptr,
	NodeStr * stateNode=nullptr;
	//Node::TokenType tokenUp=0, tokenDwn=0;
	int durUp=20000, durDwn=20000, durLockUp=4000, durLockDwn=4000, durRelaxUp=800, durRelaxDwn=0, durWait=1000, timerId=0;
};


}
#endif // ALGSHUTTER_H
