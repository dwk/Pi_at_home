#ifndef ALGPULSE_H
#define ALGPULSE_H

#include "piadh.h"
#include "device.h"
#include "node.h"
#include "algorithm.h"

namespace pi_at_home
{

/* AlgPulse
 * Parameters from Device and Algorithm do apply!
 * required attributes
 *	type="Pulse"			fixed
 *	op="x"					x = operation; list of valid operations see below
 * optional attributes
 *	<none>
 * output node
 *  <prefix>out			bool	channel 0, result of the operation
 *
 *
*/
class AlgPulse : public Algorithm
{
	Q_OBJECT
public:
	enum class BaseOp{ UNDEF=0, UNITY, AND, OR, PARITY, CONSTANT};
	explicit AlgPulse(const QDomElement &elem);
	virtual ~AlgPulse() {}
	virtual void execute() override;
	virtual bool safeState() override {return true;}
protected:
	virtual Result preExec() override;
	virtual Result postExec() override;
	virtual void timerEvent(QTimerEvent *event) override;
protected slots:
	void valCaptureSl(const NodeValue & newValue, qint64 timestamp);
	void resCaptureSl(const NodeValue & newValue, qint64 timestamp);
protected:
	QString inName;
	bool rising=true, falling=false, lastVal;
	Node *ndInput=nullptr, *ndReset=nullptr, *ndOut=nullptr;
	Node::TokenType token=0;
	int dur=200, timerId=0;
};


}
#endif // ALGPULSE_H
