#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "piadh.h"
#include "device.h"
#include <QPointer>
#include "commandtarget.h"

namespace pi_at_home
{
class Node;

// DOC MOVED TO ARCHITECTURE.ODT
class Algorithm : public Device, public CommandTarget
{
	Q_OBJECT
friend class DataConD;
public:
	enum class AlgState{ VIRGIN=0, STARTUP, RUNNING, SHUTDOWN, STOPED, ERROR };
	Algorithm(const QDomElement &elem);
	Algorithm(int id=0, QObject * parent =nullptr);
	virtual ~Algorithm();
	int prio() const {return prio_;} // a hint where to place it in the process queue, higher numbers will get served first
	// Algorithm
	virtual void execute() =0;
	virtual bool safeState() =0;
	// Device
	virtual Result init();
	virtual Result shutdown(); // usually not overwritten
	// CommandTarget
	virtual void processCommand(Command & cmd, Fragment & relevant);
protected:
	// Algorithm protected
	virtual Result preExec() =0;
	virtual Result postExec() =0;
	AlgState algState=AlgState::VIRGIN;
	int prio_=0;
};
typedef std::vector<QPointer<Algorithm> > AlgorithmVQP;
typedef std::map<int, QPointer<Algorithm> > AlgorithmMQP;
typedef std::multimap<int, QPointer<Algorithm> > AlgorithmMMIQP;


}
#endif // ALGORITHM_H
