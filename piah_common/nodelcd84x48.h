﻿#ifndef NODELCD84X48_H
#define NODELCD84X48_H

#include "piadh.h"
#include "node.h"
#include "device.h"

/* *** Device LCD84x48
 * required attributes
 *	id="x"			x = vaild device address
 *	type="LCD84x48"
 *	SPI="b"			x = 0 | 1; SPI channel
 *	d_c="WPx"		x = any valid port number in Wiring Pi notation; data / command pin
 *	reset="WPx"		x = any valid port number in Wiring Pi notation; reset pin
 * optional attributes
 *	hidden="b"		b = true | false; set the hide state of its nodes, see doc of Node
 *
 * Creates 6 line nodes (type NodeLcd84x48Line) automatically. Turns the display off
 * during shutdown.
 */
namespace pi_at_home
{
class HwPort;

class NodeLcd84x48Line : public Node
{
	Q_OBJECT
public:
	NodeLcd84x48Line(const QString & name, int device, int channel, QObject *parent);
	virtual const QString & type() const;
	virtual Node::Unit unit() const {return Unit::NONE;}
	virtual NodeValue defaultValue() const {return NodeValue(QString(""));}
	virtual bool canSetValue(const NodeValue & value) const {Q_UNUSED(value); return true;}
	virtual Result init();
	//virtual value();
	virtual void setValue(const NodeValue & value, TokenType token=0);
	virtual void processCommand(Command & cmd, Fragment & relevant);
public slots:
	virtual void newValSl(const NodeValue & newValue);
protected:
	void setValue(const QString & value);
	QString val_;
};

class DeviceLcd84x48 : public Device
{
	Q_OBJECT
	friend class NodeLcd84x48Line;
public:
	explicit DeviceLcd84x48(const QDomElement &elem);
	virtual Result init();
	virtual Result shutdown();
private:
	void moveToLine(int line);
	void writeLine(const QString text, int line);
	int portSpi_=-1, portDC=-1, portReset=-1;
	HwPort * hwPortDC=nullptr, * hwPortReset=nullptr;
	bool hiddenNodes=false;
};

}

#endif // NODELCD84X48_H
