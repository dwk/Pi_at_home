#ifndef NODESWITCHPORT_FAC_H
#define NODESWITCHPORT_FAC_H

#include "nodeswitchport_nm.h"

namespace
{
pi_at_home::Device * Creator(const QDomElement &elem)
{
	return new pi_at_home::DeviceSwitchPort(elem);
}
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, Creator);
}

#endif // NODESWITCHPORT_FAC_H
