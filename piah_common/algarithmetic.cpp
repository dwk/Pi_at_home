#include "algarithmetic.h"
#include "algarithmetic_fac.h"
#include "node.h"
#include "command.h"
#include "piadi.h"

AlgArithmetic::AlgArithmetic(const QDomElement &elem) :
	Algorithm(elem)
{
	ndOutNm=elem.attribute("output");
	expressionString=elem.attribute("expression").toStdString();
	QString templ="p%1";
	for(int i=0; true; ++i)
	{
		QString nm=elem.attribute(templ.arg(i));
		if(nm.isEmpty())
			break;
		ndInsNm<<nm;
	}
}

void AlgArithmetic::execute()
{
	if(algState!=AlgState::RUNNING)
		return;
	NodeValue newVal;
	for(unsigned int i=0; i<pSize; ++i)
	{
		QPointer< Node > nd=ndIns.at(i);
		if(!nd)
		{
			qCritical()<<"AlgArithmetic lost input node #"<<i;
			algState=AlgState::ERROR;
			return;
		}
		nd->readValue(newVal);
		bool ok=false;
		px[i]=newVal.toDouble(&ok);
		if(!ok)
		{
			qCritical()<<"AlgArithmetic input"<<nd->address().toStringVerbose()<<"not convertible to double:"<<newVal;
			algState=AlgState::ERROR;
			return;
		}
	}
	ExprT res = expression.value();
	ndOut->setValue(NodeValue(res), token); // will bypass if not changed
}

pi_at_home::Result AlgArithmetic::preExec()
{
	foreach(QString ndnm, ndInsNm)
	{
		Node * nd=DCON.findNode(ndnm, false);
		if(nd)
			ndIns.push_back(QPointer<Node>(nd));
		else
		{
			algState=AlgState::ERROR;
			qCritical()<<objectName()<<"cannot find input"<<ndnm;
			break;
		}
	}
	if(algState==AlgState::ERROR)
		return Result::FAILED;
	pSize=ndInsNm.size();
	px=new double[pSize];
	std::fill(px, px+pSize, 0.);
	QString tpl="p%1";
	for(unsigned int i=0; i<pSize; ++i)
	{
		symbolTable.add_variable(tpl.arg(i).toUtf8().data(), px[i]);
	}
	ndOut=DCON.findNode(ndOutNm, false);
	if(!ndOut)
	{
		qCritical()<<objectName()<<"cannot acquire output"<<ndOutNm;
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	if(ndOut->getLocker())
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<ndOutNm<<"already locked";
		return Result::FAILED;
	}
	token=ndOut->lock(DCON.uid(), id_, objectName());
	if(!token)
	{
		algState=AlgState::ERROR;
		qCritical()<<objectName()<<"cannot lock output"<<ndOutNm;
		return Result::FAILED;
	}
	expression.register_symbol_table(symbolTable);
	if(!parser.compile(expressionString, expression))
	{
		qDebug()<<"AlgArithmetic: Compilation error in expression"<<expressionString.c_str();
		return Result::FAILED;
	}
	return Result::COMPLETED;
}

pi_at_home::Result AlgArithmetic::postExec()
{
	if(ndOut)
	{
		ndOut->setValue(ndOut->defaultValue(), token);
		ndOut->unlock(token);
	}
	return Result::COMPLETED;
}
