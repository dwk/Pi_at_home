#ifndef NODEADDRESS_H
#define NODEADDRESS_H

#include "piadh.h"

namespace pi_at_home
{
// alias avUid are additional information and thus not included in comparision / orderning!
// Be aware of the special meaning of empty uid and negative device and channel!
class NodeAddress
{
public:
	static void setDefaultUid(const QString & uid) {defUid=uid;}
	NodeAddress(const QString uid, int device, int channel, const QString alias=QString()) :
		uid_(uid), device_(device), channel_(channel), alias_(alias), valid(true) {}
	NodeAddress(int device, int channel, const QString alias=QString()) :
		uid_(defUid), device_(device), channel_(channel), alias_(alias), valid(true) {}
	explicit NodeAddress(const QString textForm, bool throwIfInvalid = true);
	explicit NodeAddress() :
		uid_(defUid), device_(0), channel_(0), valid(true) {}
	bool isWildcard() const {return uid_.isEmpty() || device_<0 || channel_<0;}
	bool isValid() const {return valid;}
	bool operator<(const NodeAddress & o) const;
	bool operator==(const NodeAddress & o) const;
	QString toString() const {return QString("%1:%2:%3:%4").arg(avUid_, uid_).arg(device_).arg(channel_);}
	QString toStringVerbose() const;
	QString toStringUidAlias() const;
	QString toSettingsKey() const;
	static void uidAlias(const QString & uidAlias, QString & uid, QString & alias);
	int channel() const {return channel_;}
	void setChannel(int channel) {channel_=channel;}
	int device() const {return device_;}
	void setDevice(int device) {device_=device;}
	const QString & uid() const {return uid_;}
	void setUid(const QString uid) {uid_=uid;}
	bool isAvatar() const {return !avUid_.isEmpty();}
	const QString & avatarUid() const {return avUid_;}
	void convertToAvatar(const QString avUid = QString()) {if(avUid.isEmpty()) avUid_=defUid; else avUid_=avUid;}
	void convertToNode() {avUid_.clear();}
	const QString & alias() const {return alias_;}
	void setAlias(const QString alias) {alias_=alias;}
protected:
	static QString defUid;
	QString uid_, avUid_; // uid_ empty means wildcard
	int device_=0, channel_=0;  // <0 means wildcard
	QString alias_; // no wildcard mechanism for alias_ and avUid_
	bool valid;
};

}
#endif // NODEADDRESS_H
