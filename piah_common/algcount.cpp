#include "algcount.h"
#include "algcount_fac.h"
#include "node.h"
#include "command.h"
#include "nodealg.h"
#include "piadi.h"

AlgCount::AlgCount(const QDomElement &elem) :
	Algorithm(elem)
{
	bool ok=false;
	if(elem.hasAttribute("max-count"))
	{
		int m=elem.attribute("max-count").toInt(&ok);
		if(ok)
			cntMax=m;
		else
			qWarning()<<"AlgCount invalid max-count"<<elem.attribute("max-count");
	}
	if(elem.hasAttribute("min-count"))
	{
		int m=elem.attribute("min-count").toInt(&ok);
		if(ok)
			cntMin=m;
		else
			qWarning()<<"AlgCount invalid min-count"<<elem.attribute("min-count");
	}
	if(elem.hasAttribute("startval"))
	{
		int m=elem.attribute("startval").toInt(&ok);
		if(ok)
			startVal=m;
		else
			qWarning()<<"AlgCount invalid startval"<<elem.attribute("startval");
	}
	if(elem.hasAttribute("flow2start"))
	{
		QString v=elem.attribute("flow2start");
		if(v==QStringLiteral("false"))
			ouflow2start=false;
		else if(v==QStringLiteral("true"))
			ouflow2start=true;
		else
			qWarning()<<"AlgCount invalid flow2start"<<elem.attribute("flow2start");
	}
	if(cntMin>=cntMax)
	{
		qWarning()<<"AlgCount invalid cntMin>=cntMax (reset to default -1, 1)"<<cntMin<<cntMax;
		cntMin=-1;
		cntMax=1;
	}
	if(startVal<cntMin || startVal>cntMax)
	{
		qWarning()<<"AlgCount startVlal not in (min. max) (reset to default min-count)"<<startVal<<cntMin<<cntMax;
		startVal=cntMin;
	}
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement e=nodes.item(i).toElement();
		QString tn=e.attribute(QStringLiteral("type_name"));
		if(tn==QStringLiteral("incr"))
			ndNmIncr=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("decr"))
			ndNmDecr=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("reset"))
			ndNmReset=e.attribute(QStringLiteral("name"));
		else if(tn==QStringLiteral("count"))
			ndCount=new NodeInt(e, id_, this);
		else if(tn==QStringLiteral("isneg"))
			ndNeg=new NodeBool(e, id_, this);
		else if(tn==QStringLiteral("iszero"))
			ndZero=new NodeBool(e, id_, this);
		else if(tn==QStringLiteral("ispos"))
			ndPos=new NodeBool(e, id_, this);
		else if(tn==QStringLiteral("overflow"))
			ndOverflow=new NodeBool(e, id_, this);
		else if(tn==QStringLiteral("underflow"))
			ndUnderflow=new NodeBool(e, id_, this);
		else if(tn==QStringLiteral("lastcnt"))
			ndLastCnt=new NodeInt(e, id_, this);
	}
	count=startVal;
	lastCnt=count;
}

void AlgCount::execute()
{
	if(algState!=AlgState::RUNNING && algState!=AlgState::STARTUP)
		return;
	if(ndCount)
		ndCount->setValue(NodeValue(count)); // will bypass if not changed
	if(ndLastCnt)
		ndLastCnt->setValue(NodeValue(lastCnt)); // will bypass if not changed
	if(ndNeg)
		ndNeg->setValue(NodeValue(count<0));
	if(ndZero)
		ndZero->setValue(NodeValue(count==0));
	if(ndPos)
		ndPos->setValue(NodeValue(count>0));
	if(ndOverflow)
		ndOverflow->setValue(NodeValue(ovfl));
	if(ndUnderflow)
		ndUnderflow->setValue(NodeValue(unfl));
}

pi_at_home::Result AlgCount::preExec()
{
	if(algState!=AlgState::STARTUP )
	{
		algState=AlgState::ERROR;
		return Result::FAILED;
	}
	if(!ndNmIncr.isEmpty())
	{
		ndIncr=DCON.findNode(ndNmIncr, false);
		if(ndIncr)
			connect(ndIncr, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, 100);});
		else
			qCritical()<<objectName()<<"AlgCount no incr"<<ndNmIncr;
	}
	if(!ndNmDecr.isEmpty())
	{
		ndDecr=DCON.findNode(ndNmDecr, false);
		if(ndDecr)
			connect(ndDecr, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, 101);});
		else
			qCritical()<<objectName()<<"AlgCount no decr"<<ndNmDecr;
	}
	if(!ndNmReset.isEmpty())
	{
		ndReset=DCON.findNode(ndNmReset, false);
		if(ndReset)
			connect(ndReset, &Node::newValSig, [this](const NodeValue & newValue, qint64 timestamp) -> void {valCaptureSl(newValue, timestamp, 102);});
		else
			qCritical()<<objectName()<<"AlgCount no reset"<<ndNmReset;
	}
	execute();
	return Result::COMPLETED;
}

pi_at_home::Result AlgCount::postExec()
{
	return Result::COMPLETED;
}

void AlgCount::valCaptureSl(const NodeValue &newValue, qint64 timestamp, int inChannel)
{
	Q_UNUSED(timestamp)
	//qDebug()<<"valCaptureSl"<<inChannel<<newValue;
	if(!newValue.toBool())
		return; // only positivr transition counts
	if(inChannel==100)
		++count;
	else if(inChannel==101)
		--count;
	else if(inChannel==102)
	{
		lastCnt=count;
		count=startVal;
	}
	else
		return;
	ovfl=false;
	unfl=false;
	if(count>cntMax)
	{
		ovfl=true;
		if(ouflow2start)
			count=startVal;
		else
			count=cntMin;
	}
	else if(count<cntMin)
	{
		unfl=true;
		if(ouflow2start)
			count=startVal;
		else
			count=cntMax;
	}
	execute();
}
