#include "nodetadlbus.h"
#include "nodetadlbus_fac.h"
#include <math.h>
#include <wiringPi.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <poll.h>
#include <sys/resource.h>
#include "command.h"
#include "piadi.h"

NodeTaDlBusSensorT::NodeTaDlBusSensorT(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent), val_(-1000.) // std::numeric_limits<double>::quiet_NaN() nan do not compare!
{
	int ch=adr_.channel();
	if(ch<1 || ch>15)
		qWarning()<<"NodeTaDlBusSensorT on channel"<<ch;
}

const QString &NodeTaDlBusSensorT::type() const
{
	return typeNameSensorT;
}

void NodeTaDlBusSensorT::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	Q_UNUSED(value);
	Q_UNUSED(token);
	qCritical()<<"setValue on NodeTaDlBusSensorT";
}

void NodeTaDlBusSensorT::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		updateValue();
		relevant.setAttribute("val", QString::number(val_));
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeTaDlBusSensorT"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeTaDlBusSensorT::newValSl(const NodeValue &newValue)
{
	Q_UNUSED(newValue);
	qCritical()<<"newValSl on NodeTaDlBusSensorT";
}

void NodeTaDlBusSensorT::updateValue()
{
	DeviceTaDlBus *d=qobject_cast<DeviceTaDlBus*>(parent());
	if(!d)
		throw Ex(QString("readValue failed on %1").arg(adr_.toStringVerbose()));
	qint64 newTst=d->currentTst();
	if(newTst==lastTst)
		return;
	lastTst=newTst;
	int unitId;
	double newValue=d->readSensor(adr_.channel(), unitId);
	if(unitId!=2)
	{
		qCritical()<<"T sensor on"<<adr_.toStringVerbose()<<"got unitId"<<unitId;
		return;
	}
	//qDebug()<<"reading value"<<newValue<<val_;
	if(fabs(newValue-val_)>=0.1)
	{
		val_=newValue;
		value_=val_;
		//qDebug()<<"value"<<value_<<val_;
		state_=State::READY;
		emit newValSig(value_, lastTst);
	}
	return;
}

NodeTaDlBusRpm::NodeTaDlBusRpm(const QDomElement &elem, int device, QObject *parent) :
	Node(elem, device, parent, 17)
{
}

const QString &NodeTaDlBusRpm::type() const
{
	return typeNameRpm;
}

void NodeTaDlBusRpm::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	Q_UNUSED(value);
	Q_UNUSED(token);
	qCritical()<<"setValue on NodeTaDlBusRpm";
}

void NodeTaDlBusRpm::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		updateValue();
		relevant.setAttribute("val", QString::number(val_));
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeTaDlBusRpm"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeTaDlBusRpm::newValSl(const NodeValue &newValue)
{
	Q_UNUSED(newValue);
	qCritical()<<"newValSl on NodeTaDlBusRpm";
}

void NodeTaDlBusRpm::updateValue()
{
	DeviceTaDlBus *d=qobject_cast<DeviceTaDlBus*>(parent());
	if(!d)
		throw Ex(QString("readValue failed on %1").arg(adr_.toStringVerbose()));
	qint64 newTst=d->currentTst();
	if(newTst==lastTst)
		return;
	lastTst=newTst;
	unsigned int newValue=d->readRpm();
	if(newValue!=val_)
	{
		val_=newValue;
		value_=val_;
		//qDebug()<<"value"<<value_<<val_;
		state_=State::READY;
		emit newValSig(value_, lastTst);
	}
	return;
}

NodeTaDlBusTst::NodeTaDlBusTst(const QString & name, int device, QObject *parent) :
	Node(name, device, 0, parent)
{
}

const QString &NodeTaDlBusTst::type() const
{
	return typeNameTst;
}

void NodeTaDlBusTst::setValue(const NodeValue &value, pi_at_home::Node::TokenType token)
{
	Q_UNUSED(value);
	Q_UNUSED(token);
	qCritical()<<"setValue on NodeTaDlBusTst";
}

void NodeTaDlBusTst::processCommand(Command &cmd, pi_at_home::Fragment &relevant)
{
	if(relevant.cmd()=="noderq")
	{
		updateValue();
		relevant.setAttribute("val", val_.toString(Qt::ISODate));
		relevant.setAttribute("state", Node::state(state()));
		cmd.convertToReply();
		return;
	}
	else
		qCritical()<<"unknown command for NodeTaDlBusTst"<<relevant.cmd()<<adr_.toString();
	return;
}

void NodeTaDlBusTst::newValSl(const NodeValue &newValue)
{
	Q_UNUSED(newValue);
	qCritical()<<"newValSl on NodeTaDlBusTst";
}

void NodeTaDlBusTst::updateValue()
{
	DeviceTaDlBus *d=qobject_cast<DeviceTaDlBus*>(parent());
	if(!d)
		throw Ex(QString("readValue failed on %1").arg(adr_.toStringVerbose()));
	qint64 newTst=d->currentTst();
	if(newTst==lastTst)
		return;
	lastTst=newTst;
	QDateTime newValue=d->readTst();
	//qDebug()<<lastId<<newValue;
	if(newValue!=val_)
	{
		val_=newValue;
		value_=val_;
		state_=State::READY;
		emit newValSig(value_, lastTst);
	}
	return;
}

DeviceTaDlBusDecoder::DoubleBuffer::DoubleBuffer() :
	idCount(0), bufferA(new unsigned char[64]), bufferB(new unsigned char[64])
{
	writeBuffer=bufferA;
}

DeviceTaDlBusDecoder::DoubleBuffer::~DoubleBuffer()
{
	delete[] bufferA;
	delete[] bufferB;
}

unsigned char * DeviceTaDlBusDecoder::DoubleBuffer::switchBuffers()
{
	QMutexLocker lock(&mtx);
	if(writeBuffer==bufferA)
	{
		readBuffer=bufferA;
		writeBuffer=bufferB;
	}
	else
	{
		readBuffer=bufferB;
		writeBuffer=bufferA;
	}
	++idCount;
	return writeBuffer;
}

void DeviceTaDlBusDecoder::DoubleBuffer::debugDumpWirteBuffer(int count)
{
	QStringList index;
	QStringList numbers;
	QString t="%1 ";
	for(int i=0; i<count; ++i)
	{
		index<<t.arg(i, 2);
		numbers<<t.arg((int)writeBuffer[i], 2, 16);
	}
	qDebug()<<index.join("");
	qDebug()<<numbers.join("");
}

DeviceTaDlBusDecoder::DeviceTaDlBusDecoder(int wiringPiPin, DeviceTaDlBusDecoder::DoubleBuffer *buffer, int readDelay) :
	wiringPiPin(wiringPiPin), readDelay_(readDelay), buffer(buffer)
{
	connect(this, SIGNAL(normalStart()), this, SLOT(startTimerSl()), Qt::QueuedConnection); // cross thread comm!
	connect(this, SIGNAL(normalStop()), this, SLOT(stopTimerSl()), Qt::BlockingQueuedConnection); // cross thread comm!
	struct timespec ts;
	if(clock_getres(CLOCK_MONOTONIC_RAW, &ts))
		qCritical()<<"cannot read timer resolution"<<strerror(errno);
	if(ts.tv_nsec>1000)
		qWarning()<<"insufficient timer resolution"<<ts.tv_nsec;
}

void DeviceTaDlBusDecoder::timerEvent(QTimerEvent *event)
{
	Q_UNUSED(event);
	try
	{
		bool bit = true;
		unsigned char byte = 0;
		int mode = 0, byteCnt = 0, bitCnt = 0, ticks=0, res=0;
		long delta;
		struct timespec framestart;
		if(clock_gettime(CLOCK_MONOTONIC_RAW, &framestart))
		{
			qCritical()<<"cannot read timer"<<strerror(errno);
			estop();
			return;
		}
		long reftime=framestart.tv_nsec;
		while(mode<2)
		{
			res=waitForEdge(1000);
			if(!res)
			{
				qWarning()<<"ISR timed out in mode"<<mode;
				break; // start from the beginning
			}
			else if(res!=1)
			{
				qCritical()<<"edge trigger DeviceTaDlBusDecoder failed in mode"<<mode<<"with"<<res<<strerror(errno);
				estop();
				return;
			}
			if(clock_gettime(CLOCK_MONOTONIC_RAW, &framestart))
			{
				qCritical()<<"cannot read timer in mode"<<mode<<"with"<<res<<strerror(errno);
				estop();
				return;
			}
			delta = framestart.tv_nsec - reftime;
			if(delta<0)
				delta+=1000000000;
			reftime = framestart.tv_nsec;
			ticks=(delta+sampleOffset)/tickUnit;
			switch(mode)
			{
			case 0: // frame sync
			{
				if(ticks==2)
					++bitCnt;
				else
					bitCnt=0;
				if(bitCnt==16)
				{
					//qDebug()<<"starting frame possible";
					mode=-1;
					bitCnt=0;
					byteCnt=0;
					byte=0;
					bit=true;
				}
				break;
			} // mode 0
			case -1: // waiting for first start bit
			{
				if(ticks==2)
					break; // still waiting
				else if(ticks==3 || ticks==4)
				{
					//qDebug()<<"starting frame detected";
					mode=1;
					// intentionally no break, go on with case 1
				}
				else
				{
					qWarning()<<"missed start bit"<<ticks;
					mode=3;
					break;
				}
			} // mode -1
			// fall through
			case 1: // data reading
			{
				//qDebug()<<"bitread delta"<<delta<<"ticks"<<ticks<<"bit"<<bit<<"bitCnt"<<bitCnt<<"byteCnt"<<byteCnt;
				if(ticks==2)
					; // same bit again
				else if(ticks==3 && bit)
					bit = (!bit);
				else if(ticks==3 && !bit)
				{ // we missed a zero bit and a 0->1 transition
					++bitCnt;
					byte=byte>>1;
					bit=true;
				}
				else if(ticks==4)
				{ // we missed one bit of opposite sign
					++bitCnt;
					byte=byte>>1;
					if(!bit)
						byte+=128;
				}
				else
				{
					qDebug()<<"invalid duration"<<delta<<ticks<<"bitCnt"<<bitCnt<<"byteCnt"<<byteCnt;
					mode=3;
					break;
				}
				switch(bitCnt)
				{
				case 0:
					if(bit)
					{
						qWarning()<<"startbit is high at byte"<<byteCnt;
						mode=3;
					}
					else
						++bitCnt;
					break;
				case 9:
					if(!bit)
					{
						qDebug()<<"stopbit is low at byte"<<byteCnt;
						mode=3;
					}
					else
					{
						//qDebug()<<"byte"<<QString::number(byte, 16)<<"byteCnt"<<byteCnt;
						if(!byteCnt && !byte)
						{
							qInfo()<<"skipping slave frame";
							mode=4;
							break;
						}
						buffer->writeBuffer[byteCnt++]=byte;
						bitCnt=0;
						byte=0;
					}
					break;
				case 10:
					qCritical()<<"bitcount = 10";
					mode=3;
					break;
				default:
					++bitCnt;
					byte=byte>>1;
					if(bit)
						byte+=128;
				}
				if(mode<2 && byteCnt>=frameSize)
					mode=2;
				break;
			} // mode 1
			default:
				qCritical()<<"invalid mode"<<mode;
				estop();
				return;
			}
		} // while
		if(mode==2) // data evaluation
		{
			//buffer->debugDumpWirteBuffer(byteCnt);
			if(verifyBuffer())
			{ // valid frame
				buffer->switchBuffers();
				//qDebug()<<"valid buffer";
				emit bufferReadySig(buffer->readBuffer); // Well done! Timer will wait before next attempt.
			}
		} // mode 2
		/*if(mode==3)
		{
			buffer->debugDumpWirteBuffer(byteCnt);
		}*/
		if(shutdown && timerId)
		{
			//qDebug()<<"DeviceTaDlBusDecoder timer stopped";
			killTimer(timerId);
			timerId=0;
		}
	} // try
	catch(Ex & ex)
	{
		qCritical()<<"Exception catched"<<ex.getCode()<<ex.getMessage();
		estop();
	}
	catch(...)
	{
		qCritical()<<"Exception catched";
		estop();
	}
}

void DeviceTaDlBusDecoder::decoderControl(bool start)
{
	if(start)
	{
		if(timerId)
			qCritical()<<"double start???";
		else
		{
			// edges seem to be inverted - or I've twisted the Gray decoding somehow. Anyway, falling edge works
			QByteArray cmd=QString("/usr/bin/gpio edge %1 falling").arg(wpiPinToGpio(wiringPiPin)).toUtf8();
			pinMode(wiringPiPin, INPUT);
			pullUpDnControl(wiringPiPin, PUD_OFF);
			int res=system(cmd.data());
			if(res)
			{
				qCritical()<<"cannot set pin for ISR"<<res<<cmd;
				emit emergencyStop();
				return;
			}
			// fd handling inspired from https://github.com/WiringPi/WiringPi/blob/master/wiringPi/wiringPi.c
			// direct use of waitForInterrupt() is broken because fd will never be opened.
			if (fd < 0)
			{
				QByteArray fName = QString("/sys/class/gpio/gpio%1/value").arg(wpiPinToGpio(wiringPiPin)).toUtf8();
				fd=open(fName.data(), O_RDWR);
				if (fd < 0)
				{
					qCritical()<<"copen failed on"<<fName<<strerror(errno);
					emit emergencyStop();
					return;
				}
				// flush pin
				int count = 0;
				char c;
				ioctl (fd, FIONREAD, &count) ;
				for (int i = 0 ; i < count ; ++i)
					read(fd, &c, 1);
			}
			//qDebug()<<"base prio"<<getpriority(PRIO_PROCESS, 0);
			nice(-20);
			//qDebug()<<"nice return"<<newNice<<"tuned prio"<<getpriority(PRIO_PROCESS, 0);
			emit normalStart();
			qInfo()<<"DeviceTaDlBusDecoder starting at priority"<<getpriority(PRIO_PROCESS, 0);
		}
	}
	else
	{
		if(timerId)
		{
			shutdown=true;
			emit normalStop();
		}
		else
			qCritical()<<"double stop???";
	}
}

void DeviceTaDlBusDecoder::startTimerSl()
{
	timerId=startTimer(readDelay_);
}

void DeviceTaDlBusDecoder::stopTimerSl()
{
	killTimer(timerId);
	timerId=0;
}

int DeviceTaDlBusDecoder::waitForEdge(int timeout)
{
	int res=0;
	// Setup poll structure
	struct pollfd polls;
	polls.fd     = fd;
	polls.events = POLLPRI;
	polls.revents= 0;
	// Wait for it
	for(;;)
	{
		res=poll(&polls, 1, timeout);
		if(res!=-1 || polls.revents)
			break;
		qWarning()<<"poll (TaDlBus) returned -1 with no bits in revents set - will try again";
	}
	// read and seek, see https://www.kernel.org/doc/Documentation/gpio/sysfs.txt
	lseek (fd, 0, SEEK_SET) ;
	uint8_t c ;
	(void)read (fd, &c, 1) ;
	return res;
}

bool DeviceTaDlBusDecoder::verifyBuffer()
{
	unsigned char checksum = 0;
	for(int i=0; i<frameSize-1; ++i)
		checksum+=buffer->writeBuffer[i];
	if((checksum & 255) != buffer->writeBuffer[frameSize-1])
	{
		qWarning()<<"checksum error, expected"<<(checksum & 255)<<"read"<<buffer->writeBuffer[frameSize-1];
		return false;
	}
	if(buffer->writeBuffer[0]!=0x90 || buffer->writeBuffer[1]!=0x9F)
	{
		qWarning()<<"device not an UVR61-3 v>=8.3"<<(buffer->writeBuffer[0])<<(buffer->writeBuffer[1]);
		return false;
	}
	return true;
}

void DeviceTaDlBusDecoder::estop()
{
	killTimer(timerId);
	timerId=0;
	emit emergencyStop();
}

DeviceTaDlBus::DeviceTaDlBus(const QDomElement &elem) :
	Device(elem), buffer(new DeviceTaDlBusDecoder::DoubleBuffer)
{
	QString p=elem.attribute("port");
	bool ok=false;
	if(p.startsWith("WP"))
		basePortWp=p.mid(2).toInt(&ok);
	if(!ok)
		throw Ex(QString("invalid port specification <%1> at device %2").arg(p).arg(id_));
	int rdly=elem.attribute("read_periode").toInt(&ok);
	decoder=new DeviceTaDlBusDecoder(basePortWp, buffer, ok?rdly:10000);
	decoder->moveToThread(&worker);
	connect(decoder, &DeviceTaDlBusDecoder::bufferReadySig, this, &DeviceTaDlBus::bufferReadySl);
	//
	new NodeTaDlBusTst(namePrefix+"timestamp", id_, this);
	QDomNodeList nodes=elem.elementsByTagName("node");
	for(int i=0; i<nodes.size(); ++i)
	{
		QDomElement nel=nodes.item(i).toElement();
		QString nodeType=nel.attribute("type_name");
		Node *n = nullptr;
		if(nodeType=="T")
			n=new NodeTaDlBusSensorT(nel, id_, this);
		else if(nodeType=="rpm")
			n=new NodeTaDlBusRpm(nel, id_, this);
		else if(nodeType=="timestamp")
			continue;
		if(!n)
			qCritical()<<"unknown node type in DeviceTaDlBus"<<nodeType;
	}
}

DeviceTaDlBus::~DeviceTaDlBus()
{
	delete decoder;
	delete buffer;
}

pi_at_home::Result DeviceTaDlBus::init()
{
	if(basePortWp<0)
		throw Ex(QString("init failed on device %1, port not set %2").arg(id_).arg(basePortWp));
	Result res=Device::init();
	if(res==Result::COMPLETED)
	{
		// start actual decoding driven by QTHreads event queue
		// first connect request may not get val-attributes because datanodes will be initialized during first read - not a bug :-)
		worker.start();
		decoder->decoderControl(true);
	}
	return res;
}

pi_at_home::Result DeviceTaDlBus::shutdown()
{
	if(basePortWp<0)
		throw Ex(QString("shutdown failed on device %1, port not set %2").arg(id_).arg(basePortWp));
	qInfo()<<"stopping DeviceTaDlBus worker...";
	decoder->decoderControl(false); // call directly for instant action
	worker.exit(); // kill event queue
	bool res=worker.wait(11000); // timeout should be longer than ISR timeout
	if(res)
		qInfo()<<"worker stopped";
	else
		qCritical()<<"waiting for worker timed out";
	return Result::COMPLETED;
}

double DeviceTaDlBus::readSensor(int channel, int & unitId)
{
	if(!buffer || !buffer->readBuffer)
	{
		unitId=-1;
		return 0.;
	}
	QMutexLocker lock(&buffer->mtx);
	unsigned char * base = buffer->readBuffer+6+channel*2;
	unitId=(base[1]&0x70)/16;
	int16_t resRaw = (int)(base[1])*256+(int)base[0];
	double res;
	if(resRaw&0x8000)
		res=static_cast<double>(resRaw | 0x7000);
	else
		res=static_cast<double>(resRaw & 0x0FFF);
	return res*0.1;
}

unsigned int DeviceTaDlBus::readRpm() const
{
	if(!buffer || !buffer->readBuffer)
	{
		return 0;
	}
	QMutexLocker lock(&buffer->mtx);
	if(*(buffer->readBuffer+39) & 0x80)
		return 0;
	return (unsigned int)((double)(*(buffer->readBuffer+39) & 0x1f)*3.33+.1);
}

QDateTime DeviceTaDlBus::readTst()
{
	if(!buffer || !buffer->readBuffer)
	{
		return QDateTime();
	}
	QMutexLocker lock(&buffer->mtx);
	unsigned char * base = buffer->readBuffer;
	// assuming DST bit is set according to Qt::LocalTime, we can ignore it
	return QDateTime(QDate(2000+(int)base[7], (int)base[6], (int)base[5]), QTime((int)(base[4]&0x1F), (int)base[3]));
}

void DeviceTaDlBus::bufferReadySl()
{
	decoderTst=DCON.getUsecTst();
	QList<Node * > chldr=findChildren<Node * >();
	foreach(Node * n, chldr)
	{
		NodeTaDlBus *nn=dynamic_cast<NodeTaDlBus *>(n);
		if(n)
			nn->updateValue(); // trigger signals
	}
}
