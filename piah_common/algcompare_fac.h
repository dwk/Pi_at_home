#ifndef ALGCOMPARE_FAC_H
#define ALGCOMPARE_FAC_H

namespace
{
pi_at_home::Device * AlgCreator(const QDomElement &elem)
{
	return new pi_at_home::AlgCompare(elem);
}
const QString typeName="Compare";
bool deviceRegistered=pi_at_home::DeviceFactory::registerDevice(typeName, AlgCreator);
}

#endif // ALGCOMPARE_FAC_H
