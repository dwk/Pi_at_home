#include "datahistory.h"
#include "piadi.h"


DataHistory::DataHistory(const pi_at_home::NodeAddress &adr) :
	adr(adr)
{

}

const QString &DataHistory::getName() const
{
	return adr.alias();
}

void DataHistory::setUnit(Node::Unit unit)
{
	if(unit_==unit)
		return;
	if(unit_!=Node::Unit::UNDEF)
	{
		qWarning()<<"inconsistent unit on"<<adr.toStringVerbose()<<Node::unit(unit_)<<Node::unit(unit);
		return;
	}
	unit_=unit;
}

QDateTime DataHistory::getFirstTimestamp() const
{
	if(!dr.size())
		return QDateTime();
	return dr.begin()->first;
}

QDateTime DataHistory::getLastTimestamp() const
{
	if(!dr.size())
		return QDateTime();
	return dr.rbegin()->first;
}

int DataHistory::getCount() const
{
	return (int)dr.size();
}

bool DataHistory::isEmpty() const
{
	if(!dr.size())
		return true;
	// check for all NaN's
	for(DataRow::const_iterator it=dr.begin(); it!=dr.end(); ++it)
	{
		if(it->second == it->second) // will be false for NaN or true for any real value
			return false;
	}
	return true;
}

bool DataHistory::addData(const QDateTime &timestamp, double value)
{
	return dr.insert(DataRow::value_type(timestamp, value)).second;
}

void  DataHistory::addData(const DataHistory &other)
{
	if(unit_==Node::Unit::UNDEF)
		unit_=other.unit_;
	else if(unit_!=other.unit_)
	{
		qWarning()<<"unit mismatch, will not add data"<<adr.toStringVerbose()<<Node::unit(unit_)<<Node::unit(other.unit_);
		return;
	}
	dr.insert(other.dr.cbegin(), other.dr.cend());
	return;
}

QSharedPointer<QObject> DataHistory::guiData()
{
	if(guiData_.isNull())
	{
		QObject * p=DCON.extendDataHistory(*this);
		if(p)
			guiData_=QSharedPointer < QObject > (p);
		if(guiData_.isNull())
		{
			qCritical()<<"cannot extend DataHistroy"<<adr.toStringVerbose()<<"DCON returned"<<p;
			throw Ex("cannot extend DH");
		}
	}
	return guiData_;
}
